﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Maintenance;
using Telerik.Reporting;

public partial class printReturnFormAccessories : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string pageType = Request["_pageType"];
        string method = Request["_method"];
        string isNew = Request["_isNew"];
        string controlID =  Request["_controlID"];
        ExportToFile(pageType, controlID, method, isNew);
    }

    protected void ExportToFile(string _pageType,string _controlID, string _method, string isNew)
    {
        var reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        Maintenance.ReturnFormAccessories returnFormAccessories = new Maintenance.ReturnFormAccessories();
        ReportBook rptbook = new ReportBook();
        rptbook = returnFormAccessories.printFormAccessories(_controlID, ((isNew == "1") ? true : false));

        System.Collections.Hashtable deviceInfo = new System.Collections.Hashtable();

        deviceInfo.Add("DocumentTitle", "Return Form Accessories");

        var result = reportProcessor.RenderReport(_pageType, rptbook, deviceInfo);
         
        this.Response.Clear();
        this.Response.ContentType = result.MimeType;
        this.Response.Cache.SetCacheability(HttpCacheability.Private);
        this.Response.Expires = -1;
        this.Response.Buffer = true;
        this.Response.BinaryWrite(result.DocumentBytes);
        if (_pageType == "XLS") { this.Response.AddHeader("content-disposition", "attachment;filename=Return Form Accessories.xls"); }
        //this.Response.AddHeader("Content-Disposition", string.Format("{0};FileName=\"{1}\"", "attachment", "Logbook Summary.pdf"));
        this.Response.End();
    }
}