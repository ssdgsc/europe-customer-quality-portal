﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Europe Customer Quality Portal</title>
        <link type="text/css" rel="Stylesheet" href="css/default.css" />
        <link type="text/css" rel="Stylesheet" href="css/style.css" />
</head>
<body>
    <form id="frmMain" runat="server">
    <div>
        <div id="holder_main" align="center">
            <div id="holderBody" style="width: 500px;">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr valign="top">
                            <td valign="top" height="100%" style="width:770px">
                                <div id="holderMaincontent">
                                    <div>
                                        <div>
                                            <% string sysmode = System.Configuration.ConfigurationManager.AppSettings["SystemMode"]; %>
                                            <h1>User Login <% Response.Write((sysmode == "TEST") ? "(TEST SITE)" : "(PRODUCTION)"); %></h1>
                                        </div>
                                        <hr />
                                        <table align="center">
                                            <tr>
                                                <td colspan="3" align="center">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblMessage" runat="server">Supply user credentials</asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Username
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtUsername" runat="server"></asp:TextBox>
                                                </td>
                                                <%--<td><asp:RequiredFieldValidator ID="usernameValidator" runat="server" ControlToValidate="txtUsername" Text="*"></asp:RequiredFieldValidator></td>--%>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Password
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                                                </td>
                                                <%--<td><asp:RequiredFieldValidator ID="passwordValidator" runat="server" ControlToValidate="txtPassword" Text="*"></asp:RequiredFieldValidator></td>--%>
                                            </tr>
                                        </table>
                                        <table align="center">
                                            <tr style="display:none;">
                                                <td colspan="6">
                                                    Not yet a user?
                                                    <asp:LinkButton ID="cmdRegister" runat="server" Text="Request Access" OnClick="cmdRegister_Click" style="display:none;"></asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                 <td >
                                                    <asp:LinkButton ID="cmdCahngePassword" runat="server" Text="Forgot Password" OnClick="cmdChangePassword_Click"></asp:LinkButton>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:Button ID="cmdSubmit" runat="server" Text="Login" OnClick="cmdSubmit_Click" class="buttons" />
                                                </td>
                                                <td>
                                                </td>
                                               
                                            </tr>
                                            <tr>
                                                
                                            </tr>
                                              
                                        </table>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
