﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Script.Serialization;
using System.IO;
using General;
using System.Web.Services;
using System.Globalization;
using General.AvianCrypto; 
using System.Web.Services.Protocols;
using System.Web.Script;
using System.Web.Script.Services;
using Maintenance;
using System.Text.RegularExpressions;
 
public partial class usermanagement : System.Web.UI.Page  
{
    static DataTable dtCustomers;
    public ArrayList newCustomers = new ArrayList();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserID"] == null )
        {
            Response.Redirect("login.aspx");
            return;
        }
        lbxCustomer.Attributes.Add("style", "display:none;");
        Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
        dtCustomers = mrfd.getCustomerList();

        if (!Page.IsPostBack)
        {
            bindUserTypeDropDown();
            SecurityMaster sm = new SecurityMaster();
            string user = sm.Decrypt(Session["UserType"].ToString());
            //if (user != "CUSTOMER QUALITY")
            //{
            //    PanelUnAuthorizedAccess.Visible = true;
            //    pnlGridView.Visible = false;
            //    UserPanel.Visible = false;
            //    return;
            //}
            //else { 
            
            //}
            bindData();
           
            lbxCustomer.DataSource = dtCustomers;
            lbxCustomer.DataValueField = "CustomerID";
            lbxCustomer.DataTextField = "CustomerNameNumber";
            lbxCustomer.DataBind();

            txtSalesOfficeCode.SelectedItem.Value = "";

            Maintenance.Customers customers = new Maintenance.Customers();
            txtSalesOfficeCode.DataSource = customers.getCSOList();
            txtSalesOfficeCode.DataValueField = "CSOID";
            txtSalesOfficeCode.DataTextField = "DESCRIPTION";
            txtSalesOfficeCode.DataBind();
           
        }
       
        
    }

    public void bindUserTypeDropDown()
    {
        UserInformation ui = new UserInformation();
        drpUserType.DataSource = ui.getUserType();
        drpUserType.DataValueField = "UserType";
        drpUserType.DataTextField = "UserType";
        drpUserType.DataBind();


    }


    protected void optProductType_SelectedIndexChanged(object sender, EventArgs e)
    {
      
        string assign = optProductType.SelectedValue;

        assign = (assign.IndexOf("Compressor") > -1) ? "Compressor" : "Control";
 

        //lbxCustomer.DataSource =  dtCustomers.Select(assign + " = 1").CopyToDataTable();
        //lbxCustomer.DataValueField = "CustomerID";
        //lbxCustomer.DataTextField = "CustomerNameNumber";
        //lbxCustomer.DataBind();

 

        foreach (ListItem item in lbxCustomer.Items) { 
            DataRow[] row = dtCustomers.Select(assign + " = 1 AND CustomerID = " + item.Value);
            if (row.Length > 0) {
                item.Enabled = true;
            }
            else
                item.Enabled = false;
                
        }

        if (!Page.IsPostBack)
        {
            Maintenance.UserInformation uiMaintenace = new Maintenance.UserInformation();
            Label lblUserID = (Label)UserGrid.SelectedRow.Cells[1].FindControl("lblUserID");
            Model.UserInformation ui = uiMaintenace.GetUserInfo(lblUserID.Text);
            if (ui.UserType == "CUSTOMER")
            {
                DataTable dt = new DataTable();
                dt = uiMaintenace.getUserCustomerMappingByUserID(ui.UserID);
                foreach (DataRow row in dt.Rows)
                {

                    int selectInd = lbxCustomer.Items.IndexOf(lbxCustomer.Items.FindByValue(row["CustomerID"].ToString()));
                    if (selectInd != -1)
                    {
                        lbxCustomer.Items[selectInd].Selected = true;
                    }
                }
            }
        }
         

       
    }
     
    protected void bindData()
    {
        Maintenance.UserInformation userInformation = new Maintenance.UserInformation();
        DataTable dt = new DataTable();
        SecurityMaster sm = new SecurityMaster();
        string user = sm.Decrypt(Session["UserType"].ToString()); 
        string usertype = (user == "CUSTOMER ADMIN") ? "CUSTOMER" : "";
        this.UserGrid.DataSource = userInformation.getUsers(usertype);
        this.UserGrid.DataBind();

       

       
    }
    protected void UserGrid_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnmode.Value = "edit";
        lblError.Text = "";
        lete.Visible = true;
        optProductType.SelectedIndex = 0;
        Label lblUserID = (Label)UserGrid.SelectedRow.Cells[1].FindControl("lblUserID");
        lbxCustomer.ClearSelection();
        Maintenance.UserInformation uiMaintenace = new Maintenance.UserInformation();
        Model.UserInformation ui = uiMaintenace.GetUserInfo(lblUserID.Text);

        hdUserID.Value = lblUserID.Text;
        holderRegLists.Visible = (ui.UserType == "CUSTOMER") ? true : false;
        drpUserType.SelectedValue = ui.UserType;
        drpUserType_SelectedIndexChanged(this, EventArgs.Empty);
        txtSalesOfficeCode.SelectedValue = (ui.CSO=="" || ui.CSO == "0") ? "" : ui.CSO;
        txtCompanyName.Text = ui.CompanyName;
        txtUsername.Text = ui.Username;
        txtUserFirstName.Text = ui.UserFirstName;
        txtUserLastName.Text = ui.UserLastName;
        txtPhoneNumber.Text = ui.PhoneNumber;
        txtUserTitle.Text = ui.UserTitle;
        txtDepartment.Text = ui.Department;
        txtFunction.Text = ui.Function;
        txtEmailAddress.Text = ui.EmailAddress; 

        foreach (ListItem item in lbxCustomer.Items)
        {
            DataRow[] row = dtCustomers.Select("Compressor = 1 AND CustomerID = " + item.Value);
            if (row.Length > 0)
            {
                item.Enabled = true;
            }
            else
                item.Enabled = false;

        }

        if (ui.UserType == "CUSTOMER") {
            DataTable dt = new DataTable();
            dt = uiMaintenace.getUserCustomerMappingByUserID(ui.UserID);
            foreach (DataRow row in dt.Rows) {
                 
                 int selectInd = lbxCustomer.Items.IndexOf(lbxCustomer.Items.FindByValue(row["CustomerID"].ToString()));
                 if (selectInd != -1){
                     lbxCustomer.Items[selectInd].Selected = true;
                 } 
            } 
        }

        this.chkReceptionActivated.Checked = ui.ReceptionActivated;
        this.chkAnalysisActivated.Checked = ui.AnalysisActivated;
        this.chkCreditDecisionActivated.Checked = ui.CDActivated;

        btnAddUser.Visible = false;
        pnlGridView.Visible = false;
        UserPanel.Visible = true;
    }
   

    protected void Page_PreRender(object sender, EventArgs e)
    {
       
    }

    protected void BtnCancel_Click(object sender, EventArgs e)
    {
        btnAddUser.Visible = true;
        pnlGridView.Visible = true;
        UserPanel.Visible = false;
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        
        Model.UserInformation userInformation = new Model.UserInformation();
        Maintenance.UserInformation um = new Maintenance.UserInformation();
        if(hdnmode.Value == "edit")
            userInformation.UserID = int.Parse(hdUserID.Value);
        userInformation.UserType = drpUserType.Text; ;
        userInformation.CSO = (userInformation.UserType == "CSO") ? txtSalesOfficeCode.SelectedValue : "";
        userInformation.CompanyName = txtCompanyName.Text;
        userInformation.Login = txtUsername.Text;
        userInformation.Username = txtUsername.Text;
        userInformation.UserFirstName = txtUserFirstName.Text;
        userInformation.UserLastName = txtUserLastName.Text;
        userInformation.PhoneNumber = txtPhoneNumber.Text;
        userInformation.Department = txtDepartment.Text;
        userInformation.UserTitle = txtUserTitle.Text;
        userInformation.Function = txtFunction.Text;
        userInformation.EmailAddress = txtEmailAddress.Text;
        userInformation.ReceptionActivated = this.chkReceptionActivated.Checked;
        userInformation.AnalysisActivated = this.chkAnalysisActivated.Checked;
        userInformation.CDActivated = chkCreditDecisionActivated.Checked;
        userInformation.UserPassword = txtNewPassword.Text;

        if (validatedInput(userInformation) == true)
        {
            Maintenance.UserInformation userInformationMaintainance = new Maintenance.UserInformation();

            //[START] [jpbersonda] [5/12/2017] New | Check Username if already exist
            bool isAlreadyExist = CheckUsernameIfExist(userInformation);
            //[END] [jpbersonda] [5/12/2017] New | Check Username if already exist
            if (isAlreadyExist)
            {
                lblError.Text = "Username already exist.";
            }
            else
            {
                if (hdnmode.Value == "edit")
                {
                    um.clearUserCustomerMappingByUserID(userInformation.UserID);
                    for (int i = 0; i < lbxCustomer.Items.Count; i++)
                    {
                        if (lbxCustomer.Items[i].Selected == true)
                            um.SaveUserCustomerMapping(userInformation.UserID, int.Parse(lbxCustomer.Items[i].Value));
                    }
                    userInformationMaintainance.updateUsers(userInformation);

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "alert" + UniqueID,
                    "alert('1 record successfully updated.');", true);

                }
                else if (hdnmode.Value == "add")
                {
                    userInformation = userInformationMaintainance.CreateUpdateUserInformation(userInformation);
                    um.clearUserCustomerMappingByUserID(userInformation.UserID);
                    for (int i = 0; i < lbxCustomer.Items.Count; i++)
                    {
                        if (lbxCustomer.Items[i].Selected == true)
                            um.SaveUserCustomerMapping(userInformation.UserID, int.Parse(lbxCustomer.Items[i].Value));
                    }
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "alert" + UniqueID,
                     "alert('1 record successfully added.');", true);
                }

                bindData();
                btnSearch_Click(this, EventArgs.Empty);
                btnAddUser.Visible = true;
                pnlGridView.Visible = true;
                UserPanel.Visible = false;
            }
   
        }
    }

    protected bool validatedInput(Model.UserInformation _userInformation)
    {
        if (this.txtConfirmPassword.Text != this.txtNewPassword.Text) {

            lblError.Text = "Password did not matched.";
            return false;
        }

        if (hdnmode.Value == "add")
        {
            if (_userInformation.UserPassword == "")
            {
                lblError.Text = "Please fill out the password";
                return false;
            }
        }

        if (_userInformation.UserType == "" ||
            _userInformation.CompanyName == "" ||
            _userInformation.Username == "" ||
            _userInformation.UserFirstName == "" ||
            _userInformation.UserLastName == "" ||
            _userInformation.EmailAddress == "")
        {

          
            lblError.Text = "Please fill out all required fields";
            return false;
        
        }

        if (_userInformation.UserType == "CSO") {
            if (_userInformation.CSO == "") {
                lblError.Text = "Please fill out all required fields";
                return false;
            }
        }

        
        // Validate the User Type for Customer
        if (_userInformation.UserType == "CUSTOMER") {
            if (this.lbxCustomer.SelectedIndex == -1)
            {
                lblError.Text = "Please select atleast one Customer.";
                return false;
            }
        }

        // [START] [jpbersonda] [5/15/2017] Email format validation
        string email = txtEmailAddress.Text.Trim().Replace(" ", ","); ;
        String[] s = email.Split(',');

        string invalidEmail = string.Empty;
        for (int i = 0; i < s.Length; i++)
        {
            string email2 = s[i];
            Regex regex = new Regex(@"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?");
            Match match = regex.Match(email2);

            if (match.Success) { }
            // Continue
            else
            {
                invalidEmail += s[i] + "\\n";
            }
        }

        if (invalidEmail != string.Empty)
        {
            lblError.Text = "Email address is invalid";
            return false;
        }

        // [END] [jpbersonda] [5/15/2017] Email format validation

        return true;
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        bindData();
        UserGrid.PageIndex = e.NewPageIndex;
        UserGrid.DataBind();
        //Your code
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        Maintenance.UserInformation userInformation = new Maintenance.UserInformation();
        DataTable dt = new DataTable();
        this.UserGrid.DataSource = userInformation.getUsersByUserName(txtUserNameSearch.Text, txtUserFirstNameSearch.Text, txtUserLastNameSearch.Text);
        this.UserGrid.DataBind();
    }
    protected void delete_Click(object sender, EventArgs e)
    {
        Maintenance.UserInformation userInformation = new Maintenance.UserInformation();
        userInformation.deactivateUserByUserId(int.Parse(this.hdUserID.Value));
        Page.ClientScript.RegisterStartupScript(this.GetType(), "alert" + UniqueID,
        "alert('1 record successfully deleted.');", true);
        PanelUnAuthorizedAccess.Visible = false;
        pnlGridView.Visible = true;
        UserPanel.Visible = false;
        bindData();
    }
    protected void drpUserType_SelectedIndexChanged(object sender, EventArgs e)
    {
        holderRegLists.Visible = (drpUserType.SelectedItem.Text.ToUpper() == "CUSTOMER") ? true : false;
        rowCSO.Visible = (drpUserType.SelectedItem.Text.ToUpper() == "CSO") ? true : false; 
    }



    protected void btnAddUser_Click(object sender, EventArgs e)
    {
        hdnmode.Value = "add";
        lbxCustomer.ClearSelection();
        optProductType.SelectedIndex = 0;
        optProductType_SelectedIndexChanged(this, EventArgs.Empty);
        drpUserType.SelectedIndex = -1;
        drpUserType_SelectedIndexChanged(this, EventArgs.Empty);
        txtCompanyName.Text = "";
        txtUserFirstName.Text = "";
        txtUserLastName.Text = "";
        txtUsername.Text = "";
        txtDepartment.Text = "";
        txtUserTitle.Text = "";
        txtPhoneNumber.Text = "";
        txtFunction.Text = "";
        txtEmailAddress.Text = "";
        chkAnalysisActivated.Checked = false;
        chkCreditDecisionActivated.Checked = false;
        chkReceptionActivated.Checked = false;
        txtNewPassword.Text = "";
        txtConfirmPassword.Text = "";
        lete.Visible = false;
        btnAddUser.Visible = false;
        pnlGridView.Visible = false;
        UserPanel.Visible = true;
    }

    //[START] [jpbersonda] [5/12/2017] New | Check Username if already exist
    protected bool CheckUsernameIfExist(Model.UserInformation objUserInformation)
    {
        bool isAlreadyExist = false;
        Maintenance.UserInformation userInformation = new Maintenance.UserInformation();
        try
        {
            isAlreadyExist = userInformation.CheckUsernameIfExist(objUserInformation.Username, objUserInformation.UserID);

        }
        catch (Exception)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "", "alert('System Error');", true);
        }
        finally
        {
            userInformation = null;
        }
        
        return isAlreadyExist;
        
    }
    //[END] [jpbersonda] [5/12/2017] New | Check Username if already exist
}