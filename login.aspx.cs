﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        checkAction();
        this.txtUsername.Focus();
    }
    protected void cmdSubmit_Click(object sender, EventArgs e)
    {
        string username = txtUsername.Text;
        Maintenance.UserInformation um = new Maintenance.UserInformation();
        Model.UserInformation ui = um.ValidateLoginCredentials(username);
        General.AvianCrypto.SecurityMaster sm = new General.AvianCrypto.SecurityMaster();

        if (ui.UserID > 0)
        {
            if (!(ui.UserPassword.Equals(txtPassword.Text, StringComparison.CurrentCulture)))
            {
                lblMessage.Text = "Username and Password do not match.";
                txtPassword.Focus();
                return;
            }
            
            Session.Add("UserID", sm.Encrypt(ui.UserID.ToString()));
            Session.Add("CustomerName", sm.Encrypt(ui.CustomerName));
            Session.Add("UserType", sm.Encrypt(ui.UserType));
            Session.Add("CompanyName", sm.Encrypt(ui.CompanyName));
            Session.Add("UserName", sm.Encrypt(ui.Username));
            Session.Add("UserFullName", sm.Encrypt(ui.UserFullName));
            Session.Add("PhoneNumber", sm.Encrypt(ui.PhoneNumber));
            Session.Add("EmailAddress", sm.Encrypt(ui.EmailAddress));
            Session.Add("SortExpression", "SubmissionDate");
            Session.Add("SortDirection", "DESC");
            Session.Add("IsFirstLogin", ui.IsFirstLogin);
            Session.Add("IsFirstLogin", ui.IsFirstLogin);
            Session.Add("UserAccess",  um.getUserType(ui.UserType));

           

            if (ui.IsFirstLogin)
            { Response.Redirect(@"changepassword.aspx"); }
            else
            {    //HttpCookie loginDetailCookie = new HttpCookie("validated", sm.Encrypt(details));
                //Response.Cookies.Add(loginDetailCookie);
                Response.Redirect("home.aspx");
            }
        }
        else
        {
            lblMessage.Text = "Invalid username!";
            txtUsername.Focus();
            return;
        }
    }
    protected void cmdChangePassword_Click(object sender, EventArgs e)
    {
        Response.Redirect("ForgotPassword.aspx");
    }
    protected void cmdRegister_Click(object sender, EventArgs e)
    {
        Response.Redirect("Register.aspx");
    }

    private void checkAction()
    {
        if (Request.QueryString["action"] != null)
        {
            switch (Request.QueryString["action"])
            {
                case "logout":
                    lblMessage.ForeColor = System.Drawing.Color.Green;
                    Session.Abandon();
                    lblMessage.Text = "You have successfully logged out of the system";
                    break;
                case "changepass":
                    lblMessage.ForeColor = System.Drawing.Color.Green;
                    lblMessage.Text = "Password successfully changed.";
                    break;
                default:
                    lblMessage.ForeColor = System.Drawing.Color.Gray;
                    break;
            }
        }
    }
}
