﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Register" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Europe Customer Quality Portal</title>
        <link type="text/css" rel="Stylesheet" href="css/default.css" />
        <link type="text/css" rel="Stylesheet" href="css/style.css" />
</head>
<body>
    <form id="frmMain" runat="server">
    <div>
        <div id="holder_main" align="center">
            <div id="holderBody" style="width: 980px;">
                    <div class="MainPanel2">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr valign="top">
                            
                            <td valign="top" height="100%" class="DottedBorderLeft" style="width:770px">
                                <div id="holderMaincontent">
                                    <div style="position: relative;">
                                        <h1>
                                            Request For Access</h1>
                                        <hr />
                                        <table>
                                            <tr>
                                                <td colspan="5">
                                                    <ul>
                                                        <li>Use the form below to create a new account</li>
                                                        <li>Passwords required atleast 6 characters in length</li>
                                                        <li><font color="red">*</font> indicates a required field</li></ul>
                                                </td>
                                            </tr>
                                        </table>
                                        <div>
                                            <asp:Label ID="lblMessage" runat="server" Font-Bold="True" ForeColor="Red" Visible="False"></asp:Label></div>
                                    </div>
                                    <br />
                                    <div style="width: 740px;">
                                        <table>
                                            <tr>
                                                <td>
                                                    <div id="holderRegFields">
                                                        <div>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td colspan="5">
                                                                        Account Information
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Company Name
                                                                    </td>
                                                                    <td>
                                                                        :
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtCompanyName" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:RequiredFieldValidator ID="companynameValidator" runat="server" ControlToValidate="txtCompanyName"
                                                                            Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Username
                                                                    </td>
                                                                    <td>
                                                                        :
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtUsername" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:RequiredFieldValidator ID="usernameValidator" runat="server" ControlToValidate="txtUsername"
                                                                            Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        User first name
                                                                    </td>
                                                                    <td>
                                                                        :
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtUserFirstName" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:RequiredFieldValidator ID="userfirstnameValidator" runat="server" ControlToValidate="txtUserFirstName"
                                                                            Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        User last name
                                                                    </td>
                                                                    <td>
                                                                        :
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtUserLastName" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:RequiredFieldValidator ID="userlastnameValidator" runat="server" ControlToValidate="txtUserLastName"
                                                                            Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Department
                                                                    </td>
                                                                    <td>
                                                                        :
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtDepartment" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:RequiredFieldValidator ID="departmentValidator" runat="server" ControlToValidate="txtDepartment"
                                                                            Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Phone Number
                                                                    </td>
                                                                    <td>
                                                                        :
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtPhoneNumber" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:RequiredFieldValidator ID="phonenumberValidator" runat="server" ControlToValidate="txtPhoneNumber"
                                                                            Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                        <asp:RegularExpressionValidator ID="regexPhone" runat="server" ErrorMessage="Invalid contact number format"
                                                                            ControlToValidate="txtPhoneNumber" ValidationExpression="^[-+]?(ext)?[\- \d]+(\d+)+$" ForeColor="Red"></asp:RegularExpressionValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Email Address
                                                                    </td>
                                                                    <td>
                                                                        :
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtEmailAddress" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:RequiredFieldValidator ID="emailAddressValidator" runat="server" ControlToValidate="txtEmailAddress"
                                                                            Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                        <asp:RegularExpressionValidator ID="regexEmail" runat="server" ErrorMessage="Invalid email address format"
                                                                            ControlToValidate="txtEmailAddress" ValidationExpression="^(([^<>()[\]\\.,;:\s@\“]+(\.[^<>()[\]\\.,;:\s@\“]+)*)|(\“.+\“))@((\[(2([0-4]\d|5[0-5])|1?\d{1,2})(\.(2([0-4]\d|5[0-5])|1?\d{1,2})){3} \])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))" ForeColor="Red"></asp:RegularExpressionValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Login
                                                                    </td>
                                                                    <td>
                                                                        :
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtLogin" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:RequiredFieldValidator ID="loginValidator" runat="server" ControlToValidate="txtLogin"
                                                                            Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Password
                                                                    </td>
                                                                    <td>
                                                                        :
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtPassword" runat="server" CausesValidation="False" TextMode="Password"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:RequiredFieldValidator ID="passwordValidator" runat="server" ControlToValidate="txtPassword"
                                                                            Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Confirm Password
                                                                    </td>
                                                                    <td>
                                                                        :
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:RequiredFieldValidator ID="confrimPasswordValidator" runat="server" ControlToValidate="txtConfirmPassword"
                                                                            Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Backup Name
                                                                    </td>
                                                                    <td>
                                                                        :
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtBackupName" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Backup First Name
                                                                    </td>
                                                                    <td>
                                                                        :
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtBackupFirstName" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Backup Email Address
                                                                    </td>
                                                                    <td>
                                                                        :
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtBackupEmailAddress" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:RegularExpressionValidator ID="regexEmail2" runat="server" ErrorMessage="Invalid email address format"
                                                                            ControlToValidate="txtBackupEmailAddress" ValidationExpression="^(([^<>()[\]\\.,;:\s@\“]+(\.[^<>()[\]\\.,;:\s@\“]+)*)|(\“.+\“))@((\[(2([0-4]\d|5[0-5])|1?\d{1,2})(\.(2([0-4]\d|5[0-5])|1?\d{1,2})){3} \])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))" ForeColor="Red"></asp:RegularExpressionValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5" >
                                                                         <br />
                                                                         <div>
                                                                            <img src="GenerateCaptcha.ashx" alt="Captcha Code" width="269px"/><br /> 
                                                                        </div>
                                                                        <div>
                                                                         <br />
                                                                            <asp:TextBox ID="txtCaptchaText" runat="server" 
                                                                                CssClass="txtInput" Width="172px" placeholder="Enter the Captcha code"></asp:TextBox>
                                                                             <asp:Button ID="btnReGenerate" runat="server" 
                                                                                Text="Another code" OnClick="btnReGenerate_Click"  />
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5" align="center">
                                                                        <asp:Button ID="cmdSubmit" runat="server" Text="SUBMIT" OnClick="cmdSubmit_Click" class="buttons" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td valign="top" style="display:none;">
                                                    <div id="holderRegLists">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    Select Customer Name:
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ListBox ID="lbxCustomer" SelectionMode="Multiple" runat="server" Height="320px"></asp:ListBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
