﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" EnableEventValidation="false" validateRequest="false" CodeFile="MyReturn.aspx.cs"
    Inherits="MyReturn" %>
  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server"> 
    <title>Europe Customer Quality Portal</title>
    <link type="text/css" rel="Stylesheet" href="css/default.css" />
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
    <link type="text/css" rel="Stylesheet" href="css/searchable-option/sol.css" />
    <link type="text/css" rel="Stylesheet" href="css/searchable-option/multiple-select.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"/>
  
    <script type="text/javascript" src="js/jquery.1.10.2.js"></script> 
   <%--<link rel="stylesheet" href="/resources/demos/style.css"/>--%>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            if ($(".paginationGridView > td > table > tbody > tr > td:last-child > a").text() == "...")
                $(".paginationGridView > td > table > tbody > tr > td:last-child > a").text("Next >");
            
            if ($(".paginationGridView > td > table > tbody > tr > td:first-child > a").text() == "...")
                $(".paginationGridView > td > table > tbody > tr > td:first-child > a").text("< Prev");
        });
        function selectedRequestSpecificTest() {
            alert("Selecting Specific Test may be subject to additional cost.");
        }
        function calculateWarranty() {
            var datacode = document.getElementById("txtControlDataCode").value;
            var warranty = document.getElementById("txtControlWarranty");
            PageMethods.getWarranty(datacode, onSuccess, onError);
            function onSuccess(result) {
                warranty.value = result;
                document.getElementById("hdnControlWarranty").value = result;
            }
            function onError(result) {
                alert("Can not calculate Warranty from the given Datacode.");
            }
        }

       
   

        function checkPendingTransaction(recordCount, page) {
            if (recordCount > 0) {
                response = confirm("You have pending transaction. Leaving this page will disregard this.");
                if (response == true) {
                    window.location.href = page;
                }
                else {
                    document.getElementById("optProductType").rows[0].cells[0].childNodes[0].checked = true;
                    document.getElementById("optProductType").selectedIndex = 0;
                    __doPostBack();
                }
            }
            else {
                window.location.href = page;
            }
        }
    </script>
    <style type="text/css">
        .hiddencol
        {
            display: none;
        }
        .auto-style1 {
            width: 55px;
        }
        .auto-style3 {
            width: 149px;
        }
        .auto-style4 {
            width: 109px;
        }
    </style>
</head>
<body>
    <form id="frmMyReturn" runat="server">
    <asp:HiddenField ID="hfRole" runat="server" />
    <asp:HiddenField ID="hfCustomerName" runat="server" /> 
    <asp:HiddenField ID="hfStreet" runat="server" />
    <asp:HiddenField ID="hfState" runat="server" />
    <asp:HiddenField ID="hfZipCode" runat="server" />
    <asp:HiddenField ID="hfCity" runat="server" />
    <asp:HiddenField ID="hfCountry" runat="server" />
    <asp:HiddenField ID="rowIDSelected" runat="server" />
       <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>

    <asp:HiddenField ID="hfCustomerEmail" runat="server" />
    <div>
        <div id="holder_main" align="center">
            <div id="holderBody" style="width: 980px;">
                <div class="MainPanel2">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr valign="top">
                            <td valign="top" height="100%" class="LeftNavigation">
                                <% Response.Write(Helpers.menuNavi(3)); %>
                            </td>
                            <td valign="top" height="100%" class="DottedBorderLeft" style="width: 770px">
                                <div id="holderMaincontent">
                                    <div>
                                        <asp:Label ID="LabelMessage" runat="server" Font-Bold="True" ForeColor="Red" Text=""
                                            Visible="False"></asp:Label>
                                    </div>
                                    <br />
                                       <asp:Panel ID="pnlProduct" runat="server">
                                            <span>Select Product</span>
                                            <div> 
                                                <asp:DropDownList ID="ddProductType" runat="server" AutoPostBack="True" 
                                                    onselectedindexchanged="ddProductType_SelectedIndexChanged" Width="210px">
                                                    <asp:ListItem>Compressors / Drives / Units</asp:ListItem>
                                                    <asp:ListItem>Electronics / Mechanical controls</asp:ListItem>
                                                    <asp:ListItem>Accessories</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <br />
                                        </asp:Panel>
                                       <asp:Panel ID="pnlCompressorCustomerList" runat="server"  style="display:none;"> 
                                            <span>Select Customer Name</span> 
                                           <div>
                                               <asp:ListBox ID="ddCustomerList" runat="server" Width="300px" SelectionMode="Multiple"></asp:ListBox>
                                               <span class="reset-list">RESET</span>  
                                               <asp:Button ID="btnGo" runat="server" Text="Go" OnClick="btnGo_Click" 
                                                    class="buttons" Width="60px" />
                                            </div>
                                            <br />
                                        </asp:Panel>
                                        
                                        <asp:Panel ID="pnlControlCustomerList" runat="server" Visible="false"  style="display:none;">
                                            <span>Select Customer Name</span>
                                            <div>
                                                <asp:ListBox ID="ddControlCustomerList" runat="server" Width="300px" SelectionMode="Multiple"></asp:ListBox>
                                                <span class="reset-list">RESET</span>   
                                                <asp:Button ID="btnControlGo" runat="server" Text="Go" 
                                                    OnClick="btnControlGo_Click" class="buttons" Width="60px" />
                                            </div>
                                            <br />
                                        </asp:Panel>
                                     
                                    <asp:Panel ID="pnlCompressorSearch" runat="server">
                                           <span class="clear-search">CLEAR SEARCH</span>
                                            <div>
                                                <table>
                                                    <tr>
                                                        <td colspan="3">
                                                            <asp:Label ID="lblSearch" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txtSearch" runat="server" Width="200px"></asp:TextBox>
                                                        </td>
                                                         <td>
                                                            <asp:RadioButton runat="server" GroupName="searchGroup" ID="radioSerial" Checked="true"  Text="Search By Serial" />&nbsp;
                                                            <asp:RadioButton runat="server" GroupName="searchGroup" ID="radioModel" Text="Search By Model" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click"
                                                                class="buttons" />
                                                        </td>
                                                        <td>
                                                          
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hdnSearchBy" Value="0" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div style="display:none;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btnSearchBySerial" runat="server" Text="Search By Serial" OnClick="btnSearchBySerial_Click"
                                                                class="buttons" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnSearchByModel" runat="server" Text="Search By Model" OnClick="btnSearchByModel_Click"
                                                                class="buttons" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnDownloadList1" runat="server" Text="Download List" OnClick="btnDownloadList_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <br />
                                            </asp:Panel>   
                                    <asp:Panel ID="pnlReturn" runat="server">
                                        <div>
                                            <asp:HiddenField ID="rfhid" runat="server" />
                                            <!-- Grid View for Compressor -->
                                            <asp:Panel ID="pnlGridView" runat="server" Width="745px" ScrollBars="Auto">
                                                <asp:GridView ID="gvMyReturn" runat="server" CssClass="ui_grid" PageSize="20" AllowPaging="True"
                                                    AllowSorting="True" AutoGenerateColumns="False" EmptyDataText="No Record Found"
                                                    OnPageIndexChanging="gvMyReturn_PageIndexChanging" OnSorting="gvMyReturn_Sorting"
                                                    OnSelectedIndexChanged="gvMyReturn_SelectedIndexChanged" Width="740px">
                                                     <PagerStyle CssClass="paginationGridView"/>
                                                    <Columns>
                                                        <asp:CommandField ShowSelectButton="True">
                                                            <ControlStyle Font-Italic="True" />
                                                        </asp:CommandField>
                                                        <asp:BoundField DataField="RFDetailID" HeaderText="RFDetailID">
                                                            <HeaderStyle CssClass="hiddencol" />
                                                            <ItemStyle CssClass="hiddencol" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CustomerName" HeaderText="Customer Name" ItemStyle-Width="100px" 
                                                            ReadOnly="True" SortExpression="Model">
                                                        <ItemStyle CssClass="cell_text" Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Model" HeaderText="Model" ReadOnly="True" ItemStyle-Width="100px"
                                                            SortExpression="Model">
                                                            <ItemStyle Width="100px" CssClass="cell_text" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="SerialNumber" HeaderText="Serial Number" ReadOnly="True"
                                                            ItemStyle-Width="100px" SortExpression="SerialNumber">
                                                            <ItemStyle Width="100px" CssClass="cell_tex"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Portal Reference" ItemStyle-Width="100px" SortExpression="ReferenceNumber">
                                                            
                                                            <ItemTemplate>
                                                                <asp:Label id="link"  CssClass="tooltip-contact"  Text='<%# Bind("ReferenceNumber")%>' runat="server"></asp:Label> <%--ToolTip='<%# Bind("ContactInfo")%>'--%>
                               
                                                            </ItemTemplate>
                                                        </asp:TemplateField> 

                                                      <%--  <asp:BoundField DataField="ReferenceNumber" HeaderText="Portal Reference" ReadOnly="True"
                                                            ItemStyle-Width="100px" SortExpression="ReferenceNumber">
                                                            <ItemStyle Width="100px" CssClass="cell_text"></ItemStyle>
                                                               
                                                        </asp:BoundField>--%>
                                                        <asp:BoundField DataField="OracleNumber" HeaderText="Return Number" ReadOnly="True"
                                                            ItemStyle-Width="100px" SortExpression="OracleNumber">
                                                            <ItemStyle Width="100px" CssClass="cell_text"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CustRef" HeaderText="Cust Ref" ReadOnly="True" SortExpression="CustRef">
                                                            <ItemStyle CssClass="cell_text" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Reject" HeaderText="Reject" ReadOnly="True" SortExpression="Reject">
                                                            <ItemStyle CssClass="cell_text" />
                                                        </asp:BoundField>  
                                                        <asp:BoundField DataField="SubmissionDate" HeaderText="Submission Date" DataFormatString="{0:MM/dd/yyyy}"
                                                            ReadOnly="True" SortExpression="SubmissionDate">
                                                            <ItemStyle CssClass="cell_date"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ReceptionDate" HeaderText="Reception Date" DataFormatString="{0:MM/dd/yyyy}"
                                                            ReadOnly="True" SortExpression="ReceptionDate">
                                                            <ItemStyle CssClass="cell_date" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="AnalyzeDate" HeaderText="Analyze Date" DataFormatString="{0:MM/dd/yyyy}"
                                                            ReadOnly="True" SortExpression="AnalyzeDate">
                                                            <ItemStyle CssClass="cell_date" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CreditNote" HeaderText="Credit Note" ReadOnly="True" SortExpression="CreditNote">
                                                            <ItemStyle CssClass="cell_text" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <AlternatingRowStyle CssClass="ui_gridAltItem" />
                                                </asp:GridView>
                                                </div>
                                        </asp:Panel>
                                  
                                             <%--Download and pager--%>
                                            <asp:Panel ID="pnlCompressorSearch2" runat="server"> 
                                                     <div align="right" style="padding:5px;">
                                                        Show
                                                        <asp:LinkButton ID="lnk20" runat="server" OnClick="lnk20_Click">20</asp:LinkButton>
                                                        <asp:LinkButton ID="lnk50" runat="server" OnClick="lnk50_Click">50</asp:LinkButton>
                                                        <asp:LinkButton ID="lnk100" runat="server" OnClick="lnk100_Click">100</asp:LinkButton>
                                                        results
                                                         <br />
                                                         <asp:Button ID="btnDownloadList" runat="server" Text="Download List" OnClick="btnDownloadList_Click"
                                                                        class="buttons"  style="margin-top:5px;"/>
                                                    </div>
                                                </asp:Panel>
                                        </div>
                                        <div>



                                            <asp:Panel ID="pnlControlSearch" runat="server" Visible="false">
                                        
                                            <div>
                                                <span class="clear-search">CLEAR SEARCH</span>
                                                <table>
                                                    <tr>
                                                        <td colspan="3">
                                                            <asp:Label ID="lblControlSearch" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>

                                                            <asp:TextBox ID="txtControlSearch" runat="server" Width="200px"></asp:TextBox>
                                                        </td>
                                                         <td>
                                                            <asp:RadioButton runat="server" GroupName="searchGroup" ID="radioControlPCN" 
                                                                 Checked="true"  Text="Search By PCN Number" />&nbsp;
                                                            <asp:RadioButton runat="server" GroupName="searchGroup" ID="radioControlProduct" 
                                                                 Text="Search By Product" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnControlSearch" runat="server" Text="Search" OnClick="btnControlSearch_Click"
                                                                class="buttons" />
                                                        </td>
                                                        <td>
                                                            
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hdnControlSearchBy" Value="0" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div style="display:none;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btnControlSearchByPCNNumber" runat="server" Text="Search By Serial" OnClick="btnControlSearchByPCNNumber_Click"
                                                                class="buttons" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnControlSearchByProduct" runat="server" Text="Search By Model" OnClick="btnControlSearchByProduct_Click"
                                                                class="buttons" />
                                                        </td>
                                                        <td>
                                                           
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <br />
                                        </asp:Panel>
                                            <!-- Grid View for Control -->
                                            <asp:HiddenField ID="hdnRFDetailID" runat="server" />
                                            <asp:Panel ID="pnlGridViewControl" runat="server" ScrollBars="Auto" 
                                                Width="745px">
                                            <asp:GridView ID="gvMyReturnControl" runat="server" AllowPaging="True" 
                                                AllowSorting="True" AutoGenerateColumns="False" CssClass="ui_grid" 
                                                EmptyDataText="No Record Found" 
                                                OnPageIndexChanging="gvMyReturnControl_PageIndexChanging" 
                                                onselectedindexchanged="gvMyReturnControl_SelectedIndexChanged" 
                                                OnSorting="gvMyReturnControl_Sorting" PageSize="20" Visible="False" 
                                                Width="740px">
                                                 <PagerStyle CssClass="paginationGridView"/>

                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="True">
                                                    <ControlStyle Font-Italic="True" />
                                                    </asp:CommandField>
                                                    <asp:BoundField DataField="RFDetailID" HeaderText="RFDetailID">
                                                    <HeaderStyle CssClass="hiddencol" />
                                                    <ItemStyle CssClass="hiddencol" />
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="Customer" HeaderText="Customer Name" ItemStyle-Width="100px" 
                                                        ReadOnly="True" SortExpression="Model">
                                                    <ItemStyle CssClass="cell_text" Width="100px" />
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="Model" HeaderText="Product" ItemStyle-Width="100px" 
                                                        ReadOnly="True" SortExpression="Model">
                                                    <ItemStyle CssClass="cell_text" Width="100px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="PCNNumber" HeaderText="PCN Number" 
                                                        ItemStyle-Width="100px" ReadOnly="True" SortExpression="PCNNumber">
                                                    <ItemStyle CssClass="cell_tex" Width="100px" />
                                                    </asp:BoundField>
                                                      <asp:TemplateField HeaderText="Portal Reference" ItemStyle-Width="100px" SortExpression="ReferenceNumber"> 
                                                            <ItemTemplate>
                                                                <asp:Label id="link" CssClass="tooltip-contact" Text='<%# Bind("ReferenceNumber")%>' runat="server"></asp:Label>  <%-- ToolTip='<%# Bind("ContactInfo")%>'--%>
                               
                                                            </ItemTemplate>
                                                        </asp:TemplateField> 
                                                   <%-- <asp:BoundField DataField="ReferenceNumber" HeaderText="Portal Reference" 
                                                        ItemStyle-Width="100px" ReadOnly="True" SortExpression="ReferenceNumber">
                                                    <ItemStyle CssClass="cell_text" Width="100px" />
                                                    </asp:BoundField>--%>
                                                    <asp:BoundField DataField="OracleNumber" HeaderText="Return Number" ReadOnly="True"
                                                        ItemStyle-Width="100px" SortExpression="OracleNumber">
                                                        <ItemStyle Width="100px" CssClass="cell_text"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="CustRef" HeaderText="Customer Reference" 
                                                        ReadOnly="True" SortExpression="CustRef">
                                                    <ItemStyle CssClass="cell_text" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Reject" HeaderText="Reject" ReadOnly="True" 
                                                        SortExpression="Reject">
                                                    <ItemStyle CssClass="cell_text" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="SubmissionDate" DataFormatString="{0:MM/dd/yyyy}" 
                                                        HeaderText="Submission Date" ReadOnly="True" SortExpression="SubmissionDate">
                                                    <ItemStyle CssClass="cell_date" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="ReceptionDate" DataFormatString="{0:MM/dd/yyyy}" 
                                                        HeaderText="Reception Date" ReadOnly="True" SortExpression="ReceptionDate">
                                                    <ItemStyle CssClass="cell_date" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="AnalyzeDate" DataFormatString="{0:MM/dd/yyyy}" 
                                                        HeaderText="Analyze Date" ReadOnly="True" SortExpression="AnalyzeDate">
                                                    <ItemStyle CssClass="cell_date" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="CreditNote" HeaderText="Credit Note" ReadOnly="True" 
                                                        SortExpression="CreditNote">
                                                    <ItemStyle CssClass="cell_text" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="ui_gridAltItem" />
                                            </asp:GridView>
                                            
                                            </asp:Panel>
                                           
                                            <%--Download and Pager--%>
                                            <asp:Panel ID="pnlControlSearch2" runat="server">
                                                <div align="right" style="padding: 5px">
                                                Show
                                                <asp:LinkButton ID="lnkControl20" runat="server" OnClick="lnkControl20_Click">20</asp:LinkButton>
                                                <asp:LinkButton ID="lnkControl50" runat="server" OnClick="lnkControl50_Click">50</asp:LinkButton>
                                                <asp:LinkButton ID="lnkControl100" runat="server" OnClick="lnkControl100_Click">100</asp:LinkButton>
                                                results
                                                    <br />
                                                     <asp:Button ID="btnControlDownloadList" runat="server" Text="Download List" OnClick="btnControlDownloadList_Click"
                                                                class="buttons" style="margin-top:5px;"/>
                                            </div>
                                            </asp:Panel>
                                                
                                            
                                        </div>
                                    </asp:Panel>
                                    
                                    
                                    
                                    <asp:Panel ID="pnlControlDetail" runat="server">
                                        <div>
                                              <% string sysmode = System.Configuration.ConfigurationManager.AppSettings["SystemMode"]; %>
                                             <h1>Return Form <% Response.Write((sysmode == "TEST") ? "(TEST SITE)" : "(PRODUCTION)"); %></h1>
                                             
                                        </div>
                                        <div>
                                            <asp:HiddenField ID="hdnRFHeaderID" runat="server" />
                                            <asp:HiddenField ID="hdnCustomerNumber" runat="server" />
                                            <asp:HiddenField ID="hdnCurrentProductType" runat="server" />
                                            <asp:HiddenField ID="hdControlID" runat="server" />
                                              

                                            <asp:RadioButtonList ID="optProductTypeControl" runat="server" RepeatDirection="Horizontal"
                                                Enabled="false">
                                                <asp:ListItem>Compressor / Drives / Units</asp:ListItem>
                                                <asp:ListItem>Electronics / Mechanical controls</asp:ListItem>
                                                <asp:ListItem>Accessories</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                        <hr />
                                        <!--For Control-->
                                        <asp:Panel ID="pnlControlPage" runat="server">
                                            <table>
                                                <tr>
                                                    <td>
                                                        Portal Reference
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td class="auto-style4">
                                                        <asp:Label ID="lblPortalReference" runat="server"></asp:Label>
                                                       
                                                    </td>
                                                        <td> <label style="float:right;">Return Number:</label> </td>
                                                        <td> </td>
                                                        <td>
                                                            <asp:Textbox ID="txtOracleNumber" runat="server" MaxLength="12" Width="180px"  ></asp:Textbox>
                                                            <%--<asp:Textbox ID="txtOracle1" runat="server" MaxLength="1" Width="6px" ></asp:Textbox>
                                                            <asp:Textbox ID="txtOracle2" runat="server" MaxLength="1" Width="6px"></asp:Textbox>
                                                            <asp:Textbox ID="txtOracle3" runat="server" MaxLength="1" Width="6px" ></asp:Textbox>
                                                            <asp:Textbox ID="txtOracle4" runat="server" MaxLength="1" Width="6px" ></asp:Textbox>
                                                            <asp:Textbox ID="txtOracle5" runat="server" MaxLength="1" Width="6px" ></asp:Textbox>
                                                            <asp:Textbox ID="txtOracle6" runat="server" MaxLength="1" Width="6px" ></asp:Textbox>
                                                            <asp:Textbox ID="txtOracle7" runat="server" MaxLength="1" Width="6px" ></asp:Textbox>
                                                            <asp:Textbox ID="txtOracle8" runat="server" MaxLength="1" Width="6px" ></asp:Textbox>
                                                            <asp:Textbox ID="txtOracle9" runat="server" MaxLength="1" Width="6px" ></asp:Textbox>
                                                            <asp:Textbox ID="txtOracle10" runat="server" MaxLength="1" Width="6px" ></asp:Textbox>
                                                            <asp:Textbox ID="txtOracle11" runat="server" MaxLength="1" Width="6px" ></asp:Textbox>
                                                            <asp:Textbox ID="txtOracle12" runat="server" MaxLength="1" Width="6px" ></asp:Textbox>
                                                            <asp:Label ID="clearOracle" runat="server" Width="10px">
                                                                <button type="button" title="Clear" id="btnClearOracle" style="color: red;font-size: 10px;width: 10px; height: 15px;padding: 0;">&times</button>

                                                            </asp:Label>--%>
                                                          

                                                        </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Customer Name
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td class="auto-style4">
                                                        <asp:Label ID="lblControlCustomerName" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Customer Location</td>
                                                    <td>
                                                    </td>
                                                    <td class="auto-style4">
                                                        <asp:DropDownList ID="ddLocation" runat="server" Width="261px" > </asp:DropDownList>
                                                    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Claim:</span>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td class="auto-style4">         
                                                        <asp:DropDownList ID="ddControlClaimLists" runat="server" 
                                                        AutoPostBack="true" Enabled="False"  AppendDataBoundItems="True"
                                                            onselectedindexchanged="ddControlClaimLists_SelectedIndexChanged" Width="261px">
                                                            <asp:ListItem> </asp:ListItem>
                                                         <%--   <asp:ListItem>External leakage</asp:ListItem>
                                                        <asp:ListItem>Internal / Seat leakage</asp:ListItem>
                                                        <asp:ListItem>Setting out of factory setting</asp:ListItem>
                                                        <asp:ListItem>Functional Issue</asp:ListItem>
                                                        <asp:ListItem>Electrical Issue</asp:ListItem>
                                                        <asp:ListItem>Wrong delivery</asp:ListItem>
                                                        <asp:ListItem>Incorrectly ordered</asp:ListItem>
                                                        <asp:ListItem>Buy Back</asp:ListItem>
                                                        <asp:ListItem>Transport Damage</asp:ListItem>
                                                        <asp:ListItem>Other</asp:ListItem>--%>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <asp:Panel ID="pnlControlClaimDescription" runat="server" Visible="true">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblControlClaimDescription" runat="server" Visible="false" Text="Claim Description: "></asp:Label>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtControlClaimDescription" runat="server" Visible="false" Width="230px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                </asp:Panel>
                                                <tr>
                                                    <td>
                                                        <span>Customer Claim Reference:</span>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td class="auto-style4">
                                                        <asp:TextBox ID="txtControlCustomerClaimReference" runat="server" 
                                                            Visible="true" Width="247px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                     <td>
                                                        <asp:Image ID="imgControlClaimReference" class="help-icon" ToolTip="This is the general reference for your whole claim" runat="server" ImageUrl="~/resources/images/tooltip.jpg"  Visible="false"/>

                                                    </td>
                                                </tr>
                                                
                                            </table>
                                            <hr />
                                            <div>
                                                <h3>
                                                    Description of <span runat="server" id="lblDescription"></span></h3>
                                            </div>
                                            <!--Description Fields-->
                                            <table>
                                                <tr>
                                                    <td style="width:151px;">
                                                        <asp:Label ID="lblPCNNumber" runat="server" Text="PCN number: "></asp:Label>
                                                        <%--<span id="lblPCNNumber">PCN number:</span>--%>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtControlPCNNumber" runat="server" Width="244px" Enabled="false" placeholder="Type PCN number here.."></asp:TextBox>
                                                        <span id="pPCNSrc">No result(s) found.</span>
                                                        <asp:Image id="imgPCNLoading" ImageUrl="~/images/loading.gif" runat="server"/>  
                                                        <asp:Button ID="btnCheckControlPCNNumber" runat="server" 
                                                        Text="Check" class="buttons" CausesValidation="false" Visible="false" 
                                                            onclick="btnCheckControlPCNNumber_Click"/>
                                                        <span id="empty-message"></span>
                                                        <asp:RequiredFieldValidator ID="reqControlPCNNumber" runat="server" ControlToValidate="txtControlPCNNumber" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>
                                                          <asp:Label ID="lblProduct" runat="server" Text="Product: "></asp:Label>
                                                        <%--<span id="lblProduct">Product:</span>--%>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="txtControlProduct" runat="server" AutoPostBack="true" 
                                                            OnSelectedIndexChanged="txtControlModel_SelectedIndexChanged" Width="260px">
                                                            <asp:ListItem disabled="" Value=""></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <asp:Panel ID="pnlControlPCN" runat="server" Visible="false"> 
                                                <tr>
                                                    <td>
                                                        <span>Product Group:</span>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtControlProductGroup" runat="server" Width="244px" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr> 
                                                <tr style="display:none;">
                                                    <td>
                                                        <span>Line</span>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtControlLine" runat="server" Width="244px" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                </asp:Panel>
                                                <tr>
                                                    <td>
                                                        <span>Datacode:</span>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtControlDataCode" runat="server" Width="244px" Enabled="false" MaxLength="4"></asp:TextBox> 
                                                        <asp:RegularExpressionValidator ID="reqControlDataCode" ControlToValidate="txtControlDataCode" ValidationExpression="^[0-9]{4}" runat="server" ErrorMessage="Format should be YYCW" ForeColor="Red"></asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Warranty:</span>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtControlWarranty" runat="server" Width="244px" Enabled="false"></asp:TextBox>
                                                        <asp:HiddenField ID="hdnControlWarranty" runat="server"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Qty:</span>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtControlQty" runat="server" Width="49px" MaxLength="6" Enabled="false"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="reqControlQty" ControlToValidate="txtControlQty" ValidationExpression="^[0-9]+$" 
                                                        runat="server" ErrorMessage="Please input numbers only"></asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Date of Claim:</span>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtControlDateOfClaim" runat="server" Width="244px" 
                                                        Enabled="False" MaxLength="10"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span><span runat="server" id="lblReference"></span> Reference:</span>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtControlComponentReference" runat="server" Width="244px" 
                                                            MaxLength="50" Enabled="false"></asp:TextBox>
                                                        <asp:Image ID="imgComponentReference"  class="help-icon"
                                                            ToolTip="This is the single reference for the component. This is a unique number which will be also mentioned on our inspection report and on the credit note if any." 
                                                            runat="server" ImageUrl="~/resources/images/tooltip.jpg" Visible="false"/>
                                                        <%--<asp:RequiredFieldValidator ID="reqControlComponentReference" runat="server" ControlToValidate="txtControlComponentReference" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>--%>
                                                    </td>
                                                </tr>
                                            </table>
                                            <hr />
                                            <div>
                                                <h3>
                                                    Description of Application</h3>
                                            </div>
                                            <!--Description Application-->
                                            <table>
                                                <tr>
                                                    <td>
                                                        <span>Type Of Return:</span>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:RadioButtonList ID="optControlTypeOfReturn" runat="server" RepeatDirection="Horizontal"
                                                            Width="500px" Enabled="false">
                                                            <asp:ListItem>Line Return</asp:ListItem>
                                                            <asp:ListItem>Field Return</asp:ListItem>
                                                            <asp:ListItem>Lab/Sample Test</asp:ListItem>
                                                            <asp:ListItem>Commercial Return</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                    <td><asp:Image ID="imgControlTypeOfReturn" runat="server" ImageUrl="~/resources/images/tooltip.jpg" Visible="false"/></td>
                                                    <td><asp:RequiredFieldValidator ID="reqControlTypeOfReturn" ControlToValidate="optControlTypeOfReturn" runat="server" ErrorMessage="Required field."></asp:RequiredFieldValidator></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Application:</span>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:RadioButtonList ID="optControlApplication" runat="server" RepeatDirection="Horizontal"
                                                            Width="400px" Enabled="false">
                                                            <asp:ListItem>Air Conditioning</asp:ListItem>
                                                            <asp:ListItem>HeatPump</asp:ListItem>
                                                            <asp:ListItem>Refrigeration</asp:ListItem>
                                                            <asp:ListItem>Transport</asp:ListItem>
                                                            <asp:ListItem>Unknown</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                    <td></td>
                                                    <td><asp:RequiredFieldValidator ID="reqControlApplication" ControlToValidate="optControlApplication" runat="server" ErrorMessage="Required field."></asp:RequiredFieldValidator></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Refrigerant:</span>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:RadioButtonList ID="optControlRefrigerant" runat="server" RepeatDirection="Horizontal"
                                                            Width="500px" Enabled="false" AutoPostBack="True" 
                                                            onselectedindexchanged="optControlRefrigerant_SelectedIndexChanged">
                                                            <asp:ListItem>R410A</asp:ListItem>
                                                            <asp:ListItem>R407C</asp:ListItem>
                                                            <asp:ListItem>R404A</asp:ListItem>
                                                            <asp:ListItem>R134A</asp:ListItem>
                                                            <asp:ListItem>R744/CO2</asp:ListItem>
                                                            <asp:ListItem>Other</asp:ListItem>
                                                            <asp:ListItem>Unknown</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                    <td></td>
                                                    <td>
                                                        <asp:RequiredFieldValidator ID="reqControlRefrigerant" ControlToValidate="optControlRefrigerant"
                                                        runat="server" ErrorMessage="Required field."></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <asp:Panel ID="pnlControlRefrigerant" runat="server" Visible="false">
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <span>Please specify:</span>&nbsp;<asp:TextBox ID="txtControlRefrigerant" 
                                                            runat="server" Width="320px" MaxLength="20" Enabled="false"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="reqControlSpecifyRefrigerant" runat="server" ControlToValidate="txtControlRefrigerant" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                </asp:Panel>
                                            </table>
                                            <table>
                                                <tr>
                                                    <td colspan="3">
                                                        <asp:Panel ID="pnlControlWorkingPoint" runat="server" Visible="false">
                                                        <table width="100%">
                                                            <tr>
                                                                <td colspan="6">
                                                                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="WorkingPoint:  "></asp:Label>&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="style17">
                                                                    <span>to=</span>
                                                                </td>
                                                                <td class="style18">
                                                                    <asp:TextBox ID="txtControlWorkingPointTO" runat="server" Width="80px" 
                                                                        MaxLength="6"></asp:TextBox>&nbsp;<span>°C</span>
                                                                </td>
                                                                <td>
                                                                
                                                                    <asp:RegularExpressionValidator ID="req17" runat="server" 
                                                                        ControlToValidate="txtControlWorkingPointTO" 
                                                                        ErrorMessage="Please input in Celsius format" 
                                                                        ValidationExpression="^\-{0,1}\d+(.\d+){0,1}$"></asp:RegularExpressionValidator>
                                                                
                                                                </td>
                                                                <td class="style14">
                                                                    <span>tc=</span>
                                                                </td>
                                                                <td class="style18">
                                                                    <asp:TextBox ID="txtControlWorkingPointTC" runat="server" Width="80px" 
                                                                        MaxLength="6"></asp:TextBox>
                                                                    <span>°C</span>
                                                                </td>
                                                                <td>
                                                                
                                                                    <asp:RegularExpressionValidator ID="req18" runat="server" 
                                                                        ControlToValidate="txtControlWorkingPointTC" 
                                                                        ErrorMessage="Please input in Celsius format" 
                                                                        ValidationExpression="^\-{0,1}\d+(.\d+){0,1}$"></asp:RegularExpressionValidator>
                                                                
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="style17">
                                                                    <span>Superheat=</span>
                                                                </td>
                                                                <td class="style18">
                                                                    <asp:TextBox ID="txtControlWorkingPointSuperHeat" runat="server" Width="80px" 
                                                                        MaxLength="6"></asp:TextBox>&nbsp;<span>K</span>
                                                                </td>
                                                                <td>
                                                                
                                                                    <asp:RegularExpressionValidator ID="req19" runat="server" 
                                                                        ControlToValidate="txtControlWorkingPointSuperHeat" 
                                                                        ErrorMessage="Please input in Kelvin format" 
                                                                        ValidationExpression="^\-{0,1}\d+(.\d+){0,1}$"></asp:RegularExpressionValidator>
                                                                
                                                                </td>
                                                                <td class="style14">
                                                                    <span>Subcooling=</span>
                                                                </td>
                                                                <td class="style18">
                                                                    <asp:TextBox ID="txtControlWorkingPointSubCooling" runat="server" Width="80px" 
                                                                        MaxLength="6"></asp:TextBox>&nbsp<span>K</span>
                                                                </td>
                                                                <td>
                                                                
                                                                    <asp:RegularExpressionValidator ID="req20" runat="server" 
                                                                        ControlToValidate="txtControlWorkingPointSubCooling" 
                                                                        ErrorMessage="Please input in Kelvin format" 
                                                                        ValidationExpression="^\-{0,1}\d+(.\d+){0,1}$"></asp:RegularExpressionValidator>
                                                                
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Request:</span>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:RadioButtonList ID="optControlRequest1" runat="server" 
                                                        RepeatDirection="Horizontal" Width="260px" AutoPostBack="True" 
                                                        onselectedindexchanged="optControlRequest1_SelectedIndexChanged" 
                                                            Enabled="False" >
                                                            <asp:ListItem>Warranty Analysis</asp:ListItem>
                                                           <%-- <asp:ListItem>Analysis</asp:ListItem>
                                                            <asp:ListItem>Out of Warranty Analysis</asp:ListItem>--%>
                                                            <asp:ListItem>Specific Request</asp:ListItem>
                                                        </asp:RadioButtonList> 
                                                    </td>
                                                </tr>
                                                <asp:Panel ID="pnlControlRequestSpecificTest" runat="server" Visible="false">
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtControlRequestSpecificTest" runat="server" Columns="50" 
                                                            Rows="5" Visible="false" Enabled="false" TextMode="MultiLine"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="reqControlRequestSpecificTest" runat="server" ControlToValidate="txtControlRequestSpecificTest" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                </asp:Panel>
                                                <tr>
                                                    <td>
                                                        <span>Configuration:</span>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:RadioButtonList ID="optControlConfiguration" runat="server" RepeatDirection="Horizontal"
                                                            Width="300px" Enabled="false">
                                                            <asp:ListItem>Single</asp:ListItem>
                                                            <asp:ListItem>Tandem</asp:ListItem>
                                                            <asp:ListItem>Rack</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Date of Failure:</span>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtControlDateOfFailure" runat="server" Width="115px" Enabled="false" placeholder="dd/mm/yyyy" MaxLength="10"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="reqControlDateOfFailure" runat="server" ErrorMessage="Invalid date format (dd/mm/yyyy)" ControlToValidate="txtControlDateOfFailure" ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/-](0?[1-9]|1[012])[\/-]\d{4}$" ForeColor="Red"></asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Running Hours:</span>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtControlRunningHours"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' runat="server" MaxLength="5" Width="115px" Enabled="false">0</asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <!--Remarks-->
                                        <div>
                                            <span>Additional information/comments and specific customer instruction</span>
                                        </div>
                                        <div>
                                            
                                            <asp:TextBox ID="txtControlComments" runat="server" Columns="50" 
                                            Rows="5" Visible="true" Enabled="false" TextMode="MultiLine" style="resize:none;height:120px;width:100%;"></asp:TextBox>
                                        </div>
                                        <!--File Attachment-->
                                        <div>
                                            <span>Attached document to the claim :</span>
                                        </div>
                                        <div>
                                          <asp:HyperLink ID="aFileAttachment" runat="server" target="_blank"  title="Click here to download this file" ></asp:HyperLink>
                                          <asp:Label ID="lblAttachment" runat="server" style="color:#BBBBBB;">None</asp:Label>
                                        </div>

                                        <br />
                                        <hr />
                                        <asp:Panel ID="pnlControlClosedDate" runat="server" Visible="true">  
                                        <table>
                                            <tr>
                                                <td class="style12 ctrl-return">
                                                    <span>Received in Kolin:</span>
                                                </td>
                                                <td></td>
                                                <td>
                                                    <asp:TextBox ID="txtReceiveInKolin" runat="server" Width="115px" 
                                                        Enabled="false" placeholder="dd/mm/yyyy" AutoPostBack="True" MaxLength="10" OnTextChanged="txtReceiveInKolin_TextChanged"></asp:TextBox>
                                                    <asp:Image ID="Image1" ToolTip="Input in dd/mm/yyyy format" runat="server" ImageUrl="~/resources/images/tooltip.jpg"  Visible="false"/>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                                        ErrorMessage="Invalid date format (dd/mm/yyyy)" 
                                                        ControlToValidate="txtReceiveInKolin" 
                                                        ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/-](0?[1-9]|1[012])[\/-]\d{4}$" ForeColor="Red"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style12">
                                                    <span>Received in Current Week:</span>
                                                </td>
                                                <td></td>
                                                <td>
                                                    <asp:TextBox ID="txtReceivedInCurrentWeek" runat="server" Width="115px" 
                                                        Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style12">
                                                    <span>Receive in Month:</span>
                                                </td>
                                                <td></td>
                                                <td>
                                                    <asp:TextBox ID="txtReceivedInMonth" runat="server" Width="115px" 
                                                        Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style12">
                                                    <span>Fiscal Year Received:</span>
                                                </td>
                                                <td></td>
                                                <td>
                                                    <asp:TextBox ID="txtReceivedInPiscalYear" runat="server" Width="115px" 
                                                        Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>

                                        <br />
                                        <br />



                                        <table>
                                            <tr>
                                                <td class="style12 ctrl-return">
                                                    <span>Closed in Kolin:</span>
                                                </td>
                                                <td></td>
                                                <td>
                                                    <asp:TextBox ID="txtControlClosedInKolin" onblur="return false;" runat="server" Width="115px" 
                                                        Enabled="false" placeholder="dd/mm/yyyy" OnTextChanged="txtControlClosedInKolin_TextChanged" AutoPostBack="True" MaxLength="10"></asp:TextBox>
                                                    <asp:Image ID="imgControlClosedInKolin" class="help-icon" ToolTip="Input in dd/mm/yyyy format" runat="server" ImageUrl="~/resources/images/tooltip.jpg"  Visible="false"/>
                                                    <asp:RegularExpressionValidator ID="reqControlClosedInColin" runat="server" 
                                                        ErrorMessage="Invalid date format (dd/mm/yyyy)" 
                                                        ControlToValidate="txtControlClosedInKolin" 
                                                        ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/-](0?[1-9]|1[012])[\/-]\d{4}$" ForeColor="Red"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style12">
                                                    <span>Closed in Current Week:</span>
                                                </td>
                                                <td></td>
                                                <td>
                                                    <asp:TextBox ID="txtControlClosedInCureentWeek" runat="server" Width="115px" 
                                                        Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style12">
                                                    <span>Closed in Month:</span>
                                                </td>
                                                <td></td>
                                                <td>
                                                    <asp:TextBox ID="txtControlClosedInMonth" runat="server" Width="115px" 
                                                        Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style12">
                                                    <span>Fiscal Year Closed:</span>
                                                </td>
                                                <td></td>
                                                <td>
                                                    <asp:TextBox ID="txtControlClosedInFY" runat="server" Width="115px" 
                                                        Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        </asp:Panel>
                                        

                                        <asp:Panel ID="pnlControlFeedback" runat="server" Visible="true">  
                                        <table>
                                         <tr>
                                                <td>
                                                    <span>Failure Description:</span>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                     <asp:DropDownList ID="ddControlFailureDescription" runat="server" 
                                                        Enabled="False" Width="280px" AppendDataBoundItems="True">
                                                        <asp:ListItem>-Select-</asp:ListItem>
                                                        <%--<asp:ListItem>The part could not be analysed</asp:ListItem>
                                                        <asp:ListItem>The part is ok</asp:ListItem>
                                                        <asp:ListItem>Charge leak</asp:ListItem>
                                                        <asp:ListItem>External leakage</asp:ListItem>
                                                        <asp:ListItem>Internal leakage</asp:ListItem>
                                                        <asp:ListItem>Mechanical defect</asp:ListItem>
                                                        <asp:ListItem>Electronic defect</asp:ListItem>
                                                        <asp:ListItem>Functional defect</asp:ListItem>
                                                        <asp:ListItem>No analysis the part is out of warranty</asp:ListItem>
                                                        <asp:ListItem>Setting out of factory setting</asp:ListItem>
                                                        <asp:ListItem>Others</asp:ListItem>--%>

                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span>Credit decision:</span>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                     <asp:DropDownList ID="ddControlCreditDecision" runat="server" 
                                                        Enabled="False" Width="280px">
                                                        <asp:ListItem>-Select-</asp:ListItem>
                                                        <asp:ListItem>Yes</asp:ListItem>
                                                        <asp:ListItem>No</asp:ListItem>
                                                        <asp:ListItem>Yes, fair dealing</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span>Origin:</span>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                     <asp:DropDownList ID="ddControlOrigin" runat="server" 
                                                         Enabled="False" Width="280px" AppendDataBoundItems="True">
                                                        <asp:ListItem>-Select-</asp:ListItem>
                                                       <%-- <asp:ListItem>No defect found</asp:ListItem>
                                                        <asp:ListItem>The settings was changed by the Third party</asp:ListItem>
                                                        <asp:ListItem>Soldering / brazing</asp:ListItem>
                                                        <asp:ListItem>Welding</asp:ListItem>
                                                        <asp:ListItem>Micro leak</asp:ListItem>
                                                        <asp:ListItem>Cracked diaphragm</asp:ListItem>
                                                        <asp:ListItem>Overheating</asp:ListItem>
                                                        <asp:ListItem>Operator failure</asp:ListItem>
                                                        <asp:ListItem>Wear out</asp:ListItem>
                                                        <asp:ListItem>Improper/wrong assembly</asp:ListItem>
                                                        <asp:ListItem>Improper/wrong manipulation</asp:ListItem>
                                                        <asp:ListItem>Undetermined</asp:ListItem>
                                                        <asp:ListItem>High incoming voltage / short circuit / improper wiring</asp:ListItem>
                                                        <asp:ListItem>Triac / phase</asp:ListItem>
                                                        <asp:ListItem>EX controlling chip / ECD interface</asp:ListItem>
                                                        <asp:ListItem>Wrong / damaged contact</asp:ListItem>
                                                        <asp:ListItem>Discharged / wrong battery</asp:ListItem>
                                                        <asp:ListItem>Software</asp:ListItem>
                                                        <asp:ListItem>Wrong settings</asp:ListItem>
                                                        <asp:ListItem>Corrosion / Impurities</asp:ListItem>
                                                        <asp:ListItem>Jammed nut</asp:ListItem>
                                                        <asp:ListItem>Vibrations / mechanical shock</asp:ListItem>
                                                        <asp:ListItem>Wrong charge</asp:ListItem>
                                                        <asp:ListItem>Wrong dimension / shape</asp:ListItem>
                                                        <asp:ListItem>Incomplete delivery / part</asp:ListItem>
                                                        <asp:ListItem>Wrong ordered</asp:ListItem>
                                                        <asp:ListItem>Wrong delivered</asp:ListItem>
                                                        <asp:ListItem>Wrong part</asp:ListItem>
                                                        <asp:ListItem>Wrong quantity</asp:ListItem>
                                                        <asp:ListItem>Wrong label</asp:ListItem>
                                                        <asp:ListItem>Dry gasket</asp:ListItem>
                                                        <asp:ListItem>Damage</asp:ListItem>
                                                        <asp:ListItem>Unknow product / No Alco product</asp:ListItem>
                                                        <asp:ListItem>Lost during the transport</asp:ListItem>
                                                        <asp:ListItem>Old design / type of the product</asp:ListItem>
                                                        <asp:ListItem>Not returned for the analysis</asp:ListItem>
                                                        <asp:ListItem>Overpressure</asp:ListItem>
                                                        <asp:ListItem>SCAP error</asp:ListItem>
                                                        <asp:ListItem>Relay error</asp:ListItem>
                                                        <asp:ListItem>Buy Back</asp:ListItem>
                                                        <asp:ListItem>Others</asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span>Cause:</span>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                     <asp:DropDownList ID="ddControlCauseOrGuilty" runat="server" 
                                                        Enabled="False" Width="280px" AppendDataBoundItems="True">
                                                        <asp:ListItem>-Select-</asp:ListItem>
                                                      <%--  <asp:ListItem>Commercial issue</asp:ListItem>
                                                        <asp:ListItem>Design problem</asp:ListItem>
                                                        <asp:ListItem>Manufacturing problem</asp:ListItem>
                                                        <asp:ListItem>Material defect</asp:ListItem>
                                                        <asp:ListItem>No defect</asp:ListItem>
                                                        <asp:ListItem>Other / Application problem</asp:ListItem>
                                                        <asp:ListItem>Out of warranty - No analysis</asp:ListItem>
                                                        <asp:ListItem>Undetermined</asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td>
                                                    <span>Report text:</span>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                     <asp:TextBox ID="txtControlReportText" runat="server" Columns="50" 
                                            Rows="5" Visible="true" Enabled="false" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span>Sent to the supplier for analysis:</span>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtControlSupplierAnalysis" runat="server" 
                                                            Visible="true" Width="280px" Enabled="False"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span>Part returned:</span>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                     <asp:DropDownList ID="ddControlPartReturned" runat="server" 
                                                        Enabled="False">
                                                        <asp:ListItem>-Select-</asp:ListItem>
                                                        <asp:ListItem>Used</asp:ListItem>
                                                        <asp:ListItem>Unused</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span>Note:</span>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                     <asp:TextBox ID="txtControlNote" runat="server" Columns="50" 
                                            Rows="5" Visible="true" Enabled="false" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span>Status:</span>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                     <asp:TextBox ID="txtControlOpenOrClose" runat="server" 
                                                            Visible="true" Width="280px" Enabled="False"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        </asp:Panel>  
                                        
                                        <br />
                                        <div align="right">
                                           

                                            <asp:Button ID="Button2" runat="server" class="buttons" Text="Generate Report" 
                                                onclick="Button2_Click" />
                                            &nbsp;

                                            <asp:Button ID="cmdEmailControlReport" runat="server" Text="Email" 
                                                class="buttons" onclick="cmdEmailControlReport_Click" Visible="False"  /> &nbsp;

                                            <asp:Button ID="cmdPrintControlReport" runat="server" Text="Print" 
                                                class="buttons" onclick="cmdPrintControlReport_Click" /> &nbsp;

                                            <asp:Button ID="Button1" runat="server" Text="Save as Excel" 
                                                class="buttons" onclick="Button1_Click"  /> &nbsp;

                                            <asp:Button ID="cmdEditControl" runat="server" Text="Edit" class="buttons" 
                                                onclick="cmdEditControl_Click"/> &nbsp;
                                            <asp:Button ID="cmdBackFromControl" runat="server" Text="Back to My Returns" class="buttons"
                                                OnClick="cmdBackFromControl_Click" />

                                            <br /><br />
                                            <asp:Panel ID="panelAddNewControlItem" runat="server">
                                                <asp:TextBox ID="txtNewControlQty" placeholder="Qty" runat="server"></asp:TextBox>

                                                <asp:Button ID="btnAddNewControlItem" runat="server" class="buttons" Text="Add New Control" 
                                                onclick="btnAddNewControlItem_Click" 
                                                
                                                onclientclick="return confirm(&quot;Are you sure to add new control?&quot;)" />

                                            </asp:Panel>
                                             
                                           
                                             
                                            &nbsp;
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlCompressorDetail" runat="server">
                                        <div>
                                             <% string sysmode = System.Configuration.ConfigurationManager.AppSettings["SystemMode"]; %>
                                            <h1>Return Form <% Response.Write((sysmode == "TEST") ? "(TEST SITE)" : ""); %></h1>
                                        </div>
                                        <div>
                                            <asp:RadioButtonList ID="optProductTypeCompressor" runat="server" RepeatDirection="Horizontal"
                                                Enabled="false">
                                                <asp:ListItem Selected="True">Compressor / Drives / Units</asp:ListItem>
                                                <asp:ListItem>Electronics / Mechanical controls</asp:ListItem>
                                                <asp:ListItem Enabled="false">Accessories</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                        <hr />
                                        <!--For Compressor-->
                                        <div>
                                            <table>
                                                <tr>
                                                    <td class="td-label">Portal Reference</td> 
                                                    <td> <asp:Label ID="lblPortalReferenceComp" runat="server"></asp:Label> </td>
                                                    <td class="td-label"> <label style="float:right;">Return Number:</label> </td> 
                                                    <td> <asp:Textbox ID="txtOracleNumberComp" runat="server" MaxLength="12" Width="200px" ></asp:Textbox><br /> 
                                                          
                                                    </td>
                                                </tr>
                                                <tr> 
                                                    <td class="td-label"> Customer Name </td> 
                                                    <td> <asp:Label ID="lblCompressorCustomerName" runat="server"></asp:Label> </td>
                                                    <td colspan="2"> </td>
                                                     
                                                   
                                                </tr>
                                                <tr>
                                                    <td class="td-label">Customer Location</td> 
                                                    <td> <asp:DropDownList ID="ddLocationComp" runat="server" Width="270px" ></asp:DropDownList> </td>
                                                     <td colspan="2"> </td>
                                                </tr>
                                                <tr>
                                                    <td class="td-label"><span>Reason of Return:</span></td> 
                                                    <td>
                                                        <asp:DropDownList ID="ddCompressorClaimLists" runat="server" AppendDataBoundItems="True" 
                                                        AutoPostBack="true" Enabled="False" Width="270px">
                                                        <asp:ListItem></asp:ListItem>
                                                        <%--<asp:ListItem>Noisy at Start</asp:ListItem>
                                                        <asp:ListItem>Noisy When Running</asp:ListItem>
                                                        <asp:ListItem>Noisy at Shutdown</asp:ListItem>
                                                        <asp:ListItem>Vibration</asp:ListItem>
                                                        <asp:ListItem>High DLT</asp:ListItem>
                                                        <asp:ListItem>Cooling/Heating capacity too low</asp:ListItem>
                                                        <asp:ListItem>High Power Input</asp:ListItem>
                                                        <asp:ListItem>High Amps</asp:ListItem>
                                                        <asp:ListItem>Low COP/EER</asp:ListItem>
                                                        <asp:ListItem>Short Circuit</asp:ListItem>
                                                        <asp:ListItem>Down to Earth</asp:ListItem>
                                                        <asp:ListItem>Electrical Concern</asp:ListItem>
                                                        <asp:ListItem>Mechanical Concern</asp:ListItem>
                                                        <asp:ListItem>Doesn&#39;t build up pressure</asp:ListItem>
                                                        <asp:ListItem>Seized</asp:ListItem>
                                                        <asp:ListItem>Oil Leak</asp:ListItem>
                                                        <asp:ListItem>Refrigerant Leak</asp:ListItem>
                                                        <asp:ListItem>No Holding Charge</asp:ListItem>
                                                        <asp:ListItem>Seal line rubber plug missing</asp:ListItem>
                                                        <asp:ListItem>Won&#39;t start</asp:ListItem>
                                                        <asp:ListItem>Wrong delivery</asp:ListItem>
                                                        <asp:ListItem>Incorrectly booked</asp:ListItem>
                                                        <asp:ListItem>Incorrectly ordered</asp:ListItem>
                                                        <asp:ListItem>Buy back</asp:ListItem>
                                                        <asp:ListItem>Transport Damage</asp:ListItem>
                                                        <asp:ListItem>Other</asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                    </td>
                                                     <td colspan="2"> </td>

                                                </tr>
                                                <tr>
                                                    <td class="td-label"><asp:Label ID="lblCompressorClaimDesc" runat="server" Visible="false" Text="Return Description: "></asp:Label></td>  
                                                    <td>  <asp:TextBox ID="txtCompressorClaimDescription" runat="server" Visible="false" Width="256px" Enabled="False"></asp:TextBox> </td>
                                                    <td colspan="2"> </td> 
                                                </tr>
                                                <tr>
                                                    <td class="td-label"> <span>Customer Return Reference:</span>  </td> 
                                                    <td>
                                                        <asp:TextBox ID="txtCompressorCustomerClaimRef" runat="server" Visible="true" Width="257px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td colspan="2"> </td>

                                                </tr>
                                            </table>
                                            <hr />
                                            <div>
                                                <h3>
                                                    Description of Compressor / Drives / Units</h3>
                                            </div>
                                            <!--Descritpion Fields-->
                                            <table>
                                                <tr>
                                                    <td class="td-label"> <span>Serial Number:</span> </td> 
                                                    <td>
                                                        <asp:TextBox ID="txtCompressorSerialNumber" runat="server" Width="257px" Enabled="false" placeholder="Type Serial number here"></asp:TextBox>
                                                          <span id="pSerialSrc">No result(s) found.</span>
                                                         <asp:Image id="imgSerialLoading" ImageUrl="~/images/loading.gif" runat="server" /> 
                                                        <asp:Button ID="btnCheckCompressorSerialNumber" runat="server" 
                                                        Text="Check" class="buttons" CausesValidation="false" Visible="false" 
                                                            onclick="btnCheckCompressorSerialNumber_Click"/>
                                                        <asp:RequiredFieldValidator ID="req2" runat="server" ControlToValidate="txtCompressorSerialNumber" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td> </td>

                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Model:</span>
                                                    </td> 
                                                    <td>
                                                        <asp:TextBox ID="txtCompressorModel" runat="server" Width="257px" Enabled="false"></asp:TextBox>
                                                        <asp:HiddenField ID="hdnControlProductGroup" runat="server" />
                                                        <asp:HiddenField ID="hdnControlLine" runat="server" />
                                                    </td>
                                                    <td> </td>

                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Creation Date:</span>
                                                    </td> 
                                                    <td>
                                                        <asp:TextBox ID="txtCompressorCreationDate" runat="server" Width="257px" Enabled="false"></asp:TextBox>
                                                    </td>
                                                    <td> </td>

                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Compressor / Drives / Units Reference:</span>
                                                    </td> 
                                                    <td>
                                                        <asp:Label ID="lblCompressorReference" runat="server"></asp:Label><asp:TextBox ID="txtCompressorReference" runat="server" Width="257px" Enabled="false"></asp:TextBox>
                                                    </td>
                                                    <td> </td>

                                                </tr>
                                            </table>
                                            <hr />
                                            <div>
                                                <h3>
                                                    Description of Application</h3>
                                            </div>
                                            <!--Description Application-->
                                            <table>
                                                <tr> 
                                                    <td>
                                                        <span><b>Type Of Return:</b></span>
                                                        <asp:RadioButtonList ID="optCompressorTypeOfReturn" runat="server" CssClass="rdgitem" RepeatDirection="Horizontal"
                                                             Enabled="false">
                                                            <asp:ListItem Value="Line Return">Line Return</asp:ListItem>
                                                            <asp:ListItem Value="Field Return">Field Return</asp:ListItem>
                                                            <asp:ListItem Value="Lab/Sample Test">Lab/Sample Test</asp:ListItem>
                                                            <asp:ListItem Value="Buy Back">Buy Back</asp:ListItem>
                                                            <asp:ListItem Value="Wrong Delivery">Wrong Delivery</asp:ListItem>
                                                            <asp:ListItem Value="Transport Damage">Transport Damage</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                     
                                                    <td>
                                                        <span><b>Application:</b></span>
                                                        <asp:RadioButtonList ID="optCompressorApplication" runat="server" CssClass="rdgitem" RepeatDirection="Horizontal"
                                                            Width="400px" Enabled="false">
                                                            <asp:ListItem>Air Conditioning</asp:ListItem>
                                                            <asp:ListItem>Heat Pump</asp:ListItem>
                                                            <asp:ListItem>Refrigeration</asp:ListItem>
                                                            <asp:ListItem>Transport</asp:ListItem>
                                                            <asp:ListItem>Unknown</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr> 
                                                    <td>
                                                        <span><b>Refrigerant:</b></span>
                                                        <asp:RadioButtonList ID="optCompressorRefrigerant" runat="server" CssClass="rdgitem" RepeatDirection="Horizontal"
                                                            Width="500px" Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="optCompressorRefrigerant_SelectedIndexChanged">
                                                            <asp:ListItem>R410A</asp:ListItem>
                                                            <asp:ListItem>R407C</asp:ListItem>
                                                            <asp:ListItem>R404A</asp:ListItem>
                                                            <asp:ListItem>R134A</asp:ListItem>
                                                            <asp:ListItem>R744/CO2</asp:ListItem>
                                                            <asp:ListItem>Other</asp:ListItem>
                                                            <asp:ListItem>Unknown</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <asp:Panel ID="pnlCompressorRefrigerant" runat="server" Visible="false">
                                                <tr> 
                                                    <td>  
                                                        <span>Please specify:</span>&nbsp;<asp:TextBox ID="txtCompressorRefrigerant" runat="server" Width="244px" Enabled="false"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="reqCompressorRefrigerant" runat="server" ControlToValidate="txtCompressorRefrigerant" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                </asp:Panel>
                                                <!--Working Point-->
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:Panel ID="pnlCompressorWorkingPoint" runat="server" Visible="false">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td colspan="4">
                                                                        <asp:Label ID="LabelWorkPt" runat="server" Font-Bold="True" Text="WorkingPoint:  "></asp:Label>&nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <span>to=</span>
                                                                        <asp:TextBox ID="txtCompressorWorkingPointTo" runat="server" Width="88px" Enabled="false"></asp:TextBox>&nbsp;
                                                                        <span>°C</span>
                                                                    </td>
                                                                    <td>
                                                                        <span>tc=</span>
                                                                        <asp:TextBox ID="txtCompressorWorkingPointTc" runat="server" Width="88px" Enabled="false"></asp:TextBox>&nbsp;
                                                                        <span>°C</span>
                                                                    </td>
                                                                    <td>
                                                                        <span>Superheat=</span>
                                                                        <asp:TextBox ID="txtCompressorSuperheat" runat="server" Width="88px" Enabled="false"></asp:TextBox>&nbsp;
                                                                        <span>K</span>
                                                                    </td>
                                                                    <td>
                                                                        <span>Subcooling=</span>
                                                                        <asp:TextBox ID="txtCompressorSubCooling" runat="server" Width="88px" Enabled="false"></asp:TextBox>&nbsp
                                                                        <span>K</span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <!--End Working Point-->
                                                <tr>
                                                    
                                                    <td>
                                                        <span><b>Request:</b></span>
                                                        <asp:RadioButtonList ID="optCompressorRequest1" runat="server" CssClass="rdgitem"
                                                            RepeatDirection="Horizontal" Width="260px" AutoPostBack="True" 
                                                            onselectedindexchanged="optCompressorRequest1_SelectedIndexChanged">
                                                            <asp:ListItem>Warranty Analysis</asp:ListItem>
                                                          
                                                            <asp:ListItem>Specific Request</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                    <td>
                                                        <%--<asp:RequiredFieldValidator ID="reqCompressorRequest1" ControlToValidate="optCompressorRequest1"
                                                            runat="server" ErrorMessage="Required field."></asp:RequiredFieldValidator>--%>
                                                    </td>
                                                </tr>
                                                <asp:Panel ID="pnlCompressorRequestSpecificTest" runat="server" Visible="false">
                                                <tr> 
                                                    <td colspan="2">
                                                        <asp:TextBox ID="txtCompressorRequestSpecificTest" runat="server" Columns="50" 
                                                            Rows="5" TextMode="MultiLine" MaxLength="250" Visible="true"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="reqCompressorRequestSpecificTest" runat="server" ControlToValidate="txtCompressorRequestSpecificTest" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                </asp:Panel>
                                                <tr> 
                                                    <td>
                                                        <span><b>Configuration:</b></span>
                                                        <asp:RadioButtonList ID="optCompressorConfiguration" runat="server" CssClass="rdgitem" RepeatDirection="Horizontal"
                                                            Width="300px" Enabled="false">
                                                            <asp:ListItem>Single</asp:ListItem>
                                                            <asp:ListItem>Tandem</asp:ListItem>
                                                            <asp:ListItem>Rack</asp:ListItem>
                                                            <asp:ListItem>Trio</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr> 
                                                    <td>
                                                        <span>Date of Failure:</span>
                                                        <asp:Label ID="lblCompressorDateOfFailure" runat="server"></asp:Label><asp:TextBox ID="txtCompressorDateOfFailure" style="margin-left:74px;" runat="server" Width="100px" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr> 
                                                    <td>
                                                         <span>Running Hours:</span>
                                                        <asp:TextBox ID="txtCompressorRunningHours" runat="server"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' Width="100px" Enabled="false" MaxLength="5" style="margin-left:71px;"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="reqCompressorRunningHours" runat="server"  ErrorMessage="Please input numbers only" ControlToValidate="txtCompressorRunningHours" ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <hr />
                                        <!--Remarks-->
                                        <div>
                                            <span>Additional information/comments and specific customer instruction</span>
                                        </div>
                                        <div>
                                            <textarea runat="server" id="txtCompressorRemarks" disabled="disabled" name="S2" style="resize:none;height:120px;width:100%;"></textarea>
                                        </div>
                                         <!--File Attachment-->
                                        <div>
                                            <span>Attached document to the claim :</span>
                                        </div>
                                        <div>
                                          <asp:HyperLink ID="aFileAttachmentComp" runat="server" target="_blank" title="Click here to download this file" ></asp:HyperLink>
                                          <asp:Label ID="lblAttachmentComp" runat="server" style="color:#BBBBBB;">None</asp:Label>
                                        </div>
                                        <br />
                                        <div align="right">
                                            <asp:Button ID="cmdEditCompressor" runat="server" Text="Edit" class="buttons" 
                                                onclick="cmdEditCompressor_Click"/> &nbsp;
                                            <asp:Button ID="cmdBackFromCompressor" runat="server" Text="Back to My Returns" class="buttons"
                                                OnClick="cmdBackFromCompressor_Click" />
                                        </div>
                                        <asp:HiddenField ID="hdnItemNumber" runat="server" />
                                    </asp:Panel>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </form>
   
    <script type="text/javascript" src="js/jquery-ui.js"></script> 
    <script type="text/javascript" src="js/searchable-option/multiple-select.js"></script> 
    <script type="text/javascript" src="js/ajaxfunctions/myreturn.js"></script> 
     
</body>
</html>
