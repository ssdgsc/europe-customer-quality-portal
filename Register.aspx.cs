﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using General.AvianCrypto;

public partial class Register : System.Web.UI.Page
{
    Maintenance.UserInformation um = new Maintenance.UserInformation();
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!Page.IsPostBack)
        {
            Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
            lbxCustomer.DataSource = mrfd.getCustomerList();
            lbxCustomer.DataValueField = "CustomerID";
            lbxCustomer.DataTextField = "CustomerNameNumber";
            lbxCustomer.DataBind();
            UpdateCaptchaText();
        }
        lblMessage.Visible = false;
        lblMessage.Text = "";
    }


    private void UpdateCaptchaText()
    {
        txtCaptchaText.Text = string.Empty; 
        //Store the captcha text in session to validate
        Session["Captcha"] = Guid.NewGuid().ToString().Substring(0, 6);
    }
    protected void cmdSubmit_Click(object sender, EventArgs e)
    {
        try
        {

            Model.UserInformation ui = new Model.UserInformation();
            Maintenance.UserInformation um = new Maintenance.UserInformation();
            Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
            if (txtPassword.Text.Length < 6)
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Password must be atleast six characters.";
                return;
            }

            if (!(txtPassword.Text.Equals(txtConfirmPassword.Text, StringComparison.CurrentCulture)))
            {
                showMessage(true, "Password do not Match.");
                return;
            }

            //if (this.lbxCustomer.SelectedIndex == -1)
            //{
            //    showMessage(true, "Please select atleast one Customer.");
            //    return;
            //}

            if (um.isUserNameExist(txtUsername.Text.ToUpper()))
            {
                showMessage(true, "Username already used.");
                return;
            }

            if (Session["Captcha"] != null)
            {
                //Match captcha text entered by user and the one stored in session
                if (Convert.ToString(Session["Captcha"]) != txtCaptchaText.Text.Trim())
                {
                    txtCaptchaText.Text = "";
                    showMessage(true, "Please enter the captcha code");
                    return;
                }
            }

            string x = Request["UserID"];

            if ((Request["UserID"] == null) || (Request["UserID"] == ""))
            {
                //SecurityMaster sm = new SecurityMaster();
                ui.UserID = 0;
                ui.CustomerName = "";// lbxCustomer.SelectedItem.Text.Trim();
                //ui.CustomerName = lbxCustomer.SelectedItem.Text.Substring(lbxCustomer.SelectedItem.Text.LastIndexOf(" - ") + 1).Trim();
                ui.UserType = "Customer";
                ui.CompanyName = txtCompanyName.Text;
                ui.Username = txtUsername.Text;
                ui.UserFirstName = txtUserFirstName.Text;
                ui.UserLastName = txtUserLastName.Text;
                ui.Department = txtDepartment.Text;
                ui.PhoneNumber = txtPhoneNumber.Text;
                ui.EmailAddress = txtEmailAddress.Text;
                ui.Login = txtLogin.Text;
                ui.UserPassword = txtPassword.Text;
                ui.BackupName = txtBackupName.Text;
                ui.BackupFirstName = txtBackupFirstName.Text;
                ui.BackupEmailAddress = txtBackupEmailAddress.Text;

                um.CreateUpdateUserInformation(ui);
                
                for (int i = 0; i < lbxCustomer.Items.Count; i++)
                {
                    if (lbxCustomer.Items[i].Selected == true)
                    {
                        um.SaveUserCustomerMapping(ui.UserID, int.Parse(lbxCustomer.Items[i].Value));
                    }
                }

                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert" + UniqueID,
                "alert('Registration Complete.');window.location = 'Login.aspx'", true);
                //Response.Redirect("Login.aspx");
                
            }
            else
            {
                ui.UserID = int.Parse(Request["UserID"]);
                ui.CustomerName = lbxCustomer.SelectedItem.Text.Trim();
                ui.UserType = "Customer";
                ui.CompanyName = txtCompanyName.Text;
                ui.Username = txtUsername.Text;
                ui.UserFirstName = txtUserFirstName.Text;
                ui.UserLastName = txtUserLastName.Text;
                ui.Department = txtDepartment.Text;
                ui.PhoneNumber = txtPhoneNumber.Text;
                ui.EmailAddress = txtEmailAddress.Text;
                ui.Login = txtLogin.Text;
                ui.UserPassword = txtPassword.Text;
                ui.BackupName = txtBackupName.Text;
                ui.BackupFirstName = txtBackupFirstName.Text;
                ui.BackupEmailAddress = txtBackupEmailAddress.Text;

                um.CreateUpdateUserInformation(ui);
                showMessage(true, "Account successfully modified.");
            }
        }
        catch (Exception ex)
        {
            showMessage(true, "There was a problem while saving your transaction. Please try again.\n" + ex.Message);
        }
    }
    private void showMessage(bool showHide, string message)
    {
        lblMessage.Text = message;
        lblMessage.Visible = showHide;
    }
    protected void btnReGenerate_Click(object sender, EventArgs e)
    {
        UpdateCaptchaText();
    }
}
