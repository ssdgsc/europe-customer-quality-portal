﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using General.AvianCrypto;

public partial class Deliveries : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
                return;
            }
            SecurityMaster sm = new SecurityMaster();
            string user = sm.Decrypt(Session["UserType"].ToString());
            if (user != "CUSTOMER QUALITY")
            {
                this.pnlMaintenance.Visible = false;
            }
            else
            {
                this.pnlMaintenance.Visible = true;
            }
        }
        catch {
            Response.Redirect("Login.aspx");
            return;
        }

    }
}
