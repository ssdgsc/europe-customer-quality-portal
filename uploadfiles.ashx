﻿<%@ WebHandler Language="C#" Class="uploadfiles" %>

using System;
using System.Web;

public class uploadfiles : IHttpHandler  {
    
    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/html";
        context.Response.ClearContent();
        context.Response.Clear();
    
        try
        {
            if (context.Request.Files.Count == 0)
            {
                context.Response.Write("{\"Error\":\"There is a problem with file you are uploading.\"}");
            }
            else
            {
                HttpPostedFile uploadedfile = context.Request.Files[0];
                DateTime dt = DateTime.Now;
                string sessionKey = "FILE" + dt.ToFileTime().ToString();
                string Filename = uploadedfile.FileName;
                string Filetype = uploadedfile.ContentType;
                int Filesize = uploadedfile.ContentLength;
               
                if (Filesize <= 1000000)
                {
                    //context.Session.Add("ReturnFormUploadedFile", Filename.ToString());
                    
                    string[] tmp1 = Filename.Split(".".ToCharArray());
                    string fileExtension = tmp1[tmp1.Length - 1];

                    string[] tmp = Filename.Split(@"\".ToCharArray());
                    Filename = tmp[tmp.Length - 1];

                    string finalSaveFilename = context.Server.MapPath("./") + "\\UploadedFiles\\";
                    uploadedfile.SaveAs(System.IO.Path.Combine(finalSaveFilename, sessionKey + "." + fileExtension));
                    
                    string ret = "{\"Result\":\"OK\", \"Filename\":\"" + sessionKey + "." + fileExtension + "\", \"OrigFilename\":\"" + Filename + "\"}";
                    context.Response.Write(ret);
                }
                else
                {
                    context.Response.ContentType = "text/html";
                    context.Response.Write("{\"Result\":\"Error\", \"Message\":\"The file exceeded the 1MB limit.\"}");
                }
            }

        }
        catch (Exception ex)
        {
            context.Response.ContentType = "text/html";
            context.Response.Write("{\"Result\":\"Error\", \"Message\":\"" + ex.Message + "\"}");

        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}