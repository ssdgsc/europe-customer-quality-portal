﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Web.UI.HtmlControls;

using System.Collections;
using System.Configuration;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using General; 
using System.Web.Script.Serialization;
using System.Globalization;
using System.Web.Services;
using Maintenance;
using System.Net.Mail;
using General.AvianCrypto;
using System.Xml;
using SpreadsheetLight;
using System.IO.Packaging;

public partial class MyReturn : System.Web.UI.Page
{
    // search by 0-none, 1-by serial, 2-model
    General.AvianCrypto.SecurityMaster sm = new General.AvianCrypto.SecurityMaster();
    //
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UserID"] == null)
        {
            Response.Redirect("Login.aspx");
            return;
        }
        // CHeck if admin to show/hide the maintenancne
        SecurityMaster sm = new SecurityMaster();
        string user = sm.Decrypt(Session["UserType"].ToString());
        this.hfRole.Value = user;
        //if (user != "CUSTOMER QUALITY")
        //{
        //    this.btnAddNewControlItem.Visible = false;
        //    this.txtNewControlQty.Visible = false;
        //    this.Button2.Visible = false; // generate report button
        //    this.pnlMaintenance.Visible = false;
        //    if (user == "CSO")
        //        isCSORole(true);
        //}
        //else
        //{
           
        //    this.btnAddNewControlItem.Visible = true;
        //    this.txtNewControlQty.Visible = true;
        //    this.Button2.Visible = true; // generate report button
        //}

        initialization();
        if (!Page.IsPostBack)
        {

            
            bindDataCompressor();
            initCustomerCompressor();

            bindDataControl(); 
            initCustomerControl();

            //initControlModel();

            Param prm = new Param();
            prm.getParams(ddControlFailureDescription, "FailureDescription");
            prm.getParams(ddControlOrigin, "Origin");
            prm.getParams(ddControlCauseOrGuilty, "Cause");
             
            prm.getParams(ddControlClaimLists, "CtrlClaim");
            prm.getParams(ddCompressorClaimLists, "CompReason");
            
            this.pnlCompressorDetail.Visible = false;
            this.pnlControlDetail.Visible = false;
            this.pnlReturn.Visible = true;
            this.ddControlFailureDescription.SelectedIndex = 0;
            this.ddControlCreditDecision.SelectedIndex = 0;
            this.ddControlOrigin.SelectedIndex = 0;
            this.ddControlCauseOrGuilty.SelectedIndex = 0;
            this.ddControlPartReturned.SelectedIndex = 0;
            this.pnlControlSearch2.Visible = false;

            //if (user == "CSO") {
            //    ddProductType.SelectedIndex = 1; 
            //    ddProductType_SelectedIndexChanged(this, EventArgs.Empty); 
            //}
            //// ADD THIS LINE TO SHOW CONTROL RETURN FORM ONLY 
            //ddProductType.SelectedIndex = 1;
            //ddProductType_SelectedIndexChanged(this, EventArgs.Empty); 
        }
    }


    public void isCSORole(bool toggle) {
        cmdEditCompressor.Visible = !toggle;
        cmdEditControl.Visible = !toggle;
        txtNewControlQty.Visible = !toggle;
        btnAddNewControlItem.Visible = !toggle;
 
    }
    
    protected void initControlModel()
    {
        Maintenance.ReturnFormControl mrfd = new Maintenance.ReturnFormControl();
        Maintenance.ReturnFormAccessories mrfa = new Maintenance.ReturnFormAccessories();
        DataTable dt = new DataTable();
        if (ddProductType.SelectedValue == "Accessories") 
            dt = mrfa.getModelByPCNNumber();
        else
            dt = mrfd.getModelByPCNNumber();
        txtControlProduct.Items.Add(new ListItem());
        foreach (DataRow dr in dt.Rows)
        {
            ListItem li = new ListItem();
            li.Text = dr[1].ToString() + "-" + dr[0].ToString();
            li.Value = dr[0].ToString();
            this.txtControlProduct.Items.Add(li);
        }
    }

   
    protected void initCustomerControl()
    {
        Maintenance.ReturnFormControl mrfc = new Maintenance.ReturnFormControl();
        int userID = int.Parse(sm.Decrypt(Session["UserID"].ToString()));
        if (this.hfRole.Value == "CUSTOMER")
        {
            this.ddControlCustomerList.DataSource = mrfc.getCustomerList(userID);
        }
        else
        {
            this.ddControlCustomerList.DataSource = mrfc.getAllCustomerListControl(this.hfRole.Value,userID);
        }
        this.ddControlCustomerList.DataValueField = "CUSTOMERNUMBER";
        this.ddControlCustomerList.DataTextField = "CUSTOMERNAME";
        this.ddControlCustomerList.DataBind();
        //this.ddControlCustomerList.Items.Insert(0, new ListItem("-- ALL --", "-- ALL --")); 
        this.ddControlCustomerList.SelectedIndex = -1;
    }

    protected void initCustomerCompressor()
    {
        int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString()));
        Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
        if (this.hfRole.Value == "CUSTOMER")
        {
            this.ddCustomerList.DataSource = mrfd.getCustomerList(userid);
        }
        else
        {
            this.ddCustomerList.DataSource = mrfd.getAllCustomerListCompressor(this.hfRole.Value, userid);
        }

        this.ddCustomerList.DataValueField = "CUSTOMERNUMBER";
        this.ddCustomerList.DataTextField = "CUSTOMERNAME";
        this.ddCustomerList.DataBind();
        //this.ddCustomerList.Items.Insert(0, new ListItem("-- ALL --", "-- ALL --"));
        this.ddCustomerList.SelectedIndex = -1; 
       
    }
    protected void initCustomerAccesories()
    {
        int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString()));
        Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
        if (this.hfRole.Value == "CUSTOMER")
        {
            this.ddControlCustomerList.DataSource = mrfd.getCustomerList(userid);
        }
        else
        {
            this.ddControlCustomerList.DataSource = mrfd.getAllCustomerListCompressor(this.hfRole.Value, userid);
        }

        this.ddControlCustomerList.DataValueField = "CUSTOMERNUMBER";
        this.ddControlCustomerList.DataTextField = "CUSTOMERNAME";
        this.ddControlCustomerList.DataBind();
        //this.ddCustomerList.Items.Insert(0, new ListItem("-- ALL --", "-- ALL --"));
        this.ddControlCustomerList.SelectedIndex = -1;

    }

    protected void bindDataCompressor()
    {
        // COMPRESSOR DATA
        Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
        DataTable dt = new DataTable();
        int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString()));
        string selectedCustName = this.ddCustomerList.SelectedValue;
        if (this.hfRole.Value == "CUSTOMER")
        {
            dt = mrfd.getMyReturn(userid, 0, "", "-- ALL --"); 
        }
        else
        {
            dt = mrfd.getMyReturnAll(0, "", "-- ALL --",this.hfRole.Value,userid);
        }
        this.gvMyReturn.DataSource = dt;
        this.gvMyReturn.DataBind();
        //searchStatus(false);  

    }
    protected void bindDataControl()
    {
        //CONTROL DATA
        Maintenance.ReturnFormControl mrfc = new Maintenance.ReturnFormControl();
        DataTable dtc = new DataTable();
        int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString()));
        string selectedCustName = this.ddCustomerList.SelectedValue;

        if (this.hfRole.Value == "CUSTOMER")
        {
            dtc = mrfc.getMyReturnControl(userid, 0, "", "-- ALL --");
        }
        else
        {
            dtc = mrfc.getMyReturnControlAll(0, "", "-- ALL --", this.hfRole.Value, userid);
        }

        this.gvMyReturnControl.DataSource = dtc;
        this.gvMyReturnControl.DataBind();
        //searchControlStatus(false);


        //sm.Decrypt(Session["UserType"].ToString());
        if (sm.Decrypt(Session["UserType"].ToString()) == "CUSTOMER")
        {
            pnlControlFeedback.Visible = false;
            pnlControlClosedDate.Visible = false;
        }

        Button2.Visible = true; // Generate Report button
        cmdPrintControlReport.Visible = true;
        Button1.Visible = true; //Save as Excel button
        panelAddNewControlItem.Visible = true;   

    }

    protected void bindDataAccessories()
    {
        //CONTROL DATA
        Maintenance.ReturnFormAccessories mrfc = new Maintenance.ReturnFormAccessories();
        DataTable dtc = new DataTable();
        int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString()));
        string selectedCustName = this.ddCustomerList.SelectedValue;

        if (this.hfRole.Value == "CUSTOMER" )
        {
            dtc = mrfc.getMyReturnControl(userid, 0, "", "-- ALL --");
        }
        else
        {
            dtc = mrfc.getMyReturnControlAll(0, "", "-- ALL --",this.hfRole.Value, userid);
        }

        this.gvMyReturnControl.DataSource = dtc;
        this.gvMyReturnControl.DataBind();
        //searchControlStatus(false);


        //sm.Decrypt(Session["UserType"].ToString());
        if (sm.Decrypt(Session["UserType"].ToString()) == "CUSTOMER")
        {
            pnlControlFeedback.Visible = false;
            pnlControlClosedDate.Visible = false;
        }

        // [START] [jpbersonda] [4/12/17] Hide these panels if the product is Accessories
             pnlControlClosedDate.Visible = false;
             pnlControlFeedback.Visible = false;
             Button2.Visible = false; // Generate Report button
             cmdPrintControlReport.Visible = false;
             Button1.Visible = false; //Save as Excel button
             panelAddNewControlItem.Visible = false;   
        // [END] [jpbersonda] [4/12/17]
    }
    protected void btnSearchBySerial_Click(object sender, EventArgs e)
    {
        searchStatus(true);
        this.hdnSearchBy.Value = "1";
        this.lblSearch.Text = "Search by Serial";
        this.txtSearch.Text = "";
    }
    protected void btnControlSearchByPCNNumber_Click(object sender, EventArgs e)
    {
        searchControlStatus(true);
        this.hdnControlSearchBy.Value = "1";
        this.lblControlSearch.Text = "Search by PCN Number";
        this.txtControlSearch.Text = "";
    }
    protected void btnSearchByModel_Click(object sender, EventArgs e)
    {
        searchStatus(true);
        this.hdnSearchBy.Value = "2";
        this.lblSearch.Text = "Search by Model";
        this.txtSearch.Text = "";
    }
    protected void btnControlSearchByProduct_Click(object sender, EventArgs e)
    {
        searchControlStatus(true);
        this.hdnControlSearchBy.Value = "2";
        this.lblControlSearch.Text = "Search by Product";
        this.txtControlSearch.Text = "";
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.hdnSearchBy.Value = radioSerial.Checked ? "1" : "2";
            Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
            DataTable dt = new DataTable();

            string selectedCustomer = getSelectedCustomer(ddCustomerList);

            string searchtext = this.txtSearch.Text;
            if (String.IsNullOrEmpty(this.txtSearch.Text))
                searchtext = "*";
            int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString()));

            if (this.hfRole.Value == "CUSTOMER")
            {
                dt = mrfd.getMyReturn(userid, int.Parse(this.hdnSearchBy.Value), searchtext, selectedCustomer);
            }
            else
            {
                dt = mrfd.getMyReturnAll(int.Parse(this.hdnSearchBy.Value), searchtext, selectedCustomer, this.hfRole.Value,userid);
            }


            this.gvMyReturn.DataSource = dt;
            this.gvMyReturn.DataBind();
            //searchStatus(false);
        }
        catch (Exception ex)
        {
            LabelMessage.Text = ex.GetBaseException().ToString();
            LabelMessage.Visible = true;
        }
    }
    protected void btnControlSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.hdnControlSearchBy.Value = radioControlPCN.Checked ? "1" : "2";
            dynamic mrfc = toggleControlOrAccessoriesMaintenance(ddProductType.SelectedValue);

             
            DataTable dtc = new DataTable();
            string searchtext = this.txtControlSearch.Text;

            string selectedCustomer = getSelectedCustomer(ddControlCustomerList);

            if (String.IsNullOrEmpty(this.txtControlSearch.Text))
                searchtext = "*";
            int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString()));
            if (this.hfRole.Value == "CUSTOMER")
            {
                dtc = mrfc.getMyReturnControl(userid, int.Parse(this.hdnControlSearchBy.Value), searchtext, selectedCustomer);
            }
            else
            {
                dtc = mrfc.getMyReturnControlAll(int.Parse(this.hdnControlSearchBy.Value), searchtext, selectedCustomer, this.hfRole.Value,userid);
            }

            this.gvMyReturnControl.DataSource = dtc;
            this.gvMyReturnControl.DataBind();
            //searchControlStatus(false);
        }
        catch (Exception ex)
        {
            LabelMessage.Text = ex.GetBaseException().ToString();
            LabelMessage.Visible = true;
        }
    }
    protected void btnDownloadList_Click(object sender, EventArgs e)
    {
        try
        {
            Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
            DataTable dt = new DataTable();
            int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString()));

            string selectedCustomer = getSelectedCustomer(ddCustomerList);

            if (this.hfRole.Value == "CUSTOMER")
            {
                dt = mrfd.getMyReturn(userid, int.Parse(this.hdnSearchBy.Value), this.txtSearch.Text, selectedCustomer);
            }
            else
            {
                dt = mrfd.getMyReturnAll(int.Parse(this.hdnSearchBy.Value), this.txtSearch.Text, selectedCustomer, this.hfRole.Value, userid);
            }

            this.gvMyReturn.DataSource = dt;
            this.gvMyReturn.AllowPaging = false;
            this.gvMyReturn.DataBind();

            //Response.ClearContent();
            //Response.AddHeader("content-disposition", "attachment; filename=MyReturn.xls");
            //Response.ContentType = "application/ms-excel"; //application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
            //StringWriter sw = new StringWriter();
            //HtmlTextWriter htw = new HtmlTextWriter(sw);
            //HtmlForm frm = new HtmlForm();
            //GridView grd = gvMyReturn; // here you call the gridview ID from the aspx page
            //grd.Parent.Controls.Add(frm);
            //frm.Attributes["runat"] = "server";
            //frm.Controls.Add(grd);
            //frm.RenderControl(htw);
            //Response.Write(sw.ToString()); //
            //Response.End();

            // [START] [jpbersonda] [5/5/17] -- Alternative to HtmlTextWriter
            SLDocument objSLDocument = new SLDocument();

            // Headers
            objSLDocument.SetCellValue(1, 1, "RF Detail ID");
            objSLDocument.SetCellValue(1, 2, "Customer Name");
            objSLDocument.SetCellValue(1, 3, "Model");
            objSLDocument.SetCellValue(1, 4, "Serial Number");
            objSLDocument.SetCellValue(1, 5, "Portal Reference");
            objSLDocument.SetCellValue(1, 6, "Return Number");
            objSLDocument.SetCellValue(1, 7, "Cust Ref");
            objSLDocument.SetCellValue(1, 8, "Reject");
            objSLDocument.SetCellValue(1, 9, "Submission Date");
            objSLDocument.SetCellValue(1, 10, "Reception Date");
            objSLDocument.SetCellValue(1, 11, "Analyze Date");
            objSLDocument.SetCellValue(1, 12, "Credit Note");

            // Cell values
            for (int x = 0; x < dt.Rows.Count; x++)
            {
                objSLDocument.SetCellValue(x + 2, 1, dt.Rows[x]["RFDetailID"].ToString());
                objSLDocument.SetCellValue(x + 2, 2, dt.Rows[x]["CustomerName"].ToString());
                objSLDocument.SetCellValue(x + 2, 3, dt.Rows[x]["Model"].ToString());
                objSLDocument.SetCellValue(x + 2, 4, dt.Rows[x]["SerialNumber"].ToString());
                objSLDocument.SetCellValue(x + 2, 5, dt.Rows[x]["ReferenceNumber"].ToString());
                //objSLDocument.SetCellValue(x + 2, 6, dt.Rows[x]["ReturnNumber"].ToString());
                objSLDocument.SetCellValue(x + 2, 7, dt.Rows[x]["CustRef"].ToString());
                objSLDocument.SetCellValue(x + 2, 8, dt.Rows[x]["Reject"].ToString());
                objSLDocument.SetCellValue(x + 2, 9, dt.Rows[x]["SubmissionDate"].ToString());
                objSLDocument.SetCellValue(x + 2, 10, dt.Rows[x]["ReceptionDate"].ToString());
                objSLDocument.SetCellValue(x + 2, 11, dt.Rows[x]["AnalyzeDate"].ToString());
                objSLDocument.SetCellValue(x + 2, 12, dt.Rows[x]["CreditNote"].ToString());
            }

            // Cell properties / style
            objSLDocument.AutoFitColumn(1, 12);
            SLStyle slStyle = objSLDocument.CreateStyle();
            slStyle.SetFontBold(true);
            slStyle.SetFontColor(SLThemeColorIndexValues.Dark2Color, 0.3);
            
            // Header Style
            for (int columnIndex = 1; columnIndex <= 12; columnIndex++)
            {
                objSLDocument.SetCellStyle(1, columnIndex, slStyle);
            }
               


            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=MyReturn.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            objSLDocument.SaveAs(Response.OutputStream);

            Response.End();

            // [END] [jpbersonda] [5/5/17] 
            
        }
        catch (Exception ex)
        {
            LabelMessage.Text = ex.GetBaseException().ToString();
            LabelMessage.Visible = true;
        }
    }
    protected void btnControlDownloadList_Click(object sender, EventArgs e)
    {
        try
        {
            dynamic mrfc = toggleControlOrAccessoriesMaintenance(ddProductType.SelectedValue);
 
            DataTable dtc = new DataTable();
            string selectedCustomer = getSelectedCustomer(ddControlCustomerList);
            int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString()));
            if (this.hfRole.Value == "CUSTOMER")
            {
                dtc = mrfc.getMyReturnControl(userid, int.Parse(this.hdnControlSearchBy.Value), this.txtControlSearch.Text, selectedCustomer);
            }
            else
            {
                dtc = mrfc.getMyReturnControlAll(int.Parse(this.hdnControlSearchBy.Value), this.txtControlSearch.Text, selectedCustomer, this.hfRole.Value, userid);
            }


            this.gvMyReturnControl.DataSource = dtc;
            this.gvMyReturnControl.AllowPaging = false;
            this.gvMyReturnControl.DataBind();

            //Response.ClearContent();
            //Response.AddHeader("content-disposition", "attachment; filename=MyReturn.xls");
            //Response.ContentType = "application/ms-excel";
            //StringWriter sw = new StringWriter();
            //HtmlTextWriter htw = new HtmlTextWriter(sw);
            //HtmlForm frm = new HtmlForm();
            //GridView grd = gvMyReturnControl; // here you call the gridview ID from the aspx page
            //grd.Parent.Controls.Add(frm);
            //frm.Attributes["runat"] = "server";
            //frm.Controls.Add(grd);
            //frm.RenderControl(htw);
            //Response.Write(sw.ToString());
            //Response.End();

            // [START] [jpbersonda] [5/5/17] -- Alternative to HtmlTextWriter
            SLDocument objSLDocument = new SLDocument();

            // Headers
            objSLDocument.SetCellValue(1, 1, "RF Detail ID");
            objSLDocument.SetCellValue(1, 2, "Customer Name");
            objSLDocument.SetCellValue(1, 3, "Product");
            objSLDocument.SetCellValue(1, 4, "PCN Number");
            objSLDocument.SetCellValue(1, 5, "Portal Reference");
            objSLDocument.SetCellValue(1, 6, "Return Number");
            objSLDocument.SetCellValue(1, 7, "Cust Ref");
            objSLDocument.SetCellValue(1, 8, "Reject");
            objSLDocument.SetCellValue(1, 9, "Submission Date");
            objSLDocument.SetCellValue(1, 10, "Reception Date");
            objSLDocument.SetCellValue(1, 11, "Analyze Date");
            objSLDocument.SetCellValue(1, 12, "Credit Note");

            // Cell values
            for (int x = 0; x < dtc.Rows.Count; x++)
            {
                objSLDocument.SetCellValue(x + 2, 1, dtc.Rows[x]["RFDetailID"].ToString());
                objSLDocument.SetCellValue(x + 2, 2, dtc.Rows[x]["Customer"].ToString());
                objSLDocument.SetCellValue(x + 2, 3, dtc.Rows[x]["Model"].ToString());
                objSLDocument.SetCellValue(x + 2, 4, dtc.Rows[x]["PCNNumber"].ToString());
                objSLDocument.SetCellValue(x + 2, 5, dtc.Rows[x]["ReferenceNumber"].ToString());
                //objSLDocument.SetCellValue(x + 2, 6, dt.Rows[x]["ReturnNumber"].ToString());
                objSLDocument.SetCellValue(x + 2, 7, dtc.Rows[x]["CustRef"].ToString());
                objSLDocument.SetCellValue(x + 2, 8, dtc.Rows[x]["Reject"].ToString());
                objSLDocument.SetCellValue(x + 2, 9, dtc.Rows[x]["SubmissionDate"].ToString());
                objSLDocument.SetCellValue(x + 2, 10, dtc.Rows[x]["ReceptionDate"].ToString());
                objSLDocument.SetCellValue(x + 2, 11, dtc.Rows[x]["AnalyzeDate"].ToString());
                objSLDocument.SetCellValue(x + 2, 12, dtc.Rows[x]["CreditNote"].ToString());
            }

            // Cell properties / style
            objSLDocument.AutoFitColumn(1, 12);
            SLStyle slStyle = objSLDocument.CreateStyle();
            slStyle.SetFontBold(true);
            slStyle.SetFontColor(SLThemeColorIndexValues.Dark2Color, 0.3);

            // Header Style
            for (int columnIndex = 1; columnIndex <= 12; columnIndex++)
            {
                objSLDocument.SetCellStyle(1, columnIndex, slStyle);
            }



            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=MyReturn.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            objSLDocument.SaveAs(Response.OutputStream);

            Response.End();

            // [END] [jpbersonda] [5/5/17] 
        }
        catch (Exception ex)
        {
            LabelMessage.Text = ex.GetBaseException().ToString();
            LabelMessage.Visible = true;
        }
    }
    protected void searchStatus(bool status)
    {
        //this.txtSearch.Visible = status;
        //this.btnSearch.Visible = status;
        //this.lblSearch.Visible = status;
        //this.lblSearch.Text = "";
    }
    protected void searchControlStatus(bool status)
    {
        //this.txtControlSearch.Visible = status;
        //this.btnControlSearch.Visible = status;
        //this.lblControlSearch.Visible = status;
        //this.lblControlSearch.Text = "";
    }

    private string getSelectedCustomer(ListBox list) {
        List<string> listCustomer = new List<string>();
        foreach (ListItem item in list.Items)
        {
            if (item.Selected)
                listCustomer.Add(item.Value);
        }
        string selectedCustName = (listCustomer.Count == list.Items.Count) ? "-- ALL --" : String.Join(",", listCustomer);
        selectedCustName = (selectedCustName != "") ? selectedCustName : "-- ALL --";
       
        return selectedCustName;
    }

    protected void gvMyReturn_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
        DataTable dt = new DataTable();
        int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString()));

        string selectedCustomer = getSelectedCustomer(ddCustomerList);

        if (this.hfRole.Value == "CUSTOMER")
        {
            dt = mrfd.getMyReturn(userid, int.Parse(this.hdnSearchBy.Value), this.txtSearch.Text, selectedCustomer);
        }
        else
        {
            dt = mrfd.getMyReturnAll(int.Parse(this.hdnSearchBy.Value), this.txtSearch.Text, selectedCustomer, this.hfRole.Value, userid);
        }

        this.gvMyReturn.DataSource = dt; // sortGridView(dt, true);
        this.gvMyReturn.PageIndex = e.NewPageIndex;
        this.gvMyReturn.DataBind();
        searchStatus(false);
    }
    protected void gvMyReturnControl_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        dynamic mrfc = toggleControlOrAccessoriesMaintenance(ddProductType.SelectedValue);
        DataTable dtc = new DataTable();
        int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString()));
        //ADDED BY MF 10062015
        int searchBy = 0;
        if (this.txtControlSearch.Text != "")
            searchBy = (this.radioControlPCN.Checked) ? 1 : 2;

        string selectedCustomer = getSelectedCustomer(ddControlCustomerList);


        if (this.hfRole.Value == "CUSTOMER" )
        {
            dtc = mrfc.getMyReturnControl(userid, searchBy, this.txtControlSearch.Text, selectedCustomer);
        }
        else
        {
            dtc = mrfc.getMyReturnControlAll(searchBy, this.txtControlSearch.Text, selectedCustomer, this.hfRole.Value, userid);
        }

        this.gvMyReturnControl.DataSource = dtc;// sortGridView(dtc, true);
        this.gvMyReturnControl.PageIndex = e.NewPageIndex;
        this.gvMyReturnControl.DataBind();
        //searchControlStatus(false);
    }
    protected void lnk20_Click(object sender, EventArgs e)
    {
        Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
        DataTable dt = new DataTable();
        int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString()));

        string selectedCustomer = getSelectedCustomer(ddCustomerList);

        if (this.hfRole.Value == "CUSTOMER")
        {
            dt = mrfd.getMyReturn(userid, int.Parse(this.hdnSearchBy.Value), this.txtSearch.Text, selectedCustomer);
        }
        else
        {
            dt = mrfd.getMyReturnAll(int.Parse(this.hdnSearchBy.Value), this.txtSearch.Text, selectedCustomer, this.hfRole.Value, userid);
        }

        this.gvMyReturn.PageSize = 20;
        this.gvMyReturn.DataSource = dt;
        this.gvMyReturn.DataBind();
        searchStatus(false);
    }
    protected void lnkControl20_Click(object sender, EventArgs e)
    {
        dynamic mrfc = toggleControlOrAccessoriesMaintenance(ddProductType.SelectedValue);

        DataTable dtc = new DataTable();
        int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString()));

        string selectedCustomer = getSelectedCustomer(ddControlCustomerList);

        if (this.hfRole.Value == "CUSTOMER" )
        {
            dtc = mrfc.getMyReturnControl(userid, int.Parse(this.hdnControlSearchBy.Value), this.txtControlSearch.Text, selectedCustomer);
        }
        else
        {
            dtc = mrfc.getMyReturnControlAll(int.Parse(this.hdnControlSearchBy.Value), this.txtControlSearch.Text, selectedCustomer, this.hfRole.Value, userid);
        }

        this.gvMyReturnControl.PageSize = 20;
        this.gvMyReturnControl.DataSource = dtc;
        this.gvMyReturnControl.DataBind();
        searchControlStatus(false);
    }
    protected void lnk50_Click(object sender, EventArgs e)
    {
        Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
        DataTable dt = new DataTable();
        int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString()));
        string selectedCustomer = getSelectedCustomer(ddCustomerList);
        if (this.hfRole.Value == "CUSTOMER")
        {
            dt = mrfd.getMyReturn(userid, int.Parse(this.hdnSearchBy.Value), this.txtSearch.Text, selectedCustomer);
        }
        else
        {
            dt = mrfd.getMyReturnAll(int.Parse(this.hdnSearchBy.Value), this.txtSearch.Text, selectedCustomer, this.hfRole.Value, userid);
        }
        this.gvMyReturn.PageSize = 50;
        this.gvMyReturn.DataSource = dt;
        this.gvMyReturn.DataBind();
        searchStatus(false);
    }
    protected void lnkControl50_Click(object sender, EventArgs e)
    {
        dynamic mrfc = toggleControlOrAccessoriesMaintenance(ddProductType.SelectedValue);
        DataTable dtc = new DataTable();
        int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString()));

        string selectedCustomer = getSelectedCustomer(ddControlCustomerList);

        if (this.hfRole.Value == "CUSTOMER")
        {
            dtc = mrfc.getMyReturnControl(userid, int.Parse(this.hdnControlSearchBy.Value), this.txtControlSearch.Text, selectedCustomer);
        }
        else
        {
            dtc = mrfc.getMyReturnControlAll(int.Parse(this.hdnControlSearchBy.Value), this.txtControlSearch.Text, selectedCustomer,this.hfRole.Value, userid);
        }


        this.gvMyReturnControl.PageSize = 50;
        this.gvMyReturnControl.DataSource = dtc;
        this.gvMyReturnControl.DataBind();
        searchControlStatus(false);
    }
    protected void lnk100_Click(object sender, EventArgs e)
    {
        Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
        DataTable dt = new DataTable();
        int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString())); 

        string selectedCustomer = getSelectedCustomer(ddCustomerList);

        if (this.hfRole.Value == "CUSTOMER")
        {
            dt = mrfd.getMyReturn(userid, int.Parse(this.hdnSearchBy.Value), this.txtSearch.Text, selectedCustomer);
        }
        else
        {
            dt = mrfd.getMyReturnAll(int.Parse(this.hdnSearchBy.Value), this.txtSearch.Text, selectedCustomer, this.hfRole.Value, userid);
        }

        this.gvMyReturn.PageSize = 100;
        this.gvMyReturn.DataSource = dt;
        this.gvMyReturn.DataBind();
        searchStatus(false);
    }
    protected void lnkControl100_Click(object sender, EventArgs e)
    {
        dynamic mrfc = toggleControlOrAccessoriesMaintenance(ddProductType.SelectedValue);
        DataTable dtc = new DataTable();
        int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString()));

        string selectedCustomer = getSelectedCustomer(ddControlCustomerList);

        if (this.hfRole.Value == "CUSTOMER")
        {
            dtc = mrfc.getMyReturnControl(userid, int.Parse(this.hdnControlSearchBy.Value), this.txtControlSearch.Text, selectedCustomer);
        }
        else
        {
            dtc = mrfc.getMyReturnControlAll(int.Parse(this.hdnControlSearchBy.Value), this.txtControlSearch.Text, selectedCustomer, this.hfRole.Value, userid);
        }

        this.gvMyReturnControl.PageSize = 100;
        this.gvMyReturnControl.DataSource = dtc;
        this.gvMyReturnControl.DataBind();
        searchControlStatus(false);
    }
    protected void gvMyReturn_Sorting(object sender, GridViewSortEventArgs e)
    {
        Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
        Session["SortExpression"] = e.SortExpression;
        int curPageIndex = this.gvMyReturn.PageIndex;

        string selectedCustomer = getSelectedCustomer(ddCustomerList);

        DataTable dt = new DataTable();
        int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString()));
        if (this.hfRole.Value == "CUSTOMER")
        {
            dt = mrfd.getMyReturn(userid, int.Parse(this.hdnSearchBy.Value), this.txtSearch.Text, selectedCustomer);
        }
        else
        {
            dt = mrfd.getMyReturnAll(int.Parse(this.hdnSearchBy.Value), this.txtSearch.Text, selectedCustomer, this.hfRole.Value, userid);
        }

        this.gvMyReturn.DataSource = sortGridView(dt, false);
        this.gvMyReturn.DataBind();
        this.gvMyReturn.PageIndex = curPageIndex;
        searchStatus(false);
    }
    protected void gvMyReturnControl_Sorting(object sender, GridViewSortEventArgs e)
    {
        dynamic mrfc = toggleControlOrAccessoriesMaintenance(ddProductType.SelectedValue);
        Session["SortExpression"] = e.SortExpression;
        int curPageIndex = this.gvMyReturnControl.PageIndex;

        string selectedCustomer = getSelectedCustomer(ddControlCustomerList);

        DataTable dtc = new DataTable();
        int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString()));
        if (this.hfRole.Value == "CUSTOMER")
        {
            dtc = mrfc.getMyReturnControl(userid, int.Parse(this.hdnControlSearchBy.Value), this.txtControlSearch.Text, selectedCustomer);
        }
        else
        {
            dtc = mrfc.getMyReturnControlAll(int.Parse(this.hdnControlSearchBy.Value), this.txtControlSearch.Text, selectedCustomer, this.hfRole.Value, userid);
        }

        this.gvMyReturnControl.DataSource = sortGridView(dtc, false);
        this.gvMyReturnControl.DataBind();
        this.gvMyReturnControl.PageIndex = curPageIndex;
        searchControlStatus(false);
    }
    public String convertSortDirection()
    {

        String newSortDirection = "";
        if (Session["SortDirection"] == null)
        {
            Session["SortDirection"] = "ASC";
        }
        switch (Session["SortDirection"].ToString())
        {
            case "ASC":
                newSortDirection = "DESC";
                break;
            case "DESC":
                newSortDirection = "ASC";
                break;
            default:
                newSortDirection = "ASC";
                break;
        }

        return newSortDirection;
    }

    public DataView sortGridView(DataTable dataTable, Boolean isPageChanging)
    {
        if (dataTable != null)
        {
            DataView resultView = new DataView(dataTable);
            if (isPageChanging)
            {
                if (Session["SortDirection"] == null)
                {
                    Session["SortDirection"] = "ASC";
                }
                String sortExp = Session["SortExpression"].ToString();
                String sortDirection = Session["SortDirection"].ToString();
                resultView.Sort = sortExp + " " + sortDirection;
            }
            else
            {
                Session["SortDirection"] = convertSortDirection();
                resultView.Sort = Session["SortExpression"].ToString() + " " + Session["SortDirection"];
            }
            return resultView;
        }
        else
        {
            return new DataView();
        }
    }

    protected void btnGo_Click(object sender, EventArgs e)
    {
        try
        {

            Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
            DataTable dt = new DataTable();
            string selectedCustomer = getSelectedCustomer(ddCustomerList);

            //string selectedCustName = this.ddCustomerList.SelectedValue;
            int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString()));
            if (this.hfRole.Value == "CUSTOMER")
            {
                dt = mrfd.getMyReturn(userid, 0, "", selectedCustomer);
            }
            else
            {
                dt = mrfd.getMyReturnAll(0, "", selectedCustomer, this.hfRole.Value, userid);
            }
            this.gvMyReturn.DataSource = dt;
            this.gvMyReturn.DataBind();
            searchStatus(false);
            this.hdnSearchBy.Value = "0";
            this.txtSearch.Text = "";
        }
        catch (Exception ex)
        {
            LabelMessage.Text = ex.GetBaseException().ToString();
            LabelMessage.Visible = true;
        }
    }

    protected void btnControlGo_Click(object sender, EventArgs e)
    {
        try
        {
            dynamic mrfc = toggleControlOrAccessoriesMaintenance(ddProductType.SelectedValue);
            DataTable dtc = new DataTable();

            string selectedCustomer = getSelectedCustomer(ddControlCustomerList);

            //string selectedCustName = this.ddControlCustomerList.SelectedValue;
            int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString()));
            if (this.hfRole.Value == "CUSTOMER")
            {
                dtc = mrfc.getMyReturnControl(userid, 0, "", selectedCustomer);
            }
            else
            {
                dtc = mrfc.getMyReturnControlAll(0, "", selectedCustomer, this.hfRole.Value, userid);
            }

            this.gvMyReturnControl.DataSource = dtc;
            this.gvMyReturnControl.DataBind();
            //searchControlStatus(false);
            this.hdnSearchBy.Value = "0";
            this.txtSearch.Text = "";
            this.txtControlSearch.Text = "";
        }
        catch (Exception ex)
        {
            LabelMessage.Text = ex.GetBaseException().ToString();
            LabelMessage.Visible = true;
        }
    }

    protected void disableCompressor()
    {

        cmdEditCompressor.Visible = !(Boolean)getUserAccess()["MyReturnsView"];
        cmdEditCompressor.Text = "Edit";
        txtOracleNumberComp.Enabled = false; 
        ddCompressorClaimLists.Enabled = false;
        ddLocationComp.Enabled = false;
        txtCompressorClaimDescription.Enabled = false;
        txtCompressorCustomerClaimRef.Enabled = false;
        txtCompressorSerialNumber.Enabled = false;
        btnCheckCompressorSerialNumber.Visible = false;
        txtCompressorReference.Enabled = false;
        optCompressorTypeOfReturn.Enabled = false;
        optCompressorApplication.Enabled = false;
        optCompressorRefrigerant.Enabled = false;
        txtCompressorRefrigerant.Enabled = false;
        txtCompressorWorkingPointTo.Enabled = false;
        txtCompressorWorkingPointTc.Enabled = false;
        txtCompressorSuperheat.Enabled = false;
        txtCompressorSubCooling.Enabled = false;
        optCompressorRequest1.Enabled = false;
        pnlCompressorRequestSpecificTest.Enabled = false;
        optCompressorConfiguration.Enabled = false;
        txtCompressorDateOfFailure.Enabled = false;
        txtCompressorRunningHours.Enabled = false;
        txtCompressorRemarks.Disabled = true;
    }

    protected void gvMyReturn_SelectedIndexChanged(object sender, EventArgs e)
    {
        disableCompressor();
        if (sm.Decrypt(Session["UserType"].ToString()) == "CSO")
        {
            cmdEditCompressor.Visible = false;
        }
        Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
        Model.ReturnFormDetail rfd = new Model.ReturnFormDetail();
        if (gvMyReturn.SelectedRow.Cells[1].Text == "&nbsp;" || String.IsNullOrEmpty(gvMyReturn.SelectedRow.Cells[1].Text))
        {
            return;
        }
        rfd = mrfd.getReturnFormDetail(int.Parse(gvMyReturn.SelectedRow.Cells[1].Text));

        this.ddProductType.Visible = false;
        this.pnlProduct.Visible = false;
        this.pnlCompressorCustomerList.Visible = false;
        this.pnlControlCustomerList.Visible = false;
        this.pnlCompressorDetail.Visible = true;
        this.pnlControlDetail.Visible = false;
        this.pnlCompressorSearch.Visible = false;

        for (int i = 0; i < this.optProductTypeCompressor.Items.Count; i++)
        {
            if (rfd.ProductType == this.optProductTypeCompressor.Items[i].Value.ToUpper())
            {
                this.optProductTypeCompressor.SelectedIndex = i;
                break;
            }
        }

       

        //this.optProductTypeCompressor.SelectedValue = rfd.ProductType;
        this.rfhid.Value = gvMyReturn.SelectedRow.Cells[1].Text;
        this.lblCompressorCustomerName.Text = getCustomerName(rfd.CustomerNumber.ToString());

        int userId = (Session["UserID"] == null) ? 0 : int.Parse(sm.Decrypt(Session["UserID"].ToString()));
        //bool isCustomer = (sm.Decrypt(Session["UserType"].ToString()) == "CUSTOMER") ? true : false;
        string usertype = sm.Decrypt(Session["UserType"].ToString());

        Maintenance.Customers c = new Maintenance.Customers();
        ddLocationComp.DataSource = null;
        ddLocationComp.DataSource = c.getLocationByCustomerNumberDT(rfd.CustomerNumber,userId, usertype);
        ddLocationComp.DataTextField = "LocationName";
        ddLocationComp.DataValueField = "LocationID";
        ddLocationComp.DataBind();

        lblAttachmentComp.Visible = false;
        aFileAttachmentComp.Visible = false;
        if (rfd.FilePath != "")
        {
            string[] filename = rfd.FilePath.Split('|');
            
            aFileAttachmentComp.Visible = true;
            aFileAttachmentComp.NavigateUrl = "UploadedFiles\\" + ((filename.Length > 1) ? filename[0].ToString() : filename[0].ToString().Replace("UPLOADEDFILES\\", ""));
            aFileAttachmentComp.Text =  ((filename.Length > 1) ? filename[1].ToString() : rfd.FilePath.ToString().Replace("UPLOADEDFILES\\", ""));
        }
        else
            lblAttachmentComp.Visible = true;

        if (rfd.LocationID != "0")
                ddLocationComp.SelectedValue = rfd.LocationID.ToString();

        lblPortalReferenceComp.Text = rfd.ReferenceNo;


        //Selected Value for Claim Lists
        for (int i = 0; i < this.ddCompressorClaimLists.Items.Count; i++)
        {
            if (rfd.Claim == this.ddCompressorClaimLists.Items[i].Value.ToUpper())
            {
                this.ddCompressorClaimLists.SelectedIndex = i;
                break;
            }
        }
        this.txtOracleNumberComp.Text = rfd.OracleNumber;

        //this.ddCompressorClaimLists.SelectedValue = rfd.Claim;
        this.txtControlWarranty.Text = getWarranty(rfd.DataCode);
        this.txtCompressorClaimDescription.Text = rfd.ClaimDescription;
        this.txtCompressorCustomerClaimRef.Text = rfd.CustomerClaimReference;
        this.txtCompressorSerialNumber.Text = rfd.SerialNumber;
        this.txtCompressorModel.Text = rfd.ModelProduct;
        this.hdnItemNumber.Value = rfd.ItemNumber;
        this.txtCompressorCreationDate.Text = formatDateFromSQL(rfd.DateOfClaim);
        this.txtCompressorReference.Text = rfd.CompressorComponentReference;
        this.optCompressorTypeOfReturn.SelectedValue = getSelectedReturnTypeValue(rfd.TypeOfReturn);

        //this.optCompressorApplication.SelectedValue = rfd.Application;
        for (int i = 0; i < this.optCompressorTypeOfReturn.Items.Count; i++)
        {
            if (rfd.TypeOfReturn == this.optCompressorTypeOfReturn.Items[i].Value.ToUpper())
            {
                this.optCompressorTypeOfReturn.SelectedIndex = i;
                break;
            }
        }

        for (int i = 0; i < this.optCompressorApplication.Items.Count; i++)
        {
            if (rfd.Application == this.optCompressorApplication.Items[i].Value.ToUpper())
            {
                this.optCompressorApplication.SelectedIndex = i;
                break;
            }
        }

        //this.optCompressorRefrigerant.SelectedValue = rfd.Refrigerant;
        for (int i = 0; i < this.optCompressorRefrigerant.Items.Count; i++)
        {
            if (rfd.Refrigerant == this.optCompressorRefrigerant.Items[i].Value.ToUpper())
            {
                this.optCompressorRefrigerant.SelectedIndex = i;
                break;
            }
        }

        //[START] [jpbersonda] [5/25/2017] [Changes for Request radio button values]
        optCompressorRequest1.SelectedIndex = -1; //CLEAR SELECTED BEFORE SET THE NEW VALUE
        //Selected Value for Compressor Request
        for (int i = 0; i < this.optCompressorRequest1.Items.Count; i++)
        {
            //For old data | Warranty Analysis is equal to Warranty or Analysis
            if(rfd.Request == "WARRANTY" || rfd.Request == "ANALYSIS")
            {
                this.optCompressorRequest1.SelectedIndex = 0;
                break;
            }

            //For old data - Specific Test is equal to Specific Request
            if (rfd.Request == "SPECIFIC TEST")
            {
                this.optCompressorRequest1.SelectedIndex = 1;
                break;
            }

            if (rfd.Request == this.optCompressorRequest1.Items[i].Value.ToUpper())
            {
                this.optCompressorRequest1.SelectedIndex = i;
                break;
            }
        }
        
        //This will show up if the Value for Request is SPECIFIC TEST
        if (rfd.Request == "SPECIFIC REQUEST")
        {
            this.pnlCompressorRequestSpecificTest.Visible = true;
            //this.txtCompressorRequestSpecificTest.Visible = true;
            this.txtCompressorRequestSpecificTest.Text = rfd.RequestSpecificTestRemarks;
        }
        else
        {
            this.pnlCompressorRequestSpecificTest.Visible = false;
            //this.txtCompressorRequestSpecificTest.Visible = false;
            this.txtCompressorRequestSpecificTest.Text = "";
        }
        //[END] [jpbersonda] [5/25/2017] [Changes for Request radio button values]

        if (rfd.Claim == "HIGH DLT" ||
            rfd.Claim == "COOLING/HEATING CAPACITY TOO LOW" ||
            rfd.Claim == "HIGH POWER INPUT" ||
            rfd.Claim == "HIGH AMPS" ||
            rfd.Claim == "LOW COP/EER")
        {
            pnlCompressorWorkingPoint.Visible = true;
            this.txtCompressorWorkingPointTo.Text = rfd.WorkingPointTo;
            this.txtCompressorWorkingPointTc.Text = rfd.WorkingPointTc;
            this.txtCompressorSuperheat.Text = rfd.WorkingPointSuperHeat;
            this.txtCompressorSubCooling.Text = rfd.WorkingPointSubcooling;
        }
        else
        {
            pnlCompressorWorkingPoint.Visible = false;
            this.txtCompressorWorkingPointTo.Text = "";
            this.txtCompressorWorkingPointTc.Text = "";
            this.txtCompressorSuperheat.Text = "";
            this.txtCompressorSubCooling.Text = "";
        }

        if (rfd.Claim == "OTHER")
        {
            lblCompressorClaimDesc.Visible = true;
            txtCompressorClaimDescription.Visible = true;
            txtCompressorClaimDescription.Text = rfd.ClaimDescription;
        }
        else
        {
            lblCompressorClaimDesc.Visible = false;
            txtCompressorClaimDescription.Visible = false;
            txtCompressorClaimDescription.Text = rfd.ClaimDescription;
        }

        if (rfd.Refrigerant == "OTHER")
        {
            this.pnlCompressorRefrigerant.Visible = true;
            this.txtCompressorRefrigerant.Text = rfd.OtherRefrigerant;
        }
        else
        {
            this.pnlCompressorRefrigerant.Visible = false;
            this.txtCompressorRefrigerant.Text = rfd.OtherRefrigerant;
        }

        //this.optCompressorConfiguration.SelectedValue = rfd.Configuration;
        for (int i = 0; i < this.optCompressorConfiguration.Items.Count; i++)
        {
            if (rfd.Configuration == this.optCompressorConfiguration.Items[i].Value.ToUpper())
            {
                this.optCompressorConfiguration.SelectedIndex = i;
                break;
            }
        }
        this.txtCompressorDateOfFailure.Text = formatDateFromSQL(rfd.DateOfFailure);
        this.txtCompressorRunningHours.Text = rfd.RunningHours;
        this.txtCompressorRemarks.Value = rfd.Remarks;
        //Response.Write(sm.Decrypt(Session["Username"].ToString()));
        if (sm.Decrypt(Session["UserType"].ToString()) == "CUSTOMER QUALITY")
        {
            pnlControlFeedback.Visible = true;
        }
        else
        {
            pnlControlFeedback.Visible = false;
        }
        this.pnlReturn.Visible = false;
    }

    public string getCustomerName(string _custNumber)
    {
        Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
        string customerName = mrfd.getSpecificCustomerName(_custNumber);
        return customerName;
    }
    /*
    public int getRFDetailID(int _RFDetailID)
    {
        Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
        Model.ReturnFormDetail rfd = new Model.ReturnFormDetail();
        rfd = mrfd.getReturnFormDetail(_RFDetailID);
        return rfd;
    }
    */
    protected string getSelectedReturnTypeValue(string _value)
    {
        string returnedValue;
        switch (_value)
        {
            case "LR":
                {
                    returnedValue = "Line Return";
                    break;
                }
            case "FR":
                {
                    returnedValue = "Field Return";
                    break;
                }
            case "CR":
                {
                    returnedValue = "Commercial Return";
                    break;
                }
            case "BB":
                {
                    returnedValue = "Buy Back";
                    break;
                }
            case "WD":
                {
                    returnedValue = "Wrong Delivery";
                    break;
                }
            case "TD":
                {
                    returnedValue = "Transport Damage";
                    break;
                }
            default:
                {
                    returnedValue = "Lab/Sample Test";
                    break;
                }
        }
        return returnedValue;
    }
    protected void cmdBackFromControl_Click(object sender, EventArgs e)
    {
        ddProductType.SelectedValue = optProductTypeControl.SelectedValue;
        this.ddProductType.Visible = true;
        //this.ddProductType.SelectedValue = "CONTROL";
        this.pnlProduct.Visible = true;
        this.pnlCompressorCustomerList.Visible = false;
        this.pnlControlCustomerList.Visible = true;
        this.pnlCompressorDetail.Visible = false;
        this.pnlControlDetail.Visible = false;
        this.pnlReturn.Visible = true;
        string y = this.ddControlCustomerList.SelectedValue;
        if (ddProductType.SelectedValue == "Accessories")
            bindDataAccessories();
        else
            bindDataControl();
        //bindDataCompressor();
        //bindDataControl();
    }
    public string formatDateForSQL(string date)
    {
        if (date == "") { return ""; }
        if (date.Length > 10) { return ""; }


        try
        {
            if (date.Length == 10)
                return DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy");
            else if (date.Length == 9)
                return DateTime.ParseExact(date, "dd/M/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy");
            else if (date.Length == 8)
                return DateTime.ParseExact(date, "d/M/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy");
            return "";

        }
        catch (Exception e)
        {
            return DateTime.ParseExact(date, "d/MM/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy");
        }

    }



    // 11/17/2014
    protected void cmdEditControl_Click(object sender, EventArgs e)
    {
        if (cmdEditControl.Text == "Edit")
        {
            cmdEditControl.Text = "Save";
            //this.optProductTypeControl.Enabled = true;

            ddControlClaimLists.Enabled = true;
            ddLocation.Enabled = true;
            reqControlSpecifyRefrigerant.Enabled = true;
            if (optControlRequest1.SelectedIndex == 1) 
                reqControlRequestSpecificTest.Enabled = true;
            else
                reqControlRequestSpecificTest.Enabled = false;
            txtControlClaimDescription.Enabled = true;
            //enableOracleNumber(true);
            txtOracleNumber.Enabled = true;
            txtControlCustomerClaimReference.Enabled = true;
            imgControlClaimReference.Visible = true;
            txtControlPCNNumber.Enabled = true;
            txtControlProduct.Enabled = true;
            btnCheckControlPCNNumber.Visible = true;
            txtControlDataCode.Enabled = true;
            txtControlQty.Enabled = true;
            txtControlComponentReference.Enabled = true;
            imgComponentReference.Visible = true;
            optControlTypeOfReturn.Enabled = true;
            optControlApplication.Enabled = true;
            optControlRefrigerant.Enabled = true;
            txtControlRefrigerant.Enabled = true;
            txtControlWorkingPointTO.Enabled = true;
            txtControlWorkingPointTC.Enabled = true;
            txtControlWorkingPointSuperHeat.Enabled = true;
            txtControlWorkingPointSuperHeat.Enabled = true;
            optControlRequest1.Enabled = true;
            //pnlControlRequestSpecificTest.Visible = true;
            txtControlRequestSpecificTest.Enabled = true;
            optControlConfiguration.Enabled = true;
            txtControlDateOfFailure.Enabled = true;
            txtControlRunningHours.Enabled = true;
            txtControlComments.Enabled = true;
            //ddControlFailureDescription.Enabled = true;
            ddControlOrigin.Enabled = true;
            ddControlCauseOrGuilty.Enabled = true;
            //ddControlCreditDecision.Enabled = true;
            txtControlReportText.Enabled = true;
            txtControlSupplierAnalysis.Enabled = true;
            ddControlPartReturned.Enabled = true;
            txtControlNote.Enabled = true;

            //VARIABLE FOR RECEIVED DATE/CLOSED DATE FOR CHECKING IF THE DATE HAS CHANGES //09292015
            Session["curReceiveDate"] = txtReceiveInKolin.Text;
            Session["curClosedDate"] = txtControlClosedInKolin.Text;
            Session["curCreditNote"] = ddControlCreditDecision.SelectedValue;

            if (sm.Decrypt(Session["UserType"].ToString()) == "CUSTOMER QUALITY")
            {
                this.txtReceiveInKolin.Enabled = true;
                txtControlClosedInKolin.Enabled = true;
                imgControlClosedInKolin.Visible = true;
                ddControlFailureDescription.Enabled = true;
                ddControlCreditDecision.Enabled = true;
            }
        }
        else
        { 
             
            int curRFHeader;
            txtControlOpenOrClose.Text = "OPEN";
            //if (getOracleNumber().Length != 0) {
            if (txtOracleNumber.Text.Length != 0)
            {
                long x = 0;
                //if (!checkOracleNumber(getOracleNumber()))
                if (!long.TryParse(txtOracleNumber.Text, out x))
                {
                    showMessage(true, "Return Number must be numeric values");
                    return;
                }
                //if (!checkOracleNumber(getOracleNumber()))
                if (!checkOracleNumber(txtOracleNumber.Text))
                {
                    showMessage(true, "Return Number must be 12 digits.");
                    return;
                }
                
            }

            dynamic mrfc = toggleControlOrAccessoriesMaintenance(optProductTypeControl.SelectedValue);
            dynamic rfc = toggleControlOrAccessoriesModel(optProductTypeControl.SelectedValue);
           
           
            //DataTable dt = new DataTable();
            //rfd = mrfd.getReturnFormDetail(int.Parse(gvMyReturn.SelectedRow.Cells[1].Text));
            rfc.RFDetailID = int.Parse(hdnRFDetailID.Value);
            curRFHeader = int.Parse(hdnRFHeaderID.Value);
            rfc.LocationID = (ddLocation.SelectedValue==null) || string.IsNullOrEmpty(ddLocation.SelectedValue.ToString()) ? "0" : ddLocation.SelectedValue;
            rfc.Claim = ddControlClaimLists.SelectedValue;
            rfc.ClaimDescription = txtControlClaimDescription.Text;
            rfc.ControlCustomerClaimReference = txtControlCustomerClaimReference.Text;
            rfc.PCNNumber = txtControlPCNNumber.Text;
            int modelProductExtract = txtControlProduct.SelectedItem.Text.LastIndexOf("-");
            rfc.ModelProduct = txtControlProduct.SelectedItem.Text.Substring(0, modelProductExtract);
            rfc.ProductGroup = txtControlProductGroup.Text;
            rfc.Line = txtControlLine.Text;
            rfc.DataCode = txtControlDataCode.Text;
            rfc.Warranty = formatDateForSQL(txtControlWarranty.Text);
            rfc.Quantity = txtControlQty.Text;
            rfc.DateOfClaim = formatDateForSQL(txtControlDateOfClaim.Text);
            rfc.ControlComponentReference = txtControlComponentReference.Text;
            rfc.TypeOfReturn = getReturnTypeValue(optControlTypeOfReturn.SelectedValue);
            rfc.Application = optControlApplication.SelectedValue;

            rfc.Refrigerant = optControlRefrigerant.SelectedValue;
            if (optControlRefrigerant.SelectedValue == "Other" || optControlRefrigerant.SelectedValue == "OTHER")
            { rfc.OtherRefrigerant = txtControlRefrigerant.Text; }
            else
            { rfc.OtherRefrigerant = ""; }

            //[START] [jpbersonda] [5/25/2017] [New data: Specific Request | Old data: Specific test]
            rfc.Request = optControlRequest1.SelectedValue;
            if (optControlRequest1.SelectedValue == "Specific Test" || optControlRequest1.SelectedValue == "SPECIFIC TEST" 
                || optControlRequest1.SelectedValue == "Specific Request" || optControlRequest1.SelectedValue == "SPECIFIC REQUEST")
            {
                rfc.RequestSpecificTestRemarks = txtControlRequestSpecificTest.Text;
                //pnlControlRequestSpecificTest.Visible = true;
            }
            else
            {
                rfc.RequestSpecificTestRemarks = "";
                //pnlControlRequestSpecificTest.Visible = false;
            }
            //[END]


            if (ddControlClaimLists.SelectedValue == "External leakage" ||
               ddControlClaimLists.SelectedValue == "Cooling/Heating capacity too low" ||
               ddControlClaimLists.SelectedValue == "High Power Input" ||
               ddControlClaimLists.SelectedValue == "High Amps" ||
               ddControlClaimLists.SelectedValue == "Low COP/EER")
            {
                rfc.WorkingPointTO = txtControlWorkingPointTO.Text;
                rfc.WorkingPointTC = txtControlWorkingPointTC.Text;
                rfc.WorkingPointSuperHeat = txtControlWorkingPointSuperHeat.Text;
                rfc.WorkingPointSubCooling = txtControlWorkingPointSubCooling.Text;
            }
            else
            {
                rfc.WorkingPointTO = "";
                rfc.WorkingPointTC = "";
                rfc.WorkingPointSuperHeat = "";
                rfc.WorkingPointSubCooling = "";
            }

            //rfc.CustomerNumber = int.Parse(ddCustomerList.SelectedValue);
            //rfc.ItemNumber = hdnItemNumber.Value;

            rfc.Configuration = optControlConfiguration.SelectedValue;
            rfc.DateOfFailure = formatDateForSQL(txtControlDateOfFailure.Text);
            rfc.RunningHours = (txtControlRunningHours.Text == "") ? "0" : txtControlRunningHours.Text;
            rfc.Remarks = txtControlComments.Text;

            rfc.ClosedInKolin = formatDateForSQL(txtControlClosedInKolin.Text);
            rfc.ReceivedInKolin = formatDateForSQL(txtReceiveInKolin.Text);

            if (txtControlClosedInKolin.Text != "")
            {
                /*
                CultureInfo ciCurr = CultureInfo.CurrentCulture;
                int weekNum = ciCurr.Calendar.GetWeekOfYear(Convert.ToDateTime(txtControlClosedInKolin.Text), CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
                rfc.ClosedInCurrentWeek = Convert.ToDateTime(txtControlClosedInKolin.Text).Year.ToString() + "/" + weekNum;
                rfc.ClosedInMonthIncome = Convert.ToDateTime(txtControlClosedInKolin.Text).Year.ToString() + "/" + Convert.ToDateTime(txtControlClosedInKolin.Text).Month.ToString();
                if (Convert.ToDateTime(txtControlClosedInKolin.Text).Month >= 10)
                { rfc.FiscalYearClosed = "FY" + (Convert.ToDateTime(txtControlClosedInKolin.Text).Year + 1).ToString().Substring(2, 2); }
                else
                { rfc.FiscalYearClosed = "FY" + Convert.ToDateTime(txtControlClosedInKolin.Text).Year.ToString().Substring(2, 2); }

                rfc.OpenOrClosed = "Closed";
                */

                CultureInfo ciCurr = CultureInfo.CurrentCulture;
                int weekNum = ciCurr.Calendar.GetWeekOfYear(Convert.ToDateTime(formatDateForSQL(txtControlClosedInKolin.Text)), CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
                txtControlClosedInCureentWeek.Text = Convert.ToDateTime(formatDateForSQL(txtControlClosedInKolin.Text)).Year.ToString() + "/" + weekNum;
                txtControlClosedInMonth.Text = Convert.ToDateTime(formatDateForSQL(txtControlClosedInKolin.Text)).Year.ToString() + "/" + Convert.ToDateTime(formatDateForSQL(txtControlClosedInKolin.Text)).Month.ToString();
                if (Convert.ToDateTime(formatDateForSQL(txtControlClosedInKolin.Text)).Month >= 10)
                { txtControlClosedInFY.Text = "FY" + (Convert.ToDateTime(formatDateForSQL(txtControlClosedInKolin.Text)).Year + 1).ToString().Substring(2, 2); }
                else
                { txtControlClosedInFY.Text = "FY" + Convert.ToDateTime(formatDateForSQL(txtControlClosedInKolin.Text)).Year.ToString().Substring(2, 2); }

                txtControlOpenOrClose.Text = "CLOSED";
            }
            else
            {
                txtControlClosedInCureentWeek.Text = "";
                txtControlClosedInMonth.Text = "";
                txtControlClosedInFY.Text = "";
                rfc.OpenOrClosed = "";
            }

            rfc.ClosedInCurrentWeek = txtControlClosedInCureentWeek.Text;
            rfc.ClosedInMonthIncome = txtControlClosedInMonth.Text;
            rfc.FiscalYearClosed = txtControlClosedInFY.Text;


            // kim

            if (this.txtReceiveInKolin.Text != "")
            {
                /*
                CultureInfo ciCurr = CultureInfo.CurrentCulture;
                int weekNum = ciCurr.Calendar.GetWeekOfYear(Convert.ToDateTime(txtReceivedInKolin.Text), CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
                rfc.ClosedInCurrentWeek = Convert.ToDateTime(txtReceivedInKolin.Text).Year.ToString() + "/" + weekNum;
                rfc.ClosedInMonthIncome = Convert.ToDateTime(txtReceivedInKolin.Text).Year.ToString() + "/" + Convert.ToDateTime(txtReceivedInKolin.Text).Month.ToString();
                if (Convert.ToDateTime(txtReceivedInKolin.Text).Month >= 10)
                { rfc.FiscalYearClosed = "FY" + (Convert.ToDateTime(txtReceivedInKolin.Text).Year + 1).ToString().Substring(2, 2); }
                else
                { rfc.FiscalYearClosed = "FY" + Convert.ToDateTime(txtReceivedInKolin.Text).Year.ToString().Substring(2, 2); }

                rfc.OpenOrClosed = "Closed";
                */

                CultureInfo ciCurr = CultureInfo.CurrentCulture;
                int weekNum = ciCurr.Calendar.GetWeekOfYear(Convert.ToDateTime(formatDateForSQL(txtReceiveInKolin.Text)), CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
                this.txtReceivedInCurrentWeek.Text = Convert.ToDateTime(formatDateForSQL(txtReceiveInKolin.Text)).Year.ToString() + "/" + weekNum;
                txtReceivedInMonth.Text = Convert.ToDateTime(formatDateForSQL(txtReceiveInKolin.Text)).Year.ToString() + "/" + Convert.ToDateTime(formatDateForSQL(txtReceiveInKolin.Text)).Month.ToString();
                if (Convert.ToDateTime(formatDateForSQL(txtReceiveInKolin.Text)).Month >= 10)
                { this.txtReceivedInPiscalYear.Text = "FY" + (Convert.ToDateTime(formatDateForSQL(txtReceiveInKolin.Text)).Year + 1).ToString().Substring(2, 2); }
                else
                { txtReceivedInPiscalYear.Text = "FY" + Convert.ToDateTime(formatDateForSQL(txtReceiveInKolin.Text)).Year.ToString().Substring(2, 2); }

                txtControlOpenOrClose.Text = "CLOSED";
            }
            else
            {
                txtReceivedInCurrentWeek.Text = "";
                txtReceivedInMonth.Text = "";
                txtReceivedInPiscalYear.Text = "";
                rfc.OpenOrClosed = "";
            }

            rfc.ReceivedInCurrentWeek = txtReceivedInCurrentWeek.Text;
            rfc.ReceivedInMonthIncome = this.txtReceivedInMonth.Text;
            rfc.FiscalYearReceived = this.txtReceivedInPiscalYear.Text;

            // -kim

            if (ddControlFailureDescription.SelectedValue != "-Select-")
            { rfc.FailureDescription = ddControlFailureDescription.SelectedValue; }
            else
            { rfc.FailureDescription = ""; }

            if (ddControlOrigin.SelectedValue != "-Select-")
            { rfc.Origin = ddControlOrigin.SelectedValue; }
            else
            { rfc.Origin = ""; }

            if (ddControlCauseOrGuilty.SelectedValue != "-Select-")
            { rfc.CauseOrGuilty = ddControlCauseOrGuilty.SelectedValue; }
            else
            { rfc.CauseOrGuilty = ""; }

            if (ddControlCreditDecision.SelectedValue != "-Select-")
            { rfc.CreditDecision = ddControlCreditDecision.SelectedValue; }
            else
            { rfc.CreditDecision = ""; }

            rfc.ReportText = txtControlReportText.Text;
            rfc.SupplierAnalysis = txtControlSupplierAnalysis.Text;

            if (ddControlPartReturned.SelectedValue != "-Select-")
            { rfc.PartReturned = ddControlPartReturned.SelectedValue; }
            else
            { rfc.PartReturned = ""; }

            rfc.Note = txtControlNote.Text;


            rfc.OpenOrClosed = txtControlOpenOrClose.Text;
            rfc.OracleNumber = txtOracleNumber.Text; //NEW
            //rfc.OracleNumber = getOracleNumber();

            if (checkPCNNumber() == false)
            {
                return;
            }


            if (this.hdnCurrentProductType.Value == optProductTypeControl.SelectedValue)
            {
                if (optProductTypeControl.SelectedValue == "Accessories")
                    mrfc.updateReturnFormAccessories(rfc);
                else
                    mrfc.updateReturnFormControl(rfc);
            }
            else
            {
                rfc.RFHeaderID = int.Parse(hdnRFHeaderID.Value);
                rfc.CustomerNumber = hdnCustomerNumber.Value;
                rfc.Customer = hdnCustomerNumber.Value + " - " + lblControlCustomerName.Text;
                if (optProductTypeControl.SelectedValue == "Accessories")
                {
                    
                    rfc.ProductType = "ACCESSORIES";
                    Maintenance.ReturnFormControl.deleteReturnFormControl(rfc.RFDetailID);
                    mrfc.addReturnFormAccessories(rfc); 
                    bindDataAccessories();

                }
                else 
                {

                    rfc.ProductType = "CONTROL";
                    Maintenance.ReturnFormAccessories.deleteReturnFormAccessories(rfc.RFDetailID);
                    mrfc.addReturnFormControl(rfc);
                    bindDataControl();

                }
            }

          
            
            //SEND EMAIL TO CUSTOMER EMAIL IF THE RECEIVEDDATE,CLOSEDDATE HAS CHANGES

            string returnFormTemp = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("./") + "\\resources\\mail_template\\returnformcontrol.html");
            string li = "";

            if (txtReceiveInKolin.Text != Session["curReceiveDate"].ToString())
            {
                li += "<li style=\"font-size:10px;\"><b>Receiption Date: </b> " + txtReceiveInKolin.Text + "</li>";
            }

            if (txtControlClosedInKolin.Text != Session["curClosedDate"].ToString())
            {
                li += "<li style=\"font-size:10px;\"><b>Analyze Date: </b> " + txtControlClosedInKolin.Text + "</li>";
            }

            if (ddControlCreditDecision.SelectedValue != Session["curCreditNote"].ToString())
            {
                li += "<li style=\"font-size:10px;\"><b>Credit Note: </b> " + ddControlCreditDecision.SelectedValue + "</li>";
            }


            if (li != "")
            {
                MailMaster mail = new MailMaster();
                Maintenance.ReturnFormControl returnFormControl1 = new Maintenance.ReturnFormControl();
                returnFormControl1.getContactPersonsEmailByHeaderID(curRFHeader);
                string emailAddress = returnFormControl1.getContactPersonsEmailByHeaderID(curRFHeader);
                if (emailAddress == "")
                {
                    int customerNumber = int.Parse(returnFormControl1.getCustomerNumberByDetailID(rfc.RFDetailID));
                    Customers customer = new Customers();
                    DataTable cust = customer.getCustomersByCustomerNumberDT(customerNumber);
                    emailAddress = (emailAddress != "") ? emailAddress : cust.Rows[0]["CustomerEmail"].ToString();
                }

                returnFormTemp = returnFormTemp.Replace("#UPDATELIST#", "<ul>" + li + "</ul>");
                mail.sendMail(emailAddress, returnFormTemp, "Return Status Update of Portal Reference No. " + lblPortalReference.Text);
            }

            disableControl();
          
        }
    }
    protected void disableControl()
    {
        cmdEditControl.Text = "Edit";
        optProductTypeControl.Enabled = false;
        ddLocation.Enabled = false;
        ddControlClaimLists.Enabled = false;
        txtOracleNumber.Enabled = false;
        //enableOracleNumber(false);
        txtControlClaimDescription.Enabled = false;
        txtControlCustomerClaimReference.Enabled = false;
        imgControlClaimReference.Visible = false;
        txtControlPCNNumber.Enabled = false;
        txtControlProduct.Enabled = false;
        btnCheckControlPCNNumber.Visible = false;
        txtControlDataCode.Enabled = false;
        txtControlQty.Enabled = false;
        txtControlComponentReference.Enabled = false;
        imgComponentReference.Visible = false;
        optControlTypeOfReturn.Enabled = false;
        optControlApplication.Enabled = false;
        optControlRefrigerant.Enabled = false;
        txtControlRefrigerant.Enabled = false;
        txtControlWorkingPointTO.Enabled = false;
        txtControlWorkingPointTC.Enabled = false;
        txtControlWorkingPointSuperHeat.Enabled = false;
        txtControlWorkingPointSuperHeat.Enabled = false;
        optControlRequest1.Enabled = false;
        txtReceiveInKolin.Enabled = false;

        txtControlRequestSpecificTest.Enabled = false;
        optControlConfiguration.Enabled = false;
        txtControlDateOfFailure.Enabled = false;
        txtControlRunningHours.Enabled = false;
        txtControlComments.Enabled = false;
        //ddControlFailureDescription.Enabled = false;
        ddControlOrigin.Enabled = false;
        ddControlCauseOrGuilty.Enabled = false;
        //ddControlCreditDecision.Enabled = false;
        txtControlReportText.Enabled = false;
        txtControlSupplierAnalysis.Enabled = false;
        ddControlPartReturned.Enabled = false;
        txtControlNote.Enabled = false;
        if (sm.Decrypt(Session["UserType"].ToString()) == "CUSTOMER QUALITY")
        {
            txtControlClosedInKolin.Enabled = false;
            imgControlClosedInKolin.Visible = false;
            ddControlFailureDescription.Enabled = false;
            ddControlCreditDecision.Enabled = false;
        }
    }



    protected void cmdEditCompressor_Click(object sender, EventArgs e)
    {
        if (cmdEditCompressor.Text == "Edit")
        {
            cmdEditCompressor.Text = "Save";  
            txtOracleNumberComp.Enabled = true; 
            ddCompressorClaimLists.Enabled = true;
            ddLocationComp.Enabled = true;
            txtCompressorClaimDescription.Enabled = true;
            txtCompressorCustomerClaimRef.Enabled = true;
            txtCompressorSerialNumber.Enabled = true;
            btnCheckCompressorSerialNumber.Visible = true;
            txtCompressorReference.Enabled = true;
            optCompressorTypeOfReturn.Enabled = true;
            optCompressorApplication.Enabled = true;
            optCompressorRefrigerant.Enabled = true;
            txtCompressorRefrigerant.Enabled = true;
            txtCompressorWorkingPointTo.Enabled = true;
            txtCompressorWorkingPointTc.Enabled = true;
            txtCompressorSuperheat.Enabled = true;
            txtCompressorSubCooling.Enabled = true;
            optCompressorRequest1.Enabled = true;
            pnlCompressorRequestSpecificTest.Enabled = true;
            optCompressorConfiguration.Enabled = true;
            txtCompressorDateOfFailure.Enabled = true;
            txtCompressorRunningHours.Enabled = true;
            txtCompressorRemarks.Disabled = false;
        }
        else
        {
            if (txtOracleNumberComp.Text.Length != 0)
            {
                long x = 0;
                //if (!checkOracleNumber(getOracleNumber()))
                if (!long.TryParse(txtOracleNumberComp.Text, out x))
                {
                    showMessage(true, "Return Number must be numeric values");
                    return;
                }
                //if (!checkOracleNumber(getOracleNumber()))
                if (!checkOracleNumber(txtOracleNumberComp.Text))
                {
                    showMessage(true, "Return Number must be 12 digits.");
                    return;
                }
                
            }
            
            Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
            Model.ReturnFormDetail rfd = new Model.ReturnFormDetail();
            //DataTable dt = new DataTable();
            //rfd = mrfd.getReturnFormDetail(int.Parse(gvMyReturn.SelectedRow.Cells[1].Text));
            rfd.RFDetailID = int.Parse(rfhid.Value);

            rfd.Claim = ddCompressorClaimLists.SelectedValue;

            rfd.LocationID = (ddLocationComp.SelectedValue == null) || string.IsNullOrEmpty(ddLocationComp.SelectedValue.ToString()) ? "0" : ddLocationComp.SelectedValue;  
            rfd.ClaimDescription = txtCompressorClaimDescription.Text;
            rfd.CustomerClaimReference = txtCompressorCustomerClaimRef.Text;
            rfd.SerialNumber = txtCompressorSerialNumber.Text;
            rfd.ModelProduct = txtCompressorModel.Text;
            rfd.ItemNumber = hdnItemNumber.Value;
            rfd.DateOfClaim = formatDateForSQL(txtCompressorCreationDate.Text);
            rfd.CompressorComponentReference = txtCompressorReference.Text;
            rfd.TypeOfReturn = getReturnTypeValue(optCompressorTypeOfReturn.SelectedValue);
            rfd.Application = optCompressorApplication.SelectedValue;
            rfd.Refrigerant = optCompressorRefrigerant.SelectedValue;
            rfd.OracleNumber = txtOracleNumberComp.Text;

            if (ddCompressorClaimLists.SelectedValue == "External leakage" ||
               ddCompressorClaimLists.SelectedValue == "Cooling/Heating capacity too low" ||
               ddCompressorClaimLists.SelectedValue == "High Power Input" ||
               ddCompressorClaimLists.SelectedValue == "High Amps" ||
               ddCompressorClaimLists.SelectedValue == "Low COP/EER")
            {
                rfd.WorkingPointTo = txtCompressorWorkingPointTo.Text;
                rfd.WorkingPointTc = txtCompressorWorkingPointTc.Text;
                rfd.WorkingPointSuperHeat = txtCompressorSuperheat.Text;
                rfd.WorkingPointSubcooling = txtCompressorSubCooling.Text;
            }
            else
            {
                rfd.WorkingPointTo = "";
                rfd.WorkingPointTc = "";
                rfd.WorkingPointSuperHeat = "";
                rfd.WorkingPointSubcooling = "";
            }
            /*
            if (ddCompressorClaimLists.SelectedValue == "Other")
                rfd.ClaimDescription = txtCompressorClaimDescription.Text;
            else
                rfd.ClaimDescription = "";
            */
            if (optCompressorRefrigerant.SelectedValue == "Other")
                rfd.OtherRefrigerant = txtCompressorRefrigerant.Text;
            else
                rfd.OtherRefrigerant = "";

            //[START] [jpbersonda] [5/25/2017]
            rfd.Request = optCompressorRequest1.SelectedValue;
            if (optCompressorRequest1.SelectedValue == "Specific Test" || optCompressorRequest1.SelectedValue == "SPECIFIC TEST"
                || optCompressorRequest1.SelectedValue == "Specific Request" || optCompressorRequest1.SelectedValue == "SPECIFIC REQUEST")
            { rfd.RequestSpecificTestRemarks = txtCompressorRequestSpecificTest.Text; }
            else
            { rfd.RequestSpecificTestRemarks = ""; }
            //[END] [jpbersonda] [5/25/2017]

            rfd.ItemNumber = hdnItemNumber.Value;

            rfd.Configuration = optCompressorConfiguration.SelectedValue;
            rfd.DateOfFailure = formatDateForSQL(txtCompressorDateOfFailure.Text);
            rfd.RunningHours = (txtCompressorRunningHours.Text == "") ? "0" : txtCompressorRunningHours.Text; 
            rfd.Remarks = txtCompressorRemarks.Value;

            if (checkSerialNumber() == false)
            {
                return;
            }

            mrfd.updateReturnFormDetail(rfd);
            bindDataCompressor();
            disableCompressor();
        }
    }
    protected void btnCheckCompressorSerialNumber_Click(object sender, EventArgs e)
    {
        checkSerialNumber();
    }
    public bool checkSerialNumber()
    {
        try
        {
            Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
            Model.ReturnFormDetail rfd = new Model.ReturnFormDetail();
            Utility u = new Utility();
            DataTable dt = mrfd.getModelBySerial(txtCompressorSerialNumber.Text);
            foreach (DataRow dr in dt.Rows)
            {
                rfd.ModelProduct = u.extractStringValue(dr["DESCP"], Utility.DataType.STRING);
                rfd.ItemNumber = u.extractStringValue(dr["ITNBR"], Utility.DataType.STRING);
            }
            if (String.IsNullOrEmpty(rfd.ModelProduct))
            {
                txtCompressorModel.Text = "";
                hdnItemNumber.Value = "";
                showMessage(true, "Please double check the serial number. See our help for detailed information on serial numbers. If compressor has been produced in 2007 of before - serial number starting with 07 or lower - this compressor is too old for standard analysis. Please contact your Emerson Climate Technologies local representative to give some background of this return.");
                return false;
            }
            else
            {
                txtCompressorModel.Text = rfd.ModelProduct;
                hdnItemNumber.Value = rfd.ItemNumber;
                showMessage(false, "");
                return true;
            }
        }
        catch (Exception ex)
        {
            showMessage(true, ex.Message);
            return false;
        }
    }
    public bool checkPCNNumber()
    {
        try
        {
            Maintenance.ReturnFormControl mrfc = new Maintenance.ReturnFormControl();
            Model.ReturnFormControl rfc = new Model.ReturnFormControl();
            Utility u = new Utility();
            initialization();
            DataTable dt = mrfc.getModelByPCNNumber(txtControlPCNNumber.Text);
            foreach (DataRow dr in dt.Rows)
            {
                rfc.PCNNumber = u.extractStringValue(dr["PCNNumber"], Utility.DataType.STRING);
                rfc.ModelProduct = u.extractStringValue(dr["Description1"], Utility.DataType.STRING);
                rfc.ProductGroup = u.extractStringValue(dr["ItemType"], Utility.DataType.STRING);
                rfc.Line = u.extractStringValue(dr["Line"], Utility.DataType.STRING);
                //rfd.ItemNumber = u.extractStringValue(dr["ITNBR"], Utility.DataType.STRING);
            }
            if (String.IsNullOrEmpty(rfc.ModelProduct))
            {
                txtControlProduct.SelectedValue = "";
                txtControlProductGroup.Text = "";
                txtControlLine.Text = "";
                showMessage(true, "Please double check the serial number. See our help for detailed information on serial numbers. If compressor has been produced in 2007 of before - serial number starting with 07 or lower - this compressor is too old for standard analysis. Please contact your Emerson Climate Technologies local representative to give some background of this return.");
                //hdnItemNumber.Value = "";
                return false;
            }
            else
            {
                showMessage(false, "");
                txtControlProduct.SelectedValue = rfc.PCNNumber;
                txtControlProductGroup.Text = rfc.ProductGroup;
                txtControlLine.Text = rfc.Line;
                return true;
                //hdnItemNumber.Value = rfd.ItemNumber;
            }
        }
        catch (Exception ex)
        {
            showMessage(true, ex.Message);
            return false;
        }
    }
    protected string getReturnTypeValue(string _value)
    {
        string returnedValue;
        switch (_value)
        {
            case "Line Return":
                {
                    returnedValue = "LR";
                    break;
                }
            case "Field Return":
                {
                    returnedValue = "FR";
                    break;
                }
            case "Buy Back":
                {
                    returnedValue = "BB";
                    break;
                }
            case "Wrong Delivery":
                {
                    returnedValue = "WD";
                    break;
                }
            case "Transport Damage":
                {
                    returnedValue = "TD";
                    break;
                }
            default:
                {
                    returnedValue = "SA";
                    break;
                }
        }
        return returnedValue;
    }
    private void showMessage(bool showHide, string message)
    {
        LabelMessage.Text = message;
        LabelMessage.Visible = showHide;
    }
    public string formatDateFromSQL(string date)
    {
        if (date == "") { return ""; }
        return DateTime.ParseExact(date, "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
    }
    // FUNCTION FOR ORACLE NUMBER
    public Boolean checkOracleNumber(String str){
        return (str.Length == 12) ? true : false;
    }

    //public string getOracleNumber() 
    //{
    //    string str = "";
    //    str += txtOracle1.Text;
    //    str += txtOracle2.Text;
    //    str += txtOracle3.Text;
    //    str += txtOracle4.Text;
    //    str += txtOracle5.Text;
    //    str += txtOracle6.Text;
    //    str += txtOracle7.Text;
    //    str += txtOracle8.Text;
    //    str += txtOracle9.Text;
    //    str += txtOracle10.Text;
    //    str += txtOracle11.Text;
    //    str += txtOracle12.Text;

    //    return str; 
    //}

    public void setOracleNumber(string str){
        if (str.Length == 0) { return; }
        for (int i = 1; i <= 12; i++) {
            Control ctr = FindControl("txtOracle" + i);
            TextBox txt = (TextBox)ctr;
            txt.Text = str.Substring((i - 1), 1); 
        }
    }

    public void clearOracleNumber()
    {
         
        for (int i = 1; i <= 12; i++)
        {
            Control ctr = FindControl("txtOracle" + i);
            TextBox txt = (TextBox)ctr;
            txt.Text = "";
        }
    }

    public void enableOracleNumber(Boolean toggle)
    {
        for (int i = 1; i <= 12; i++)
        {
            Control ctr = FindControl("txtOracle" + i);
            TextBox txt = (TextBox)ctr;
            txt.Enabled = toggle;
        }
        //clearOracle.Visible = toggle;
    }
    //

    public DataRow getUserAccess() {
        DataTable dt = (DataTable)HttpContext.Current.Session["UserAccess"];
        return dt.Rows[0];
    }

    protected void gvMyReturnControl_SelectedIndexChanged(object sender, EventArgs e)
    { 
        reqControlRequestSpecificTest.Enabled = false;

        hdnCurrentProductType.Value = ddProductType.SelectedValue;
        lblDescription.InnerText = ddProductType.SelectedValue;
        lblReference.InnerText = ddProductType.SelectedValue;
        optProductTypeControl.ClearSelection();
        optProductTypeControl.Items[ddProductType.SelectedIndex].Selected = true;
        this.ddControlFailureDescription.SelectedIndex = 0;
        this.ddControlCreditDecision.SelectedIndex = 0;
        this.ddControlOrigin.SelectedIndex = 0;
        this.ddControlCauseOrGuilty.SelectedIndex = 0;
        this.ddControlPartReturned.SelectedIndex = 0;
        this.optProductTypeControl.Enabled = false;
        this.hdControlID.Value = gvMyReturnControl.SelectedRow.Cells[1].Text;
        disableControl();


        cmdEditControl.Visible = !(Boolean)getUserAccess()["MyReturnsView"];

        //if (sm.Decrypt(Session["UserType"].ToString()) == "CSO")
        //{
        //    cmdEditControl.Visible = false;
        //}
        if (sm.Decrypt(Session["UserType"].ToString()) == "CUSTOMER QUALITY")
        {
            pnlControlPCN.Visible = true;
        }

        dynamic mrfc = toggleControlOrAccessoriesMaintenance(ddProductType.SelectedValue);
        dynamic rfc = toggleControlOrAccessoriesModel(ddProductType.SelectedValue);
           
       

        if (gvMyReturnControl.SelectedRow.Cells[1].Text == "&nbsp;" || String.IsNullOrEmpty(gvMyReturnControl.SelectedRow.Cells[1].Text))
        {
            return;
        }
        rowIDSelected.Value = gvMyReturnControl.SelectedRow.Cells[1].Text;
        if (ddProductType.SelectedValue == "Accessories")
        {
            rfc = mrfc.getReturnFormAccessories(gvMyReturnControl.SelectedRow.Cells[1].Text);
            lblPCNNumber.Text = "Item Number: ";
            lblProduct.Text = "Item Description: ";
        
        }
           
        else
            rfc = mrfc.getReturnFormControl(gvMyReturnControl.SelectedRow.Cells[1].Text);



        initialization();
        this.ddProductType.Visible = false;
        this.pnlProduct.Visible = false;
        this.pnlCompressorCustomerList.Visible = false;
        this.pnlControlCustomerList.Visible = false;
        this.pnlCompressorDetail.Visible = false;
        this.pnlControlDetail.Visible = true;
        //clearOracleNumber();
        lblAttachment.Visible = false;
        aFileAttachment.Visible = false;
        if (rfc.FilePath != null && rfc.FilePath != "")
        {  
            string[] filename = rfc.FilePath.Split('|');
            aFileAttachment.Visible = true;
            aFileAttachment.NavigateUrl = "UploadedFiles\\" + ((filename.Length > 1) ? filename[0].ToString() : filename[0].ToString().Replace("UPLOADEDFILES\\", ""));
            aFileAttachment.Text = ((filename.Length > 1) ? filename[1].ToString() : rfc.FilePath.ToString().Replace("UPLOADEDFILES\\", ""));
        }
        else 
            lblAttachment.Visible = true;
        
            
       


        //for (int i = 0; i < this.optProductTypeControl.Items.Count; i++)
        //{
        //    if (rfc.ProductType == this.optProductTypeControl.Items[i].Value.ToUpper())
        //    {
        //        this.optProductTypeControl.SelectedIndex = i;
        //        break;
        //    }
        //}
        this.hdnRFDetailID.Value = gvMyReturnControl.SelectedRow.Cells[1].Text;
        hdnRFHeaderID.Value = rfc.RFHeaderID.ToString();
        hdnCustomerNumber.Value = rfc.CustomerNumber.ToString();
        Maintenance.Customers customersMaintenance = new Maintenance.Customers();
        // Modified the Query to add the Condition for Location- MF 20160331
        Model.Customers customers = (Model.Customers)customersMaintenance.getCustomersByCustomerNumberLocation(int.Parse(rfc.CustomerNumber.ToString()), rfc.LocationID)[0];
        //Model.Customers customers = (Model.Customers)customersMaintenance.getCustomersByCustomerNumber(int.Parse(rfc.CustomerNumber.ToString()))[0];

        int userId = (Session["UserID"] == null) ? 0 : int.Parse(sm.Decrypt(Session["UserID"].ToString()));
        //bool isCustomer = (sm.Decrypt(Session["UserType"].ToString()) == "CUSTOMER") ? true : false;
        string usertype = sm.Decrypt(Session["UserType"].ToString());

        Maintenance.Customers c = new Maintenance.Customers();
        ddLocation.DataSource = null;
        ddLocation.DataSource = c.getLocationByCustomerNumberDT(rfc.CustomerNumber,userId,usertype);
        ddLocation.DataTextField = "LocationName";
        ddLocation.DataValueField = "LocationID";
        ddLocation.DataBind();
        if (rfc.LocationID != "0")
            ddLocation.SelectedValue = rfc.LocationID.ToString();

        this.lblControlCustomerName.Text = customers.CustomerName;
        this.hfCustomerEmail.Value = customers.CustomerEmail;


        Model.ReturnFormHeader rfh = new Model.ReturnFormHeader();


        //CUSTOMER LOCATION FIELDS 
        hfCustomerName.Value = rfc.CustomerName;
        hfCity.Value = rfc.City;
        hfState.Value = rfc.State;
        hfZipCode.Value = rfc.ZipCode;
        hfCountry.Value = rfc.Country;
        hfStreet.Value = "";//rfc.Street;
        //


        this.lblPortalReference.Text = rfc.PortalReference; //mrfc.getSavedReturnFormHeaderByID(rfc.RFHeaderID).Rows[0]["ReferenceNumber"].ToString();
        this.txtOracleNumber.Text = rfc.OracleNumber; // NEW
        //setOracleNumber(rfc.OracleNumber);
        this.rfhid.Value = rfc.RFHeaderID.ToString();

        //Selected Value for Claim Lists

        for (int i = 0; i < this.ddControlClaimLists.Items.Count; i++)
        {
            if (rfc.Claim == this.ddControlClaimLists.Items[i].Value.ToUpper())
            {
                this.ddControlClaimLists.SelectedIndex = i;
                break;
            }
        }

        //ControlClaimDescription will show up if the Value for Claim is OTHER
        if (rfc.Claim == "OTHER" || rfc.Claim == "Other")
        {
            pnlControlClaimDescription.Visible = true;
            txtControlClaimDescription.Visible = true;
            txtControlClaimDescription.Text = rfc.ClaimDescription;
        }
        else
        {
            pnlControlClaimDescription.Visible = false;
            txtControlClaimDescription.Visible = false;
            txtControlClaimDescription.Visible = false;
            txtControlClaimDescription.Text = "";
        }


        this.txtControlWarranty.Text = getWarranty(rfc.DataCode);
        this.txtControlCustomerClaimReference.Text = rfc.ControlCustomerClaimReference;
        this.txtControlPCNNumber.Text = rfc.PCNNumber;

        try
        {
            this.txtControlProduct.SelectedValue = rfc.PCNNumber;
        }
        catch
        {
            this.txtControlProduct.SelectedValue = "";
        }
        this.txtControlProductGroup.Text = rfc.ProductGroup;
        this.txtControlLine.Text = rfc.Line;
        this.txtControlDataCode.Text = rfc.DataCode;
        this.txtControlQty.Text = rfc.Quantity;
        this.txtControlDateOfClaim.Text = rfc.DateOfClaim; ;
        this.txtControlComponentReference.Text = rfc.ControlComponentReference;
        reqControlSpecifyRefrigerant.Enabled = false;
        this.optControlTypeOfReturn.SelectedValue = getSelectedReturnTypeValue(rfc.TypeOfReturn);
        //Selected Value for Application
        for (int i = 0; i < this.optControlApplication.Items.Count; i++)
        {
            if (rfc.Application == this.optControlApplication.Items[i].Value.ToUpper())
            {
                this.optControlApplication.SelectedIndex = i;
                break;
            }
        }

        //Selected Value for Refrigerant
        for (int i = 0; i < this.optControlRefrigerant.Items.Count; i++)
        {
            if (rfc.Refrigerant == this.optControlRefrigerant.Items[i].Value.ToUpper())
            {
                this.optControlRefrigerant.SelectedIndex = i;
                break;
            }
        }
        //This will show up if the Value for Refrigerant is OTHER
        if (rfc.Refrigerant == "OTHER")
        {
            this.pnlControlRefrigerant.Visible = true;
            this.txtControlRefrigerant.Text = rfc.OtherRefrigerant;
        }
        else
        {
            this.pnlControlRefrigerant.Visible = false;
            this.txtControlRefrigerant.Text = "";
        }

        //[START] [jpbersonda] [5/25/2017]
        //Selected Value for Control Request
        for (int i = 0; i < this.optControlRequest1.Items.Count; i++)
        {
            //For Old data - Warranty Analysis is equal to Warranty or Analysis
            if (rfc.Request == "WARRANTY" || rfc.Request == "ANALYSIS")
            {
                this.optControlRequest1.SelectedIndex = 0;
                break;
            }

            //For old data - Specific Test is equal to Specific Request
            if (rfc.Request == "SPECIFIC TEST")
            {
                this.optControlRequest1.SelectedIndex = 1;
                break;
            }

            if (rfc.Request == this.optControlRequest1.Items[i].Value.ToUpper())
            {
                this.optControlRequest1.SelectedIndex = i;
                break;
            }
        }
        //[END]

        //This will show up if the Value for Request is SPECIFIC TEST
        if (rfc.Request == "SPECIFIC TEST")
        {
            this.pnlControlRequestSpecificTest.Visible = true;
            this.txtControlRequestSpecificTest.Visible = true;
            this.txtControlRequestSpecificTest.Text = rfc.RequestSpecificTestRemarks;
        }
        else
        {
            this.pnlControlRequestSpecificTest.Visible = false;
            this.txtControlRequestSpecificTest.Visible = false;
            this.txtControlRequestSpecificTest.Text = "";
        }

        for (int i = 0; i < this.optControlConfiguration.Items.Count; i++)
        {
            if (rfc.Configuration == this.optControlConfiguration.Items[i].Value.ToUpper())
            {
                this.optControlConfiguration.SelectedIndex = i;
                break;
            }
        }

        this.txtControlDateOfFailure.Text = rfc.DateOfFailure;
        this.txtControlRunningHours.Text = rfc.RunningHours;
        this.txtControlComments.Text = rfc.Remarks;

        //Selected Value for Received in Kolin
        if (rfc.ReceivedInKolin != "")
        {
            this.txtReceiveInKolin.Text = rfc.ReceivedInKolin;
            this.txtReceivedInCurrentWeek.Text = rfc.ReceivedInCurrentWeek;
            this.txtReceivedInMonth.Text = rfc.ReceivedInMonthIncome;
            this.txtReceivedInPiscalYear.Text = rfc.FiscalYearReceived;
        }
        else
        {
            this.txtReceiveInKolin.Text = "";
            this.txtReceivedInCurrentWeek.Text = "";
            this.txtReceivedInMonth.Text = "";
            this.txtReceivedInPiscalYear.Text = "";
        }


        //Selected Value for Closed in Kolin
        if (rfc.ClosedInKolin != "")
        {
            this.txtControlClosedInKolin.Text = rfc.ClosedInKolin;
            this.txtControlClosedInCureentWeek.Text = rfc.ClosedInCurrentWeek;
            this.txtControlClosedInMonth.Text = rfc.ClosedInMonthIncome;
            this.txtControlClosedInFY.Text = rfc.FiscalYearClosed;
        }
        else
        {
            this.txtControlClosedInKolin.Text = "";
            this.txtControlClosedInCureentWeek.Text = "";
            this.txtControlClosedInMonth.Text = "";
            this.txtControlClosedInFY.Text = "";
        }

        //Selected Value for Failure Description
        for (int i = 0; i < this.ddControlFailureDescription.Items.Count; i++)
        {
            if (rfc.FailureDescription == this.ddControlFailureDescription.Items[i].Value.ToUpper())
            {
                this.ddControlFailureDescription.SelectedIndex = i;
                break;
            }
        }

        //Selected Value for Origin
        for (int i = 0; i < this.ddControlOrigin.Items.Count; i++)
        {
            if (rfc.Origin == this.ddControlOrigin.Items[i].Value.ToUpper())
            {
                this.ddControlOrigin.SelectedIndex = i;
                break;
            }
        }

        //Selected Value for Cause / Guilty
        for (int i = 0; i < this.ddControlCauseOrGuilty.Items.Count; i++)
        {
            if (rfc.CauseOrGuilty == this.ddControlCauseOrGuilty.Items[i].Value.ToUpper())
            {
                this.ddControlCauseOrGuilty.SelectedIndex = i;
                break;
            }
        }

        //Selected Value for Credit decision
        for (int i = 0; i < this.ddControlCreditDecision.Items.Count; i++)
        {
            if (rfc.CreditDecision == this.ddControlCreditDecision.Items[i].Value.ToUpper())
            {
                this.ddControlCreditDecision.SelectedIndex = i;
                break;
            }
        }

        txtControlReportText.Text = rfc.ReportText;
        txtControlSupplierAnalysis.Text = rfc.SupplierAnalysis;

        //Selected Value for Part Returned
        for (int i = 0; i < this.ddControlPartReturned.Items.Count; i++)
        {
            if (rfc.PartReturned == this.ddControlPartReturned.Items[i].Value.ToUpper())
            {
                this.ddControlPartReturned.SelectedIndex = i;
                break;
            }
        }



        this.txtControlNote.Text = rfc.Note;
        this.txtControlOpenOrClose.Text = rfc.OpenOrClosed;
        this.txtNewControlQty.Text = "";



        this.pnlReturn.Visible = false;


    }
    protected void btnCheckControlPCNNumber_Click(object sender, EventArgs e)
    {
        if (txtControlPCNNumber.Text == "")
        {
            showMessage(true, "Serial Number is empty");
        }
        else
        { checkPCNNumber(); }
    }

    private void initialization()
    {
        showMessage(false, "");
        txtControlDataCode.Attributes.Add("onblur", "calculateWarranty()");
        //txtControlWarranty.Text = hdnControlWarranty.Value;
        optCompressorRequest1.Items[1].Attributes.Add("onclick", "selectedRequestSpecificTest()");
        //string tooltip = "Line Return : if you are a OEM and that the compressor fialed during one of your assembly test\n";
        //tooltip += "Field Returns : compressor failed at final customer or you are a Wholsalers\n";
        //tooltip += "Lab/sample Test : compressor is retruned after Engineering test or developement test";
        //Image5.ToolTip = tooltip;

    }
    protected void ddControlClaimLists_SelectedIndexChanged(object sender, EventArgs e)
    {
        dynamic mrfc = toggleControlOrAccessoriesMaintenance(optProductTypeControl.SelectedValue);
        dynamic rfc = toggleControlOrAccessoriesModel(optProductTypeControl.SelectedValue);

        if (optProductTypeControl.SelectedValue == "Accessories")
            rfc = mrfc.getReturnFormAccessoroes(gvMyReturnControl.SelectedRow.Cells[1].Text);
        else
            rfc = mrfc.getReturnFormControl(gvMyReturnControl.SelectedRow.Cells[1].Text);


        if (ddControlClaimLists.SelectedValue == "Other" || ddControlClaimLists.SelectedValue == "OTHER")
        {
            pnlControlClaimDescription.Visible = true;
            txtControlClaimDescription.Text = rfc.ClaimDescription;
        }
        else
        {
            pnlControlClaimDescription.Visible = false;
            txtControlClaimDescription.Text = "";
        }
    }
    protected void optControlRefrigerant_SelectedIndexChanged(object sender, EventArgs e)
    {
        dynamic mrfc = toggleControlOrAccessoriesMaintenance(optProductTypeControl.SelectedValue);
        dynamic rfc = toggleControlOrAccessoriesModel(optProductTypeControl.SelectedValue);

        if (optProductTypeControl.SelectedValue == "Accessories")
            rfc = mrfc.getReturnFormAccessoroes(gvMyReturnControl.SelectedRow.Cells[1].Text);
        else
            rfc = mrfc.getReturnFormControl(gvMyReturnControl.SelectedRow.Cells[1].Text);

        if (optControlRefrigerant.SelectedIndex == 5)
        {

            pnlControlRefrigerant.Visible = true;
            txtControlRefrigerant.Text = rfc.OtherRefrigerant;
            reqControlSpecifyRefrigerant.Enabled = true;
        }
        else
        {
            pnlControlRefrigerant.Visible = false;
            txtControlRefrigerant.Text = "";
            reqControlSpecifyRefrigerant.Enabled = false;
        }
    }

    //[START] [jpbersonda] [5/25/2017] [Changes in Request radio button values]
    protected void optControlRequest1_SelectedIndexChanged(object sender, EventArgs e)
    {
        dynamic mrfc = toggleControlOrAccessoriesMaintenance(optProductTypeControl.SelectedValue);
        dynamic rfc = toggleControlOrAccessoriesModel(optProductTypeControl.SelectedValue);

        if (optProductTypeControl.SelectedValue == "Accessories")
            rfc = mrfc.getReturnFormAccessoroes(gvMyReturnControl.SelectedRow.Cells[1].Text);
        else
            rfc = mrfc.getReturnFormControl(gvMyReturnControl.SelectedRow.Cells[1].Text);

        if (optControlRequest1.SelectedIndex == 1)
        {
            this.pnlControlRequestSpecificTest.Visible = true;
            txtControlRequestSpecificTest.Visible = true;
            txtControlRequestSpecificTest.Text = rfc.RequestSpecificTestRemarks;
            optControlRequest1.Items[1].Attributes.Remove("onclick");
            reqControlRequestSpecificTest.Enabled = true;
        }
        else
        {
            this.pnlControlRequestSpecificTest.Visible = false;
            txtControlRequestSpecificTest.Visible = false;
            txtControlRequestSpecificTest.Text = "";
            optControlRequest1.Items[1].Attributes.Add("onclick", "selectedRequestSpecificTest()");
            reqControlRequestSpecificTest.Enabled = false;

        }

    }
    //[END] [jpbersonda] [5/25/2017] 


    protected void ddProductType_SelectedIndexChanged(object sender, EventArgs e)
    {
        int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString()));
        //this.ddCustomerList.DataSource = null;

        if (this.ddProductType.SelectedValue == "Compressors / Drives / Units") // COMPRESSOR
        {
            ddCustomerList.Visible = true;
            ddControlCustomerList.Visible = false;
            pnlCompressorCustomerList.Visible = true;
            pnlControlCustomerList.Visible = false;
            gvMyReturn.Visible = true;
            gvMyReturnControl.Visible = false;
            pnlCompressorSearch.Visible = true;
            pnlControlSearch.Visible = false;
            pnlControlSearch2.Visible = false;
            initCustomerAccesories();
            optProductTypeControl.Items[1].Selected = true;
            pnlCompressorSearch2.Visible = true;
        }
        else 
        {
            if (this.ddProductType.SelectedValue == "Accessories")
            {
                initCustomerAccesories(); 
                bindDataAccessories();
                optProductTypeControl.Items[2].Selected = true;

            }
            else {
                initCustomerControl();
                bindDataControl();
            }


            initControlModel(); 
            ddCustomerList.Visible = false;
            ddControlCustomerList.Visible = true;
            pnlCompressorCustomerList.Visible = false;
            pnlControlCustomerList.Visible = true;
            gvMyReturn.Visible = false;
            gvMyReturnControl.Visible = true;
            pnlCompressorSearch.Visible = false;
            pnlCompressorSearch2.Visible = false;
            pnlControlSearch.Visible = true;
            pnlControlSearch2.Visible = true;

        }
       
       
    }

     

    [WebMethod]
    public static string getWarranty(string datacode)
    {


        if (datacode == null || datacode == string.Empty)
        {
            return string.Empty;
        }
        int year = int.Parse("20" + datacode.Substring(0, 2));
        int weekOfYear = int.Parse(datacode.Substring(2, 2));

        DateTime date = new DateTime(year, 1, 1);

        CalendarWeekRule cwr = CultureInfo.CurrentCulture.DateTimeFormat.CalendarWeekRule;
        DayOfWeek dow = CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;

        int daysOffset = (int)dow - (int)date.DayOfWeek;

        DateTime firstdateofcalweek = date.AddDays(daysOffset);

        int firstWeek = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(date, cwr, dow);

        if (firstWeek <= 1)
        {
            weekOfYear -= 1;
        }

        DateTime dateBaseOnYearWeek = firstdateofcalweek.AddDays(weekOfYear * 7);

        DateTime result = dateBaseOnYearWeek.AddMonths(18);
        string warrantyDate = result.ToString("dd/MM/yyyy");
        return warrantyDate;
    }


    protected void cmdBackFromCompressor_Click(object sender, EventArgs e)
    { 
        this.ddProductType.Visible = true;
        this.ddProductType.SelectedIndex = 0;
        this.pnlProduct.Visible = true;
        this.pnlCompressorCustomerList.Visible = true;
        this.pnlControlCustomerList.Visible = false;
        this.pnlCompressorDetail.Visible = false;
        this.pnlControlDetail.Visible = false;
        this.pnlReturn.Visible = true;
        this.pnlCompressorSearch.Visible = true;
        
    }
    protected void optCompressorRefrigerant_SelectedIndexChanged(object sender, EventArgs e)
    {
        Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
        Model.ReturnFormDetail rfd = new Model.ReturnFormDetail();
        rfd = mrfd.getReturnFormDetail(int.Parse(gvMyReturn.SelectedRow.Cells[1].Text));

        if (optCompressorRefrigerant.SelectedIndex == 5)
        {
            pnlCompressorRefrigerant.Visible = true;
            txtCompressorRefrigerant.Text = rfd.OtherRefrigerant;
        }
        else
        {
            pnlCompressorRefrigerant.Visible = false;
            txtCompressorRefrigerant.Text = "";
        }
    }

    //[START] [jpbersonda] [5/25/2017] [Changes in Request radio button values]
    protected void optCompressorRequest1_SelectedIndexChanged(object sender, EventArgs e)
    {
        Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
        Model.ReturnFormDetail rfd = new Model.ReturnFormDetail();
        rfd = mrfd.getReturnFormDetail(int.Parse(gvMyReturn.SelectedRow.Cells[1].Text));
        if (optCompressorRequest1.SelectedIndex == 1)
        {
            pnlCompressorRequestSpecificTest.Visible = true;
            //txtCompressorRequestSpecificTest.Visible = true;
            txtCompressorRequestSpecificTest.Text = rfd.RequestSpecificTestRemarks;
            optCompressorRequest1.Items[1].Attributes.Remove("onclick");
        }
        else
        {
            pnlCompressorRequestSpecificTest.Visible = false;
            //txtCompressorRequestSpecificTest.Visible = false;
            txtCompressorRequestSpecificTest.Text = "";
            optCompressorRequest1.Items[1].Attributes.Add("onclick", "selectedRequestSpecificTest()");
        }
    }
    //[END [jpbersonda] [5/25/2017]

    protected void cmdPrintControlReport_Click(object sender, EventArgs e)
    {
        int controlID = int.Parse(this.hdControlID.Value);
        string form = "";
        if (optProductTypeControl.SelectedValue == "Accessories")
            form = "printReturnFormAccessories";
        else
            form = "printReturnFormControl";
        //Response.Redirect("printReturnFormControl.aspx?_pageType=pdf&_controlID=" + controlID);
        Response.Write("<script>window.open('"+form+".aspx?_controlID=" + controlID + "&_pageType=pdf','_blank')</script>");
    }
    protected void cmdEmailControlReport_Click(object sender, EventArgs e)
    {

        dynamic returnFormControl = toggleControlOrAccessoriesMaintenance(optProductTypeControl.SelectedValue);
        dynamic returnFormDetailControl = toggleControlOrAccessoriesModel(optProductTypeControl.SelectedValue);

        if (optProductTypeControl.SelectedValue == "Accessories")
            returnFormDetailControl = returnFormControl.getReturnFormAccessories(this.hdControlID.Value);
        else
            returnFormDetailControl = returnFormControl.getReturnFormControl(this.hdControlID.Value);


        var reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        //Maintenance.ReturnFormControl returnFormControl = new Maintenance.ReturnFormControl();
        var result = reportProcessor.RenderReport("PDF", returnFormControl.printFormControl(this.hdControlID.Value), null);

        MailMaster mailMaster = new MailMaster();
        returnFormDetailControl = returnFormControl.getReturnForm(this.hdControlID.Value);
        string emailAddress = returnFormControl.getContactPersonsEmailByHeaderID(returnFormDetailControl.RFHeaderID);

        string x = this.ddCustomerList.SelectedValue;



        if (hfCustomerEmail.Value != "")
        {
            MemoryStream ms = new MemoryStream(result.DocumentBytes);
            ms.Position = 0;
            Attachment attachment = new Attachment(ms, "Return Form Control.pdf");

            mailMaster.sendMailWithAttachment(hfCustomerEmail.Value, emailAddress, "Portal Reference No: " + returnFormDetailControl.PortalReference, "Attached file is the Return Form Control", attachment);
            string emails = hfCustomerEmail.Value.Replace(",", "\\n");

            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert" + UniqueID,
            "alert('Email Successfully Send to: \\n " + emails + " \\nCopy to: \\n" + emailAddress + "');", true);
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert" + UniqueID,
            "alert('Email sending failed. No email address found in selected customer');", true);
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        int controlID = int.Parse(this.hdControlID.Value);

        string form = (optProductTypeControl.SelectedValue == "Accessories") ? "printReturnFormAccessories" : "printReturnFormControl";
 
        //Response.Redirect("printReturnFormControl.aspx?_pageType=pdf&_controlID=" + controlID);
        Response.Write("<script>window.open('"+ form +".aspx?_controlID=" + controlID + "&_pageType=xls','_blank')</script>");
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
     

            dynamic returnForm = toggleControlOrAccessoriesMaintenance(optProductTypeControl.SelectedValue);

            var reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();

            dynamic result;
            if (optProductTypeControl.SelectedValue == "Accessories")
                result = reportProcessor.RenderReport("PDF", returnForm.printFormAccessories(this.hdControlID.Value), null);
            else
                result = reportProcessor.RenderReport("PDF", returnForm.printFormControl(this.hdControlID.Value), null);


            MailMaster mailMaster = new MailMaster();
            string emails = hfCustomerEmail.Value.Replace(",", "\\n");
            ArrayList returnFormDetailControlArraylist = new ArrayList();
            if (optProductTypeControl.SelectedValue == "Accessories")
                returnFormDetailControlArraylist = returnForm.getReturnFormAccessoriesArray(int.Parse(rfhid.Value), int.Parse(rowIDSelected.Value));
            else
                returnFormDetailControlArraylist = returnForm.getReturnFormControlArray(int.Parse(rfhid.Value), int.Parse(rowIDSelected.Value));

            string emailAddress = returnForm.getContactPersonsEmailByHeaderID(int.Parse(rfhid.Value));

            // logic for pdf
            //MemoryStream ms = new MemoryStream(result.DocumentBytes);
            //ms.Position = 0;

            //MemoryStream ms2 = new MemoryStream(result.DocumentBytes);
            //ms2.Position = 0;
            //DataTable rfHeaderDataTable = returnFormControl.getHeaderByHeaderID(int.Parse(rfhid.Value));
            //DataRow dr = rfHeaderDataTable.Rows[0]; 
            ////returnFormControl.ftpTransfer(ms, dr["ReferenceNumber"].ToString() + ".pdf");
            //returnFormControl.ftpTransferPDF(ms2, dr["ReferenceNumber"].ToString() + ".pdf");

            // login for xml
            string xml = "";

            xml += "    <SdDataSlice>";

            xml += "    <SdCompanyHeader>";
            xml += "    <LegalName>Emerson Climate Technologies GmbH</LegalName>";
            xml += "    <TransmissionDate>#=TransmissionDate</TransmissionDate>";
            xml += "    <Source>Webportal</Source>";
            xml += "    <FromTo>ToOnBase</FromTo>";
            xml += "    </SdCompanyHeader>";

            xml += "    <DoctypeRec>";
            xml += "    <CustomerId>#=CustomerNumber</CustomerId>";
            xml += "    <CustomerName>#=CustomerName</CustomerName>";
            //CUSTOMER LOCATION FIELD
            xml += "    <LocationID>#=LocationID</LocationID>";
            xml += "    <City>#=City</City>";
            xml += "    <Street>#=Street</Street>";
            xml += "    <State>#=State</State>";
            xml += "    <ZipCode>#=ZipCode</ZipCode>";
            xml += "    <Country>#=Country</Country>";
            //
            xml += "    <DoctypeId>Returns</DoctypeId>";
            xml += "    <DocumentNumber>#=PortalReferenceNo</DocumentNumber>";
            // ADD ORACLE REF
            xml += "    <OracleNumber>#=OracleNumber</OracleNumber>";

            xml += "    <EmailSubject>#=EmailSubject</EmailSubject>";
            xml += "    </DoctypeRec>";

            xml += "    <eInvoicing>";
            xml += "    <AddLogo>NO</AddLogo>";
            xml += "    <AddTC>NO</AddTC>";
            xml += "    <AddeSignature>NO</AddeSignature>";
            xml += "    </eInvoicing>";

            emails = emails.Replace(" ", ",");
            emails = emails.Replace(";", ",");
            emails = emails.Replace("\r", "");
            emails = emails.Replace("\n", ",");
            emails = emails.Replace("\\r", "");
            emails = emails.Replace("\\n", ",");
            String[] s = emails.Split(',');
            for (int i = 0; i < s.Length; i++)
            {
                xml += "    <Dst_Mail>";
                xml += "    <EmailAddress>" + s[i] + "</EmailAddress>";
                xml += "    <BounceBack>alco.service@emerson.com</BounceBack>";
                xml += "    </Dst_Mail>";
            }

            xml += "    </SdDataSlice>";
            string CustomerNumber = "";
            string CustomerName = "";
            string City = "";
            string Street = "";
            string State = "";
            string ZipCode = "";
            string Country = "";
            string OracleNumber = "";

            string referenceNumberDetail = "";
            if (optProductTypeControl.SelectedValue == "Accessories")
            {
                foreach (Model.ReturnFormAccessories rtnFrmCtrl in returnFormDetailControlArraylist)
                {
                    CustomerNumber = rtnFrmCtrl.CustomerNumber.ToString();
                    CustomerName = rtnFrmCtrl.CustomerName.ToString();
                    City = rtnFrmCtrl.City.ToString();
                    Street = "";// rtnFrmCtrl.Street.ToString();
                    State = rtnFrmCtrl.State.ToString();
                    ZipCode = rtnFrmCtrl.ZipCode.ToString();
                    Country = rtnFrmCtrl.Country.ToString();
                    OracleNumber = rtnFrmCtrl.OracleNumber.ToString();
                    referenceNumberDetail = rtnFrmCtrl.PortalReference;
                }
            }
            else // CONTROLS
            {
                foreach (Model.ReturnFormControl rtnFrmCtrl in returnFormDetailControlArraylist)
                {
                    CustomerNumber = rtnFrmCtrl.CustomerNumber.ToString();
                    CustomerName = rtnFrmCtrl.CustomerName.ToString();
                    City = rtnFrmCtrl.City.ToString();
                    Street = "";// rtnFrmCtrl.Street.ToString();
                    State = rtnFrmCtrl.State.ToString();
                    ZipCode = rtnFrmCtrl.ZipCode.ToString();
                    Country = rtnFrmCtrl.Country.ToString();
                    OracleNumber = rtnFrmCtrl.OracleNumber.ToString();
                    referenceNumberDetail = rtnFrmCtrl.PortalReference;
                }
            }

            //RENAME PDF FILE SPECIFIC PORTAL REFERENCE NUMBER
            MemoryStream ms2 = new MemoryStream(result.DocumentBytes);
            ms2.Position = 0;
            DataTable rfHeaderDataTable = returnForm.getHeaderByHeaderID(int.Parse(rfhid.Value));
            DataRow dr = rfHeaderDataTable.Rows[0];
            //returnFormControl.ftpTransfer(ms, dr["ReferenceNumber"].ToString() + ".pdf");
            returnForm.ftpTransferPDF(ms2, referenceNumberDetail + ".pdf");

            // Append XML Body
            // CUSTOMER LOCATION FIELDS
            xml = xml.Replace("#=CustomerNumber", CustomerNumber);
            xml = xml.Replace("#=CustomerName", convertedXMLformat(CustomerName));
            xml = xml.Replace("#=LocationID", ddLocation.SelectedValue);
            xml = xml.Replace("#=City", City);
            xml = xml.Replace("#=Street", Street);
            xml = xml.Replace("#=State", State);
            xml = xml.Replace("#=ZipCode", ZipCode);
            xml = xml.Replace("#=Country", Country);
            //
            xml = xml.Replace("#=TransmissionDate", DateTime.Now.ToShortDateString());
            xml = xml.Replace("#=PortalReferenceNo", referenceNumberDetail);
            //xml = xml.Replace("#=PortalReferenceNo", dr["ReferenceNumber"].ToString());
            xml = xml.Replace("#=OracleNumber", OracleNumber);
            xml = xml.Replace("#=EmailSubject", "Portal Reference No. " + referenceNumberDetail);
            //xml = xml.Replace("#=EmailSubject", "Portal Reference No. " + dr["ReferenceNumber"].ToString());


            xml = xml.Replace("&", "&amp;");

            //returnFormControl.generateXmlOnbase(xml, dr["ReferenceNumber"].ToString() + ".xml");
            returnForm.generateXmlOnbase(xml, referenceNumberDetail + ".xml");
            returnForm.generateXml(printReportAsXML(returnFormDetailControlArraylist, rfHeaderDataTable), referenceNumberDetail + "-printformat.xml");

            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert" + UniqueID,
           "alert('Report was successfully generated.');", true);

    }

    public string convertedXMLformat(string str) {
        return System.Security.SecurityElement.Escape(str);
        
    }

    public string xmlPrintForm()
    {
        string detailsXML = "";
        detailsXML += "    <List_Section>";
        detailsXML += "    <RFDetailID>#=RFDetailID</RFDetailID>";
        detailsXML += "    <ClaimID>#=ClaimID</ClaimID>";
        // ADD ORACLE NUMBER
        detailsXML += "    <OracleNumber>#=OracleNumber</OracleNumber>";
        // CUSTOMER LOCATION FIELDS
        detailsXML += "    <CustomerID>#=CustomerNumber</CustomerID>";
        detailsXML += "    <CustomerName>#=CustomerName</CustomerName>";
        detailsXML += "    <City>#=City</City>";
        detailsXML += "    <Street>#=Street</Street>";
        detailsXML += "    <State>#=State</State>";
        detailsXML += "    <ZipCode>#=ZipCode</ZipCode>";
        detailsXML += "    <Country>#=Country</Country>";
        //
        detailsXML += "    <LocationID>#=LocationID</LocationID>";
        detailsXML += "    <RFHeaderID>#=RFHeaderID</RFHeaderID>";
        detailsXML += "    <ProductType>#=ProductType</ProductType>";
        detailsXML += "    <Claim>#=Claim</Claim>";
        detailsXML += "    <ClaimDescription>#=ClaimDescription</ClaimDescription>";
        detailsXML += "    <ControlCustomerClaimReference>#=ControlCustomerClaimReference</ControlCustomerClaimReference>";
        detailsXML += "    <PCNNumber>#=PCNNumber</PCNNumber>";
        detailsXML += "    <ModelProduct>#=ModelProduct</ModelProduct>";
        detailsXML += "    <ProductGroup>#=ProductGroup</ProductGroup>";
        detailsXML += "    <Line>#=Line</Line>";
        detailsXML += "    <DataCode>#=DataCode</DataCode>";
        detailsXML += "    <Warranty>#=Warranty</Warranty>";
        detailsXML += "    <Quantity>#=Quantity</Quantity>";
        detailsXML += "    <DateOfClaim>#=DateOfClaim</DateOfClaim>";
        detailsXML += "    <TypeOfReturn>#=TypeOfReturn</TypeOfReturn>";
        detailsXML += "    <Application>#=Application</Application>";
        detailsXML += "    <Refrigerant>#=Refrigerant</Refrigerant>";
        detailsXML += "    <OtherRefrigerant>#=OtherRefrigerant</OtherRefrigerant>";
        detailsXML += "    <WorkingPointTO>#=WorkingPointTO</WorkingPointTO>";
        detailsXML += "    <WorkingPointTC>#=WorkingPointTC</WorkingPointTC>";
        detailsXML += "    <WorkingPointSuperHeat>#=WorkingPointSuperHeat</WorkingPointSuperHeat>";
        detailsXML += "    <WorkingPointSubCooling>#=WorkingPointSubCooling</WorkingPointSubCooling>";
        detailsXML += "    <Request>#=Request</Request>";
        detailsXML += "    <RequestWarranty>#=RequestWarranty</RequestWarranty>";
        detailsXML += "    <RequestAnalysis>#=RequestAnalysis</RequestAnalysis>";
        detailsXML += "    <RequestCreditOfBody>#=RequestCreditOfBody</RequestCreditOfBody>";
        detailsXML += "    <RequestOutOfWarranty>#=RequestOutOfWarranty</RequestOutOfWarranty>";
        detailsXML += "    <RequestSpecificTest>#=RequestSpecificTest</RequestSpecificTest>";
        detailsXML += "    <RequestSpecificTestRemarks>#=RequestSpecificTestRemarks</RequestSpecificTestRemarks>";
        detailsXML += "    <Configuration>#=Configuration</Configuration>";
        detailsXML += "    <DateOfFailure>#=DateOfFailure</DateOfFailure>";
        detailsXML += "    <RunningHours>#=RunningHours</RunningHours>";
        detailsXML += "    <DateOfPartIncome>#=DateOfPartIncome</DateOfPartIncome>";
        detailsXML += "    <CurrentWeekOfPartIncome>#=CurrentWeekOfPartIncome</CurrentWeekOfPartIncome>";
        detailsXML += "    <MonthIncome>#=MonthIncome</MonthIncome>";
        detailsXML += "    <FiscalYearIncome>#=FiscalYearIncome</FiscalYearIncome>";
        detailsXML += "    <Remarks>#=Remarks</Remarks>";
        detailsXML += "    <FilePath>#=FilePath</FilePath>";
        detailsXML += "    <CustomerNumber>#=CustomerNumber</CustomerNumber>";
        detailsXML += "    <ItemNumber>#=ItemNumber</ItemNumber>";
        detailsXML += "    <ControlComponentReference>#=ControlComponentReference</ControlComponentReference>";
        detailsXML += "    <ReferenceNo>#=ReferenceNo</ReferenceNo>";
        detailsXML += "    <ReturnDate>#=ReturnDate</ReturnDate>";
        detailsXML += "    <FailureDescription>#=FailureDescription</FailureDescription>";
        detailsXML += "    <Origin>#=Origin</Origin>";
        detailsXML += "    <CauseOrGuilty>#=CauseOrGuilty</CauseOrGuilty>";
        detailsXML += "    <CreditDecision>#=CreditDecision</CreditDecision>";
        detailsXML += "    <ReportText>#=ReportText</ReportText>";
        detailsXML += "    <SupplierAnalysis>#=SupplierAnalysis</SupplierAnalysis>";
        detailsXML += "    <PartReturned>#=PartReturned</PartReturned>";
        detailsXML += "    <Note>#=Note</Note>";
        detailsXML += "    <ClosedInKolin>#=ClosedInKolin</ClosedInKolin>";
        detailsXML += "    <ClosedInCurrentWeek>#=ClosedInCurrentWeek</ClosedInCurrentWeek>";
        detailsXML += "    <ClosedInMonthIncome>#=ClosedInMonthIncome</ClosedInMonthIncome>";
        detailsXML += "    <FiscalYearClosed>#=FiscalYearClosed</FiscalYearClosed>";
        detailsXML += "    <ReceivedInKolin>#=ReceivedInKolin</ReceivedInKolin>";
        detailsXML += "    <ReceivedInCurrentWeek>#=ReceivedInCurrentWeek</ReceivedInCurrentWeek>";
        detailsXML += "    <ReceivedInMonthIncome>#=ReceivedInMonthIncome</ReceivedInMonthIncome>";
        detailsXML += "    <FiscalYearReceived>#=FiscalYearReceived</FiscalYearReceived>";
        detailsXML += "    <OpenOrClosed>#=OpenOrClosed</OpenOrClosed>";
        detailsXML += "    <PortalReference>#=PortalReference</PortalReference>";
        detailsXML += "    </List_Section>";
        return detailsXML;
    }

    protected string printReportAsXML(ArrayList _returnFormDetailControl, DataTable _rfHeaderDataTable)
    {
        string xml = "";

        xml += "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
        xml += "<!-- Generated by WebPortal Reports version -->";
        xml += "<XXWP_RET_PRINT_ECT_EU>";
        xml += "    <Header_Section>";
        xml += "    <RFHeaderID>#=RFHeaderID</RFHeaderID>";
        xml += "    <ReferenceNumber>#=ReferenceNumber</ReferenceNumber>"; 
        xml += "    <ReturnDate>#=ReturnDate</ReturnDate>";
        xml += "    </Header_Section>";

        string portalReferenceNoDetail = "";
        if (optProductTypeControl.SelectedValue == "Accessories")
        {
            foreach (Model.ReturnFormAccessories returnForm in _returnFormDetailControl)
            {
                string detailsXML = xmlPrintForm();

                detailsXML = detailsXML.Replace("#=RFDetailID", returnForm.RFDetailID.ToString());
                detailsXML = detailsXML.Replace("#=ClaimID", returnForm.ClaimID.ToString());
                //ADD ORACLE NUMBER
                detailsXML = detailsXML.Replace("#=OracleNumber", returnForm.OracleNumber);
                //CUSTOMER LOCATION FIELDS
                detailsXML = detailsXML.Replace("#=CustomerNumber", returnForm.CustomerNumber.ToString());
                detailsXML = detailsXML.Replace("#=CustomerName", convertedXMLformat(returnForm.CustomerName));
                detailsXML = detailsXML.Replace("#=LocationID", returnForm.LocationID.ToString());
                detailsXML = detailsXML.Replace("#=City", convertedXMLformat(returnForm.City));
                detailsXML = detailsXML.Replace("#=State", convertedXMLformat(returnForm.State));
                detailsXML = detailsXML.Replace("#=Street", "");//returnForm.State);
                detailsXML = detailsXML.Replace("#=ZipCode", returnForm.ZipCode);
                detailsXML = detailsXML.Replace("#=Country", convertedXMLformat(returnForm.Country));
                //
                detailsXML = detailsXML.Replace("#=RFHeaderID", returnForm.RFHeaderID.ToString());
                detailsXML = detailsXML.Replace("#=ProductType", returnForm.ProductType);
                detailsXML = detailsXML.Replace("#=Claim", convertedXMLformat(returnForm.Claim));
                detailsXML = detailsXML.Replace("#=ClaimDescription", convertedXMLformat(returnForm.ClaimDescription));
                detailsXML = detailsXML.Replace("#=ControlCustomerClaimReference", convertedXMLformat(returnForm.ControlCustomerClaimReference));
                detailsXML = detailsXML.Replace("#=PCNNumber", returnForm.PCNNumber);
                detailsXML = detailsXML.Replace("#=ModelProduct", returnForm.ModelProduct);
                detailsXML = detailsXML.Replace("#=ProductGroup", returnForm.ProductGroup);
                detailsXML = detailsXML.Replace("#=Line", returnForm.Line);
                detailsXML = detailsXML.Replace("#=DataCode", returnForm.DataCode);
                detailsXML = detailsXML.Replace("#=Warranty", returnForm.Warranty);
                detailsXML = detailsXML.Replace("#=Quantity", returnForm.Quantity);
                detailsXML = detailsXML.Replace("#=DateOfClaim", returnForm.DateOfClaim);
                detailsXML = detailsXML.Replace("#=TypeOfReturn", returnForm.TypeOfReturn);
                detailsXML = detailsXML.Replace("#=Application", returnForm.Application);
                detailsXML = detailsXML.Replace("#=Refrigerant", returnForm.Refrigerant);
                detailsXML = detailsXML.Replace("#=OtherRefrigerant", returnForm.OtherRefrigerant);
                detailsXML = detailsXML.Replace("#=WorkingPointTO", returnForm.WorkingPointTO);
                detailsXML = detailsXML.Replace("#=WorkingPointTC", returnForm.WorkingPointTC);
                detailsXML = detailsXML.Replace("#=WorkingPointSuperHeat", returnForm.WorkingPointSuperHeat);
                detailsXML = detailsXML.Replace("#=WorkingPointSubCooling", returnForm.WorkingPointSubCooling);
                detailsXML = detailsXML.Replace("#=Request", returnForm.Request);
                detailsXML = detailsXML.Replace("#=RequestWarranty", returnForm.RequestWarranty.ToString());
                detailsXML = detailsXML.Replace("#=RequestAnalysis", returnForm.RequestAnalysis.ToString());
                detailsXML = detailsXML.Replace("#=RequestCreditOfBody", returnForm.RequestCreditOfBody.ToString());
                detailsXML = detailsXML.Replace("#=RequestOutOfWarranty", returnForm.RequestOutOfWarranty.ToString());
                detailsXML = detailsXML.Replace("#=RequestSpecificTest", returnForm.RequestSpecificTest.ToString());
                detailsXML = detailsXML.Replace("#=RequestSpecificTestRemarks", convertedXMLformat(returnForm.RequestSpecificTestRemarks));
                detailsXML = detailsXML.Replace("#=Configuration", returnForm.Configuration);
                detailsXML = detailsXML.Replace("#=DateOfFailure", returnForm.DateOfFailure);
                detailsXML = detailsXML.Replace("#=RunningHours", returnForm.RunningHours);
                detailsXML = detailsXML.Replace("#=DateOfPartIncome", returnForm.DateOfPartIncome);
                detailsXML = detailsXML.Replace("#=CurrentWeekOfPartIncome", returnForm.CurrentWeekOfPartIncome);
                detailsXML = detailsXML.Replace("#=MonthIncome", returnForm.MonthIncome);
                detailsXML = detailsXML.Replace("#=FiscalYearIncome", returnForm.FiscalYearIncome);
                detailsXML = detailsXML.Replace("#=Remarks", convertedXMLformat(returnForm.Remarks));
                detailsXML = detailsXML.Replace("#=FilePath", returnForm.FilePath.Split('|')[0]);
                detailsXML = detailsXML.Replace("#=CustomerNumber", returnForm.CustomerNumber.ToString());
                detailsXML = detailsXML.Replace("#=ItemNumber", returnForm.ItemNumber);
                detailsXML = detailsXML.Replace("#=ControlComponentReference", convertedXMLformat(returnForm.ControlComponentReference));
                detailsXML = detailsXML.Replace("#=ReferenceNo", returnForm.ReferenceNo);

                detailsXML = detailsXML.Replace("#=ReturnDate", returnForm.ReturnDate);
                detailsXML = detailsXML.Replace("#=FailureDescription", convertedXMLformat(returnForm.FailureDescription));
                detailsXML = detailsXML.Replace("#=Origin", returnForm.Origin);
                detailsXML = detailsXML.Replace("#=CauseOrGuilty", returnForm.CauseOrGuilty);
                detailsXML = detailsXML.Replace("#=CreditDecision", returnForm.CreditDecision);
                detailsXML = detailsXML.Replace("#=ReportText", convertedXMLformat(returnForm.ReportText));
                detailsXML = detailsXML.Replace("#=SupplierAnalysis", convertedXMLformat(returnForm.SupplierAnalysis));
                detailsXML = detailsXML.Replace("#=PartReturned", returnForm.PartReturned);
                detailsXML = detailsXML.Replace("#=Note", convertedXMLformat(returnForm.Note));
                detailsXML = detailsXML.Replace("#=ClosedInKolin", returnForm.ClosedInKolin);
                detailsXML = detailsXML.Replace("#=ClosedInCurrentWeek", returnForm.ClosedInCurrentWeek);
                detailsXML = detailsXML.Replace("#=ClosedInMonthIncome", returnForm.ClosedInMonthIncome);
                detailsXML = detailsXML.Replace("#=FiscalYearClosed", returnForm.FiscalYearClosed);
                detailsXML = detailsXML.Replace("#=ReceivedInKolin", returnForm.ReceivedInKolin);
                detailsXML = detailsXML.Replace("#=ReceivedInCurrentWeek", returnForm.ReceivedInCurrentWeek);
                detailsXML = detailsXML.Replace("#=ReceivedInMonthIncome", returnForm.ReceivedInMonthIncome);
                detailsXML = detailsXML.Replace("#=FiscalYearReceived", returnForm.FiscalYearReceived);
                detailsXML = detailsXML.Replace("#=OpenOrClosed", returnForm.OpenOrClosed);
                detailsXML = detailsXML.Replace("#=PortalReference", returnForm.PortalReference);

                portalReferenceNoDetail = returnForm.PortalReference;

                xml += detailsXML;
            }
        }
        else
        {
            foreach (Model.ReturnFormControl returnForm in _returnFormDetailControl)
            {
                string detailsXML = xmlPrintForm();

                detailsXML = detailsXML.Replace("#=RFDetailID", returnForm.RFDetailID.ToString());
                detailsXML = detailsXML.Replace("#=ClaimID", returnForm.ClaimID.ToString());
                //ADD ORACLE NUMBER
                detailsXML = detailsXML.Replace("#=OracleNumber", returnForm.OracleNumber);
                //CUSTOMER LOCATION FIELDS
                detailsXML = detailsXML.Replace("#=CustomerNumber", returnForm.CustomerNumber.ToString());
                detailsXML = detailsXML.Replace("#=CustomerName", convertedXMLformat(returnForm.CustomerName));
                detailsXML = detailsXML.Replace("#=LocationID", returnForm.LocationID.ToString());
                detailsXML = detailsXML.Replace("#=City", convertedXMLformat(returnForm.City));
                detailsXML = detailsXML.Replace("#=State", convertedXMLformat(returnForm.State));
                detailsXML = detailsXML.Replace("#=Street", "");//returnForm.State);
                detailsXML = detailsXML.Replace("#=ZipCode", returnForm.ZipCode);
                detailsXML = detailsXML.Replace("#=Country", convertedXMLformat(returnForm.Country));
                //
                detailsXML = detailsXML.Replace("#=RFHeaderID", returnForm.RFHeaderID.ToString());
                detailsXML = detailsXML.Replace("#=ProductType", returnForm.ProductType);
                detailsXML = detailsXML.Replace("#=Claim", convertedXMLformat(returnForm.Claim));
                detailsXML = detailsXML.Replace("#=ClaimDescription", convertedXMLformat(returnForm.ClaimDescription));
                detailsXML = detailsXML.Replace("#=ControlCustomerClaimReference", convertedXMLformat(returnForm.ControlCustomerClaimReference));
                detailsXML = detailsXML.Replace("#=PCNNumber", returnForm.PCNNumber);
                detailsXML = detailsXML.Replace("#=ModelProduct", returnForm.ModelProduct);
                detailsXML = detailsXML.Replace("#=ProductGroup", returnForm.ProductGroup);
                detailsXML = detailsXML.Replace("#=Line", returnForm.Line);
                detailsXML = detailsXML.Replace("#=DataCode", returnForm.DataCode);
                detailsXML = detailsXML.Replace("#=Warranty", returnForm.Warranty);
                detailsXML = detailsXML.Replace("#=Quantity", returnForm.Quantity);
                detailsXML = detailsXML.Replace("#=DateOfClaim", returnForm.DateOfClaim);
                detailsXML = detailsXML.Replace("#=TypeOfReturn", returnForm.TypeOfReturn);
                detailsXML = detailsXML.Replace("#=Application", returnForm.Application);
                detailsXML = detailsXML.Replace("#=Refrigerant", returnForm.Refrigerant);
                detailsXML = detailsXML.Replace("#=OtherRefrigerant", returnForm.OtherRefrigerant);
                detailsXML = detailsXML.Replace("#=WorkingPointTO", returnForm.WorkingPointTO);
                detailsXML = detailsXML.Replace("#=WorkingPointTC", returnForm.WorkingPointTC);
                detailsXML = detailsXML.Replace("#=WorkingPointSuperHeat", returnForm.WorkingPointSuperHeat);
                detailsXML = detailsXML.Replace("#=WorkingPointSubCooling", returnForm.WorkingPointSubCooling);
                detailsXML = detailsXML.Replace("#=Request", returnForm.Request);
                detailsXML = detailsXML.Replace("#=RequestWarranty", returnForm.RequestWarranty.ToString());
                detailsXML = detailsXML.Replace("#=RequestAnalysis", returnForm.RequestAnalysis.ToString());
                detailsXML = detailsXML.Replace("#=RequestCreditOfBody", returnForm.RequestCreditOfBody.ToString());
                detailsXML = detailsXML.Replace("#=RequestOutOfWarranty", returnForm.RequestOutOfWarranty.ToString());
                detailsXML = detailsXML.Replace("#=RequestSpecificTest", returnForm.RequestSpecificTest.ToString());
                detailsXML = detailsXML.Replace("#=RequestSpecificTestRemarks", convertedXMLformat(returnForm.RequestSpecificTestRemarks));
                detailsXML = detailsXML.Replace("#=Configuration", returnForm.Configuration);
                detailsXML = detailsXML.Replace("#=DateOfFailure", returnForm.DateOfFailure);
                detailsXML = detailsXML.Replace("#=RunningHours", returnForm.RunningHours);
                detailsXML = detailsXML.Replace("#=DateOfPartIncome", returnForm.DateOfPartIncome);
                detailsXML = detailsXML.Replace("#=CurrentWeekOfPartIncome", returnForm.CurrentWeekOfPartIncome);
                detailsXML = detailsXML.Replace("#=MonthIncome", returnForm.MonthIncome);
                detailsXML = detailsXML.Replace("#=FiscalYearIncome", returnForm.FiscalYearIncome);
                detailsXML = detailsXML.Replace("#=Remarks", convertedXMLformat(returnForm.Remarks));
                detailsXML = detailsXML.Replace("#=FilePath", returnForm.FilePath.Split('|')[0]);
                detailsXML = detailsXML.Replace("#=CustomerNumber", returnForm.CustomerNumber.ToString());
                detailsXML = detailsXML.Replace("#=ItemNumber", returnForm.ItemNumber);
                detailsXML = detailsXML.Replace("#=ControlComponentReference", convertedXMLformat(returnForm.ControlComponentReference));
                detailsXML = detailsXML.Replace("#=ReferenceNo", returnForm.ReferenceNo);

                detailsXML = detailsXML.Replace("#=ReturnDate", returnForm.ReturnDate);
                detailsXML = detailsXML.Replace("#=FailureDescription", convertedXMLformat(returnForm.FailureDescription));
                detailsXML = detailsXML.Replace("#=Origin", returnForm.Origin);
                detailsXML = detailsXML.Replace("#=CauseOrGuilty", returnForm.CauseOrGuilty);
                detailsXML = detailsXML.Replace("#=CreditDecision", returnForm.CreditDecision);
                detailsXML = detailsXML.Replace("#=ReportText", convertedXMLformat(returnForm.ReportText));
                detailsXML = detailsXML.Replace("#=SupplierAnalysis", convertedXMLformat(returnForm.SupplierAnalysis));
                detailsXML = detailsXML.Replace("#=PartReturned", returnForm.PartReturned);
                detailsXML = detailsXML.Replace("#=Note", convertedXMLformat(returnForm.Note));
                detailsXML = detailsXML.Replace("#=ClosedInKolin", returnForm.ClosedInKolin);
                detailsXML = detailsXML.Replace("#=ClosedInCurrentWeek", returnForm.ClosedInCurrentWeek);
                detailsXML = detailsXML.Replace("#=ClosedInMonthIncome", returnForm.ClosedInMonthIncome);
                detailsXML = detailsXML.Replace("#=FiscalYearClosed", returnForm.FiscalYearClosed);
                detailsXML = detailsXML.Replace("#=ReceivedInKolin", returnForm.ReceivedInKolin);
                detailsXML = detailsXML.Replace("#=ReceivedInCurrentWeek", returnForm.ReceivedInCurrentWeek);
                detailsXML = detailsXML.Replace("#=ReceivedInMonthIncome", returnForm.ReceivedInMonthIncome);
                detailsXML = detailsXML.Replace("#=FiscalYearReceived", returnForm.FiscalYearReceived);
                detailsXML = detailsXML.Replace("#=OpenOrClosed", returnForm.OpenOrClosed);
                detailsXML = detailsXML.Replace("#=PortalReference", returnForm.PortalReference);

                portalReferenceNoDetail = returnForm.PortalReference;

                xml += detailsXML;
            }
        }

  

        
        xml += "</XXWP_RET_PRINT_ECT_EU>";
        DataRow dr = _rfHeaderDataTable.Rows[0];
        xml = xml.Replace("#=RFHeaderID", dr["RFHeaderID"].ToString());
        xml = xml.Replace("#=ReferenceNumber", portalReferenceNoDetail);
        //xml = xml.Replace("#=ReferenceNumber", dr["ReferenceNumber"].ToString());
        xml = xml.Replace("#=ReturnDate", dr["ReturnDate"].ToString());


        //HANDLE & for xml
        xml = xml.Replace("&", "&amp;");

        return xml;
    }
    protected void txtControlClosedInKolin_TextChanged(object sender, EventArgs e)
    {
        try
        {
            if (this.txtControlClosedInKolin.Text.Length > 10) { return; }
            if (txtControlClosedInKolin.Text != "")
            {
                CultureInfo ciCurr = CultureInfo.CurrentCulture;
                int weekNum = ciCurr.Calendar.GetWeekOfYear(Convert.ToDateTime(formatDateForSQL(txtControlClosedInKolin.Text)), CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
                txtControlClosedInCureentWeek.Text = Convert.ToDateTime(formatDateForSQL(txtControlClosedInKolin.Text)).Year.ToString() + "/" + weekNum;
                txtControlClosedInMonth.Text = Convert.ToDateTime(formatDateForSQL(txtControlClosedInKolin.Text)).Year.ToString() + "/" + Convert.ToDateTime(formatDateForSQL(txtControlClosedInKolin.Text)).Month.ToString();
                if (Convert.ToDateTime(formatDateForSQL(txtControlClosedInKolin.Text)).Month >= 10)
                { txtControlClosedInFY.Text = "FY" + (Convert.ToDateTime(formatDateForSQL(txtControlClosedInKolin.Text)).Year + 1).ToString().Substring(2, 2); }
                else
                { txtControlClosedInFY.Text = "FY" + Convert.ToDateTime(formatDateForSQL(txtControlClosedInKolin.Text)).Year.ToString().Substring(2, 2); }

                txtControlOpenOrClose.Text = "CLOSED";
            }
            else
            {
                txtControlClosedInCureentWeek.Text = "";
                txtControlClosedInMonth.Text = "";
                txtControlClosedInFY.Text = "";
                txtControlOpenOrClose.Text = "";
            }
        }
        catch (Exception)
        {
            txtControlClosedInCureentWeek.Text = "";
            txtControlClosedInMonth.Text = "";
            txtControlClosedInFY.Text = "";
            txtControlOpenOrClose.Text = "";
            return;
        }

    }
    protected void txtReceiveInKolin_TextChanged(object sender, EventArgs e)
    {
        try
        {
            if (this.txtReceiveInKolin.Text.Length > 10) { return; }
            if (this.txtReceiveInKolin.Text != "")
            {

                CultureInfo ciCurr = CultureInfo.CurrentCulture;
                int weekNum = ciCurr.Calendar.GetWeekOfYear(Convert.ToDateTime(formatDateForSQL(txtReceiveInKolin.Text)), CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
                this.txtReceivedInCurrentWeek.Text = Convert.ToDateTime(formatDateForSQL(txtReceiveInKolin.Text)).Year.ToString() + "/" + weekNum;
                txtReceivedInMonth.Text = Convert.ToDateTime(formatDateForSQL(txtReceiveInKolin.Text)).Year.ToString() + "/" + Convert.ToDateTime(formatDateForSQL(txtReceiveInKolin.Text)).Month.ToString();
                if (Convert.ToDateTime(formatDateForSQL(txtReceiveInKolin.Text)).Month >= 10)
                { this.txtReceivedInPiscalYear.Text = "FY" + (Convert.ToDateTime(formatDateForSQL(txtReceiveInKolin.Text)).Year + 1).ToString().Substring(2, 2); }
                else
                { txtReceivedInPiscalYear.Text = "FY" + Convert.ToDateTime(formatDateForSQL(txtReceiveInKolin.Text)).Year.ToString().Substring(2, 2); }

                txtControlOpenOrClose.Text = "CLOSED";
            }
            else
            {
                txtReceivedInCurrentWeek.Text = "";
                txtReceivedInMonth.Text = "";
                txtReceivedInPiscalYear.Text = "";
                txtControlOpenOrClose.Text = "";
            }
        }
        catch (Exception)
        {
            txtReceivedInCurrentWeek.Text = "";
            txtReceivedInMonth.Text = "";
            txtReceivedInPiscalYear.Text = "";
            txtControlOpenOrClose.Text = "";
            return;
        }

    }

    protected void btnAddNewControlItem_Click(object sender, EventArgs e)
    {
        try
        {
            int qty = int.Parse(txtNewControlQty.Text);

        }
        catch
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert" + UniqueID,
          "alert('Invalid Quantity Format.');", true);
            return;
        }

        if (int.Parse(txtNewControlQty.Text) >= int.Parse(this.txtControlQty.Text))
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert" + UniqueID,
            "alert('Invalid Quantity, Set quantity less than the current quantity of control.');", true);
            return;
        }
         
        dynamic mrfc = toggleControlOrAccessoriesMaintenance(optProductTypeControl.SelectedValue);
        dynamic rfc = toggleControlOrAccessoriesModel(optProductTypeControl.SelectedValue);

      
        rfc = mrfc.getReturnForm(gvMyReturnControl.SelectedRow.Cells[1].Text);
        rfc.RFDetailID = int.Parse(gvMyReturnControl.SelectedRow.Cells[1].Text);
        rfc.DateOfClaim = formatDateForSQL(rfc.DateOfClaim);
        rfc.DateOfFailure = formatDateForSQL(rfc.DateOfFailure);
        rfc.DateOfPartIncome = formatDateForSQL(rfc.DateOfPartIncome);
        rfc.Quantity = (int.Parse(rfc.Quantity) - int.Parse(txtNewControlQty.Text)).ToString();
        rfc.ClosedInKolin = formatDateForSQL(rfc.ClosedInKolin);
        rfc.ReceivedInKolin = formatDateForSQL(rfc.ReceivedInKolin);

        if (optProductTypeControl.SelectedValue == "Accessories")
            mrfc.updateReturnFormAccessories(rfc);
        else
            mrfc.updateReturnFormControl(rfc);

        // Add new entry of the same control but no claims included
        Model.ReturnFormControl rfcNewControl = new Model.ReturnFormControl();
        rfcNewControl = mrfc.getReturnForm(gvMyReturnControl.SelectedRow.Cells[1].Text);
        rfcNewControl.RFHeaderID = int.Parse(rfhid.Value);
        rfcNewControl.DateOfClaim = (rfc.DateOfClaim);
        rfcNewControl.DateOfFailure = (rfc.DateOfFailure);
        rfcNewControl.DateOfPartIncome = (rfc.DateOfPartIncome);
        rfcNewControl.Quantity = txtNewControlQty.Text;
        rfcNewControl.ClosedInKolin = (rfc.ClosedInKolin);
        rfcNewControl.ReceivedInKolin = (rfc.ReceivedInKolin);
        rfc = mrfc.addReturnForm(rfcNewControl);
       

        // Update current entry's claim
        rfcNewControl.RFDetailID = rfc.RFDetailID;
        //mrfc.updateQtyCurrentControl(rfcNewControl);

        Page.ClientScript.RegisterStartupScript(this.GetType(), "alert" + UniqueID,
           "alert('New control successfully added, the quantity of control is splitted.');", true);


        this.ddProductType.Visible = true;
        this.ddProductType.SelectedValue = "CONTROL";
        this.pnlProduct.Visible = true;
        this.pnlCompressorCustomerList.Visible = false;
        this.pnlControlCustomerList.Visible = true;
        this.pnlCompressorDetail.Visible = false;
        this.pnlControlDetail.Visible = false;
        this.pnlReturn.Visible = true;
        string y = this.ddControlCustomerList.SelectedValue;
        bindDataCompressor();
        bindDataControl();
    }

    public dynamic toggleControlOrAccessoriesMaintenance(string cur)
    {
        dynamic mrfc;

        if(cur == "Accessories")
            mrfc = new Maintenance.ReturnFormAccessories();
        else
            mrfc = new Maintenance.ReturnFormControl(); 
        return mrfc;
    }
    public dynamic toggleControlOrAccessoriesModel(string cur)
    {
        dynamic rfc;

        if (cur == "Accessories")
            rfc = new Model.ReturnFormAccessories();
        else
            rfc = new Model.ReturnFormControl();
        return rfc;
    }

    protected void ddControlCustomerList_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void txtControlModel_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.txtControlPCNNumber.Text = this.txtControlProduct.SelectedValue;
    }
     
}