﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ForgotPassword.aspx.cs" Inherits="ForgotPassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Europe Customer Quality Portal</title>
        <link type="text/css" rel="Stylesheet" href="css/default.css" />
        <link type="text/css" rel="Stylesheet" href="css/style.css" />
    <style type="text/css">
        .auto-style1 {
            height: 29px;
        }
    </style>
</head>
<body>
    <form id="frmMain" runat="server">
    <div>
        <div id="holder_main" align="center">
            <div id="holderBody" style="width: 500px;">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr valign="top">
                            <td valign="top" height="100%" style="width:770px">
                                <div id="holderMaincontent">
                                    <div>
                                        <div>
                                            <h1>Forgot Password</h1>
                                        </div>
                                        <hr />
                                        <table align="center">
                                            <tr>
                                                <td colspan="5" align="center">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblMessage" runat="server">Please supply your Username to reset your password</asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                 
                                                
                                                <td>
                                                    <asp:TextBox ID="txtEmailaddress" runat="server" Width="250px"></asp:TextBox>
                                                </td>
                                               
                                                <td class="auto-style1">
                                                    <asp:Button ID="cmdSubmit" runat="server" Text="Submit"  class="buttons" OnClick="cmdSubmit_Click" /> 
                                                </td>    
                                               <td>

                                               </td>  
                                               <td><asp:Button ID="cmdCancel" runat="server" Text="Cancel"  class="buttons" OnClick="cmdCancel_Click"  /> </td>
                                            
                                            </tr>
                                            <tr>
                                                 <td>
                                                     <span id="errromsg" style="color:red;"></span>
                                                     <asp:Label ID="noemail" ForeColor="Red" Visible="false" runat="server">The email address does not exist on this site.</asp:Label>
                                                     <asp:Label ID="success" ForeColor="Green" Visible="false" runat="server">New password sent to your email address. Please check your email.</asp:Label>

                                                 </td>
                               

                                            </tr>
                                             
                                        </table>
                                        
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
            </div>
        </div>
    </div>
    </form>
    <script type="text/javascript" src="js/jquery.1.10.2.js"></script>
    <script type="text/javascript" src="js/ajaxfunctions/forgotpassword.js"></script>
    
</body>
</html>
