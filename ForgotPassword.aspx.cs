﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class ForgotPassword : System.Web.UI.Page
{   
    // Change Email Address to Username
    protected void Page_Load(object sender, EventArgs e)
    { 
        this.txtEmailaddress.Focus();
    } 
    protected void cmdSubmit_Click(object sender, EventArgs e)
    {
        Maintenance.UserInformation u = new Maintenance.UserInformation();
        noemail.Visible = false;
        success.Visible = false;
        if (!u.checkUsername(txtEmailaddress.Text))
        {
            txtEmailaddress.Text = "";
            noemail.Visible = true;
        }
        else { 
            Model.UserInformation mu = new Model.UserInformation();
            MailMaster mail = new MailMaster();
            mu = u.updatePasswordOfUsername(txtEmailaddress.Text); 
            mail.sendEmailforgotPassword(mu);
            success.Visible = true;
            
            //[START] [4/25/17] [jpbersonda]
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "", "alert('New password sent to your email address. Please check your email.');window.location='login.aspx';", true);
            //[END] [4/25/17] [jpbersonda]
        }

    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("home.aspx");
    }
}
