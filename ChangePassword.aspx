﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Europe Customer Quality Portal</title>
    <link type="text/css" rel="Stylesheet" href="css/default.css" />
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
</head>
<body>
    <form id="frmMain" runat="server">
    <div>
        <div id="holder_main" align="center">
            <div id="holderBody" style="width: 980px;">
                <div class="MainPanel2">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr valign="top">
                            <td valign="top" height="100%" class="LeftNavigation">
                                 <% Response.Write(Helpers.menuNavi(4)); %>
                            </td>
                            <td valign="top" height="100%" class="DottedBorderLeft" style="width: 770px">
                                <div id="holderMaincontent">
                                    <div>
                                        <div>
                                             <% string sysmode = System.Configuration.ConfigurationManager.AppSettings["SystemMode"]; %>
                                            <h1>Change Password <% Response.Write((sysmode == "TEST") ? "(TEST SITE)" : "(PRODUCTION)"); %></h1>
                                             
                                        </div>
                                        <hr />
                                        <table>
                                            <tr>
                                                <td colspan="4">
                                                    <asp:Label ID="lblMessage" ForeColor="Gray" runat="server">Fill up form to change your password.</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                    Old Password
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtOldPassword" runat="server" TextMode="Password"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                    New Password
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                    Confirm New Password
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtConfirmNewPassword" runat="server" TextMode="Password"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="center">

                                                    <asp:Button ID="cmdSubmit" runat="server" Text="Submit" OnClick="cmdSubmit_Click"
                                                        class="buttons" />
                                                    <asp:Button ID="cmdCancel" runat="server" Text="Cancel" OnClick="cmdCancel_Click"
                                                        class="buttons" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </form>
    <script type="text/javascript" src="js/jquery.1.10.2.js" language="javascript"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        jQuery(window).load(function () 
        });
    </script>
</body>
</html>
