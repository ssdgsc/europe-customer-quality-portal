﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="usermanagement.aspx.cs" Inherits="usermanagement" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link type="text/css" rel="Stylesheet" href="css/default.css" />
    <link type="text/css" rel="Stylesheet" href="css/style.css" />

    <style type="text/css">
        .style2
        {
            width: 124px;
        }
        .style3
        {
            width: 106px;
        }
        .style4
        {
            width: 144px;
        }
        .style5
        {
            width: 201px;
        }
        .style6
        {
            color: #FF3300;
        }
        .style7
        {
            width: 2px;
        }
    </style>
    <script type="text/javascript" src="js/jquery.1.10.2.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function () {
            if ($(".paginationGridView > td > table > tbody > tr > td:last-child > a").text() == "...")
                $(".paginationGridView > td > table > tbody > tr > td:last-child > a").text("Next >");

            if ($(".paginationGridView > td > table > tbody > tr > td:first-child > a").text() == "...")
                $(".paginationGridView > td > table > tbody > tr > td:first-child > a").text("< Prev");
        });
            //function filterCustomerByAssign() {
            //    var assign = jQuery("input[name=optProductType]:checked").val();
            //    PageMethods.filterCustomerByAssign(assign, onSuccess, onError);
            //    function onSuccess(result) {
            //        var data = result;
            //        jQuery("#lbxCustomer p.empty-row").remove();
            //        jQuery("#lbxCustomer input[type=checkbox]").closest("tr").hide();
            //        for (x in data) {
            //            jQuery("#lbxCustomer input[type=checkbox][value=" + data[x].CustomerID + "]").closest("tr").show();
            //        }
            //        if (!data.length)
            //            jQuery("#lbxCustomer tbody").prepend("<p class='empty-row'>No result(s) founds.</p>")
            //    }
            //    function onError(result) {
            //        alert("Can not calculate Warranty from the given Datacode.");
            //    }
            //}
            
</script>
</head>
<body>
    <form id="frmMain" runat="server" style="font-size: 11px">
    <asp:HiddenField ID="hdcuscomp" runat="server" />
    <asp:HiddenField ID="hdcusctrl" runat="server" />
    <asp:HiddenField ID="hdcusaccs" runat="server" />
    <asp:HiddenField ID="hdUserID" runat="server" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <div>
        <div id="holder_main" align="center">
            <div id="holderBody" class="container">
                <div class="MainPanel2">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr valign="top">
                            <td valign="top" height="100%" class="LeftNavigation">
                                  <% Response.Write(Helpers.menuNavi(6)); %>
                            </td>
                            <td valign="top" height="100%" class="DottedBorderLeft" style="width:770px">
                                <div id="holderMaincontent">
                                    <div style="position: relative; top:  0px; padding-right: 5px; padding-bottom: 5px; padding-top: 0px; display: table-cell; text-align: left;">
                                        <div>
                                            <% string sysmode = System.Configuration.ConfigurationManager.AppSettings["SystemMode"]; %>
                                            <h3>Users Maintenance <% Response.Write((sysmode == "TEST") ? "(TEST SITE)" : "(PRODUCTION)"); %></h3>
                                        </div>
                                         <asp:Panel ID="pnlGridView" runat="server" Width="745px" ScrollBars="Auto">
                                             <table style="width: 100%;">
                                                 <tr>
                                                     <td class="style4">
                                                         User Name
                                                     </td>
                                                     <td class="style5">
                                                         <asp:TextBox ID="txtUserNameSearch" runat="server" Width="190px"></asp:TextBox>
                                                     </td>
                                                     <td></td>
                                                 </tr>
                                                 <tr>
                                                     <td class="style4">
                                                         User First Name
                                                     </td>
                                                     <td class="style5">
                                                         <asp:TextBox ID="txtUserFirstNameSearch" runat="server" Width="190px"></asp:TextBox>
                                                     </td>
                                                     <td></td>
                                                 </tr>
                                                 <tr>
                                                     <td class="style4">
                                                         User Last Name
                                                     </td>
                                                     <td class="style5">
                                                         <asp:TextBox ID="txtUserLastNameSearch" runat="server" Width="190px"></asp:TextBox>
                                                     </td>
                                                     <td>
                                                         <asp:Button ID="btnSearch" class="buttons" runat="server" Text="Search" 
                                                             onclick="btnSearch_Click" />
                                                     </td>
                                                 </tr>
                                             </table>
                                         
                                            <br />

                                            <asp:GridView ID="UserGrid" runat="server" AllowPaging="True" 
                                                OnPageIndexChanging="GridView1_PageIndexChanging"
                                                AllowSorting="True" CellPadding="4" CssClass="ui_grid" 
                                                EmptyDataText="No Record Found" ForeColor="#333333" 
                                                onselectedindexchanged="UserGrid_SelectedIndexChanged" PageSize="20" 
                                                Width="740px" AutoGenerateColumns="False">
                                                <AlternatingRowStyle BackColor="White" />
                                               
                                                <PagerStyle CssClass="paginationGridView"/>

                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="True" />
                                                    <asp:TemplateField HeaderText="User ID" Visible="False">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("UserID") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblUserID" runat="server" Text='<%# Bind("UserID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle Width="0px" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Username" HeaderText="Username" />
                                                    <asp:BoundField DataField="UserFirstName" HeaderText="User First Name" />
                                                    <asp:BoundField DataField="UserLastName" HeaderText="User Last Name" />
                                                    <asp:BoundField DataField="UserType" HeaderText="User Type" />
                                                    <asp:BoundField DataField="CompanyName" HeaderText="Company Name" />
                                                    <asp:BoundField DataField="PhoneNumber" HeaderText="Phone Number" />
                                                    <asp:BoundField DataField="EmailAddress" HeaderText="Email Address" />
                                                    <asp:BoundField DataField="UserTitle" HeaderText="User Title" />
                                                    <asp:BoundField DataField="Function" HeaderText="Function" />
                                                    <asp:CheckBoxField DataField="ReceptionActivated" Text="Reception Activated" />
                                                    <asp:CheckBoxField DataField="AnalysisActivated" Text="Analysis Activated" />
                                                    <asp:CheckBoxField DataField="CDActivated" 
                                                        Text="Credit Decision Activated" />
                                                </Columns>
                                                <EditRowStyle BackColor="#2461BF" />
                                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Left" />
                                                <RowStyle BackColor="#EFF3FB" />
                                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                            </asp:GridView>
                                        </asp:Panel>
                                        <asp:HiddenField ID="hdnmode" runat="server"/>
                                        <asp:Button ID="btnAddUser" Text="Add User" runat="server" CssClass="buttons" OnClick="btnAddUser_Click"/>

                                        <asp:Panel ID="UserPanel" runat="server" Visible="False">
                                            <table style="width:100%;">
                                              <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td align="right">                                                        <asp:Button
                                                         onclientclick="return confirm(&quot;Are you sure to delete this record?&quot;)"
                                                         ID="lete"  class="buttons" runat="server" Text="Delete" 
                                                         onclick="delete_Click" 
                                                        />
                                                    </td>
                                              </tr>
                                                <tr>
                                                    <td class="style3">
                                                        User Type </td>
                                                    <td class="style7">
                                                        <span class="style6"><strong>*</strong></span></td>
                                                    <td class="style2">
                                                        <asp:DropDownList ID="drpUserType" runat="server" OnSelectedIndexChanged="drpUserType_SelectedIndexChanged" AutoPostBack="True" AppendDataBoundItems="True">
                                                            <asp:ListItem></asp:ListItem>
                                                            
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td rowspan="17">
                                                         <div id="holderRegLists" runat="server" style="width:100px;">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        Select Customer Name:
                                                                        <span style="float:right;">
                                                                            <input type="text" id="customerSearch" placeholder="Search a customer here.." style="width:200px;"/>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right">
                                                                        <asp:RadioButtonList ID="optProductType" runat="server" 
                                                                            RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="optProductType_SelectedIndexChanged" TextAlign="Right" style="font-size:11px;">
                                                                            <asp:ListItem>Compressor, Accessories, Drives , Units</asp:ListItem>
                                                                            <asp:ListItem>Electronics / Mechanicals controls</asp:ListItem>
                                                                            <%--<asp:ListItem>Accessories</asp:ListItem>--%>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div style="height:500px;width:438px;overflow:auto;border:1px solid #e3e3e3">
                                                                            <div id="loading" align="center"><img alt="" src="images/loading.gif" style="vertical-align: -3px" /><span style="font-size:12px;">Loading data...</span></div>
                                                                            <asp:CheckBoxList ID="lbxCustomer" runat="server" Width="500px" Font-Size="11px"></asp:CheckBoxList>
                                                                        </div>
                                                                        <p id="totalSelected"></p>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                   
                                                </tr>
                                                <tr id="rowCSO" runat="server">

                                                    <td class="style3">Sales Office Code </td>
                                                    <td class="style7"> <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtSalesOfficeCode" ErrorMessage="*" ForeColor="red"></asp:RequiredFieldValidator> </td>
                                                    <td class="style2">
                                                       <asp:DropDownList ID="txtSalesOfficeCode" Width="214px" runat="server" AppendDataBoundItems="True">
                                                            <asp:ListItem>-Select-</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td> 
                                                    
                                                </tr>

                                                <tr>
                                                    <td class="style3">
                                                        Company Name </td>
                                                    <td class="style7"><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCompanyName" ErrorMessage="*" ForeColor="red"></asp:RequiredFieldValidator></td>
                                                    <td class="style2">
                                                        <asp:TextBox ID="txtCompanyName" runat="server" Width="200px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style3">
                                                        Username </td>
                                                    <td class="style7"><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtUsername" ErrorMessage="*" ForeColor="red"></asp:RequiredFieldValidator></td>
                                                    
                                                    <td class="style2">
                                                        <asp:TextBox ID="txtUsername" runat="server" Width="200px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style3">
                                                        User First Name </td>
                                                    <td class="style7"><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtUserFirstName" ErrorMessage="*" ForeColor="red"></asp:RequiredFieldValidator></td>
                                                     
                                                    <td class="style2">
                                                        <asp:TextBox ID="txtUserFirstName" runat="server" Width="200px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style3">
                                                        User Last Name </td>
                                                    <td class="style7"><asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtUserLastName" ErrorMessage="*" ForeColor="red"></asp:RequiredFieldValidator></td>
                                                     
                                                    <td class="style2">
                                                        <asp:TextBox ID="txtUserLastName" runat="server" Width="200px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style3">
                                                        Department </td>
                                                     <td class="style7">
                                                         &nbsp;</td>
                                                    <td class="style2">
                                                        <asp:TextBox ID="txtDepartment" runat="server" Width="200px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                 <tr>
                                                    <td class="style3">
                                                        User Title </td>
                                                     <td class="style7">
                                                         &nbsp;</td>
                                                    <td class="style2">
                                                        <asp:TextBox ID="txtUserTitle" runat="server" Width="200px"></asp:TextBox>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="style3">
                                                        Phone Number </td>
                                                    <td class="style7">
                                                        &nbsp;</td>
                                                    <td class="style2">
                                                        <asp:TextBox ID="txtPhoneNumber" runat="server" Width="200px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style3">
                                                        Function </td>
                                                    <td class="style7">
                                                        &nbsp;</td>
                                                    <td class="style2" >
                                                        <asp:TextBox ID="txtFunction" runat="server" Width="200px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style3">
                                                        Email Address </td>
                                                    <td class="style7"><asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtEmailAddress" ErrorMessage="*" ForeColor="red"></asp:RequiredFieldValidator></td>
                                                     
                                                    <td class="style2">
                                                        <asp:TextBox ID="txtEmailAddress" runat="server" Width="200px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr> 
                                                    <td class="style2"> <b>Receive Notification</b></td>
                                                    <td class="style7"></td>
                                                    <td class="style2"><b>Yes</b> </td>
                                                </tr>
                                                <tr>
                                                    <td class="style3">
                                                        Reception Activated </td>
                                                    <td class="style7">
                                                        &nbsp;</td>
                                                    <td class="style2">
                                                        <asp:CheckBox ID="chkReceptionActivated" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style3">
                                                        Analysis Activated </td>
                                                    <td class="style7">
                                                        &nbsp;</td>
                                                    <td class="style2">
                                                        <asp:CheckBox ID="chkAnalysisActivated" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style2">
                                                        Credit Decision Activated </td>
                                                    <td class="style7">
                                                       </td>
                                                    <td class="style2">
                                                        <asp:CheckBox ID="chkCreditDecisionActivated" runat="server" />
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td><b>Change Password</b></td>
                                                    <td></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>New Password  </td>
                                                    <td>&nbsp;</td>
                                                    <td><asp:TextBox ID="txtNewPassword" runat="server" Width="200px" 
                                                            TextMode="Password"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Confirm Password   </td>
                                                    <td>&nbsp;</td>
                                                    <td><asp:TextBox ID="txtConfirmPassword" runat="server" Width="200px" 
                                                            TextMode="Password"></asp:TextBox></td>
                                                </tr>



                                                <tr>
                                                    <td class="style3">
                                                        &nbsp;</td>
                                                    <td class="style7">
                                                        &nbsp;</td>
                                                    <td class="style2">
                                                        <asp:Button ID="btnSave" class="buttons" runat="server" Text="Save" onclick="btnSave_Click" />
                                                        &nbsp;<asp:Button class="buttons" ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false" 
                                                            onclick="BtnCancel_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style3">
                                                        &nbsp;</td>
                                                    <td class="style7">
                                                        &nbsp;</td>
                                                    <td class="style2">
                                                        <asp:Label ID="lblError" runat="server" style="color: #FF0000"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                           
                                        </asp:Panel>

                                        <asp:Panel ID="PanelUnAuthorizedAccess" runat="server" Visible="False">
                                            <b>Authorized users can access this page.</b>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
<script type="text/javascript" src="js/ajaxfunctions/usermanagement.js"></script>

</html>
