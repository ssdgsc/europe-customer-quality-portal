﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using General.AvianCrypto;

public partial class ChangePassword : System.Web.UI.Page
{
    General.AvianCrypto.SecurityMaster sm = new General.AvianCrypto.SecurityMaster();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserID"] == null)
        {
            Response.Redirect("Login.aspx");
            return;
        }

        SecurityMaster sm = new SecurityMaster();
        string user = sm.Decrypt(Session["UserType"].ToString());
        //if (user != "CUSTOMER QUALITY")
        //{
        //    this.pnlMaintenance.Visible = false;
        //}
        //if (!Page.IsPostBack)
        //{

        //}
    }

    protected void cmdSubmit_Click(object sender, EventArgs e)
    {
        Maintenance.UserInformation um = new Maintenance.UserInformation();
        Model.UserInformation ui = um.ValidateLoginCredentials(sm.Decrypt(Session["UserName"].ToString()));

        if (ui.UserID > 0)
        {
            if (!(txtOldPassword.Text.Trim().Equals(ui.UserPassword, StringComparison.CurrentCulture)))
            {
                lblMessage.ForeColor = System.Drawing.Color.Red;
                lblMessage.Text = "Invalid password. Cannot change your password.";
                return;
            }

            //[START] [4/25/17] [jpbersonda] [Old Pass & New Pass comparison]
            if (txtNewPassword.Text.Trim().Equals(ui.UserPassword, StringComparison.CurrentCulture))
            {
                lblMessage.ForeColor = System.Drawing.Color.Red;
                lblMessage.Text = "Old and New Password must not be equal. Please choose a new password";
                return;
            }
            //[END] [4/25/17] [jpbersonda]

            if (!(txtNewPassword.Text.Trim().Equals(txtConfirmNewPassword.Text.Trim())))
            {
                lblMessage.ForeColor = System.Drawing.Color.Red;
                lblMessage.Text = "Passwords do not match.";
                return;
            }

            ui.UserPassword = txtNewPassword.Text.Trim();
            um.ChangePassword(ui);
            um.changeUserStatus(sm.Decrypt(Session["UserID"].ToString()));
            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert" + UniqueID,
            "alert('Password is successfully changed.');", true);
            lblMessage.Text = "";

            if (Session["IsFirstLogin"] != null)
            {
                if ((bool)Session["IsFirstLogin"])
                    Response.Redirect(@"useraccount.aspx");
            }
            else
                Response.Redirect(@"Login.aspx?action=changepass");
        }
        else
        {
            lblMessage.Text = "The username you enter is invalid.";
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("ReturnFormCompressor.aspx");
    }
}
