﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="useraccount.aspx.cs" Inherits="UserAccount" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Europe Customer Quality Portal</title>
    <link type="text/css" rel="Stylesheet" href="css/default.css" />
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
</head>
<body>
    <form id="frmMain" runat="server">
    <div>
        <div id="holder_main" align="center">
            <div id="holderBody" style="width: 980px;">
                <div class="MainPanel2">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr valign="top">
                            <td valign="top" height="100%" class="LeftNavigation">
                                <div id="holderLeftNav">
                                    <table>
                                        <tr>
                                            <td>
                                                <a href="home.aspx">Portal Home</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="ReturnFormCompressor.aspx">Return Authorization Form</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="myreturn.aspx">My Returns</a>
                                            </td>
                                        </tr>
                                       <%-- <tr>
                                            <td>
                                                <a href="statistics.aspx">Statistics</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="deliveries.aspx">Deliveries</a>
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <td>
                                                <a style="color: #f7941d;">Change Password</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="logout.aspx">Logout</a>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <asp:Panel ID="pnlMaintenance" runat="server">
                                        <table>
                                            <tr>
                                                <th><a style="color:Blue">Maintenance</a><br /></th>
                                            </tr>
                                            <tr>
                                                <td><a href="customers.aspx">Customers</a></td>
                                            </tr>
                                            <tr>
                                                <td><a href="usermanagement.aspx">User Management</a></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                            </td>
                            <td valign="top" height="100%" class="DottedBorderLeft" style="width: 770px">
                                <div id="holderMaincontent">
                                    <div id="userInformation">
                                        <h1>
                                            User Information</h1>
                                        <hr />
                                        <%--<div><asp:Label ID="labelMessage" runat="server" ForeColor="Green"></asp:Label></div>--%>
                                        <table>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Label ID="lblMessage" ForeColor="Gray" Font-Bold="true" runat="server">Fill up your user information</asp:Label>
                                                    <asp:ValidationSummary ID="ValidationSummary1" CssClass="pnlLabelMessage" runat="server"
                                                        ValidationGroup="userinformationvalidation" ForeColor="Red" DisplayMode="List" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    First Name
                                                </td>
                                                <td>
                                                    :
                                                    <asp:TextBox runat="server" ID="textboxFirstName"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="userinformationvalidation"
                                                        ControlToValidate="textboxFirstName" Text="*" ForeColor="Red" runat="server"
                                                        ErrorMessage="First Name is required.">
                                                    </asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Last Name
                                                </td>
                                                <td>
                                                    :
                                                    <asp:TextBox runat="server" ID="textboxLastName"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="userinformationvalidation"
                                                        ControlToValidate="textboxLastName" Text="*" ForeColor="Red" runat="server" ErrorMessage="Last Name is required.">
                                                    </asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Title
                                                </td>
                                                <td>
                                                    :
                                                    <%--<asp:TextBox runat="server" ID="textboxTitle"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="userinformationvalidation"
                                                    ControlToValidate="textboxTitle" Text="*" ForeColor="Red" runat="server" ErrorMessage="Title is required.">
                                                    </asp:RequiredFieldValidator>--%>
                                                    <asp:TextBox ID="dropdownTitle" runat="server"/> 
                                                   <%-- <asp:DropDownList runat="server" ID="dropdownTitle">
                                                        <asp:ListItem Value="" Text=""></asp:ListItem>
                                                        <asp:ListItem Value="DR" Text="Dr"></asp:ListItem>
                                                        <asp:ListItem Value="MR" Text="Mr"></asp:ListItem>
                                                        <asp:ListItem Value="MRS" Text="Mrs"></asp:ListItem>
                                                        <asp:ListItem Value="MS" Text="Ms"></asp:ListItem>
                                                    </asp:DropDownList>--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Email
                                                </td>
                                                <td>
                                                    :
                                                    <asp:TextBox runat="server" ID="textboxEmail"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="userinformationvalidation"
                                                        ControlToValidate="textboxEmail" Text="*" ForeColor="Red" runat="server" ErrorMessage="Email is required.">
                                                    </asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Telephone No.
                                                </td>
                                                <td>
                                                    :
                                                    <asp:TextBox runat="server" ID="textboxTelephone"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Function
                                                </td>
                                                <td>
                                                    :
                                                    <asp:TextBox runat="server" ID="textboxFunction"></asp:TextBox>
                                                </td>
                                            </tr>
                                           
                                            <tr>   
                                                <td>
                                                    Receive Notification
                                                </td>
                                                <td align="center">
                                                    Yes&nbsp;&nbsp;&nbsp;No
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    At reception of the goods
                                                </td>
                                                <td align="center">
                                                    <asp:CheckBox runat="server" ID="chkYesReception" onclick="javascript: jQuery('#chkNoReception').prop('checked',false);" />&nbsp;&nbsp;&nbsp;
                                                    <asp:CheckBox runat="server" ID="chkNoReception" onclick="javascript: jQuery('#chkYesReception').prop('checked',false);" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    At analysis of the goods
                                                </td>
                                                <td align="center">
                                                    <asp:CheckBox runat="server" ID="chkYesAnalysis" onclick="javascript: jQuery('#chkNoAnalysis').prop('checked',false);" />&nbsp;&nbsp;&nbsp;
                                                    <asp:CheckBox runat="server" ID="chkNoAnalysis" onclick="javascript: jQuery('#chkYesAnalysis').prop('checked',false);" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Once credit decision is taken
                                                </td>
                                                <td align="center">
                                                    <asp:CheckBox runat="server" ID="chkYesCredit" onclick="javascript: jQuery('#chkNoCredit').prop('checked',false);" />&nbsp;&nbsp;&nbsp;
                                                    <asp:CheckBox runat="server" ID="chkNoCredit" onclick="javascript: jQuery('#chkYesCredit').prop('checked',false);" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td align="center">
                                                    <asp:Button ID="cmdCancel" runat="server" Text="Cancel" class="buttons" CausesValidation="false" OnClick="cmdCancel_Click1" />
                                                    <asp:Button ID="cmdSubmit" runat="server" Text="Submit" class="buttons" ValidationGroup="userinformationvalidation"
                                                        OnClick="cmdSubmit_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </form>
    <script type="text/javascript" src="js/jquery.1.10.2.js" language="javascript"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        jQuery(window).load(function () {
        });
    </script>
</body>
</html>
