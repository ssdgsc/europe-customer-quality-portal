﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="home.aspx.cs" Inherits="home" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link type="text/css" rel="Stylesheet" href="css/default.css" />
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
</head>
<body>
    <form id="frmMain" runat="server">
    <div>
        <div id="holder_main" align="center">
            <div id="holderBody" class="container">
                <div class="MainPanel2">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr valign="top">
                            <td valign="top" height="100%" class="LeftNavigation">
                                <% Response.Write(Helpers.menuNavi(1)); %>
                            </td>
                             <% string sysmode = System.Configuration.ConfigurationManager.AppSettings["SystemMode"]; %>
                            <td valign="top" height="100%" class="DottedBorderLeft" style="width:770px">
                                <div id="holderMaincontent">
                                    <div style="position: relative; top:  0px; padding-right: 5px; padding-bottom: 5px; padding-top: 0px; display: table-cell; text-align: left;">
                                        <div>
                                            <b style="margin: 0; color: black; font-family: Arial!important; text-decoration: none;
                                                font-size: 14px;">Welcome to Returns Portal <% Response.Write((sysmode == "TEST") ? "(TEST SITE)" : "(PRODUCTION)"); %></a></b></div>
                                        <br />
                                        <div>In this application you can:</div>
                                        <div style="padding-left:5em">Create Returns</div>
                                        <div style="padding-left:5em">Submit Claims/Shipments</div>
                                        <div style="padding-left:5em">Review Status of Claims</div>
                                        <br />
                                       <%-- <div>If you need assistance, please refer to our training documentations:</div>
                                        <br />--%>
                                        <%--<div style="padding-left:5em"><a>Warranty Quick Reference Guide</a></div>
                                        <div style="padding-left:5em"><a>Warranty User Manual</a></div>--%>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
