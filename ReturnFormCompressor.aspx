﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReturnFormCompressor.aspx.cs" Inherits="ReturnFormCompressor" MaintainScrollPositionOnPostback="true"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Compressor / Drives / Units - Return Authorization Form</title>
        <link type="text/css" rel="Stylesheet" href="css/default.css" />
        <link type="text/css" rel="Stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"/>
        <script type="text/javascript" src="js/jquery.1.10.2.js"></script> 
        <style type="text/css">
        .defaultText { }
    .defaultTextActive { color: #a1a1a1; font-style: italic; }
        </style>
        
    <script type="text/javascript" language="javascript">
        var listSerialNumber;
        function selectedRequestSpecificTest() {
            alert("Selecting Specific Request may be subject to additional cost.");
        }

        function checkPendingTransaction(recordCount, page) {
            if (recordCount > 0) {
                response = confirm("You have pending transaction. Leaving this page will disregard this.");
                if (response == true) {
                    window.location.href = page;
                }
                else {
                    document.getElementById("optProductType").rows[0].cells[0].childNodes[0].checked = true;
                    document.getElementById("optProductType").selectedIndex = 0;
                    __doPostBack();
                }
            }
            else {
                window.location.href = page;
            }
        }

        //function getSerialNumber() { 
        //    PageMethods.GetSerialNumber(onSuccess, onError);
        //    function onSuccess(result) {
        //        listSerialNumber = result.ResponseItem;
        //    }
        //    function onError(result) {
        //       console.log(result.ErrorMessage)
        //    }
        //}

        // [START] [jpbersonda] [4/27/17]
        function TextUpperCase() {
            jQuery("#txtCompressorSerialNumber").val(jQuery("#txtCompressorSerialNumber").val().toUpperCase());
        }
        // [END] [jpbersonda] [4/27/17]
    </script>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
</head>
<body runat="server" id="bodyform">
    <form id="frmMain" runat="server">
    <asp:HiddenField ID="hfRole" runat="server" />
    <asp:ScriptManager ID="smCompressorForm" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <div>
        <div id="holder_main" align="center">
            <div id="holderBody" style="width: 980px;">
                <div class="MainPanel2">
                <table cellpadding="0" cellspacing="0" border="0">
                    <tr valign="top">
                        <td valign="top" height="100%" class="LeftNavigation">
                          <% Response.Write(Helpers.menuNavi(2)); %>
                        </td>
                        <td valign="top" height="100%" class="DottedBorderLeft" style="width:770px">
                            <div id="holderMaincontent">
                                <!--Return Form Page-->
                                <!--Label Message-->
                                <asp:Panel ID="pnlLabelMessage" runat="server" Visible="false">
                                    <asp:Label ID="LabelMessage" runat="server" Font-Bold="True" ForeColor="Red" Text="Return Form Submitted." Visible="False"></asp:Label>
                                </asp:Panel> 
                                <asp:Panel ID="pnlReturnForm" runat="server">
                                    <div>
                                        <% string sysmode = System.Configuration.ConfigurationManager.AppSettings["SystemMode"]; %>
                                        <h1>Return Form <% Response.Write((sysmode == "TEST") ? "(TEST SITE)" : "(PRODUCTION)"); %></h1>
                                    </div>
                                    <!--Product Types-->
                                    <div>
                                        <asp:RadioButtonList ID="optProductType" runat="server" 
                                            RepeatDirection="Horizontal" AutoPostBack="True" 
                                            onselectedindexchanged="optProductType_SelectedIndexChanged">
                                            <asp:ListItem Selected="True">Compressor / Drives / Units</asp:ListItem>
                                            <asp:ListItem>Electronics / Mechanical controls</asp:ListItem>
                                            <asp:ListItem >Accessories</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                    <hr />
                                    <!--For Compressor-->
                                    <asp:Panel ID="pnlCompressorPage" runat="server">
                                        <!--Label Transaction-->
                                        <div>
                                            <asp:LinkButton ID="lnkViewList" runat="server" CausesValidation="false" onclick="lnkViewList_Click" Font-Underline="True" Font-Italic="True"></asp:LinkButton> 
                                        </div>
                                        <!--Entry Fields-->
                                        <br />
                                        <table width="100%">
                                            <tr>
                                                <td width="154px">Customer Name</td>
                                                <td></td>
                                                <td>          
                                                            <asp:DropDownList ID="ddCustomerList" Width="307px" runat="server" 
                                                                onselectedindexchanged="ddCustomerList_SelectedIndexChanged" 
                                                                AppendDataBoundItems="True" AutoPostBack="true">
                                                                <asp:ListItem> </asp:ListItem>
                                                            </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Customer Location</td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddLocation" runat="server" Width="307px" AppendDataBoundItems="True">
                                                        <asp:ListItem> </asp:ListItem>
                                                    </asp:DropDownList>
                                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddLocation" ErrorMessage="Required field" ForeColor="Red"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span>Reason of Return:</span>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddCompressorClaimLists" runat="server" 
                                                        AutoPostBack="true" Width="307px"
                                                        onselectedindexchanged="ddCompressorClaimLists_SelectedIndexChanged" 
                                                        AppendDataBoundItems="True">
                                                        <asp:ListItem> </asp:ListItem>
                                                        <%--<asp:ListItem>Noisy at Start</asp:ListItem>
                                                        <asp:ListItem>Noisy When Running</asp:ListItem>
                                                        <asp:ListItem>Noisy at Shutdown</asp:ListItem>
                                                        <asp:ListItem>Vibration</asp:ListItem>
                                                        <asp:ListItem>High DLT</asp:ListItem>
                                                        <asp:ListItem>Cooling/Heating capacity too low</asp:ListItem>
                                                        <asp:ListItem>High Power Input</asp:ListItem>
                                                        <asp:ListItem>High Amps</asp:ListItem>
                                                        <asp:ListItem>Low COP/EER</asp:ListItem>
                                                        <asp:ListItem>Short Circuit</asp:ListItem>
                                                        <asp:ListItem>Down to Earth</asp:ListItem>
                                                        <asp:ListItem>Electrical Concern</asp:ListItem>
                                                        <asp:ListItem>Mechanical Concern</asp:ListItem>
                                                        <asp:ListItem>Doesn&#39;t build up pressure</asp:ListItem>
                                                        <asp:ListItem>Seized</asp:ListItem>
                                                        <asp:ListItem>Oil Leak</asp:ListItem>
                                                        <asp:ListItem>Refrigerant Leak</asp:ListItem>
                                                        <asp:ListItem>No Holding Charge</asp:ListItem>
                                                        <asp:ListItem>Seal line rubber plug missing</asp:ListItem>
                                                        <asp:ListItem>Won&#39;t start</asp:ListItem>
                                                        <asp:ListItem>Wrong delivery</asp:ListItem>
                                                        <asp:ListItem>Incorrectly booked</asp:ListItem>
                                                        <asp:ListItem>Incorrectly ordered</asp:ListItem>
                                                        <asp:ListItem>Buy back</asp:ListItem>
                                                        <asp:ListItem>Transport Damage</asp:ListItem>
                                                        <asp:ListItem>Other</asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="req1" runat="server" ControlToValidate="ddCompressorClaimLists" ErrorMessage=" Required field" ForeColor="Red"></asp:RequiredFieldValidator>
                                                
                                                     </td>
                                               <%-- <td>
                                                   <asp:Label ID="lblReasonOfReturnDisclaimer" 
                                                    runat="server" Visible="false" ForeColor="Blue" 
                                                    Text="Buy back only authorized after written approval from Emerson Customer Service">
                                                   </asp:Label>
                                                </td>--%>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblCompressorClaimDescription" runat="server" Visible="false" Text="Return Description: "></asp:Label>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCompressorClaimDescription" runat="server" Visible="false" Width="294px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span>Customer Return Reference:</span>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCustomerClaimReference" runat="server" Width="294px"></asp:TextBox>
                                                    <asp:Image ID="img1" class="help-icon" ToolTip="In case you return several compressors, this is the reference for the whole batch of returns." runat="server" ImageUrl="~/resources/images/tooltip.jpg" />
                                                </td>
                                            </tr>
                                        </table>
                                        <hr />
                                        <div>
                                            <h3>Description of Compressor / Drives / Units</h3>
                                        </div>
                                        <!--Descritpion Fields-->
                                        <table width="100%">
                                            <tr>
                                                <td width="154px">
                                                    <span>Serial Number:</span>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbl1" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                                <td> 
                                                    <asp:TextBox ID="txtCompressorSerialNumber" runat="server" Width="294px" placeholder="Type Serial number here" onkeypress="TextUpperCase()" ></asp:TextBox>
                                                    <span id="pSerialSrc">No result(s) found.</span>
                                                    <asp:Image id="imgSerialLoading" ImageUrl="~/images/loading.gif" runat="server"/> 
                                                 <%--[START] [7/6/2017] -- Change Request | Remove check in serial number auto complete--%>
                                                      <%-- <asp:Button ID="btnCheckCompressorSerialNumber" runat="server" 
                                                        Text="Check" class="buttons" CausesValidation="false"
                                                        onclick="btnCheckCompressorSerialNumber_Click" />--%>
                                                    <%--[END] [7/6/2017] -- Change Request | Remove check in serial number auto complete--%>
                                                    <asp:RequiredFieldValidator ID="req2" runat="server" ControlToValidate="txtCompressorSerialNumber" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                   <span>Model:</span>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCompressorModel" runat="server" Width="294px" style="background-color:rgb(235, 235, 228)" ></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="req3" runat="server" ControlToValidate="txtCompressorModel" ErrorMessage="No Model for the given Serial Number." ForeColor="Red"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span>Creation Date:</span>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCompressorCreationDate" runat="server" Width="294px" Enabled="false" ></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span>Compressor / Drives / Units Reference:</span>
                                                </td>
                                                <td>
                                                    <%--<asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*"></asp:Label>--%>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCompressorReference" runat="server" Width="294px" ></asp:TextBox>
                                                    <asp:Image ID="img2"  class="help-icon" ToolTip="This is the single reference for the compressor. This is a unique number which will be also mentioned on our inspection report and on the credit note if any." runat="server" ImageUrl="~/resources/images/tooltip.jpg" />
                                                    <%--<asp:RequiredFieldValidator ID="req9" runat="server" ControlToValidate="txtCompressorReference" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>--%>
                                                </td>
                                            </tr>
                                        </table>
                                        <hr />
                                        <div>
                                            <h3>Description of Application</h3>
                                        </div>
                                        <!--Description Application-->
                                        <table width="100%">
                                            
                                            <tr> 
                                                <td>
                                                     <span><b>Type Of Return:</b></span>
                                                     <asp:Label ID="Label13" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    <asp:RequiredFieldValidator ID="req4" ControlToValidate="optCompressorTypeOfReturn" runat="server" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>

                                                    <asp:RadioButtonList ID="optCompressorTypeOfReturn" runat="server" CssClass="rdgitem" RepeatDirection="Horizontal" Width="615px" style="float:left;">
                                                        <asp:ListItem>Line Return</asp:ListItem>
                                                        <asp:ListItem>Field Return</asp:ListItem>
                                                        <asp:ListItem>Lab/Sample Test</asp:ListItem>
                                                        <asp:ListItem>Buy Back</asp:ListItem>
                                                        <asp:ListItem>Wrong Delivery</asp:ListItem>
                                                        <asp:ListItem>Transport Damage</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:Image ID="img3" class="help-icon" runat="server" ImageUrl="~/resources/images/tooltip.jpg" style="float:left;"/>

                                                </td>
                                                <td></td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                 
                                                <td>
                                                     <span><b>Application:</b></span>
                                                    <asp:Label ID="Label15" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    <asp:RequiredFieldValidator ID="req5" ControlToValidate="optCompressorApplication" runat="server" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>

                                                    <asp:RadioButtonList ID="optCompressorApplication" runat="server" CssClass="rdgitem" RepeatDirection="Horizontal"
                                                        Width="470px">
                                                        <asp:ListItem>Air Conditioning</asp:ListItem>
                                                        <asp:ListItem>Heat Pump</asp:ListItem>
                                                        <asp:ListItem>Refrigeration</asp:ListItem>
                                                        <asp:ListItem>Transport</asp:ListItem>
                                                        <asp:ListItem>Unknown</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td></td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                               
                                                <td>
                                                    <span><b>Refrigerant:</b></span>
                                                    <asp:Label ID="Label17" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    <asp:RequiredFieldValidator ID="req6" ControlToValidate="optCompressorRefrigerant" runat="server" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                                    <asp:RadioButtonList ID="optCompressorRefrigerant" runat="server"  CssClass="rdgitem"
                                                        RepeatDirection="Horizontal" Width="500px" AutoPostBack="true" 
                                                        onselectedindexchanged="optCompressorRefrigerant_SelectedIndexChanged" >
                                                        <asp:ListItem>R410A</asp:ListItem>
                                                        <asp:ListItem>R407C</asp:ListItem>
                                                        <asp:ListItem>R404A</asp:ListItem>
                                                        <asp:ListItem>R134A</asp:ListItem>
                                                        <asp:ListItem>R744/CO2</asp:ListItem>
                                                        <asp:ListItem>Other</asp:ListItem>
                                                        <asp:ListItem>Unknown</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td></td>
                                                <td>
                                                </td>
                                            </tr>
                                            <asp:Panel ID="pnlCompressorRefrigerant" runat="server" Visible="false">
                                            <tr> 
                                                <td colspan="3">
                                                    
                                                        <span>Please specify:</span>&nbsp;<asp:TextBox ID="txtCompressorRefrigerant" runat="server" Width="262px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="reqCompressorRefrigerant" runat="server" ControlToValidate="txtCompressorRefrigerant" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                                    
                                                </td>
                                            </tr>
                                            </asp:Panel>
                                            <!--Working Point-->
                                            <tr>
                                                <td colspan="3">
                                                    <asp:Panel ID="pnlCompressorWorkingPoint" runat="server" Visible="false">
                                                        <table width="100%">
                                                            <tr>
                                                                <td colspan="4">
                                                                    <asp:Label ID="LabelWorkPt" runat="server" Font-Bold="True" Text="WorkingPoint:  "></asp:Label>&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="style17">
                                                                    <span>to=</span>
                                                                </td>
                                                                <td class="style18">
                                                                    <asp:TextBox ID="txtCompressorWorkingPointTO" runat="server" Width="80px" 
                                                                        MaxLength="6"></asp:TextBox>&nbsp;<span>°C</span>
                                                                </td>
                                                                <td>
                                                                
                                                                    <asp:RegularExpressionValidator ID="req17" runat="server" 
                                                                        ControlToValidate="txtCompressorWorkingPointTO" 
                                                                        ErrorMessage="Please input in Celsius format" 
                                                                        ValidationExpression="^\-{0,1}\d+(.\d+){0,1}$"></asp:RegularExpressionValidator>
                                                                
                                                                </td>
                                                                <td class="style14">
                                                                    <span>tc=</span>
                                                                </td>
                                                                <td class="style18">
                                                                    <asp:TextBox ID="txtCompressorWorkingPointTC" runat="server" Width="80px" 
                                                                        MaxLength="6"></asp:TextBox>
                                                                    <span>°C</span>
                                                                </td>
                                                                <td>
                                                                
                                                                    <asp:RegularExpressionValidator ID="req18" runat="server" 
                                                                        ControlToValidate="txtCompressorWorkingPointTC" 
                                                                        ErrorMessage="Please input in Celsius format" 
                                                                        ValidationExpression="^\-{0,1}\d+(.\d+){0,1}$"></asp:RegularExpressionValidator>
                                                                
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="style17">
                                                                    <span>Superheat=</span>
                                                                </td>
                                                                <td class="style18">
                                                                    <asp:TextBox ID="txtCompressorWorkingPointSuperHeat" runat="server" Width="80px" 
                                                                        MaxLength="6"></asp:TextBox>&nbsp;<span>K</span>
                                                                </td>
                                                                <td>
                                                                
                                                                    <asp:RegularExpressionValidator ID="req19" runat="server" 
                                                                        ControlToValidate="txtCompressorWorkingPointSuperHeat" 
                                                                        ErrorMessage="Please input in Kelvin format" 
                                                                        ValidationExpression="^\-{0,1}\d+(.\d+){0,1}$"></asp:RegularExpressionValidator>
                                                                
                                                                </td>
                                                                <td class="style14">
                                                                    <span>Subcooling=</span>
                                                                </td>
                                                                <td class="style18">
                                                                    <asp:TextBox ID="txtCompressorWorkingPointSubCooling" runat="server" Width="80px" 
                                                                        MaxLength="6"></asp:TextBox>&nbsp<span>K</span>
                                                                </td>
                                                                <td>
                                                                
                                                                    <asp:RegularExpressionValidator ID="req20" runat="server" 
                                                                        ControlToValidate="txtCompressorWorkingPointSubCooling" 
                                                                        ErrorMessage="Please input in Kelvin format" 
                                                                        ValidationExpression="^\-{0,1}\d+(.\d+){0,1}$"></asp:RegularExpressionValidator>
                                                                
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <!--End Working Point-->
                                            <tr>
                                                 
                                                <td colspan="2">
                                                    <span><b>Request:</b></span> 
                                                    <asp:Label ID="Label3" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                     <asp:RequiredFieldValidator ID="reqCompressorRequest1" ControlToValidate="optCompressorRequest1"
                                                        runat="server" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                                    <asp:RadioButtonList ID="optCompressorRequest1" runat="server" CssClass="rdgitem" 
                                                        RepeatDirection="Horizontal" Width="260px" AutoPostBack="True" 
                                                        onselectedindexchanged="optCompressorRequest1_SelectedIndexChanged" >
                                                        <%-- [START] [jpbersonda] [5/25/2017] [changes in request radio button values]]--%>
                                                        <asp:ListItem>Warranty Analysis</asp:ListItem>
                                                        <%--<asp:ListItem>Analysis</asp:ListItem>--%>
                                                        <%--<asp:ListItem>Out of Warranty Analysis</asp:ListItem>--%>
                                                        <asp:ListItem>Specific Request</asp:ListItem>
                                                       <%-- [END] [jpbersonda] [5/25/2017] [changes in request radio button values]]--%>
                                                    </asp:RadioButtonList>
                                                </td>
                                                
                                                <td>
                                                  
                                                </td>
                                            </tr>
                                            <asp:Panel ID="pnlCompressorRequestSpecificTest" runat="server" Visible="false">
                                            <tr> 
                                                <td colspan="2">
                                                    <asp:TextBox ID="txtCompressorRequestSpecificTest" runat="server" Columns="50" 
                                                        Rows="5" TextMode="MultiLine" MaxLength="250" Visible="true"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="reqCompressorRequestSpecificTest" runat="server" ControlToValidate="txtCompressorRequestSpecificTest" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            </asp:Panel>
                                            <tr>
                                                <td> 
                                                    <span><b>Configuration:</b></span>
                                                    <asp:RadioButtonList ID="optCompressorConfiguration" runat="server" CssClass="rdgitem" RepeatDirection="Horizontal" Width="300px">
                                                        <asp:ListItem>Single</asp:ListItem>
                                                        <asp:ListItem>Tandem</asp:ListItem>
                                                        <asp:ListItem>Rack</asp:ListItem>
                                                        <asp:ListItem>Trio</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                
                                                <td>
                                                    <span>Date of Failure:</span>
                                                    <asp:TextBox ID="txtCompressorDateOfFailure" placeholder="DD/MM/YYYY" runat="server" style="margin-left: 74px;"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="req10" runat="server" ErrorMessage="Invalid date format (dd/mm/yyyy)" ControlToValidate="txtCompressorDateOfFailure" ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/-](0?[1-9]|1[012])[\/-]\d{4}$" ForeColor="Red"></asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr> 
                                                <td>
                                                     <span>Running Hours:</span>
                                                    <asp:TextBox ID="txtCompressorRunningHours" runat="server"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' Text="0" MaxLength="5" style="margin-left: 71px;"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="reqCompressorRunningHours" runat="server" ErrorMessage="Please input numbers only" ControlToValidate="txtCompressorRunningHours" ValidationExpression="^[0-9]+$" ForeColor="Red"></asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <hr />
                                    <!--Remarks-->
                                    <div>
                                        <span>Additional information/comments and specific customer instruction</span>
                                    </div>
                                    <div>
                                        <textarea runat="server" id="txtRemarks" name="S2" style="resize:none;height:120px;width:100%;"></textarea>
                                    </div>
                                    <br />
                                    <!--File Upload-->
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <span>Attached document to the claim</span>
                                                </td>
                                                <td> 
                                                    <input type="file" id="UploadFile" name="UploadFile" runat="server" />
                                                    <asp:Image id="imgSuccess" ImageUrl="~/images/success.ico" runat="server"/> 
                                                    <asp:Image id="imgLoading" ImageUrl="~/images/loading.gif" runat="server"/> 
                                                    <asp:HiddenField id="hdnFileName" Value="" runat="server"/>
                                                </td>
                                                <td>
                                                    <asp:Image class="help-icon" ID="img5" ToolTip="Document for this compressor only. If you wish to dowload a document for the complete batch you'll have the possibility to do it later on. Document size limited to 1 MB" runat="server" ImageUrl="~/resources/images/tooltip.jpg" />
                                                   
                                                </td> 
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                     <p class="file-errormessage"></p>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <br />
                                    <!--Buttons-->
                                    <div align="right">
                                        <table>
                                            <tr>
                                                <td>
                                                    <%--<button id="btnAddSameClaim" runat="server"  onclick="btnAddSameClaim_Click" class="buttons" type="submit">
                                                            Add New Compressor<br />Same Reason of Return
                                                    </button>--%>
                                                    <asp:Button ID="btnAddSameClaim" runat="server"  
                                                        Text="Add New Compressor / Drive / Unit &#010; Same Reason of Return"
                                                        class="buttons save" 
                                                        onclick="btnAddSameClaim_Click" UseSubmitBehavior ="false"/>
                                                </td>
                                                <td>
                                                  <%--  <button id="btnAddNewClaim" runat="server"  onclick="btnAddNewClaim_Click" class="buttons" type="submit" >
                                                            Add New Compressor<br />Different Reason of Return
                                                    </button>--%>
                                                    <asp:Button ID="btnAddNewClaim" runat="server"  
                                                        Text="Add New Compressor / Drive / Unit &#010; Different Reason of Return"
                                                        class="buttons save" 
                                                        onclick="btnAddNewClaim_Click" UseSubmitBehavior ="false" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnSaveAndFinalize" runat="server" Text="Save and Finalize"  UseSubmitBehavior ="false"
                                                        CausesValidation="true" class="buttons save" onclick="btnSaveAndFinalize_Click" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                                                        CausesValidation="false" class="buttons save" onclick="btnCancel_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--Hidden Fields-->
                                    <div>
                                        <asp:HiddenField ID="hdnOperation" Value="add" runat="server" />
                                        <asp:HiddenField ID="hdnRecordsCount" Value="0" runat="server" />
                                        <asp:HiddenField ID="hdnIndexToEdit" runat="server" />
                                        <asp:HiddenField ID="hdnItemNumber" runat="server" />
                                    </div>
                                </asp:Panel>
                                <!--Confirm Modify Cancel Page-->
                                <asp:Panel ID="pnlReturnForm2" runat="server">
                                    <br />
                                    <div>You have recorded the following compressor/s:</div>
                                    <br />
                                    <div>
                                    <asp:GridView ID="gridViewAccumulateData" runat="server" 
                                            CssClass="ui_grid"
                                            onrowcommand="gridViewAccumulateData_RowCommand">
                                            <Columns>
                                                <asp:ButtonField ButtonType="Image" CommandName="RemoveSelectedRow"
                                                    ImageUrl="~/resources/images/Delete.png" />
                                                <asp:ButtonField ButtonType="Image" CommandName="EditSelectedRow"
                                                    ImageUrl="~/resources/images/Edit.png" />
                                            </Columns>
                                            <SelectedRowStyle CssClass="ui_gridSelected" />
                                            <AlternatingRowStyle CssClass="ui_gridAltItem" />
                                        </asp:GridView>
                                    </div>
                                    <br />
                                    <div>Please send them to the following address:</div>
                                    <br />
                                        <b>
                                            <% Response.Write(Helpers.getProductLocation(1)); %>
                                        </b>
                                        
                                      <%--  <div><b>Emerson Climate Technologies</b></div>
                                        <div><b>European Warranty Center</b></div>
                                        <div><b>Rue Des Trois Bourdons, 27</b></div>
                                        <div><b>B-4840 Welkenraedt</b></div>
                                        <div><b>Belgium</b></div>--%>
                                    <br />
                                    <!--[START] [4/25/17] [jpbersonda] -->
                                   <%-- <div>
                                        If you are not the person who should received further notification regarding this
                                        claim please fill below information. If blank you’ll receive these information.</div>--%>
                                  <%--  <br />--%>
                                     <!--[END] [4/25/17] [jpbersonda] -->
                                    <div style="display:none">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    Name:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtName" runat="server" Width="200px" Enabled="False"></asp:TextBox>
                                                    <asp:Label ID="rfName" runat="server" Font-Bold="True" ForeColor="Red" Text="*" Visible="False"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Email:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEmail" runat="server" Width="200px" Enabled="False"></asp:TextBox>
                                                    <asp:Label ID="rfEmail" runat="server" Font-Bold="True" ForeColor="Red" Text="*"
                                                        Visible="False"></asp:Label>
                                                    <asp:RegularExpressionValidator ID="regexEmail" runat="server" ErrorMessage="Invalid email address format"
                                                        ControlToValidate="txtEmail" ValidationExpression="^(([^<>()[\]\\.,;:\s@\“]+(\.[^<>()[\]\\.,;:\s@\“]+)*)|(\“.+\“))@((\[(2([0-4]\d|5[0-5])|1?\d{1,2})(\.(2([0-4]\d|5[0-5])|1?\d{1,2})){3} \])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))" ForeColor="Red"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Phone:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtPhone" runat="server" Width="200px" Enabled="False"></asp:TextBox>
                                                    <asp:Label ID="rfPhone" runat="server" Font-Bold="True" ForeColor="Red" Text="*"
                                                        Visible="False"></asp:Label>
                                                    <asp:RegularExpressionValidator ID="regexPhone" runat="server" ErrorMessage="Invalid contact number format"
                                                        ControlToValidate="txtPhone" ValidationExpression="^[-+]?(ext)?[\- \d]+(\d+)+$" ForeColor="Red"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <br />
                                    <div style="display:none">
                                        <asp:CheckBox ID="chkAllow" runat="server" AutoPostBack="True" 
                                            oncheckedchanged="chkAllow_CheckedChanged" />&nbsp;I allow system to use given information
                                        for further contact regarding returns listed above.</div>
                                    <br />
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="cmdBackReturn" runat="server" Text="Add new entry" 
                                                        CausesValidation="true" class="buttons" OnClick="cmdBackReturn_Click" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="cmdConfirm" runat="server" Text="Confirm" 
                                                        CausesValidation="true" class="buttons" onclick="cmdConfirm_Click" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="cmdCancel" runat="server" Text="Cancel" 
                                                        CausesValidation="false" class="buttons" onclick="cmdCancel_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                            </div>
                        </td>
                    </tr>
                </table>
                </div>
            </div>
        </div>
    </div>
    </form>
    <script type="text/javascript" src="js/jquery-ui.js"></script> 
    <script type="text/javascript" src="js/ajaxfunctions/returnformcompressor.js"></script>  

</body>
</html>