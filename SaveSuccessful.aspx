﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SaveSuccessful.aspx.cs" Inherits="SaveSuccessful" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link type="text/css" rel="Stylesheet" href="css/default.css" />
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div id="holder_main" align="center">
            <div id="holderBody" style="width: 980px;">
                <div class="MainPanel2">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr valign="top">
                            <td valign="top" height="100%" class="LeftNavigation">
                                <% Response.Write(Helpers.menuNavi(2)); %>
                            </td>
                            <td valign="top" height="100%" class="DottedBorderLeft" style="width:770px">
                                <div id="holderMaincontent">
                                    <div>
                                        <% string sysmode = System.Configuration.ConfigurationManager.AppSettings["SystemMode"]; %>
                                        <h1>Return Form <% Response.Write((sysmode == "TEST") ? "(TEST SITE)" : "(PRODUCTION)"); %></h1>
                                         
                                    </div>
                                    <hr />
                                    <div>
                                        <asp:Panel ID="pnlResult" runat="server">
                                            <div>
                                                We have registered your request.</div>
                                            <br />
                                            <div>
                                                Following Product(s) :</div>
                                            <br />
                                            <asp:Table ID="tblResult" runat="server" Width="100%">
                                                <asp:TableRow>
                                                </asp:TableRow>
                                            </asp:Table>
                                            <br />
                                            <div>
                                                <%--If you have question or concerns about this returns, please contact--%>
                                                <asp:Label ID="lblMsgBeforeCPerson" runat="server"></asp:Label>
                                                <asp:Label ID="lblContactPerson" runat="server"></asp:Label>

                                            </div>
                                            <div>
                                                Email Address:&nbsp;<asp:Label ID="lblEmailAdd" runat="server"></asp:Label>&nbsp;Phone
                                                Number:&nbsp;<asp:Label ID="lblPhoneNum" runat="server"></asp:Label></div>
                                            <br />
                                            <div>
                                                <%--When you contact, please see reference--%>
                                                Please use reference
                                                <asp:Label ID="lblRefNum" runat="server"></asp:Label>
                                                in any communication related to this return

                                            </div>
                                        </asp:Panel>
                                        <asp:HiddenField ID="hfControlID" runat="server" ></asp:HiddenField>
                                        <br />
                                        <div align="right">
                                            <asp:Button ID="cmdEmailControlReport" runat="server" Text="Email" 
                                                class="buttons" onclick="cmdEmailControlReport_Click" 
                                                CausesValidation="False" Visible="False"  /> &nbsp;

                                            <asp:Button ID="Button1" runat="server" Text="Save as Excel" 
                                                class="buttons"  
                                                CausesValidation="False" onclick="Button1_Click" Visible="False"  /> &nbsp;
                                            <%--<button id="cmdPrintControlReport" class="buttons" type="button" runat="server">Print</button>--%>  &nbsp;

                                            <asp:Button ID="cmdPrintControlReport" runat="server" Text="Print"  class="buttons" /> 
                                            <asp:Button ID="cmdAdd" runat="server" Text="Back to Return Form" OnClick="cmdAdd_Click" class="buttons" />
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </form>
    <script type="text/javascript" src="js/jquery.1.10.2.js"></script>
    <script type="text/javascript">
        jQuery(function () {
            jQuery("#cmdPrintControlReport").click(function (e) {
                var f = getParameterByName("f")

                //f = (f == "accessories") ? "printReturnFormAccessories" : "printReturnFormControl"

                if (f == "accessories")
                    f = "printReturnFormAccessories";
                else if (f == "comp") //compressor
                    f = "printReturnFormCompressor";
                else
                    f = "printReturnFormControl";

                window.open(f + '.aspx?_controlID=' + jQuery("#hfControlID").val() + '&_pageType=pdf&_isNew=1', '_blank') 

            })
        })
       
        function getParameterByName(name, url) {
            if (!url) {
                url = window.location.href;
            }
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

    </script>
</body>

</html>
