﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Script.Serialization;
using System.IO;
using General;
using System.Web.Services;
using System.Globalization;
using General.AvianCrypto; 
using System.Web.Services.Protocols;
using System.Web.Script;
using System.Web.Script.Services;
using Maintenance;
 
public partial class productlocation : System.Web.UI.Page  
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserID"] == null)
        {
            Response.Redirect("login.aspx");
            return;
        }
        bindProductLocation();
        
    }

    public void bindProductLocation() {
        ConnectionMaster cm = new ConnectionMaster();
        ProductLocationGrid.DataSource = cm.retrieveDataTable("Select * FROM ProductType");
        ProductLocationGrid.DataBind();
    }

    protected void ProductLocationGrid_SelectedIndexChanged(object sender, EventArgs e)
    {
        Label lblID = (Label)ProductLocationGrid.SelectedRow.Cells[1].FindControl("lblProductLocationID");
        DataTable dt = getProductDataByID(lblID.Text);
        DataRow row = dt.Rows[0];
        spanProductType.InnerText = row["ProductType"].ToString();
        txtlocation.InnerText = row["Location"].ToString();
        toggleEditMode(true);
    }
   
    //FUNCTION

    public void toggleEditMode(bool toggle) 
    {
        ProductLocationGrid.Visible = !toggle;
        ProductLocationPanel.Visible = toggle;

    
    }

    public DataTable getProductDataByID(string ID) {
        ConnectionMaster cm = new ConnectionMaster();
        return cm.retrieveDataTable("Select * FROM ProductType WHERE ID = " + ID);

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        bindProductLocation();
        toggleEditMode(false);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        ConnectionMaster cm = new ConnectionMaster();
        Label lblID = (Label)ProductLocationGrid.SelectedRow.Cells[1].FindControl("lblProductLocationID");
        string sql = "UPDATE ProductType SET Location = '" + txtlocation.InnerText + "' WHERE ID = "+ lblID.Text ;
        cm.executeCommand(sql, false);
        Page.ClientScript.RegisterStartupScript(this.GetType(), "alert" + UniqueID,
              "alert('1 record successfully updated.');", true);
        bindProductLocation();
        toggleEditMode(false);
    }
}