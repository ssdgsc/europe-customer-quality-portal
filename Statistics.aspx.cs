﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

using System.Web.UI.WebControls;
using System.Data;
using General.AvianCrypto;

public partial class Statistics : System.Web.UI.Page
{

    General.AvianCrypto.SecurityMaster sm = new General.AvianCrypto.SecurityMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            // CHeck if admin to show/hide the maintenancne
            General.AvianCrypto.SecurityMaster gasm = new General.AvianCrypto.SecurityMaster();
            SecurityMaster sm = new SecurityMaster();
            string user = sm.Decrypt(Session["UserType"].ToString());
            if (user != "CUSTOMER QUALITY")
            {
                this.pnlMaintenance.Visible = false;
            }

            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
                return;
            }
            Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
            if (!Page.IsPostBack)
            {

                string usertype = gasm.Decrypt(Session["UserType"].ToString());
                int userID = int.Parse(sm.Decrypt(Session["UserID"].ToString()));
                this.ddCustomerList.DataSource = mrfd.getCustomerList(userID, usertype);
                this.ddCustomerList.DataValueField = "CUSTOMERNUMBER";
                this.ddCustomerList.DataTextField = "CUSTOMERNAME";
                this.ddCustomerList.DataBind();
                this.ddCustomerList.Items.Add("-- ALL --");
                this.ddCustomerList.SelectedIndex = this.ddCustomerList.Items.Count - 1;

                chrtStatistics.DataSource = mrfd.getStatisticsByYOM(userID, this.ddCustomerList.SelectedValue);
                chrtStatistics.DataBind();

                chrtStatistics.ChartAreas[0].AxisX.Title = "Year of Manufacturing";
            }

            chrtStatistics.Series[0].XValueMember = "YEAR";
            chrtStatistics.Series[0].YValueMembers = "PPM";
            chrtStatistics.Series[0].IsValueShownAsLabel = false;

            chrtStatistics.ChartAreas[0].AxisX.Maximum = DateTime.Now.Year;
            chrtStatistics.ChartAreas[0].AxisX.Minimum = DateTime.Now.Year - 6;
            chrtStatistics.ChartAreas[0].AxisY.Title = "Pieces";
            chrtStatistics.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            chrtStatistics.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
        }
        catch
        {
            Response.Redirect("Login.aspx");
            return;
        }
    }


    protected void btnGo_Click(object sender, EventArgs e)
    {
        try
        {
            Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
            int userID = int.Parse(sm.Decrypt(Session["UserID"].ToString()));
            if (this.hdnLastButtonClicked.Value == "ByYOR")
            {
                chrtStatistics.ChartAreas[0].AxisX.Title = "Year of Return";
                chrtStatistics.DataSource = mrfd.getStatisticsByYOR(userID, this.ddCustomerList.SelectedValue);
            }
            else if (this.hdnLastButtonClicked.Value == "ByModel")
            {
                chrtStatistics.ChartAreas[0].AxisX.Title = "Year";
                chrtStatistics.DataSource = mrfd.getStatisticsByModel(userID, this.ddCustomerList.SelectedValue);
            }
            else
            {
                chrtStatistics.ChartAreas[0].AxisX.Title = "Year of Manufacturing";
                chrtStatistics.DataSource = mrfd.getStatisticsByYOM(userID, this.ddCustomerList.SelectedValue);
            }

            chrtStatistics.DataBind();

            chrtStatistics.Series[0].XValueMember = "YEAR";
            chrtStatistics.Series[0].YValueMembers = "PPM";
        }
        catch (Exception ex)
        {
            LabelMessage.Text = ex.GetBaseException().ToString();
            LabelMessage.Visible = true;
        }
    }

    protected void btnByYOA_Click(object sender, EventArgs e)
    {
        Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
        int userID = int.Parse(sm.Decrypt(Session["UserID"].ToString()));
        chrtStatistics.DataSource = mrfd.getStatisticsByYOR(userID, this.ddCustomerList.SelectedValue);
        chrtStatistics.DataBind();

        chrtStatistics.Series[0].XValueMember = "YEAR";
        chrtStatistics.Series[0].YValueMembers = "PPM";

        chrtStatistics.ChartAreas[0].AxisX.Title = "Year of Return";
        this.hdnLastButtonClicked.Value = "ByYOR";
    }

    protected void btnByYOM_Click(object sender, EventArgs e)
    {
        Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
        int userID = int.Parse(sm.Decrypt(Session["UserID"].ToString()));
        chrtStatistics.DataSource = mrfd.getStatisticsByYOM(userID, this.ddCustomerList.SelectedValue);
        chrtStatistics.DataBind();

        chrtStatistics.Series[0].XValueMember = "YEAR";
        chrtStatistics.Series[0].YValueMembers = "PPM";

        chrtStatistics.ChartAreas[0].AxisX.Title = "Year of Manufacturing";
        this.hdnLastButtonClicked.Value = "ByYOM";
    }
    protected void btnByModel_Click(object sender, EventArgs e)
    {
        Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
        int userID = int.Parse(sm.Decrypt(Session["UserID"].ToString()));
        chrtStatistics.DataSource = mrfd.getStatisticsByModel(userID, this.ddCustomerList.SelectedValue);
        chrtStatistics.DataBind();

        chrtStatistics.Series[0].XValueMember = "YEAR";
        chrtStatistics.Series[0].YValueMembers = "PPM";

        chrtStatistics.ChartAreas[0].AxisX.Title = "Year";
        this.hdnLastButtonClicked.Value = "ByModel";
    }
}
