﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReturnFormAccessories.aspx.cs" Inherits="ReturnFormAccessories" MaintainScrollPositionOnPostback="true" EnableEventValidation="false"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Accessories - Return Authorization Form</title>
        <link type="text/css" rel="Stylesheet" href="css/default.css" />
        <link type="text/css" rel="Stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"/>
        <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.0.js"></script> 
        
    <script type="text/javascript" language="javascript">
        function selectedRequestSpecificTest() {
            alert("Selecting Specific Request may be subject to additional cost.");
        }
        function calculateWarranty() {
            var datacode = document.getElementById("txtControlDataCode").value;
            var warranty = document.getElementById("txtControlWarranty");
            PageMethods.getWarranty(datacode, onSuccess, onError);
            function onSuccess(result) {
                warranty.value = result;
                document.getElementById("hdnControlWarranty").value = result;
            }
            function onError(result) {
                alert("Can not calculate Warranty from the given Datacode.");
            }
        }
        function checkPendingTransaction(recordCount, page) {
            if (recordCount > 0) {
                response = confirm("You have pending transaction. Leaving this page will disregard this.");
                if (response == true) {
                    window.location.href = page;
                }
                else {
                    document.getElementById("optProductType").rows[0].cells[0].childNodes[0].checked = true;
                    document.getElementById("optProductType").selectedIndex = 0;
                    __doPostBack();
                }
            }
            else {
                window.location.href = page;
            }
        }


    </script>
    <style type="text/css">
        .style10
        {
            width: 85px;
        }
        .style11
        {
            width: 90px;
        }
        .style12
        {
            width: 170px;
            height: 30px;
        }
        .style13
        {
            height: 30px;
        }
        .style14
        {
            width: 70px;
            text-align: right;
        }
        .style17
        {
            width: 65px;
            text-align: right;
        }
        .style18
        {
            width: 122px;
        }
        .style21
        {
            width: 170px;
        }
        .style1
        {
            font-size: small;
        }
        </style>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
</head>
<body>
    <form id="frmMain" runat="server">
    <asp:HiddenField ID="hfRole" runat="server" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <div>
        <div id="holder_main" align="center">
            <div id="holderBody" style="width: 980px;">
                <div class="MainPanel2">
                <table cellpadding="0" cellspacing="0" border="0">
                    <tr valign="top">
                        <td valign="top" height="100%" class="LeftNavigation">
                             <% Response.Write(Helpers.menuNavi(2)); %>
                        </td>
                        <td valign="top" height="100%" class="DottedBorderLeft" style="width:770px"> <!--width: 770px -->
                            <div id="holderMaincontent">
                                <!--Return Form Page-->
                                <!--Label Message-->
                                <div>
                                    <asp:Label ID="LabelMessage" runat="server" Font-Bold="True" ForeColor="Red" Text="Return Form Submitted." Visible="False"></asp:Label>
                                </div>
                                <asp:Panel ID="pnlReturnForm" runat="server">
                                    <div>
                                         <% string sysmode = System.Configuration.ConfigurationManager.AppSettings["SystemMode"]; %>
                                        <h1>Return Form <% Response.Write((sysmode == "TEST") ? "(TEST SITE)" : "(PRODUCTION)"); %></h1>
                                    </div>
                                    <!--Product Types-->
                                    <div>
                                        <asp:RadioButtonList ID="optProductType" runat="server" 
                                            RepeatDirection="Horizontal" AutoPostBack="True" 
                                            onselectedindexchanged="optProductType_SelectedIndexChanged">
                                            <asp:ListItem >Compressor / Drives / Units</asp:ListItem>
                                            <asp:ListItem >Electronics / Mechanical controls</asp:ListItem>
                                            <asp:ListItem Selected="True">Accessories</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                    <hr />
                                    <!--For Control-->
                                    <asp:Panel ID="pnlControlPage" runat="server">
                                        <!--Label Transaction-->
                                        <div>
                                            <asp:LinkButton ID="lnkViewList" runat="server" CausesValidation="false" onclick="lnkViewList_Click" Font-Underline="True" Font-Italic="True"></asp:LinkButton> 
                                        </div>
                                        <br />
                                        <!--Entry Fields-->
                                        <table width="100%">
                                            <tr>
                                                <td width="154px">Customer Name</td>
                                                <td></td>
                                                <td>
                                                    <asp:DropDownList ID="ddCustomerList" runat="server" AutoPostBack="True" 
                                                        OnSelectedIndexChanged="ddCustomerList_SelectedIndexChanged" 
                                                        AppendDataBoundItems="True" Width="308px">
                                                        <asp:ListItem> </asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddCustomerList" ErrorMessage="Required field" ForeColor="Red"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>

                                             <tr>
                                                <td>Customer Location</td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddLocation" runat="server" Width="308px" AppendDataBoundItems="True"  >
                                                        <asp:ListItem> </asp:ListItem>
                                                    </asp:DropDownList>
                                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddLocation" ErrorMessage="Required field" ForeColor="Red"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            

                                            <tr>
                                                <td>
                                                    <span>Claim:</span>
                                                </td>
                                                <td>
                                                    <asp:Label ID="LabelAsk" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddControlClaimLists" Width="308px" runat="server" AutoPostBack="true" 
                                                        onselectedindexchanged="ddControlClaimLists_SelectedIndexChanged" AppendDataBoundItems="True">
                                                        <asp:ListItem> </asp:ListItem>
                                                     <%--   <asp:ListItem>External leakage</asp:ListItem>
                                                        <asp:ListItem>Internal / Seat leakage</asp:ListItem>
                                                        <asp:ListItem>Setting out of factory setting</asp:ListItem>
                                                        <asp:ListItem>Functional Issue</asp:ListItem>
                                                        <asp:ListItem>Electical Issue</asp:ListItem>
                                                        <asp:ListItem>Wrong delivery</asp:ListItem>
                                                        <asp:ListItem>Incorrectly ordered</asp:ListItem>
                                                        <asp:ListItem>Buy Back</asp:ListItem>
                                                        <asp:ListItem>Transport Damage</asp:ListItem>
                                                        <asp:ListItem>Other</asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="req8" runat="server" 
                                                      ControlToValidate="ddControlClaimLists" ErrorMessage="Required field" 
                                                        ForeColor="Red">
                                                    </asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblReasonOfReturnDisclaimer" 
                                                    runat="server" Visible="false" ForeColor="Blue" CssClass="returnForm-disclaimer"
                                                    Text="Buy back only authorized after written approval from Emerson Customer Service">
                                                   </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblControlClaimDescription" runat="server" Visible="false" Text="Claim Description: "></asp:Label>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtControlClaimDescription" runat="server" Visible="false" 
                                                        Width="294px" MaxLength="100"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span>Customer Claim Reference:</span>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtControlCustomerClaimReference" runat="server" Width="295px" 
                                                        MaxLength="50"></asp:TextBox>
                                                    <asp:Image class="help-icon" ID="img4" ToolTip="This is the general reference for your whole claim" runat="server" ImageUrl="~/resources/images/tooltip.jpg" />
                                                </td>
                                            </tr>
                                        </table>
                                        <hr />
                                        <div>
                                            <h3>Description of Accessories</h3>
                                        </div>
                                        <!--Description Fields-->
                                        <table width="100%">
                                            <tr>
                                                <td width="154px">
                                                    <span>Item number:</span>  <%--PCN number:--%>
                                                </td>
                                                <td>
                                                    <asp:Label ID="LabelAsk0" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtControlPCNNumber" runat="server" Width="294px" 
                                                        MaxLength="15" placeholder="Type Item number here.."></asp:TextBox>
                                                    <span id="pPCNSrc">No result(s) found.</span>
                                                    <asp:Image id="imgPCNLoading" ImageUrl="~/images/loading.gif" runat="server"/>  
                                                <%--[START] [7/6/2017] -- Change Request | Remove check in PCN number auto complete--%>
                                                       <%--  <asp:Button ID="btnCheckControlPCNNumber" runat="server" 
                                                        CausesValidation="false" Text="Check" class="buttons" 
                                                        onclick="btnCheckControlPCNNumber_Click" />--%>
                                                <%--[END] [7/6/2017] -- Change Request | Remove check in PCN number auto complete--%>
                                                    <span id="empty-message"></span>
                                                    <asp:RequiredFieldValidator ID="req9" runat="server" ControlToValidate="txtControlPCNNumber" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr ID="EmptyPcn" runat="server" visible="false">
                                                <td></td>
                                                <td colspan="2">
                                                    <asp:Label runat="server" ID ="Label3" Text="Please check the PCN number or use drop down list on product field to find correct pcn" ForeColor="Red"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Item Description<span>:</span> <%--Product--%>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="txtControlModel" runat="server" AutoPostBack="true" Width="308px" OnSelectedIndexChanged="txtControlModel_SelectedIndexChanged">
                                                        <asp:ListItem Value="" disabled></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="req10" runat="server" ControlToValidate="txtControlModel" ErrorMessage="No Model for the given PCN Number." ForeColor="Red"></asp:RequiredFieldValidator>
                                                    
                                                    <asp:HiddenField ID="hdnControlProductGroup" runat="server" />
                                                    <asp:HiddenField ID="hdnControlLine" runat="server" />
                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                
                                                <td>
                                                    <span>Datacode:</span>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtControlDataCode" runat="server" Width="294px" MaxLength="4" placeholder="'year/calendar week' (yycw)"></asp:TextBox> 
                                                    <asp:RegularExpressionValidator ID="req11" ControlToValidate="txtControlDataCode" ValidationExpression="^[0-9]{4}" runat="server" ErrorMessage="Format should be YYCW" ForeColor="red"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span>Warranty:</span>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtControlWarranty" runat="server" Width="294px" 
                                                        MaxLength="20" disabled='disabled' placeholder="dd/mm/yyyy"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnControlWarranty" runat="server"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span>Qty:</span>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtControlQty" runat="server" Width="294px" MaxLength="6" ></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="req12" ControlToValidate="txtControlQty" ValidationExpression="^[0-9]+$" runat="server" ErrorMessage="Invalid Qty" ForeColor="Red"></asp:RegularExpressionValidator>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtControlQty" ErrorMessage="Required field." ForeColor="Red" style="margin-left: -61px;"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span>Date of Claim:</span>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtControlDateOfClaim" runat="server" Width="294px" 
                                                        Enabled="False" MaxLength="10"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span>Accessories Reference:</span>
                                                </td>
                                                <td>
                                                    <%--<asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*"></asp:Label>--%>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtComponentReference" runat="server" Width="294px" 
                                                        MaxLength="50" ></asp:TextBox>
                                                    <asp:Image class="help-icon" ID="img2" ToolTip="This is the single reference for the component. This is a unique number which will be also mentioned on our inspection report and on the credit note if any." runat="server" ImageUrl="~/resources/images/tooltip.jpg" />
                                                    <%--<asp:RequiredFieldValidator ID="req2" runat="server" ControlToValidate="txtComponentReference" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>--%>
                                                </td>
                                            </tr>
                                        </table>
                                        <hr />
                                        <div>
                                            <h3>Description of Application</h3>
                                        </div>
                                        <!--Description Application-->
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <span>Type Of Return:</span>
                                                </td>
                                                <td>
                                                    <asp:Label ID="LabelAsk2" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="optControlTypeOfReturn" runat="server" style="float:left;" 
                                                        RepeatDirection="Horizontal" Width="500px"  >
                                                        <asp:ListItem>Line Return</asp:ListItem>
                                                        <asp:ListItem>Field Return</asp:ListItem>
                                                        <asp:ListItem>Lab/Sample Test</asp:ListItem>
                                                        <asp:ListItem>Commercial Return</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:Image class="help-icon" ID="Image5" runat="server" ImageUrl="~/resources/images/tooltip.jpg" style="float:left;"/>
                                                </td>
                                                <td></td>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="req13" ControlToValidate="optControlTypeOfReturn" runat="server" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style10">
                                                    <span>Application:</span>
                                                </td>
                                                <td>
                                                    <asp:Label ID="LabelAsk3" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="optControlApplication" runat="server" RepeatDirection="Horizontal" Width="470px">
                                                        <asp:ListItem>Air Conditioning</asp:ListItem>
                                                        <asp:ListItem>HeatPump</asp:ListItem>
                                                        <asp:ListItem>Refrigeration</asp:ListItem>
                                                        <asp:ListItem>Transport</asp:ListItem>
                                                        <asp:ListItem>Unknown</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td></td>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="req14" ControlToValidate="optControlApplication" runat="server" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style10">
                                                    <span>Refrigerant:</span>
                                                </td>
                                                <td>
                                                    <asp:Label ID="LabelAsk4" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="optControlRefrigerant" runat="server" 
                                                        RepeatDirection="Horizontal" Width="500px" AutoPostBack="true" 
                                                        onselectedindexchanged="optControlRefrigerant_SelectedIndexChanged" >
                                                        <asp:ListItem>R410A</asp:ListItem>
                                                        <asp:ListItem>R407C</asp:ListItem>
                                                        <asp:ListItem>R404A</asp:ListItem>
                                                        <asp:ListItem>R134A</asp:ListItem>
                                                        <asp:ListItem>R744/CO2</asp:ListItem>
                                                        <asp:ListItem>Other</asp:ListItem>
                                                        <asp:ListItem>Unknown</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td></td>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="req15" ControlToValidate="optControlRefrigerant"
                                                        runat="server" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style10">
                                                </td>
                                                <td>
                                                </td>
                                                <td colspan="3">
                                                    <asp:Panel ID="pnlControlRefrigerant" runat="server" Visible="false">
                                                        <span>Please specify:</span>&nbsp;<asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            <asp:TextBox ID="txtControlRefrigerant" 
                                                            runat="server" Width="262px" MaxLength="20"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtControlRefrigerant"
                                                        runat="server" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%">
                                           <!--Working Point-->
                                            <tr>
                                                <td>
                                                    <asp:Panel ID="pnlControlWorkingPoint" runat="server" Visible="false">
                                                        <table width="100%">
                                                            <tr>
                                                                <td colspan="4">
                                                                    <asp:Label ID="LabelWorkPt" runat="server" Font-Bold="True" Text="WorkingPoint:  "></asp:Label>&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="style17">
                                                                    <span>to=</span>
                                                                </td>
                                                                <td class="style18">
                                                                    <asp:TextBox ID="txtControlWorkingPointTO" runat="server" Width="80px" 
                                                                        MaxLength="6"></asp:TextBox>&nbsp;<span>°C</span>
                                                                </td>
                                                                <td>
                                                                
                                                                    <asp:RegularExpressionValidator ID="req17" runat="server" 
                                                                        ControlToValidate="txtControlWorkingPointTO" 
                                                                        ErrorMessage="Please input in Celsius format" 
                                                                        ValidationExpression="^\-{0,1}\d+(.\d+){0,1}$"></asp:RegularExpressionValidator>
                                                                
                                                                </td>
                                                                <td class="style14">
                                                                    <span>tc=</span>
                                                                </td>
                                                                <td class="style18">
                                                                    <asp:TextBox ID="txtControlWorkingPointTC" runat="server" Width="80px" 
                                                                        MaxLength="6"></asp:TextBox>
                                                                    <span>°C</span>
                                                                </td>
                                                                <td>
                                                                
                                                                    <asp:RegularExpressionValidator ID="req18" runat="server" 
                                                                        ControlToValidate="txtControlWorkingPointTC" 
                                                                        ErrorMessage="Please input in Celsius format" 
                                                                        ValidationExpression="^\-{0,1}\d+(.\d+){0,1}$"></asp:RegularExpressionValidator>
                                                                
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="style17">
                                                                    <span>Superheat=</span>
                                                                </td>
                                                                <td class="style18">
                                                                    <asp:TextBox ID="txtControlWorkingPointSuperHeat" runat="server" Width="80px" 
                                                                        MaxLength="6"></asp:TextBox>&nbsp;<span>K</span>
                                                                </td>
                                                                <td>
                                                                
                                                                    <asp:RegularExpressionValidator ID="req19" runat="server" 
                                                                        ControlToValidate="txtControlWorkingPointSuperHeat" 
                                                                        ErrorMessage="Please input in Kelvin format" 
                                                                        ValidationExpression="^\-{0,1}\d+(.\d+){0,1}$"></asp:RegularExpressionValidator>
                                                                
                                                                </td>
                                                                <td class="style14">
                                                                    <span>Subcooling=</span>
                                                                </td>
                                                                <td class="style18">
                                                                    <asp:TextBox ID="txtControlWorkingPointSubCooling" runat="server" Width="80px" 
                                                                        MaxLength="6"></asp:TextBox>&nbsp<span>K</span>
                                                                </td>
                                                                <td>
                                                                
                                                                    <asp:RegularExpressionValidator ID="req20" runat="server" 
                                                                        ControlToValidate="txtControlWorkingPointSubCooling" 
                                                                        ErrorMessage="Please input in Kelvin format" 
                                                                        ValidationExpression="^\-{0,1}\d+(.\d+){0,1}$"></asp:RegularExpressionValidator>
                                                                
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                        <!--End Working Point-->   
                                        <table width="100%">
                                            
                                            <tr>
                                                <td >
                                                    <span>Request:</span>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                                <td colspan="2">
                                                    <asp:RadioButtonList ID="optControlRequest1" runat="server" 
                                                        RepeatDirection="Horizontal" Width="260px" AutoPostBack="True" 
                                                        onselectedindexchanged="optControlRequest1_SelectedIndexChanged" >
                                                        <asp:ListItem>Warranty Analysis</asp:ListItem>
                                                     <%--   <asp:ListItem>Analysis</asp:ListItem>
                                                        <asp:ListItem>Out of Warranty Analysis</asp:ListItem>--%>
                                                        <asp:ListItem>Specific Request</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                
                                                <td>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="optControlRequest1"
                                                        runat="server" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style11">
                                                </td>
                                                <td>
                                                </td>
                                                <td colspan="2">
                                                    <asp:TextBox ID="txtControlRequestSpecificTest" runat="server" Columns="50" 
                                                        Rows="5" Visible="false"
                                                        TextMode="MultiLine" MaxLength="250"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style11">
                                                    <span>Configuration:</span>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="optControlConfiguration" runat="server" RepeatDirection="Horizontal" Width="300px">
                                                        <asp:ListItem>Single</asp:ListItem>
                                                        <asp:ListItem>Tandem</asp:ListItem>
                                                        <asp:ListItem>Rack</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%">
                                            <tr>
                                                <td class="style12">
                                                    <span>Date of Failure:</span>
                                                </td>
                                                <td class="style13"></td>
                                                <td class="style13">
                                                    <asp:TextBox ID="txtControlDateOfFailure" runat="server" Width="115px"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="req3"   runat="server" ErrorMessage="Invalid date format (dd/mm/yyyy)" ControlToValidate="txtControlDateOfFailure" ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/-](0?[1-9]|1[012])[\/-]\d{4}$" ForeColor="Red"></asp:RegularExpressionValidator>
                                                </td>
                                                <td class="style13"></td>
                                            </tr>
                                            <tr>
                                                <td class="style21">
                                                    <span>Running Hours:</span>
                                                </td>
                                                <td></td>
                                                <td>
                                                    <asp:TextBox ID="txtControlRunningHours" runat="server" MaxLength="5" 
                                                        Width="115px" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>0</asp:TextBox>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr style="display:none;">
                                                <td class="style21">
                                                    Date of part income<span>:</span>
                                                </td>
                                                <td></td>
                                                <td>
                                                    <asp:TextBox ID="txtControlDateOfPartIncome" runat="server" MaxLength="10" 
                                                        Enabled="False" Width="115px"></asp:TextBox>

                                                    <asp:HiddenField ID="hdnControlCWOfPartIncome" runat="server"/>
                                                    <asp:HiddenField ID="hdnControlMonthIncome" runat="server"/>
                                                    <asp:HiddenField ID="hdnControlFYIncome" runat="server"/>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <hr />
                                    <!--Remarks-->
                                    <div>
                                        <span>Additional information/comments and specific customer instruction</span>
                                    </div>
                                    <div>
                                        <textarea runat="server" id="txtRemarks" name="S2" style="resize:none;height:120px;width:100%;"></textarea>
                                    </div>
                                    <br />
                                    <!--File Upload-->
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <span>Attached document to the claim</span>
                                                </td>
                                                <td> 
                                                    <input type="file" id="UploadFile" name="UploadFile" runat="server" />
                                                    <asp:Image id="imgSuccess" ImageUrl="~/images/success.ico" runat="server"/> 
                                                    <asp:Image id="imgLoading" ImageUrl="~/images/loading.gif" runat="server"/> 
                                                    <asp:HiddenField id="hdnFileName" Value="" runat="server"/>
                                                </td>
                                                <td>
                                                    <asp:Image class="help-icon" ID="img5" ToolTip="Document for this compressor only. If you wish to dowload a document for the complete batch you'll have the possibility to do it later on. Document size limited to 1 MB" runat="server" ImageUrl="~/resources/images/tooltip.jpg" />
                                                   
                                                </td> 
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                     <p class="file-errormessage"></p>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <br />
                                    <!--Buttons-->
                                        <div align="right">
                                            <table>
                                                <tr>
                                                    <td> 
                                                       <%-- <button id="btnAddSameControl" runat="server"  onclick="btnAddSameControl_Click" class="buttons" >
                                                            Add New Control<br />Same Reason of Return
                                                        </button>--%>
                                                        <asp:Button ID="btnAddSameControl" runat="server" 
                                                            Text="Add New Accessories Same Reason of Return" class="buttons save" 
                                                            onclick="btnAddSameControl_Click" />
                                                    </td>
                                                    <td>
                                                        <%-- <button id="btnAddNewControl" runat="server"  onclick="btnAddNewControl_Click" class="buttons" >
                                                            Add New Control<br />Different Reason of Return
                                                        </button>--%>
                                                        <asp:Button ID="btnAddNewControl" runat="server" 
                                                            Text="Add New Accessories Different Reason of Return" class="buttons save" 
                                                            onclick="btnAddNewControl_Click" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnControlSaveAndFinalize" runat="server" 
                                                            Text="Save and Finalize" CausesValidation="true" class="buttons save" 
                                                            onclick="btnControlSaveAndFinalize_Click" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnControlCancel" runat="server" Text="Cancel" 
                                                            CausesValidation="false" class="buttons save" onclick="btnControlCancel_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    
                                    <!--Hidden Fields-->
                                    <div>
                                        <asp:HiddenField ID="hdnOperation" Value="add" runat="server" />
                                        <%--<asp:HiddenField ID="hdnLastClicked" runat="server" />--%>
                                        <asp:HiddenField ID="hdnRecordsCount" Value="0" runat="server" />
                                        <asp:HiddenField ID="hdnIndexToEdit" runat="server" />
                                        <%--<asp:HiddenField ID="hdnClaimID" runat="server" />--%>
                                        <asp:HiddenField ID="hdnItemNumber" runat="server" /> <%--PCNNUMber--%>
                                        <asp:HiddenField ID="hdnItemDescription" runat="server"/> <%--Model/Product--%>
                                    </div>
                                </asp:Panel>
                                <!--Confirm Modify Cancel Page-->
                                <asp:Panel ID="pnlReturnForm2" runat="server">
                                    <div>
                                        <asp:Label ID="lblMessage" runat="server" Font-Bold="True" ForeColor="Red" Visible="False"></asp:Label>
                                    </div>
                                    <br />
                                    <div>
                                        You have recorded following products:</div>
                                    <br />
                                    <div>
                                        <asp:GridView CssClass="ui_grid" ID="gridViewAccumulateData"
                                            runat="server" onrowcommand="gridViewAccumulateData_RowCommand" 
                                            onselectedindexchanged="gridViewAccumulateData_SelectedIndexChanged">
                                            <Columns>
                                                <asp:ButtonField ButtonType="Image" CommandName="RemoveSelectedRow"
                                                    ImageUrl="~/resources/images/Delete.png" />
                                                <asp:ButtonField ButtonType="Image" CommandName="EditSelectedRow"
                                                    ImageUrl="~/resources/images/Edit.png" />
                                            </Columns>
                                            <SelectedRowStyle CssClass="ui_gridSelected" />
                                        </asp:GridView>
                                    </div>
                                    <br />
                                    <div>
                                        Please send them to following address:</div>
                                    <br />
                                    <div>
                                        <span class="style1">
                                        <b>
                                            <% Response.Write(Helpers.getProductLocation(3)); %>
                                        </b>
                                        </span>
                                        <%--<span class="style1"><strong>Alco Controls spol. s r.o<br />
                                         Attn.: Controls Return Center<br />
                                         K Dílnám 843<br />
                                         CZ - 28052 Kolín<br />
                                          Czech Republic
                                        <br />
                                        </strong> </span>--%>
                                        </div>
                                    <br />
                                   <%-- <div>
                                        If you are not the person who should received further notification regarding this
                                        claim please fill below information. If blank you’ll receive these information.</div>
                                    <br />--%>
                                    <div style="display:none">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    Name:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtName" runat="server" Width="200px"></asp:TextBox>
                                                    <asp:Label ID="rfName" runat="server" Font-Bold="True" ForeColor="Red" Text="*" Visible="False"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Email:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEmail" runat="server" Width="200px"></asp:TextBox>
                                                    <asp:Label ID="rfEmail" runat="server" Font-Bold="True" ForeColor="Red" Text="*"
                                                        Visible="False"></asp:Label>
                                                    <asp:RegularExpressionValidator ID="regexEmail" runat="server" ErrorMessage="Invalid email address format"
                                                        ControlToValidate="txtEmail" ValidationExpression="^(([^<>()[\]\\.,;:\s@\“]+(\.[^<>()[\]\\.,;:\s@\“]+)*)|(\“.+\“))@((\[(2([0-4]\d|5[0-5])|1?\d{1,2})(\.(2([0-4]\d|5[0-5])|1?\d{1,2})){3} \])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))" ForeColor="Red"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Phone:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtPhone" runat="server" Width="200px"></asp:TextBox>
                                                    <asp:Label ID="rfPhone" runat="server" Font-Bold="True" ForeColor="Red" Text="*"
                                                        Visible="False"></asp:Label>
                                                    <asp:RegularExpressionValidator ID="regexPhone" runat="server" ErrorMessage="Invalid contact number format"
                                                        ControlToValidate="txtPhone" ValidationExpression="^[-+]?(ext)?[\- \d]+(\d+)+$" ForeColor="Red"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <br />
                                    <div style="display:none">
                                        <asp:CheckBox ID="chkAllow" runat="server" AutoPostBack="True" 
                                            oncheckedchanged="chkAllow_CheckedChanged" />&nbsp;I allow system to use given information
                                        for further contact regarding returns listed above.</div>
                                    <br />
                                    <div>
                                        <table>
                                            <tr>
                                                 <td>
                                                    <asp:Button ID="cmdBackReturn" runat="server" Text="Add new entry" 
                                                        CausesValidation="true" class="buttons" OnClick="cmdBackReturn_Click" />
                                                </td>

                                                <td>
                                                    <asp:Button ID="cmdConfirm" runat="server" Text="Confirm" 
                                                        CausesValidation="true" class="buttons" onclick="cmdConfirm_Click" />
                                                </td>
                                                
                                                <td>
                                                    <asp:Button ID="cmdCancel" runat="server" Text="Cancel" 
                                                        CausesValidation="false" class="buttons" onclick="cmdCancel_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    </form>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script> 
    <%--<script type="text/javascript" src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  --%>
    <script type="text/javascript" src="js/jquery-ui.js"></script> 
    <script type="text/javascript" src="js/ajaxfunctions/returnformcontrol.js"></script>   
</body>
</html>
     