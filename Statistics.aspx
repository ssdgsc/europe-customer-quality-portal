﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Statistics.aspx.cs" Inherits="Statistics" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Europe Customer Quality Portal</title>
        <link type="text/css" rel="Stylesheet" href="css/default.css" />
        <link type="text/css" rel="Stylesheet" href="css/style.css" />
</head>
<body>
<form id="frmStatistics" runat="server">
    <div>
        <div id="holder_main" align="center">
            <div id="holderBody" style="width: 980px;">
                    <div class="MainPanel2">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr valign="top">
                            <td valign="top" height="100%" class="LeftNavigation">
                                <div id="holderLeftNav">
                                    <table>
                                        <tr>
                                            <td><a href="home.aspx">Portal Home</a></td>
                                        </tr>
                                        <tr>
                                            <td><a href="ReturnFormCompressor.aspx">Return Authorization Form</a></td>
                                        </tr>
                                        <tr>
                                            <td><a href="myreturn.aspx">My Returns</a></td>
                                        </tr>
                                        <%--<tr>
                                            <td><a style="color:#f7941d;">Statistics</a></td>
                                        </tr>
                                        <tr>
                                            <td><a href="deliveries.aspx">Deliveries</a></td>
                                        </tr>--%>
                                        <tr>
                                            <td><a href="changepassword.aspx">Change Password</a></td>
                                        </tr>
                                        <tr>
                                            <td><a href="logout.aspx">Logout</a></td>
                                        </tr>
                                    </table>
                                    <br />
                                    <asp:Panel ID="pnlMaintenance" runat="server">
                                        <table>
                                            <tr>
                                                <th><a style="color:Blue">Maintenance</a><br /></th>
                                            </tr>
                                            <tr>
                                                <td><a href="customers.aspx">Customers</a></td>
                                            </tr>
                                            <tr>
                                                <td><a href="usermanagement.aspx">User Management</a></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                            </td>
                            <td valign="top" height="100%" class="DottedBorderLeft" style="width:770px">
                                <div id="holderMaincontent">
                                        <div><asp:Label ID="LabelMessage" runat="server" Font-Bold="True" ForeColor="Red" Text="" Visible="False"></asp:Label></div>
                                        <br /> 
                                        <div>
                                            <asp:DropDownList ID="ddCustomerList" runat="server"></asp:DropDownList>
                                            <asp:Button ID="btnGo" runat="server" Text="Go" onclick="btnGo_Click" class="buttons" />
                                        </div>
                                        <br />
                                       <div>
                                            <div>
                                                <asp:Chart ID="chrtStatistics" runat="server" Width="572px" Height="332px">
                                                    <Titles>  
                                                          <asp:Title Text="My Statistics" />  
                                                    </Titles> 
                                                    <Series>
                                                        <asp:Series Name="serStatistics">
                                                        </asp:Series>
                                                    </Series>
                                                    <ChartAreas>
                                                        <asp:ChartArea Name="chaStatistics">
                                                        </asp:ChartArea>
                                                    </ChartAreas>
                                                </asp:Chart>
                                            </div>
                                            <br />
                                            <div>
                                                <table>
                                                    <tr>
                                                        <td><asp:Button ID="btnByYOM" runat="server" Text="By Year of Manufacturing" 
                                                                onclick="btnByYOM_Click" class="buttons" /></td>
                                                        <td><asp:Button ID="btnByYOA" runat="server" Text="By Year of Return" 
                                                                onclick="btnByYOA_Click" class="buttons" /></td>
                                                        <%--<td><asp:Button ID="btnByModel" runat="server" Text="By Line/Field Return" 
                                                                onclick="btnByModel_Click" class="buttons" /></td>--%>
                                                    </tr>
                                                </table>
                                            </div>
                                       </div>
                                       <div>
                                           <asp:HiddenField ID="hdnLastButtonClicked" runat="server" />
                                       </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                 </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
