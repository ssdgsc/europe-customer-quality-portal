﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Script.Serialization;
using System.IO;
using General;
using System.Web.Services;
using System.Globalization;
using General.AvianCrypto;
using System.Collections.Generic;
using System.Web.Script.Services;

public partial class ReturnFormAccessories : System.Web.UI.Page
{
    General.AvianCrypto.SecurityMaster sm = new General.AvianCrypto.SecurityMaster();
    protected void Page_Unload(object sender, EventArgs e) {
        if (!Page.IsPostBack)
        {
            
        }
    }
    //
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserID"] == null)
        {
            Response.Redirect("login.aspx");
            return;
        }


        // CHeck if admin to show/hide the maintenancne
        SecurityMaster sm = new SecurityMaster();
        string user = sm.Decrypt(Session["UserType"].ToString());
        this.hfRole.Value = user;
        //if (user != "CUSTOMER QUALITY")
        //{
        //    this.pnlMaintenance.Visible = false;
        //}

        Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
        if (Session["UserID"] == null)
        {
            Response.Redirect("Login.aspx");
        }
        //Response.Write(sm.Decrypt(Session["UserID"].ToString()));
        //Response.Write(sm.Decrypt(Session["UserName"].ToString()));
        initialization();
        if (!Page.IsPostBack)
        {
            Session["RFBackupDetails"] = null;
            DateTime dt = DateTime.Now;
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(dt, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            //ViewState["tempClaimID"] = 0;
            txtControlDateOfClaim.Text = dt.ToString("dd/MM/yyyy");
            txtControlDateOfFailure.Text = dt.ToString("dd/MM/yyyy");

            //txtControlModel.Attributes.Add("readonly", "readonly");
            //txtControlPCNNumber.Attributes.Add("readonly", "readonly");

            int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString()));

            if (this.hfRole.Value == "CUSTOMER QUALITY" || this.hfRole.Value == "CSO")
                this.ddCustomerList.DataSource = mrfd.getAllCustomerListCompressor(this.hfRole.Value, userid);
            else
                this.ddCustomerList.DataSource = mrfd.getCustomerList(userid);

            this.ddCustomerList.DataTextField = "CUSTOMERNAMENUMBER";
            this.ddCustomerList.DataValueField = "CUSTOMERNUMBER";
            this.ddCustomerList.DataBind();

             

            Param prm = new Param();
            prm.getParams(ddControlClaimLists, "CtrlClaim");

            /*
            if (sm.Decrypt(Session["UserType"].ToString()) == "CUSTOMER")
            {
                this.ddCustomerList.SelectedItem.Text = sm.Decrypt(Session["CustomerName"].ToString());
                ddCustomerList.Enabled = false;
            }
            */
            this.btnAddNewControl.Visible = true; 
            this.btnAddSameControl.Visible = true;
            this.pnlReturnForm.Visible = true;
            this.pnlReturnForm2.Visible = false;
            this.hdnRecordsCount.Value = "0";
            ViewState["RFDetails"] = null;

            txtControlDateOfPartIncome.Text = "";// dt.ToString("dd/MM/yyyy");
            hdnControlCWOfPartIncome.Value = dt.Year.ToString() + "/" + weekNum;
            hdnControlMonthIncome.Value = dt.Year.ToString() + "/" + dt.Month.ToString();

            if (dt.Month >= 10)
            { hdnControlFYIncome.Value = "FY" + (Convert.ToInt32(dt.Year) + 1).ToString().Substring(2, 2); }
            else
            { hdnControlFYIncome.Value = "FY" + Convert.ToInt32(dt.Year).ToString().Substring(2, 2); }

            updateLinkViewList();
            //initControlModel();

        }

        if (Page.IsPostBack)
        {
            txtControlPCNNumber.Text = hdnItemNumber.Value;
        }

    }

   
    protected void initControlModel()
    {
        Maintenance.ReturnFormAccessories mrfd = new Maintenance.ReturnFormAccessories();
        DataTable dt = new DataTable();
        dt = mrfd.getModelByPCNNumber();
        
        foreach (DataRow dr in dt.Rows)
        {
            ListItem li = new ListItem();
            li.Text = dr[1].ToString() + " - " + dr[0].ToString();
            li.Value = dr[0].ToString();
            txtControlModel.Items.Add(li);
        }
         
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static ResponseObject GetItemDescriptionDropdown(string _itemNumber)
    {
        ResponseObject objResponse = new ResponseObject();
        Maintenance.ReturnFormAccessories mrfd = new Maintenance.ReturnFormAccessories();
        List<Model.ReturnFormAccessories> list = new List<Model.ReturnFormAccessories>();
        Utility u = new Utility();
        DataTable dt = mrfd.GetItemDescriptionAll(_itemNumber);
        foreach (DataRow dr in dt.Rows)
        {
            Model.ReturnFormAccessories rfd = new Model.ReturnFormAccessories();
            rfd.ModelProduct = u.extractStringValue(dr["Description1"], Utility.DataType.STRING);
            rfd.PCNNumber = u.extractStringValue(dr["ItemNumber"], Utility.DataType.STRING);
            list.Add(rfd);
        }
       
        objResponse.ResponseItem = list;
        
        return objResponse;
    }

    public static int GetWeekNumber(DateTime dt)
    {
        CultureInfo ciCurr = CultureInfo.CurrentCulture;
        int weekNum = ciCurr.Calendar.GetWeekOfYear(dt, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        return weekNum;
    }
    protected void optProductType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (optProductType.SelectedIndex == 0)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "checkPendingTransaction", "checkPendingTransaction(" + this.hdnRecordsCount.Value + ",'ReturnFormCompressor.aspx');", true);
            //Response.Redirect("ReturnFormCompressor.aspx");
        }
        if (optProductType.SelectedIndex == 1)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "checkPendingTransaction", "checkPendingTransaction(" + this.hdnRecordsCount.Value + ",'ReturnFormControl.aspx');", true);
 
        }
    }
    protected void ddControlClaimLists_SelectedIndexChanged(object sender, EventArgs e)
    {
        //[START] [04/25/17] [jpbersonda]
        if (ddControlClaimLists.SelectedItem.Text.Equals("Buy back", StringComparison.CurrentCultureIgnoreCase))
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "", "alert('Buy back only authorized after written approval from Emerson Customer Service');", true);

        //[END] [04/25/17] [jpbersonda]

        if (ddControlClaimLists.SelectedValue == "Other")
        {
            lblControlClaimDescription.Visible = true;
            txtControlClaimDescription.Visible = true;
        }
        else
        {
            lblControlClaimDescription.Visible = false;
            txtControlClaimDescription.Visible = false;
        }

        if (ddControlClaimLists.SelectedValue == "Cooling/Heating capacity too low" ||
                ddControlClaimLists.SelectedValue == "High Power Input" ||
                ddControlClaimLists.SelectedValue == "High Amps" ||
                ddControlClaimLists.SelectedValue == "Low COP/EER")
        {
            pnlControlWorkingPoint.Visible = true;
        }
        else
        {
            pnlControlWorkingPoint.Visible = false;
        }
        txtControlDateOfPartIncome.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
    }
    protected void optControlRefrigerant_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (optControlRefrigerant.SelectedIndex == 5)
        {
           
            pnlControlRefrigerant.Visible = true;
            this.txtControlRefrigerant.Text = "";
            req15.Enabled = true;
        }
        else
           
            pnlControlRefrigerant.Visible = false;
            req15.Enabled = false;
    }
    
    //[START] [jpbersonda] [5/25/2017] [Changes for Request radio button values]
    protected void optControlRequest1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (optControlRequest1.SelectedIndex == 1)
        {
            txtControlRequestSpecificTest.Visible = true;
            optControlRequest1.Items[1].Attributes.Remove("onclick");
        }
        else
        {
            txtControlRequestSpecificTest.Visible = false;
            txtControlRequestSpecificTest.Text = "";
            optControlRequest1.Items[1].Attributes.Add("onclick", "selectedRequestSpecificTest()");
        }
    }
    //[END] [jpbersonda] [5/25/2017] [Changes for Request radio button values]

    protected void btnCheckControlPCNNumber_Click(object sender, EventArgs e)
    {
        CheckControlPCNumber();
    }

    protected void CheckControlPCNumber(){
        try
        {
            Maintenance.ReturnFormAccessories mrfd = new Maintenance.ReturnFormAccessories();
            Model.ReturnFormAccessories rfd = new Model.ReturnFormAccessories();
            Utility u = new Utility();
            initialization();
            DataTable dt = mrfd.getModelByPCNNumber(txtControlPCNNumber.Text);
            foreach (DataRow dr in dt.Rows)
            {
                rfd.ModelProduct = u.extractStringValue(dr["Description1"], Utility.DataType.STRING);
                rfd.PCNNumber = u.extractStringValue(dr["PCNNumber"], Utility.DataType.STRING);
                rfd.ProductGroup = u.extractStringValue(dr["ProductType"], Utility.DataType.STRING);
                rfd.Line = u.extractStringValue(dr["Line"], Utility.DataType.STRING);
                //rfd.ItemNumber = u.extractStringValue(dr["ITNBR"], Utility.DataType.STRING);
            }
            if (String.IsNullOrEmpty(rfd.ModelProduct))
            {
                EmptyPcn.Visible = true;
                //showMessage(true, "Please check the PCN number or use drop down list on product field to find correct pcn");
                txtControlModel.Text = "";
                hdnControlProductGroup.Value = "";
                hdnControlLine.Value = "";
                //hdnItemNumber.Value = "";
            }
            else
            {
                EmptyPcn.Visible = false;
                showMessage(false, "");
                txtControlModel.SelectedValue = rfd.PCNNumber;
                hdnControlProductGroup.Value = rfd.ProductGroup;
                hdnControlLine.Value = rfd.Line;
                //hdnItemNumber.Value = rfd.ItemNumber;
            }
        }
        catch (Exception ex)
        {
            showMessage(true, ex.Message);
        }
    }

    protected void btnAddSameControl_Click(object sender, EventArgs e)
    {
        /*if (!checkRadioButton())
            return;*/
        if (this.ddCustomerList.Items.Count == 0) {
            showMessage(true, "No customer registered for control.");
            return;
        }

        if (this.ddCustomerList.SelectedValue == "") {
            showMessage(true, "Please select customer. ");
            return;
        }

        if (!isDateValid(formatDateForSQL(this.txtControlDateOfFailure.Text)))
        {
            showMessage(true, "Invalid date for Date of Failure");
            return;
        }

        String fromview = "";
        string filename = "";
        if (ViewState["RFDetails"] != null)
            fromview = ViewState["RFDetails"].ToString();
        JavaScriptSerializer json = new JavaScriptSerializer();
        ArrayList al = new ArrayList();
        if (String.IsNullOrEmpty(fromview) == false)
            al = json.Deserialize<ArrayList>(fromview);
            
        try
        {
            //int tempClaimID = 0;
            //if (this.hdnLastClicked.Value == "Same Claim")
            //    tempClaimID = int.Parse(ViewState["tempClaimID"].ToString());
            //else if (this.hdnLastClicked.Value == "New Claim")
            //{
            //    ViewState["tempClaimID"] = int.Parse(ViewState["tempClaimID"].ToString()) + 1;
            //    tempClaimID = int.Parse(ViewState["tempClaimID"].ToString());
            //}
            //else
            //{
            //    ViewState["tempClaimID"] = 0;
            //    tempClaimID = 0;
            //}

            Model.ReturnFormAccessories rfd = new Model.ReturnFormAccessories();
            Model.ReturnFormHeader rfh = new Model.ReturnFormHeader();
            Maintenance.ReturnFormAccessories mrfd = new Maintenance.ReturnFormAccessories();

            rfh.ReferenceNumber = DateTime.Now.ToShortDateString().Substring(2, 2) + "-0000"; ;
            rfh.Description = "";
            rfh.ReturnDate = DateTime.Now.ToShortDateString();

            //rfd.ClaimID = int.Parse(ViewState["tempClaimID"].ToString());
            rfd.ProductType = optProductType.SelectedValue;
            rfd.Claim = ddControlClaimLists.SelectedValue;
            rfd.ControlCustomerClaimReference = txtControlCustomerClaimReference.Text;
            rfd.PCNNumber = hdnItemNumber.Value;//txtControlPCNNumber.Text;
            int modelProductExtract = hdnItemDescription.Value.LastIndexOf("-"); //txtControlModel.SelectedItem.Text.LastIndexOf("-");
            rfd.ModelProduct = hdnItemDescription.Value.Substring(0, modelProductExtract);// txtControlModel.SelectedItem.Text.Substring(0, modelProductExtract);
            rfd.ProductGroup = hdnControlProductGroup.Value;
            rfd.Line = hdnControlLine.Value;
            rfd.DataCode = txtControlDataCode.Text;
            rfd.Warranty = txtControlWarranty.Text;
            rfd.Quantity = txtControlQty.Text;
            rfd.DateOfClaim = txtControlDateOfClaim.Text;
            rfd.ControlComponentReference = txtComponentReference.Text;
            rfd.TypeOfReturn = getReturnTypeValue(optControlTypeOfReturn.SelectedValue);
            rfd.Application = optControlApplication.SelectedValue;
            rfd.Refrigerant = optControlRefrigerant.SelectedValue;
            rfd.WorkingPointTO = "";
            rfd.WorkingPointTC = "";
            rfd.WorkingPointSuperHeat = "";
            rfd.WorkingPointSubCooling = "";
            rfd.Request = optControlRequest1.SelectedValue;
            // ADD LOCATION ID 
            rfd.LocationID = ddLocation.SelectedValue;
            /*
            rfd.RequestWarranty = optControlRequest.Items[0].Selected;
            rfd.RequestAnalysis = optControlRequest.Items[1].Selected;
            rfd.RequestCreditOfBody = optControlRequest.Items[2].Selected;
            rfd.RequestOutOfWarranty = optControlRequest.Items[3].Selected;
            rfd.RequestSpecificTest = optControlRequest.Items[4].Selected;
            */
            rfd.CustomerNumber = this.ddCustomerList.SelectedValue;
            rfd.ItemNumber = hdnItemNumber.Value;
            rfd.Customer = this.ddCustomerList.SelectedItem.Text.Trim();
            rfd.Configuration = optControlConfiguration.SelectedValue;
            rfd.DateOfFailure = txtControlDateOfFailure.Text;
            rfd.RunningHours = (txtControlRunningHours.Text == "") ? "0" : txtControlRunningHours.Text;
            rfd.DateOfPartIncome = (txtControlDateOfPartIncome.Text);
            rfd.CurrentWeekOfPartIncome = hdnControlCWOfPartIncome.Value;
            rfd.MonthIncome = hdnControlMonthIncome.Value;
            rfd.FiscalYearIncome = hdnControlFYIncome.Value;
            rfd.Remarks = txtRemarks.Value;

            if (ddControlClaimLists.SelectedValue == "Cooling/Heating capacity too low" ||
                ddControlClaimLists.SelectedValue == "High Power Input" ||
                ddControlClaimLists.SelectedValue == "High Amps" ||
                ddControlClaimLists.SelectedValue == "Low COP/EER")
            {
                rfd.WorkingPointTO = txtControlWorkingPointTO.Text;
                rfd.WorkingPointTC = txtControlWorkingPointTC.Text;
                rfd.WorkingPointSuperHeat = txtControlWorkingPointSuperHeat.Text;
                rfd.WorkingPointSubCooling = txtControlWorkingPointSubCooling.Text;
            }
            else
            {
                rfd.WorkingPointTO = "";
                rfd.WorkingPointTC = "";
                rfd.WorkingPointSuperHeat = "";
                rfd.WorkingPointSubCooling = "";
            }

            if (ddControlClaimLists.SelectedValue == "Other")
                rfd.ClaimDescription = txtControlClaimDescription.Text;
            else
                rfd.ClaimDescription = "";

            //[START] [jpbersonda] [5/25/2017] [Changes for Request radio button values]
            if (optControlRequest1.SelectedValue == "Specific Request")
                rfd.RequestSpecificTestRemarks = txtControlRequestSpecificTest.Text;
            else
                rfd.RequestSpecificTestRemarks = "";
            //[END] [jpbersonda] [5/25/2017] [Changes for Request radio button values]


            if (optControlRefrigerant.SelectedValue == "Other")
                rfd.OtherRefrigerant = txtControlRefrigerant.Text;
            else
                rfd.OtherRefrigerant = "";

            rfd.FilePath = hdnFileName.Value;

       

            al.Add(rfd);

            //hdnLastClicked.Value = "Same Claim";
            ViewState["RFDetails"] = json.Serialize(al);
            txtControlPCNNumber.Text = "";
            txtControlModel.Text = "";
            hdnControlProductGroup.Value = "";
            hdnControlLine.Value = "";
            txtControlDataCode.Text = "";
            txtControlWarranty.Text = "";
            txtControlQty.Text = "";
            txtComponentReference.Text = "";
            //txtControlDateOfClaim.Text = "";

            int count = Int32.Parse(this.hdnRecordsCount.Value);
            count = count + 1;
            this.hdnRecordsCount.Value = count.ToString();
            updateLinkViewList();
        }
        catch (Exception ex)
        {
            showMessage(true, ex.Message);
        }
    }
    protected void btnAddNewControl_Click(object sender, EventArgs e)
    {
        /*if (!checkRadioButton())
            return;*/

        if (!isDateValid(formatDateForSQL(this.txtControlDateOfFailure.Text)))
        {
            showMessage(true, "Invalid date for Date of Failure");
            return;
        }

        String fromview = "";
        string filename = "";
        if (ViewState["RFDetails"] != null)
            fromview = ViewState["RFDetails"].ToString();
        JavaScriptSerializer json = new JavaScriptSerializer();
        ArrayList al = new ArrayList();
        if (String.IsNullOrEmpty(fromview) == false)
            al = json.Deserialize<ArrayList>(fromview);

        try
        {
            //int tempClaimID = 0;
            //if (this.hdnLastClicked.Value == "Same Claim")
            //    tempClaimID = int.Parse(ViewState["tempClaimID"].ToString());
            //else if (this.hdnLastClicked.Value == "New Claim")
            //{
            //    ViewState["tempClaimID"] = int.Parse(ViewState["tempClaimID"].ToString()) + 1;
            //    tempClaimID = int.Parse(ViewState["tempClaimID"].ToString());
            //}
            //else
            //{
            //    ViewState["tempClaimID"] = 0;
            //    tempClaimID = 0;
            //}

            Model.ReturnFormAccessories rfd = new Model.ReturnFormAccessories();
            Model.ReturnFormHeader rfh = new Model.ReturnFormHeader();
            Maintenance.ReturnFormAccessories mrfd = new Maintenance.ReturnFormAccessories();

            rfh.ReferenceNumber = DateTime.Now.ToShortDateString().Substring(2, 2) + "-0000"; ;
            rfh.Description = "";
            rfh.ReturnDate = DateTime.Now.ToShortDateString();

            //rfd.ClaimID = tempClaimID;
            rfd.ProductType = optProductType.SelectedValue;
            rfd.Claim = ddControlClaimLists.SelectedValue;
            rfd.ControlCustomerClaimReference = txtControlCustomerClaimReference.Text;
            rfd.PCNNumber = hdnItemNumber.Value;//txtControlPCNNumber.Text;
            int modelProductExtract = hdnItemDescription.Value.LastIndexOf("-"); //txtControlModel.SelectedItem.Text.LastIndexOf("-");
            rfd.ModelProduct = hdnItemDescription.Value.Substring(0, modelProductExtract);// txtControlModel.SelectedItem.Text.Substring(0, modelProductExtract);
            rfd.ProductGroup = hdnControlProductGroup.Value;
            rfd.Line = hdnControlLine.Value;
            rfd.DataCode = txtControlDataCode.Text;
            rfd.Warranty = txtControlWarranty.Text;
            rfd.Quantity = txtControlQty.Text;
            rfd.DateOfClaim = (txtControlDateOfClaim.Text);
            rfd.ControlComponentReference = txtComponentReference.Text;
            rfd.TypeOfReturn = getReturnTypeValue(optControlTypeOfReturn.SelectedValue);
            rfd.Application = optControlApplication.SelectedValue;
            rfd.Refrigerant = optControlRefrigerant.SelectedValue;
            rfd.WorkingPointTO = "";
            rfd.WorkingPointTC = "";
            rfd.WorkingPointSuperHeat = "";
            rfd.WorkingPointSubCooling = "";
            rfd.Request = optControlRequest1.SelectedValue;
            // ADD LOCATION ID
            rfd.LocationID = ddLocation.SelectedValue;
            /*
            rfd.RequestWarranty = optControlRequest.Items[0].Selected;
            rfd.RequestAnalysis = optControlRequest.Items[1].Selected;
            rfd.RequestCreditOfBody = optControlRequest.Items[2].Selected;
            rfd.RequestOutOfWarranty = optControlRequest.Items[3].Selected;
            rfd.RequestSpecificTest = optControlRequest.Items[4].Selected;
            */
            rfd.CustomerNumber = this.ddCustomerList.SelectedValue;
            rfd.ItemNumber = hdnItemNumber.Value;
            rfd.Customer = this.ddCustomerList.SelectedItem.Text.Trim();
            rfd.Configuration = optControlConfiguration.SelectedValue;
            rfd.DateOfFailure = (txtControlDateOfFailure.Text);
            rfd.RunningHours = (txtControlRunningHours.Text == "") ? "0" : txtControlRunningHours.Text;
            rfd.DateOfPartIncome = (txtControlDateOfPartIncome.Text);
            rfd.CurrentWeekOfPartIncome = hdnControlCWOfPartIncome.Value;
            rfd.MonthIncome = hdnControlMonthIncome.Value;
            rfd.FiscalYearIncome = hdnControlFYIncome.Value;
            rfd.Remarks = txtRemarks.Value;

            if (ddControlClaimLists.SelectedValue == "Cooling/Heating capacity too low" ||
                ddControlClaimLists.SelectedValue == "High Power Input" ||
                ddControlClaimLists.SelectedValue == "High Amps" ||
                ddControlClaimLists.SelectedValue == "Low COP/EER")
            {
                rfd.WorkingPointTO = txtControlWorkingPointTO.Text;
                rfd.WorkingPointTC = txtControlWorkingPointTC.Text;
                rfd.WorkingPointSuperHeat = txtControlWorkingPointSuperHeat.Text;
                rfd.WorkingPointSubCooling = txtControlWorkingPointSubCooling.Text;
            }
            else
            {
                rfd.WorkingPointTO = "";
                rfd.WorkingPointTC = "";
                rfd.WorkingPointSuperHeat = "";
                rfd.WorkingPointSubCooling = "";
            }

            if (ddControlClaimLists.SelectedValue == "Other")
                rfd.ClaimDescription = txtControlClaimDescription.Text;
            else
                rfd.ClaimDescription = "";

            //[START] [jpbersonda] [5/25/2017] [Changes for Request radio button values]
            if (optControlRequest1.SelectedValue == "Specific Request")
                rfd.RequestSpecificTestRemarks = txtControlRequestSpecificTest.Text;
            else
                rfd.RequestSpecificTestRemarks = "";
            //[END] [jpbersonda] [5/25/2017] [Changes for Request radio button values]

            if (optControlRefrigerant.SelectedValue == "Other")
                rfd.OtherRefrigerant = txtControlRefrigerant.Text;
            else
                rfd.OtherRefrigerant = "";
            rfd.FilePath = hdnFileName.Value;


            al.Add(rfd);

            //this.hdnLastClicked.Value = "New Claim";
            ViewState["RFDetails"] = json.Serialize(al);
            //ViewState["tempClaimID"] = tempClaimID;
            refreshFields();

            int count = Int32.Parse(this.hdnRecordsCount.Value);
            count = count + 1;
            this.hdnRecordsCount.Value = count.ToString();
            updateLinkViewList();
        }
        catch (Exception ex)
        {
            LabelMessage.Text = ex.GetBaseException().ToString();
            LabelMessage.Visible = true;
        }
    }

    public string formatDateForSQL(string date)
    {
        return DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy");
    }

    protected void btnControlSaveAndFinalize_Click(object sender, EventArgs e)
    {
        try
        {
            /*if (!checkRadioButton())
                return;*/

            if (!isDateValid(formatDateForSQL(txtControlDateOfFailure.Text)))
            {
                showMessage(true, "Invalid date for Date of Failure");
                return;
            }

            String fromview = "";
            string filename = "";
            if (ViewState["RFDetails"] != null)
                fromview = ViewState["RFDetails"].ToString();
            JavaScriptSerializer json = new JavaScriptSerializer();
            ArrayList al = new ArrayList();
            if (String.IsNullOrEmpty(fromview) == false)
                al = json.Deserialize<ArrayList>(fromview);

            //int tempClaimID = 0;

            //if (this.hdnLastClicked.Value == "Same Claim")
            //    tempClaimID = int.Parse(ViewState["tempClaimID"].ToString());
            //else if (this.hdnLastClicked.Value == "New Claim")
            //{
            //    ViewState["tempClaimID"] = int.Parse(ViewState["tempClaimID"].ToString()) + 1;
            //    tempClaimID = int.Parse(ViewState["tempClaimID"].ToString());
            //}
            //else
            //{
            //    ViewState["tempClaimID"] = 0;
            //    tempClaimID = 0;
            //}

            //=====
            Model.ReturnFormAccessories rfd = new Model.ReturnFormAccessories();
            Model.ReturnFormHeader rfh = new Model.ReturnFormHeader();
            Maintenance.ReturnFormAccessories mrfd = new Maintenance.ReturnFormAccessories();

            rfh.ReferenceNumber = DateTime.Now.ToShortDateString().Substring(2, 2) + "-0000";  
            rfh.Description = "";
            rfh.ReturnDate = DateTime.Now.ToShortDateString();


            //if (this.hdnOperation.Value == "add")
            //    rfd.ClaimID = tempClaimID;
            //if (this.hdnOperation.Value == "edit")
            //    rfd.ClaimID = int.Parse(this.hdnClaimID.Value);

            rfd.ProductType = optProductType.SelectedValue;
            rfd.LocationID = ddLocation.SelectedValue;
            rfd.Claim = ddControlClaimLists.SelectedValue;
            rfd.ControlCustomerClaimReference = txtControlCustomerClaimReference.Text;
            rfd.PCNNumber = hdnItemNumber.Value;//txtControlPCNNumber.Text;
            int modelProductExtract = hdnItemDescription.Value.LastIndexOf("-"); //txtControlModel.SelectedItem.Text.LastIndexOf("-");
            rfd.ModelProduct = hdnItemDescription.Value.Substring(0, modelProductExtract);// txtControlModel.SelectedItem.Text.Substring(0, modelProductExtract);
            //rfd.ModelProduct = txtControlModel.Text;
            rfd.ProductGroup = hdnControlProductGroup.Value;
            rfd.Line = hdnControlLine.Value;
            rfd.DataCode = txtControlDataCode.Text;
            rfd.Warranty = txtControlWarranty.Text;
            rfd.Quantity = txtControlQty.Text;
            //rfd.DateOfClaim = txtControlDateOfClaim.Text;
            rfd.DateOfClaim = (txtControlDateOfClaim.Text);
            rfd.ControlComponentReference = txtComponentReference.Text;
            rfd.TypeOfReturn = getReturnTypeValue(optControlTypeOfReturn.SelectedValue);
            rfd.Application = optControlApplication.SelectedValue;
            rfd.Refrigerant = optControlRefrigerant.SelectedValue;

            if (optControlRefrigerant.SelectedValue == "Other")
            { rfd.OtherRefrigerant = txtControlRefrigerant.Text; }
            else
            { rfd.OtherRefrigerant = ""; }

            rfd.WorkingPointTO = "";
            rfd.WorkingPointTC = "";
            rfd.WorkingPointSuperHeat = "";
            rfd.WorkingPointSubCooling = "";

            //[START] [jpbersonda] [5/25/2017] [Changes for Request radio button values]
            rfd.Request = optControlRequest1.SelectedValue;
            if (optControlRequest1.SelectedValue == "Specific Request")
            { rfd.RequestSpecificTestRemarks = txtControlRequestSpecificTest.Text; }
            else
            { rfd.RequestSpecificTestRemarks = ""; }
            //[END] [jpbersonda] [5/25/2017] [Changes for Request radio button values]

         
            rfd.CustomerNumber = this.ddCustomerList.SelectedValue;
            rfd.ItemNumber = hdnItemNumber.Value;
            rfd.Customer = this.ddCustomerList.SelectedItem.Text.Trim();
            rfd.Configuration = optControlConfiguration.SelectedValue;
            rfd.DateOfFailure = (txtControlDateOfFailure.Text);
            rfd.RunningHours = (txtControlRunningHours.Text == "") ? "0" : txtControlRunningHours.Text;
            rfd.DateOfPartIncome = (txtControlDateOfPartIncome.Text);
            rfd.CurrentWeekOfPartIncome = hdnControlCWOfPartIncome.Value;
            rfd.MonthIncome = hdnControlMonthIncome.Value;
            rfd.FiscalYearIncome = hdnControlFYIncome.Value;
            rfd.Remarks = txtRemarks.Value;
            
            
            if (ddControlClaimLists.SelectedValue == "Cooling/Heating capacity too low" ||
                ddControlClaimLists.SelectedValue == "High Power Input" ||
                ddControlClaimLists.SelectedValue == "High Amps" ||
                ddControlClaimLists.SelectedValue == "Low COP/EER")
            {
                rfd.WorkingPointTO = txtControlWorkingPointTO.Text;
                rfd.WorkingPointTC = txtControlWorkingPointTC.Text;
                rfd.WorkingPointSuperHeat = txtControlWorkingPointSuperHeat.Text;
                rfd.WorkingPointSubCooling = txtControlWorkingPointSubCooling.Text;
            }
            else
            {
                rfd.WorkingPointTO = "";
                rfd.WorkingPointTC = "";
                rfd.WorkingPointSuperHeat = "";
                rfd.WorkingPointSubCooling = "";
            }

            if (ddControlClaimLists.SelectedValue == "Other")
                rfd.ClaimDescription = txtControlClaimDescription.Text;
            else
                rfd.ClaimDescription = "";



            rfd.FilePath = hdnFileName.Value;
            /*
            if (optControlRequest.Items[4].Selected)
                rfd.RequestSpecificTestRemarks = txtControlRequestSpecificTest.Text;
            else
                rfd.RequestSpecificTestRemarks = "";
            */

            
            //if (UploadFile.HasFile)
            //{
            //    filename = Path.GetFileName(UploadFile.FileName);
            //    if (!File.Exists(HttpContext.Current.Server.MapPath("UploadedFiles/") + filename))
            //        UploadFile.SaveAs(HttpContext.Current.Server.MapPath("UploadedFiles/") + filename);
            //    rfd.FilePath = "UploadedFiles\\" + filename;
            //}
            //else
            //{
            //    rfd.FilePath = "";
            //}

            rfd.OpenOrClosed = "OPEN";

            if (this.hdnOperation.Value == "add")
                al.Add(rfd);
            if (this.hdnOperation.Value == "edit")
                al[int.Parse(this.hdnIndexToEdit.Value)] = rfd;

            ViewState["RFDetails"] = json.Serialize(al);
            //ViewState["tempClaimID"] = tempClaimID;

            int count = Int32.Parse(this.hdnRecordsCount.Value);
            if (this.hdnOperation.Value == "add")
                count = count + 1;
            this.hdnRecordsCount.Value = count.ToString();
            updateLinkViewList();

            saveAndFinalize();

        }
        catch (Exception ex)
        {
            showMessage(true, ex.Message);
        }
    }
    protected void saveAndFinalize()
    {
        setBackupDetails();
        try
        {
            String fromview = "";
            if (ViewState["RFDetails"] != null)
                fromview = ViewState["RFDetails"].ToString();
            JavaScriptSerializer json = new JavaScriptSerializer();
            ArrayList al = new ArrayList();
            if (String.IsNullOrEmpty(fromview) == false)
                al = json.Deserialize<ArrayList>(fromview);

            this.txtName.Enabled = false;
            this.txtEmail.Enabled = false;
            this.txtPhone.Enabled = false;

            if (ViewState["RFDetails"] == null)
            {
                showMessage(true, "No records to save.");
                refreshFields();
                return;
            }
            else
            {
                this.pnlReturnForm2.Visible = true;
                this.pnlReturnForm.Visible = false;
                displayItems();
                refreshFields();
            }
        }
        catch (Exception ex)
        {
            showMessage(true, ex.Message);
        }
    }

    // BACK UP THE DETAILS WHEN CLICK THE lnkViewList 
    public void setBackupDetails() { 
 
        if (Session["RFBackupDetails"] != null) { return;}  
        List<Model.ReturnFormAccessories> al = new List<Model.ReturnFormAccessories>(); 
       
        try
        {
      
            Model.ReturnFormAccessories rfd = new Model.ReturnFormAccessories(); 
            Maintenance.ReturnFormAccessories mrfd = new Maintenance.ReturnFormAccessories(); 
             
            rfd.ProductType = optProductType.SelectedValue;
            rfd.Claim = ddControlClaimLists.SelectedValue;
            rfd.ControlCustomerClaimReference = txtControlCustomerClaimReference.Text;
            rfd.PCNNumber = hdnItemNumber.Value;//txtControlPCNNumber.Text;
            rfd.ModelProduct = hdnItemDescription.Value; // (txtControlModel.SelectedIndex == -1) ? "" : txtControlModel.SelectedItem.Text;
            rfd.ProductGroup = hdnControlProductGroup.Value;
            rfd.Line = hdnControlLine.Value;
            rfd.DataCode = txtControlDataCode.Text;
            rfd.Warranty = txtControlWarranty.Text;
            rfd.Quantity = txtControlQty.Text;
            rfd.DateOfClaim = txtControlDateOfClaim.Text;
            rfd.ControlComponentReference = txtComponentReference.Text;
            rfd.TypeOfReturn = getReturnTypeValue(optControlTypeOfReturn.SelectedValue);
            rfd.Application = optControlApplication.SelectedValue;
            rfd.Refrigerant = optControlRefrigerant.SelectedValue;
            rfd.WorkingPointTO = "";
            rfd.WorkingPointTC = "";
            rfd.WorkingPointSuperHeat = "";
            rfd.WorkingPointSubCooling = "";
            rfd.Request = optControlRequest1.SelectedValue;
            // ADD LOCATION ID 
            rfd.LocationID = ddLocation.SelectedValue;
       
            rfd.CustomerNumber = this.ddCustomerList.SelectedValue;
            rfd.ItemNumber = hdnItemNumber.Value;
            rfd.Customer = this.ddCustomerList.SelectedItem.Text.Trim();
            rfd.Configuration = optControlConfiguration.SelectedValue;
            rfd.DateOfFailure = txtControlDateOfFailure.Text;
            rfd.RunningHours = (txtControlRunningHours.Text == "") ? "0" : txtControlRunningHours.Text;
            rfd.DateOfPartIncome = (txtControlDateOfPartIncome.Text);
            rfd.CurrentWeekOfPartIncome = hdnControlCWOfPartIncome.Value;
            rfd.MonthIncome = hdnControlMonthIncome.Value;
            rfd.FiscalYearIncome = hdnControlFYIncome.Value;
            rfd.Remarks = txtRemarks.Value;

            if (ddControlClaimLists.SelectedValue == "Cooling/Heating capacity too low" ||
                ddControlClaimLists.SelectedValue == "High Power Input" ||
                ddControlClaimLists.SelectedValue == "High Amps" ||
                ddControlClaimLists.SelectedValue == "Low COP/EER")
            {
                rfd.WorkingPointTO = txtControlWorkingPointTO.Text;
                rfd.WorkingPointTC = txtControlWorkingPointTC.Text;
                rfd.WorkingPointSuperHeat = txtControlWorkingPointSuperHeat.Text;
                rfd.WorkingPointSubCooling = txtControlWorkingPointSubCooling.Text;
            }
            else
            {
                rfd.WorkingPointTO = "";
                rfd.WorkingPointTC = "";
                rfd.WorkingPointSuperHeat = "";
                rfd.WorkingPointSubCooling = "";
            }

            if (ddControlClaimLists.SelectedValue == "Other")
                rfd.ClaimDescription = txtControlClaimDescription.Text;
            else
                rfd.ClaimDescription = "";

            //[START] [jpbersonda] [5/25/2017] [Changes for Request radio button values]
            if (optControlRequest1.SelectedValue == "Specific Request")
                rfd.RequestSpecificTestRemarks = txtControlRequestSpecificTest.Text;
            else
                rfd.RequestSpecificTestRemarks = "";
            //[END] [jpbersonda] [5/25/2017] [Changes for Request radio button values]

            if (optControlRefrigerant.SelectedValue == "Other")
                rfd.OtherRefrigerant = txtControlRefrigerant.Text;
            else
                rfd.OtherRefrigerant = "";

            rfd.FilePath = hdnFileName.Value; 
       
            al.Add(rfd);

            Session["RFBackupDetails"] = al;
        }
        catch (Exception e) { 
            
        }
    }



    protected void displayItems()
    {
        DataTable dt = new DataTable();
        String fromview = ViewState["RFDetails"].ToString();
        JavaScriptSerializer json = new JavaScriptSerializer();
        ArrayList al = json.Deserialize<ArrayList>(fromview);

        //int prevClaimID = 0;
        int lineNumber = 0;
        //dt.Columns.Add("Return ID");
        dt.Columns.Add("Line");
        dt.Columns.Add("Item Number"); // PCN Number
        dt.Columns.Add("Item Description"); // Model
        dt.Columns.Add("Claim");

        for (int x = 0; x < al.Count; x++)
        {
            Model.ReturnFormAccessories rd = new JavaScriptSerializer().ConvertToType<Model.ReturnFormAccessories>(al[x]);
            DataRow dr = dt.NewRow();
            //if (prevClaimID == rd.ClaimID)
                lineNumber = lineNumber + 1;
            //else
            //{
            //    lineNumber = 1;
            //}
            //dr["Return ID"] = rd.ClaimID + 1;
            dr["Line"] = lineNumber.ToString();
            dr["Item Number"] = rd.PCNNumber; //PCN Number
            dr["Item Description"] = rd.ModelProduct; //Model
            dr["Claim"] = rd.Claim;
            dt.Rows.Add(dr);
            //prevClaimID = rd.ClaimID;
        }

        gridViewAccumulateData.DataSource = dt;
        gridViewAccumulateData.DataBind();
    }
    protected void cmdConfirm_Click(object sender, EventArgs e)
    {
        try
        {

            //clearMessageLabel();
            if (this.chkAllow.Checked)
            {
                this.txtName.Enabled = true;
                this.txtEmail.Enabled = true;
                this.txtPhone.Enabled = true;
                bool proceed = true;

                if (String.IsNullOrEmpty(txtName.Text))
                {
                    this.rfName.Visible = true;
                    proceed = false;
                }
                else
                { this.rfName.Visible = false; }

                if (String.IsNullOrEmpty(txtEmail.Text))
                {
                    this.rfEmail.Visible = true;
                    if (proceed)
                        proceed = false;
                }
                else
                { this.rfEmail.Visible = false; }

                if (String.IsNullOrEmpty(txtPhone.Text))
                {
                    this.rfPhone.Visible = true;
                    if (proceed)
                        proceed = false;
                }
                else
                { this.rfPhone.Visible = false; }

                if (!proceed)
                { return; }
            }
            else
            {
                this.txtName.Enabled = false;
                this.txtEmail.Enabled = false;
                this.txtPhone.Enabled = false;
            }

            String fromview = "";
            if (ViewState["RFDetails"] != null)
                fromview = ViewState["RFDetails"].ToString();
            JavaScriptSerializer json = new JavaScriptSerializer();
            ArrayList al = new ArrayList();
            if (String.IsNullOrEmpty(fromview) == false)
                al = json.Deserialize<ArrayList>(fromview);

            //saves the records
            saveClaims(al);

            this.pnlReturnForm2.Visible = false;
            this.pnlReturnForm.Visible = true;
            Response.Redirect("SaveSuccessful.aspx?f=accessories");
        }
        catch (Exception ex)
        {
            showMessage(true, ex.Message);
        }
    }
    protected void saveClaims(ArrayList al)
    {
        int prevClaimID = 0;
        //int prevRFHeaderID = 0;
        int rfHeaderID = 0;
        int userID = int.Parse(sm.Decrypt(Session["UserID"].ToString()));
        for (int i = 0; i < al.Count; i++)
        {
            Model.ReturnFormAccessories rd = new JavaScriptSerializer().ConvertToType<Model.ReturnFormAccessories>(al[i]);

            //FORMAT DATE MM/DD/YYYY FROM DD/MM/YYYY
            if (rd.DateOfClaim != "")
                rd.DateOfClaim = formatDateForSQL(rd.DateOfClaim);
            if (rd.DateOfFailure != "")
                rd.DateOfFailure = formatDateForSQL(rd.DateOfFailure);
            if (rd.DateOfPartIncome != "")
                rd.DateOfPartIncome = formatDateForSQL(rd.DateOfPartIncome);


            Maintenance.ReturnFormAccessories rdMaint = new Maintenance.ReturnFormAccessories();
            Model.ContactPerson cp = new Model.ContactPerson();

            if (i == 0)
            {
             //Comment to allow different Portal Reference Number
                rfHeaderID = rdMaint.addReturnFormHeader(rd, userID);
                //add person to notify
                cp.RFHeaderID = rd.RFHeaderID;
                if (chkAllow.Checked)
                {
                    cp.Name = this.txtName.Text;
                    cp.PhoneNumber = this.txtPhone.Text;
                    cp.EmailAddress = this.txtEmail.Text;
                }
                else
                {
                    cp.Name = sm.Decrypt(Session["UserFullName"].ToString());
                    cp.PhoneNumber = sm.Decrypt(Session["PhoneNumber"].ToString());
                    cp.EmailAddress = sm.Decrypt(Session["EmailAddress"].ToString());
                }
                rdMaint.addContactPerson(cp);
            }
            //else
            //{
            //    if (prevClaimID != rd.ClaimID)
            //    {
            //        rd.RFHeaderID = rdMaint.addReturnFormHeader(rd, userID);//add person to notify
            //        cp.RFHeaderID = rd.RFHeaderID;
            //        if (chkAllow.Checked)
            //        {
            //            cp.Name = this.txtName.Text;
            //            cp.PhoneNumber = this.txtPhone.Text;
            //            cp.EmailAddress = this.txtEmail.Text;
            //        }
            //        else
            //        {
            //            cp.Name = sm.Decrypt(Session["UserFullName"].ToString());
            //            cp.PhoneNumber = sm.Decrypt(Session["PhoneNumber"].ToString());
            //            cp.EmailAddress = sm.Decrypt(Session["EmailAddress"].ToString());
            //        }
            //        rdMaint.addContactPerson(cp);
            //    }
            //    else
            //        rd.RFHeaderID = prevRFHeaderID;
            //}

            //prevRFHeaderID = rd.RFHeaderID;
            rd.RFHeaderID = rfHeaderID;
            rdMaint.addReturnFormAccessories(rd);
            //prevClaimID = rd.ClaimID;
            
        }
        Session.Add("CountClaim", al.Count);
    }
    
    protected void btnControlCancel_Click(object sender, EventArgs e)
    {
        try
        {
            if (this.hdnOperation.Value == "edit")
            {
                this.pnlReturnForm.Visible = false;
                this.pnlReturnForm2.Visible = true;
            }
            if (this.hdnOperation.Value == "add")
            {
                this.pnlReturnForm.Visible = true;
                this.pnlReturnForm2.Visible = false;
                ViewState["RFDetails"] = null;
            }
            refreshFields();
            refreshLinkViewList();
            this.hdnOperation.Value = "add";
            //this.hdnLastClicked.Value = "Cancel";
            showMessage(true, "Transaction was cancelled.");
        }
        catch (Exception ex)
        {
            showMessage(true, ex.Message);
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        showMessage(true, "Transaction was cancelled.");
        this.pnlReturnForm.Visible = true;
        this.pnlReturnForm2.Visible = false;
        this.hdnOperation.Value = "add";
        this.btnAddSameControl.Visible = true;
        this.btnAddNewControl.Visible = true;
        refreshFields();
        refreshLinkViewList();
        ViewState["RFDetails"] = null;
        //this.hdnLastClicked.Value = "Cancel";
    }
    

    // 12/16
    protected void gridViewAccumulateData_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        Type type = sender.GetType();
        int index = Int32.Parse(e.CommandArgument.ToString());
        if (e.CommandName == "EditSelectedRow")
        {
            try
            {
                if (index == -1)
                {
                    showMessage(true, "No selected row to modify.");
                    return;
                }

                String fromview = ViewState["RFDetails"].ToString();
                JavaScriptSerializer json = new JavaScriptSerializer();
                ArrayList al = json.Deserialize<ArrayList>(fromview);

                int x = index;
                this.hdnIndexToEdit.Value = x.ToString();
                Model.ReturnFormAccessories rfd = new JavaScriptSerializer().ConvertToType<Model.ReturnFormAccessories>(al[x]);

                this.btnAddNewControl.Visible = false;
                this.btnAddSameControl.Visible = false;
                this.pnlReturnForm.Visible = true;
                this.pnlReturnForm2.Visible = false;
                this.hdnOperation.Value = "edit";
                //hdnClaimID.Value = rfd.ClaimID.ToString();

                txtComponentReference.Text = rfd.ControlComponentReference;
                optProductType.SelectedValue = rfd.ProductType;
                ddControlClaimLists.SelectedValue = rfd.Claim;
                txtControlCustomerClaimReference.Text = rfd.ControlCustomerClaimReference;
                txtControlPCNNumber.Text = rfd.PCNNumber;
                txtControlModel.SelectedValue = rfd.PCNNumber;
                txtControlDataCode.Text = rfd.DataCode;
               
                

                txtControlQty.Text = rfd.Quantity;
                txtControlDateOfClaim.Text = rfd.DateOfClaim;
                txtControlCustomerClaimReference.Text = rfd.ControlCustomerClaimReference;
                optControlTypeOfReturn.SelectedValue = getSelectedReturnTypeValue(rfd.TypeOfReturn);
                optControlApplication.SelectedValue = rfd.Application;
                optControlRefrigerant.SelectedValue = rfd.Refrigerant;
                optControlRequest1.SelectedValue = rfd.Request;

                if (rfd.Claim == "Cooling/Heating capacity too low" ||
                    rfd.Claim == "High Power Input" ||
                    rfd.Claim == "High Amps" ||
                    rfd.Claim == "Low COP/EER")
                {
                    pnlControlWorkingPoint.Visible = true;
                    txtControlWorkingPointTO.Text = rfd.WorkingPointTO;
                    txtControlWorkingPointTC.Text = rfd.WorkingPointTC;
                    txtControlWorkingPointSuperHeat.Text = rfd.WorkingPointSuperHeat;
                    txtControlWorkingPointSubCooling.Text = rfd.WorkingPointSubCooling;
                }
                else
                {
                    pnlControlWorkingPoint.Visible = false;
                    txtControlWorkingPointTO.Text = "";
                    txtControlWorkingPointTC.Text = "";
                    txtControlWorkingPointSuperHeat.Text = "";
                    txtControlWorkingPointSubCooling.Text = "";
                }

                if (rfd.Claim == "Other")
                {
                    lblControlClaimDescription.Visible = true;
                    txtControlClaimDescription.Visible = true;
                    txtControlClaimDescription.Text = rfd.ClaimDescription;
                }
                else
                {
                    lblControlClaimDescription.Visible = false;
                    txtControlClaimDescription.Visible = false;
                    txtControlClaimDescription.Text = "";
                }

                if (rfd.Refrigerant == "Other")
                {
                    pnlControlRefrigerant.Visible = true;
                    txtControlRefrigerant.Text = rfd.OtherRefrigerant;
                }
                else
                {
                    pnlControlRefrigerant.Visible = false;
                    txtControlRefrigerant.Text = "";
                }

                if (rfd.Request == "Specific Test")
                {

                    txtControlRequestSpecificTest.Visible = true;
                    txtControlRequestSpecificTest.Text = rfd.RequestSpecificTestRemarks;
                }
                else
                {
                    txtControlRequestSpecificTest.Visible = false;
                    txtControlRequestSpecificTest.Text = "";
                }

                /*
                if (rfd.RequestSpecificTest)
                {
                    txtControlRequestSpecificTest.Visible = true;
                    txtControlRequestSpecificTest.Text = rfd.RequestSpecificTestRemarks;
                }
                else
                {
                    txtControlRequestSpecificTest.Visible = false;
                    txtControlRequestSpecificTest.Text = "";
                }
                 * */

                /*
                optControlRequest.Items[0].Selected = rfd.RequestWarranty;
                optControlRequest.Items[1].Selected = rfd.Analysis;
                optControlRequest.Items[2].Selected = rfd.OutOfWarranty;
                optControlRequest.Items[3].Selected = rfd.SpecificTest;
                */

                ddCustomerList.SelectedValue = rfd.CustomerNumber.ToString();
                hdnItemNumber.Value = rfd.ItemNumber;

                if (String.IsNullOrEmpty(rfd.Configuration))
                    optControlConfiguration.SelectedIndex = -1;
                else
                    optControlConfiguration.SelectedValue = rfd.Configuration;

                txtControlDateOfFailure.Text = rfd.DateOfFailure;
                txtControlRunningHours.Text = rfd.RunningHours;
                txtRemarks.Value = rfd.Remarks;
                
              
                txtControlDateOfPartIncome.Text = rfd.DateOfPartIncome;
                hdnControlCWOfPartIncome.Value = rfd.CurrentWeekOfPartIncome;
                hdnControlMonthIncome.Value = rfd.MonthIncome;



                CheckControlPCNumber();
                txtControlWarranty.Text = getWarranty(rfd.DataCode);
            }
            catch (Exception ex)
            {
                showMessage(true, ex.Message);
            }
        }
        if (e.CommandName == "RemoveSelectedRow")
        {
            String fromview = ViewState["RFDetails"].ToString();
            JavaScriptSerializer json = new JavaScriptSerializer();
            ArrayList al = json.Deserialize<ArrayList>(fromview);
            ArrayList al2 = new ArrayList();

            for (int i = 0; i < al.Count; i++)
            {
                if (i != index)
                {
                    al2.Add(al[i]);
                }
            }

            if (al2.Count == 0)
            {
                cmdCancel_Click(sender, e);
            }
            else
            {
                ViewState["RFDetails"] = json.Serialize(al2);
                saveAndFinalize();
            }
        }
    }

    #region Utilities
    private void initialization()
    {
        showMessage(false, "");
        txtControlDataCode.Attributes.Add("onblur", "calculateWarranty()");

        optControlRequest1.Items[1].Attributes.Add("onclick", "selectedRequestSpecificTest()");
        string tooltip = "Line Return : if you are a OEM and that the compressor failed during one of your assembly test \r\n";
        tooltip += "Field Returns : compressor failed at final customer or you are a Wholesalers \r\n";
        tooltip += "Lab/sample Test : compressor is returned after Engineering test or development test";

        Image5.ToolTip = tooltip.Replace("\\n", "\n"); ;
        txtControlWarranty.Text = hdnControlWarranty.Value;
    }
    private void updateLinkViewList()
    {
        String fromview = "";
        if (ViewState["RFDetails"] != null)
            fromview = ViewState["RFDetails"].ToString();
        JavaScriptSerializer json = new JavaScriptSerializer();
        ArrayList al = new ArrayList();
        if (String.IsNullOrEmpty(fromview) == false)
            al = json.Deserialize<ArrayList>(fromview);

        this.lnkViewList.Visible = false;
        if (al.Count > 0) {
            string recordCount;
            recordCount = "You have recorded " + al.Count + " accessories.";
            recordCount += " Click this to view the list.";
            this.lnkViewList.Visible = true;
            this.lnkViewList.Text = recordCount;
        }
        //if (Int32.Parse(this.hdnRecordsCount.Value) > 0)
        //{
            
        //    string recordCount;
        //    if (Int32.Parse(this.hdnRecordsCount.Value) > 1)
        //        recordCount = "You have recorded " + al.Count + " controls.";
        //    else
        //        recordCount = "You have recorded " + al.Count + " control.";
        //    recordCount += " Click this to view the list.";
        //    this.lnkViewList.Visible = true;
        //    this.lnkViewList.Text = recordCount;
        //}
    }
    private void refreshLinkViewList()
    {
        this.hdnRecordsCount.Value = "0";
        this.lnkViewList.Visible = false;
    }
    private void showMessage(bool showHide, string message)
    {
        LabelMessage.Text = message;
        LabelMessage.Visible = showHide;
    }
    /*
    protected Boolean checkRadioButton()
    {
        bool proceed = false;
        foreach (ListItem li in optControlRequest.Items)
        {
            if (li.Selected)
            {
                req1.Visible = false;
                proceed = true;
                break;
            }
            else
            {
                req1.Visible = true;
            }
        }
        return proceed;
    }
    */
    private bool isDateValid(string dateString)
    {
        bool isValid = true;
        if (!String.IsNullOrEmpty(dateString))
        {
            DateTime date;
            System.Data.SqlTypes.SqlDateTime sqlDateTime;
            if (DateTime.TryParse(dateString, out date))
            {
                try { sqlDateTime = new System.Data.SqlTypes.SqlDateTime(date); isValid = true; }
                catch { isValid = false; }
            }
            else
                isValid = false;
        }
        return isValid;
    }
    protected string getReturnTypeValue(string _value)
    {
        string returnedValue;
        switch (_value)
        {
            case "Line Return":
                {
                    returnedValue = "LR";
                    break;
                }
            case "Field Return":
                {
                    returnedValue = "FR";
                    break;
                }
            case "Commercial Return":
                {
                    returnedValue = "CR";
                    break;
                }
            default:
                {
                    returnedValue = "SA";
                    break;
                }
        }
        return returnedValue;
    }
    protected string getSelectedReturnTypeValue(string _value)
    {
        string returnedValue;
        switch (_value)
        {
            case "LR":
                {
                    returnedValue = "Line Return";
                    break;
                }
            case "FR":
                {
                    returnedValue = "Field Return";
                    break;
                }
            case "CR":
                {
                    returnedValue = "Commercial Return";
                    break;
                }
            default:
                {
                    returnedValue = "Lab/Sample Test";
                    break;
                }
        }
        return returnedValue;
    }
    protected void refreshFields()
    {
        DateTime dt = DateTime.Now;
        CultureInfo ciCurr = CultureInfo.CurrentCulture;
        int weekNum = ciCurr.Calendar.GetWeekOfYear(dt, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

        optProductType.SelectedIndex = 2;
        //ddCustomerList.SelectedIndex = 0;
        ddControlClaimLists.SelectedIndex = 0;
        lblControlClaimDescription.Visible = false;
        txtControlClaimDescription.Visible = false;
        txtControlClaimDescription.Text = "";
        txtControlCustomerClaimReference.Text = "";
        txtControlPCNNumber.Text = "";
        txtControlModel.Text = "";
        hdnControlLine.Value = "";
        hdnControlProductGroup.Value = "";
        txtControlDataCode.Text = "";
        txtControlWarranty.Text = "";
        hdnControlWarranty.Value = "";
        txtControlQty.Text = "";
        txtComponentReference.Text = "";
        txtControlRunningHours.Text = "0";
        txtRemarks.Value = "";
        optControlTypeOfReturn.SelectedIndex = -1;
        optControlApplication.SelectedIndex = -1;
        optControlRefrigerant.SelectedIndex = -1;
        pnlControlRefrigerant.Visible = false;
        pnlControlWorkingPoint.Visible = false;
        optControlRequest1.SelectedIndex = -1;
        //optControlRequest.SelectedIndex = -1;
        txtControlRequestSpecificTest.Text = "";
        txtControlRequestSpecificTest.Visible = false;
        optControlConfiguration.SelectedIndex = -1;
        txtControlDateOfClaim.Text = dt.ToString("dd/MM/yyyy");
        txtControlDateOfFailure.Text = dt.ToString("dd/MM/yyyy");
        txtControlDateOfPartIncome.Text = "";//dt.ToString("dd/MM/yyyy");
        hdnControlCWOfPartIncome.Value = dt.Year.ToString() + "/" + weekNum;
        hdnControlMonthIncome.Value = dt.Year.ToString() + "/" + dt.Month.ToString();
    }
    [WebMethod]
    public static string getWarranty(string datacode)
    {
        
        
        if (datacode == null  || datacode == string.Empty)
        {
            return string.Empty;
        }
        
        //ADD VALIDATION IS NUMERIC 
        int test;
        if (!int.TryParse(datacode, out test))
        {
            return string.Empty;
        }
        if (datacode.Length < 4)
        {
            return string.Empty;
        }

            int year = int.Parse("20" + datacode.Substring(0, 2));
            int weekOfYear = int.Parse(datacode.Substring(2, 2));

            DateTime date = new DateTime(year, 1, 1);

            CalendarWeekRule cwr = CultureInfo.CurrentCulture.DateTimeFormat.CalendarWeekRule;
            DayOfWeek dow = CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;

            int daysOffset = (int)dow - (int)date.DayOfWeek;

            DateTime firstdateofcalweek = date.AddDays(daysOffset);

            int firstWeek = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(date, cwr, dow);

            if (firstWeek <= 1)
            {
                weekOfYear -= 1;
            }

            DateTime dateBaseOnYearWeek = firstdateofcalweek.AddDays(weekOfYear * 7);

            DateTime result = dateBaseOnYearWeek.AddMonths(18);
            string warrantyDate = result.ToString("dd/MM/yyyy"); 
            return warrantyDate;
          
        
        
    }
    #endregion



    protected void lnkViewList_Click(object sender, EventArgs e)
    {
        saveAndFinalize();
    }

    protected void gridViewAccumulateData_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void chkAllow_CheckedChanged(object sender, EventArgs e)
    {
        if (this.chkAllow.Checked)
        {
            this.txtName.Enabled = true;
            this.txtEmail.Enabled = true;
            this.txtPhone.Enabled = true;
        }
        else
        {
            this.txtName.Enabled = false;
            this.txtEmail.Enabled = false;
            this.txtPhone.Enabled = false;
        }
    }

    protected void txtControlModel_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.txtControlPCNNumber.Text = this.txtControlModel.SelectedValue;
    }
    protected void optControlTypeOfReturn_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.optControlTypeOfReturn.SelectedValue == "Commercial Return") {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "scriptkey", "<script>alert('This might be chosen only if already agreed with customer service or sales representative. If not approved, return will not be accepted');</script>");
        }
    }
    protected void ddCustomerList_SelectedIndexChanged(object sender, EventArgs e)
    {
        string customerno = ddCustomerList.SelectedValue;
        int userId = (Session["UserID"] == null) ? 0 : int.Parse(sm.Decrypt(Session["UserID"].ToString()));
        //bool isCustomer = (sm.Decrypt(Session["UserType"].ToString()) == "CUSTOMER") ? true : false;
        string usertype = sm.Decrypt(Session["UserType"].ToString());

        Maintenance.Customers c = new Maintenance.Customers();
 
        ddLocation.Items.Clear();
        ddLocation.DataSource = null;
        ddLocation.DataBind();

        ddLocation.DataSource = c.getLocationByCustomerNumberDT(customerno, userId, usertype);
        ddLocation.DataTextField = "LocationName";
        ddLocation.DataValueField = "LocationID";
        ddLocation.DataBind();
    }
    protected void cmdBackReturn_Click(object sender, EventArgs e)
    {

        if (Session["RFBackupDetails"] == null) { return; }
        List<Model.ReturnFormAccessories> list = new List<Model.ReturnFormAccessories>();
        list = (List<Model.ReturnFormAccessories>)Session["RFBackupDetails"];
        refreshFields();
        foreach (Model.ReturnFormAccessories rfd in list)
        {


            txtComponentReference.Text = rfd.ControlComponentReference;
            optProductType.SelectedValue = rfd.ProductType;
            ddControlClaimLists.SelectedValue = rfd.Claim;
            txtControlCustomerClaimReference.Text = rfd.ControlCustomerClaimReference;
            txtControlPCNNumber.Text = rfd.PCNNumber;
            txtControlModel.SelectedValue = rfd.PCNNumber;
            txtControlDataCode.Text = rfd.DataCode;



            txtControlQty.Text = rfd.Quantity;
            txtControlDateOfClaim.Text = rfd.DateOfClaim;
            txtControlCustomerClaimReference.Text = rfd.ControlCustomerClaimReference;
            if (rfd.TypeOfReturn != "") 
                optControlTypeOfReturn.SelectedValue = getSelectedReturnTypeValue(rfd.TypeOfReturn);
            if (rfd.Application != "")
                optControlApplication.SelectedValue = rfd.Application;
            if (rfd.Refrigerant != "")
                optControlRefrigerant.SelectedValue = rfd.Refrigerant;
            if (rfd.Request != "")
                optControlRequest1.SelectedValue = rfd.Request;

            if (rfd.Claim == "Cooling/Heating capacity too low" ||
                rfd.Claim == "High Power Input" ||
                rfd.Claim == "High Amps" ||
                rfd.Claim == "Low COP/EER")
            {
                pnlControlWorkingPoint.Visible = true;
                txtControlWorkingPointTO.Text = rfd.WorkingPointTO;
                txtControlWorkingPointTC.Text = rfd.WorkingPointTC;
                txtControlWorkingPointSuperHeat.Text = rfd.WorkingPointSuperHeat;
                txtControlWorkingPointSubCooling.Text = rfd.WorkingPointSubCooling;
            }
            else
            {
                pnlControlWorkingPoint.Visible = false;
                txtControlWorkingPointTO.Text = "";
                txtControlWorkingPointTC.Text = "";
                txtControlWorkingPointSuperHeat.Text = "";
                txtControlWorkingPointSubCooling.Text = "";
            }

            if (rfd.Claim == "Other")
            {
                lblControlClaimDescription.Visible = true;
                txtControlClaimDescription.Visible = true;
                txtControlClaimDescription.Text = rfd.ClaimDescription;
            }
            else
            {
                lblControlClaimDescription.Visible = false;
                txtControlClaimDescription.Visible = false;
                txtControlClaimDescription.Text = "";
            }

            if (rfd.Refrigerant == "Other")
            {
                pnlControlRefrigerant.Visible = true;
                txtControlRefrigerant.Text = rfd.OtherRefrigerant;
            }
            else
            {
                pnlControlRefrigerant.Visible = false;
                txtControlRefrigerant.Text = "";
            }

            if (rfd.Request == "Specific Test")
            {

                txtControlRequestSpecificTest.Visible = true;
                txtControlRequestSpecificTest.Text = rfd.RequestSpecificTestRemarks;
            }
            else
            {
                txtControlRequestSpecificTest.Visible = false;
                txtControlRequestSpecificTest.Text = "";
            }

            /*
            if (rfd.RequestSpecificTest)
            {
                txtControlRequestSpecificTest.Visible = true;
                txtControlRequestSpecificTest.Text = rfd.RequestSpecificTestRemarks;
            }
            else
            {
                txtControlRequestSpecificTest.Visible = false;
                txtControlRequestSpecificTest.Text = "";
            }
             * */

            /*
            optControlRequest.Items[0].Selected = rfd.RequestWarranty;
            optControlRequest.Items[1].Selected = rfd.Analysis;
            optControlRequest.Items[2].Selected = rfd.OutOfWarranty;
            optControlRequest.Items[3].Selected = rfd.SpecificTest;
            */

            ddCustomerList.SelectedValue = rfd.CustomerNumber.ToString();
            hdnItemNumber.Value = rfd.ItemNumber;

            if (String.IsNullOrEmpty(rfd.Configuration))
                optControlConfiguration.SelectedIndex = -1;
            else
                optControlConfiguration.SelectedValue = rfd.Configuration;

            txtControlDateOfFailure.Text = rfd.DateOfFailure;
            txtControlRunningHours.Text = rfd.RunningHours;
            txtRemarks.Value = rfd.Remarks;


            txtControlDateOfPartIncome.Text = rfd.DateOfPartIncome;
            hdnControlCWOfPartIncome.Value = rfd.CurrentWeekOfPartIncome;
            hdnControlMonthIncome.Value = rfd.MonthIncome;
             
            CheckControlPCNumber();
            txtControlWarranty.Text = getWarranty(rfd.DataCode);
            
        }


        Session["RFBackupDetails"] = null;
        this.hdnOperation.Value = "add";

        this.btnAddNewControl.Visible = true;
        this.btnAddSameControl.Visible = true;
        this.pnlReturnForm.Visible = true;
        this.pnlReturnForm2.Visible = false;

     

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Model.ReturnFormAccessories> CheckControlPCNumber(string pcnNumber)
    {
        Maintenance.ReturnFormAccessories mrfd = new Maintenance.ReturnFormAccessories();
        List<Model.ReturnFormAccessories> list = new List<Model.ReturnFormAccessories>();
        Utility u = new Utility();
        DataTable dt = mrfd.getModelByPCNNumberSearch(pcnNumber);
        foreach (DataRow dr in dt.Rows)
        {
            Model.ReturnFormAccessories rfd = new Model.ReturnFormAccessories();
            rfd.ModelProduct = u.extractStringValue(dr["Description1"], Utility.DataType.STRING);
            rfd.PCNNumber = u.extractStringValue(dr["PCNNumber"], Utility.DataType.STRING);
            list.Add(rfd);
        }

        return list;
    }

    
}