﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using General.AvianCrypto;

public partial class UserAccount : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // CHeck if admin to show/hide the maintenancne
        SecurityMaster sm = new SecurityMaster();
        string user = sm.Decrypt(Session["UserType"].ToString());
        //if (user != "CUSTOMER QUALITY")
        //{
        //    this.pnlMaintenance.Visible = false;
        //}

        if (!IsPostBack)
        {
            if (Session["UserID"] == null)
                Response.Redirect(@"~/Login.aspx");
            else
                getUserInfo();
        }

    }
    protected void cmdSubmit_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
            saveUserInformation();
    }

    private void saveUserInformation()
    {
        Maintenance.UserInformation userMaint = new Maintenance.UserInformation();
        Model.UserInformation userinfo = new Model.UserInformation();
        General.AvianCrypto.SecurityMaster sm = new General.AvianCrypto.SecurityMaster();
        userinfo.EmailAddress =  textboxEmail.Text;
        userinfo.UserFirstName = textboxFirstName.Text;
        userinfo.UserLastName = textboxLastName.Text;
        userinfo.PhoneNumber = textboxTelephone.Text;
        userinfo.Title = dropdownTitle.Text;
        userinfo.Function = textboxFunction.Text;
        userinfo.ReceptionActivated = chkYesReception.Checked;
        userinfo.AnalysisActivated = chkYesAnalysis.Checked;
        userinfo.CDActivated = chkYesCredit.Checked;
        userinfo.UserID = int.Parse(sm.Decrypt(Session["UserID"].ToString()));
        //textboxFunction.Text
        int result = userMaint.updateUserInfo(userinfo);
        if (result > 0)
        {
            lblMessage.ForeColor = System.Drawing.Color.Green;
            lblMessage.Text = "User Information successfully updated!";
        }
        else
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = "An error encountered saving user information!";
        }
    }

    // get user info
    private void getUserInfo()
    {
        Maintenance.UserInformation maint = new Maintenance.UserInformation();
        General.AvianCrypto.SecurityMaster sm = new General.AvianCrypto.SecurityMaster();
        Model.UserInformation ui = maint.GetUserInfo(sm.Decrypt(Session["UserID"].ToString()));
        textboxFirstName.Text = ui.UserFirstName;
        textboxLastName.Text = ui.UserLastName;
        textboxEmail.Text = ui.EmailAddress;
        //ListItem itm = dropdownTitle.Items.FindByValue(ui.Title);
        //if(itm.Value != null) 
        dropdownTitle.Text = ui.UserTitle;
        textboxFunction.Text = ui.Function;
        textboxTelephone.Text = ui.PhoneNumber;

        chkYesReception.Checked = ui.ReceptionActivated;
        chkNoReception.Checked = !ui.ReceptionActivated;
        chkYesAnalysis.Checked = ui.AnalysisActivated;
        chkNoAnalysis.Checked = !ui.AnalysisActivated;
        chkYesCredit.Checked = ui.CDActivated;
        chkNoCredit.Checked = !ui.CDActivated;
    }

    protected void cmdCancel_Click1(object sender, EventArgs e)
    {
        Response.Redirect("Home.aspx");
    }
}