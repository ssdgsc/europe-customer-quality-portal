﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections.Specialized;
using System.Web.Script.Serialization;
using System.Collections;
using General;
using Maintenance;
using System.IO;
using System.Net.Mail;
using General.AvianCrypto;
using System.Text;

public partial class SaveSuccessful : System.Web.UI.Page
{
    int rfheaderid;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserID"] == null)
        {
            Response.Redirect("login.aspx");
            return;
        }

        // CHeck if admin to show/hide the maintenancne
        SecurityMaster sm = new SecurityMaster();
        System.Web.HttpRequest request = System.Web.HttpContext.Current.Request;
        string form = request.QueryString["f"]; 
        string user = sm.Decrypt(Session["UserType"].ToString());
        //if (user != "CUSTOMER QUALITY")
        //{
        //    this.pnlMaintenance.Visible = false;
        //}
        if (Session["CountClaim"] == null) { 
            if (form == "comp")
            {
                Response.Redirect("ReturnFormCompressor.aspx");
            }
            else if (form == "ctrl")
            {
                Response.Redirect("ReturnFormControl.aspx");
            }
            else if (form == "accessories")
            {
                Response.Redirect("ReturnFormAccessories.aspx");
            }
            Response.Redirect("home.aspx");
            return;
        }
        //if (!Page.IsPostBack)
        //{
            Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
            string countClaim = Session["CountClaim"].ToString();
            Utility u = new Utility();
            //ArrayList alRefNum = new ArrayList();
            string referencenumber = "";
            
            // [START] [jpbersonda] [4/12/17]
            if (int.Parse(countClaim) > 1)
                lblMsgBeforeCPerson.Text = "If you have any questions or concerns about these returns, please contact ";
            else
                lblMsgBeforeCPerson.Text = "If you have any questions or concerns about this return, please contact ";
            // [END] [jpbersonda] [4/12/17]
         
            DataTable dtHeader = new DataTable();

            if (form == "comp")
                dtHeader = mrfd.getSavedReturnFormHeader(int.Parse(countClaim));
            else if (form == "ctrl")
                dtHeader = mrfd.getSavedReturnFormHeader();
            else if (form == "accessories")
                dtHeader = mrfd.getSavedReturnFormHeader();

           
            //DataTable dtHeader = mrfd.getSavedReturnFormHeader();
            string[] listRFHeader = new string[dtHeader.Rows.Count];
            string[] listReferenceNumber = new string[dtHeader.Rows.Count];
            bool lineheader = true;
            int ctr = 0;
            
            DataTable dtDetail = new DataTable();
            foreach (DataRow drHeader in dtHeader.Rows)
            {
                referencenumber = u.extractStringValue(drHeader["ReferenceNumber"], Utility.DataType.STRING);
                rfheaderid = int.Parse(u.extractStringValue(drHeader["RFHeaderID"], Utility.DataType.NUMBER)); 

                dtDetail = new DataTable();
                listRFHeader[ctr] = rfheaderid.ToString();
                listReferenceNumber[ctr] = referencenumber;
                
                if (form == "comp")
                    dtDetail = mrfd.getSavedReturnFormDetail(rfheaderid.ToString());
                else if (form == "ctrl")
                    dtDetail = mrfd.getSavedReturnFormControl(rfheaderid);
                else if (form == "accessories")
                    dtDetail = mrfd.getSavedReturnFormAccessories(rfheaderid);
                ctr++;
                lineheader = true; 
               
                int line = 1;
                foreach (DataRow drDetail in dtDetail.Rows)
                {
                     
                    if (lineheader == true)
                    {
                        TableRow th = new TableRow();
                        TableCell th1 = new TableCell();
                        TableCell th2 = new TableCell();
                        TableCell th3 = new TableCell();
                        TableCell th4 = new TableCell();
                        TableCell th5 = new TableCell();
                        th1.Text = "Line";

                        if (form == "comp")
                            th2.Text = "Serial Number";
                        else if (form == "ctrl")
                            th2.Text = "PCN Number";
                        else if (form == "accessories")
                            th2.Text = "Item Number";

                        th3.Text = (form == "ctrl" || form == "comp") ? "Model" : "Item Description";
                        th4.Text = "Claim";

                        //if (form == "comp") {
                        //    th5.Text = "Reference No.";
                        //    th.Cells.Add(th5); 
                        //} 

                        th.Cells.Add(th1);
                        th.Cells.Add(th2);
                        th.Cells.Add(th3);
                        th.Cells.Add(th4);
                        this.tblResult.Rows.Add(th);
                    }

                    TableRow tr2 = new TableRow();
                    TableCell tc21 = new TableCell();
                    TableCell tc22 = new TableCell();
                    TableCell tc23 = new TableCell();
                    TableCell tc24 = new TableCell();
                    TableCell tc25 = new TableCell();
                    tc21.Text = line.ToString();
                    tc22.Text = u.extractStringValue(drDetail["SerialNumber"], Utility.DataType.STRING);
                    tc23.Text = u.extractStringValue(drDetail["Model"], Utility.DataType.STRING);
                    tc24.Text = u.extractStringValue(drDetail["Claim"], Utility.DataType.STRING);
                    //if (form == "comp")
                    //{
                    //    tc25.Text = u.extractStringValue(drDetail["ReferenceNumber"], Utility.DataType.STRING);
                    //    tr2.Cells.Add(tc25);
                    //} 
                    tr2.Cells.Add(tc21);
                    tr2.Cells.Add(tc22);
                    tr2.Cells.Add(tc23);
                    tr2.Cells.Add(tc24);
                    this.tblResult.Rows.Add(tr2);

                    line = line + 1;
                    lineheader = false;
                }

                TableRow tr = new TableRow();
                tr.Height = 5;
                this.tblResult.Rows.Add(tr);

                
                TableRow tr3 = new TableRow();
                TableCell tc3 = new TableCell();
                tc3.Text = "Have  been recorded and assigned number " + referencenumber;
                tc3.ColumnSpan = 4;
                tr3.Cells.Add(tc3);
                this.tblResult.Rows.Add(tr3);

                TableRow tr4 = new TableRow();
                tr4.Height = 10;
                this.tblResult.Rows.Add(tr4);
                 
                
            }

            DataTable dt = mrfd.getContactPerson();
            foreach (DataRow dr in dt.Rows)
            {
                this.lblContactPerson.Text = u.extractStringValue(dr["Name"], Utility.DataType.STRING);
                this.lblPhoneNum.Text = u.extractStringValue(dr["PhoneNumber"], Utility.DataType.STRING);
                this.lblEmailAdd.Text = u.extractStringValue(dr["EmailAddress"], Utility.DataType.STRING);
            }
 
            this.lblRefNum.Text = (form == "comp") ? String.Join(", ", listReferenceNumber) : referencenumber;
            string controlID = "";

            if (form == "ctrl") { 
                Maintenance.ReturnFormControl returnFormControl = new Maintenance.ReturnFormControl();
                Model.ReturnFormControl returnFormControlModel = returnFormControl.getReturnFormControlByHeaderID(rfheaderid.ToString());
                controlID = returnFormControlModel.RFDetailID.ToString();
            }
            if (form == "accessories")
            {
                Maintenance.ReturnFormAccessories returnFormAccessories = new Maintenance.ReturnFormAccessories();
                Model.ReturnFormAccessories returnFormAccessoriesModel = returnFormAccessories.getReturnFormAccessoriesByHeaderID(rfheaderid.ToString());
                controlID = returnFormAccessoriesModel.RFDetailID.ToString();
            }

            if (form == "comp")
            {
                Maintenance.ReturnFormDetail returnFormDetail = null;
                Model.ReturnFormDetail returnFormDetailModel = null;
                try
                {
                    // [START] [jpbersonda 6-28-2017] --For Generating Compressor Report 

                    StringBuilder tempControlID = new StringBuilder();

                    returnFormDetail = new Maintenance.ReturnFormDetail();

                    for (int x = 0; x < listRFHeader.Count(); x++)
                    {
                        returnFormDetailModel = returnFormDetail.getReturnFormDetailByHeaderId(Convert.ToInt32(listRFHeader[x]));
                        
                        if(returnFormDetail != null)
                            tempControlID.Append(returnFormDetailModel.RFDetailID.ToString() + ",");

                    }

                        string compReportControlID = tempControlID.ToString();
                        compReportControlID = compReportControlID.Substring(0, tempControlID.Length - 1);

                        Session.Add("CompReportControlID", compReportControlID);
                        controlID = "0000";

                        tempControlID = null;
                        compReportControlID = string.Empty;

                    // [END] [jpbersonda 6-28-2017]   --For Generating Compressor Report 
                    
                     // 6-28-2017 - remove
                     //returnFormDetail = new Maintenance.ReturnFormDetail();
                     //returnFormDetailModel = returnFormDetail.getReturnFormDetailByHeaderId(rfheaderid);
                     //controlID = returnFormDetailModel.RFDetailID.ToString();
                }
                catch (Exception ex){ }

                returnFormDetail = null;
                returnFormDetailModel = null;
            }
             
            hfControlID.Value = controlID;
            if (controlID == "" ) 
                cmdPrintControlReport.Visible = false;  
            cmdEmailControlReport.Visible = false;
           
        //}  

    }

    protected void cmdAdd_Click(object sender, EventArgs e)
    {
        System.Web.HttpRequest request = System.Web.HttpContext.Current.Request;
        string form = request.QueryString["f"];
        Session["CountClaim"] = null;
        if (form == "comp")
        {
            Response.Redirect("ReturnFormCompressor.aspx");
        }
        else if (form == "ctrl")
        {
            Response.Redirect("ReturnFormControl.aspx"); 
        }
       
    }
    protected void cmdEmailControlReport_Click(object sender, EventArgs e)
    {
        var reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        Maintenance.ReturnFormControl returnFormControl = new Maintenance.ReturnFormControl();
        string rfID = hfControlID.Value;
        var result = reportProcessor.RenderReport("PDF", returnFormControl.printFormControl(rfID), null);

        MailMaster mailMaster = new MailMaster();
        Model.ReturnFormControl returnFormDetailControl = new Model.ReturnFormControl();
       // returnFormDetailControl = returnFormControl.getReturnFormControl(int.Parse(this.hfControlID.Text));
        
        Maintenance.Customers customersMaintenance = new Maintenance.Customers();
        Model.Customers customers = (Model.Customers)customersMaintenance.getCustomersByCustomerNumber(returnFormDetailControl.CustomerNumber)[0];
        string emailAddress = returnFormControl.getContactPersonsEmailByHeaderID(returnFormDetailControl.RFHeaderID);


        if (customers.CustomerEmail!="")
        {
            MemoryStream ms = new MemoryStream(result.DocumentBytes);
            ms.Position = 0;
            Attachment attachment = new Attachment(ms, "Return Form Control.pdf");

            mailMaster.sendMailWithAttachment(customers.CustomerEmail, lblEmailAdd.Text, "Portal Reference No: " + returnFormDetailControl.PortalReference, "Attached file is the Return Form Control", attachment);

            string emails = customers.CustomerEmail.Replace(",","\\n");

            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert" + UniqueID,
            "alert('Email Successfully Send to: \\n" + emails + " \\nCopy to: \\n " + emailAddress + "');", true);
        }
        else{
            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert" + UniqueID,
            "alert('Email sending failed. No email address found in selected customer');", true);
        }

        // "alert('Email Successfully Send to " + customers.CustomerEmail + "');", true);
    }
    //protected void cmdPrintControlReport_Click(object sender, EventArgs e)
    //{
    //    int controlID = int.Parse(this.hfControlID.Text);
    //    //Response.Redirect("_pageType=pdf&_controlID=" + controlID);
    //    Response.Write("<script>window.open('printReturnFormControl.aspx?_controlID=" + controlID + "&_pageType=pdf&_isNew=1','_blank')</script>");
    //}
    protected void Button1_Click(object sender, EventArgs e)
    {
        int controlID = int.Parse(this.hfControlID.Value);
        //Response.Redirect("_pageType=pdf&_controlID=" + controlID);
        Response.Write("<script>window.open('printReturnFormControl.aspx?_controlID=" + controlID + "&_pageType=xls','_blank')</script>");
    }
}
