﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using General;
using System.Web.Script.Serialization;
using System.IO;
using System.Globalization;
using General.AvianCrypto;
using System.Web.Script.Services;
using System.Collections.Generic;

public partial class ReturnFormCompressor : System.Web.UI.Page
{
    General.AvianCrypto.SecurityMaster sm = new General.AvianCrypto.SecurityMaster();
    //
    protected void Page_Load(object sender, EventArgs e)
    {
        //// ADD THIS LINE TO SHOW CONTROL RETURN FORM ONLY 
        //Response.Redirect("ReturnFormControl.aspx");

        if (Session["UserID"] == null)
        {
            Response.Redirect("login.aspx");
            return;
        }

        // CHeck if admin to show/hide the maintenancne
        SecurityMaster sm = new SecurityMaster();
        string user = sm.Decrypt(Session["UserType"].ToString());
        this.hfRole.Value = user;
        //if (user != "CUSTOMER QUALITY")
        //{
        //    this.pnlMaintenance.Visible = false;
        //}

        Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();

        initialization();
        if (!Page.IsPostBack)
        {
            txtCompressorModel.Attributes.Add("readonly", "readonly");
            Session["RFCompBackupDetails"] = null;
            txtCompressorCreationDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtCompressorDateOfFailure.Text = DateTime.Now.ToString("dd/MM/yyyy");
            int userid = int.Parse(sm.Decrypt(Session["UserID"].ToString()));

            if (this.hfRole.Value == "CUSTOMER QUALITY" || this.hfRole.Value == "CSO")
                this.ddCustomerList.DataSource = mrfd.getAllCustomerListCompressor(this.hfRole.Value, userid); 
            else 
                this.ddCustomerList.DataSource = mrfd.getCustomerList(userid);

            this.ddCustomerList.DataTextField = "CUSTOMERNAMENUMBER";
            this.ddCustomerList.DataValueField = "CUSTOMERNUMBER";
            this.ddCustomerList.DataBind();

            Param prm = new Param(); 
            prm.getParams(ddCompressorClaimLists, "CompReason");
 
            /*
            if (sm.Decrypt(Session["UserType"].ToString()) == "CUSTOMER")
            {
                string x = sm.Decrypt(Session["CustomerName"].ToString());
                this.ddCustomerList.SelectedItem.Text = x;
                this.ddCustomerList.Enabled = false;
            }
            */
            this.btnAddNewClaim.Visible = true;
            this.btnAddSameClaim.Visible = true;
            this.pnlReturnForm.Visible = true;
            this.pnlReturnForm2.Visible = false;
            this.hdnRecordsCount.Value = "0";
            ViewState["RFDetails"] = null;
            updateLinkViewList();
        }
    }

    protected void optProductType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //checkPendingTransaction
        if (optProductType.SelectedIndex == 1)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "checkPendingTransaction", "checkPendingTransaction(" + this.hdnRecordsCount.Value + ",'ReturnFormControl.aspx');", true);
            //Response.Redirect("ReturnFormControl.aspx");
        }
        if (optProductType.SelectedIndex == 2)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "checkPendingTransaction", "checkPendingTransaction(" + this.hdnRecordsCount.Value + ",'ReturnFormAccessories.aspx');", true);
            //Response.Redirect("ReturnFormControl.aspx");
        }
        
    }
    protected void btnCheckCompressorSerialNumber_Click(object sender, EventArgs e)
    {
        checkSerialNumber();
    }
    public bool checkSerialNumber()
    {
        try
        {
            Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
            Model.ReturnFormDetail rfd = new Model.ReturnFormDetail();
            Utility u = new Utility();
            DataTable dt = mrfd.getModelBySerial(txtCompressorSerialNumber.Text);
            foreach (DataRow dr in dt.Rows)
            {
                rfd.ModelProduct = u.extractStringValue(dr["DESCP"], Utility.DataType.STRING);
                rfd.ItemNumber = u.extractStringValue(dr["ITNBR"], Utility.DataType.STRING);
            }
            if (String.IsNullOrEmpty(rfd.ModelProduct))
            {
                txtCompressorModel.Text = "";
                hdnItemNumber.Value = "";
                // [START] [jpbersonda] [4/26/2017]
                int tempInteger;
                // first 2 digits - year created
                string tempSerialNumber = ((!String.IsNullOrEmpty(txtCompressorSerialNumber.Text) 
                    && (txtCompressorSerialNumber.Text.Length >= 2)) ? txtCompressorSerialNumber.Text.Substring(0, 2) : "0");

                if (int.TryParse(tempSerialNumber, out tempInteger))
                {
                    // year 2007 and below
                    if (tempInteger <= 7)
                    {
                        showMessage(true, "Please double check the serial number. See our help for"
                        + "detailed information on serial numbers. If compressor has been produced in 2007 of before - "
                        + "serial number starting with 07 or lower - this compressor is too old for standard analysis. "
                        + "Please contact your Emerson Climate Technologies local representative to give some background "
                        + "of this return.");
                    }

                    // year 2008 and above
                    else if (tempInteger >= 8)
                    {
                        showMessage(true, "Invalid Serial Number");
                    }

                }
                
                // if 1st 2 digits of serial number is not numeric
                else
                    showMessage(true,"Invalid Serial Number");

                //dispose
                tempSerialNumber = string.Empty;
                tempInteger = 0;

                // [END] [jpbersonda] [4/26/2017]
                return false;
            }
            else
            {
                txtCompressorModel.Text = rfd.ModelProduct;
                hdnItemNumber.Value = rfd.ItemNumber;
                showMessage(false, "");
                return true;
            }
        }
        catch (Exception ex)
        {
            showMessage(true, ex.Message);
            return false;
        }
    }
    public DataTable getSerialNumber()
    { 
        Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
        Model.ReturnFormDetail rfd = new Model.ReturnFormDetail();
        Utility u = new Utility();
        return  mrfd.getModelBySerial(); 
    }

    protected void optCompressorRefrigerant_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (optCompressorRefrigerant.SelectedIndex == 5)
        {
            this.pnlCompressorRefrigerant.Visible = true;
            this.txtCompressorRefrigerant.Text = "";
        }
        else
            this.pnlCompressorRefrigerant.Visible = false;
    }

    //[START] [jpbersonda] [5/25/2017] [changes in request radio button values]
    protected void optCompressorRequest1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (optCompressorRequest1.SelectedIndex == 1)
        {
            //txtCompressorRequestSpecificTest.Visible = true;
            pnlCompressorRequestSpecificTest.Visible = true;
            optCompressorRequest1.Items[1].Attributes.Remove("onclick");
        }
        else
        {
            //txtCompressorRequestSpecificTest.Visible = false;
            pnlCompressorRequestSpecificTest.Visible = false;
            txtCompressorRequestSpecificTest.Text = "";
            optCompressorRequest1.Items[1].Attributes.Add("onclick", "selectedRequestSpecificTest()");
        }
    }
    //[END] [jpbersonda] [5/25/2017] [changes in request radio button values]

    protected void btnAddSameClaim_Click(object sender, EventArgs e)
    {
        
        if (!isDateValid(formatDateForSQL(this.txtCompressorDateOfFailure.Text.ToString())))
        {
            showMessage(true, "Invalid date for Date of Failure");
            return;
        }

        String fromview = "";
        string filename = "";
        if (ViewState["RFDetails"] != null)
            fromview = ViewState["RFDetails"].ToString();

        JavaScriptSerializer json = new JavaScriptSerializer();
        ArrayList al = new ArrayList();
        if (String.IsNullOrEmpty(fromview) == false)
            al = json.Deserialize<ArrayList>(fromview);

        try
        {
            Model.ReturnFormDetail rfd = new Model.ReturnFormDetail();
            Model.ReturnFormHeader rfh = new Model.ReturnFormHeader();
            Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();

            rfh.ReferenceNumber = DateTime.Now.ToShortDateString().Substring(2, 2) + "-0000"; ;
            rfh.Description = "";
            rfh.ReturnDate = DateTime.Now.ToShortDateString();
            rfd.ProductType = optProductType.SelectedValue;

           
            rfd.Claim = ddCompressorClaimLists.SelectedValue;
            if (ddCompressorClaimLists.SelectedValue == "Cooling/Heating capacity too low" ||
                ddCompressorClaimLists.SelectedValue == "High Power Input" ||
                ddCompressorClaimLists.SelectedValue == "High Amps" ||
                ddCompressorClaimLists.SelectedValue == "Low COP/EER")
            {
                rfd.WorkingPointTo = txtCompressorWorkingPointTO.Text;
                rfd.WorkingPointTc = txtCompressorWorkingPointTC.Text;
                rfd.WorkingPointSuperHeat = txtCompressorWorkingPointSuperHeat.Text;
                rfd.WorkingPointSubcooling = txtCompressorWorkingPointSubCooling.Text;
            }
            else
            {
                rfd.WorkingPointTo = "";
                rfd.WorkingPointTc = "";
                rfd.WorkingPointSuperHeat = "";
                rfd.WorkingPointSubcooling = "";
            }
            if (ddCompressorClaimLists.SelectedValue == "Other")
            { rfd.ClaimDescription = txtCompressorClaimDescription.Text; }
            else
            { rfd.ClaimDescription = ""; }
            // ADD LOCATION ID
            rfd.LocationID = ddLocation.SelectedValue;
            rfd.CustomerClaimReference = txtCustomerClaimReference.Text;
            rfd.SerialNumber = txtCompressorSerialNumber.Text;
            rfd.ModelProduct = txtCompressorModel.Text;
            rfd.DataCode = "";
            rfd.Warranty = "";
            rfd.Quantity = "";
            rfd.DateOfClaim = txtCompressorCreationDate.Text;
            rfd.CompressorComponentReference = txtCompressorReference.Text;
            rfd.TypeOfReturn = getReturnTypeValue(optCompressorTypeOfReturn.SelectedValue);
            rfd.Application = optCompressorApplication.SelectedValue;

            rfd.Refrigerant = optCompressorRefrigerant.SelectedValue;
            if (optCompressorRefrigerant.SelectedValue == "Other")
            { rfd.OtherRefrigerant = txtCompressorRefrigerant.Text; }
            else
            { rfd.OtherRefrigerant = ""; }

            // [START] [jpbersonda] [5/25/2017] [changes in request radio button values]]
            rfd.Request = optCompressorRequest1 .SelectedValue;
            if (optCompressorRequest1.SelectedValue == "Specific Request")
            { rfd.RequestSpecificTestRemarks = txtCompressorRequestSpecificTest.Text; }
            else
            { rfd.RequestSpecificTestRemarks = ""; }
            // [END] [jpbersonda] [5/25/2017] [changes in request radio button values]]


            rfd.CustomerNumber = ddCustomerList.SelectedValue;
            rfd.ItemNumber = hdnItemNumber.Value;
            rfd.Configuration = optCompressorConfiguration.SelectedValue;
            rfd.DateOfFailure = txtCompressorDateOfFailure.Text;
            rfd.RunningHours = (txtCompressorRunningHours.Text == "") ? "0" : txtCompressorRunningHours.Text;
            rfd.Remarks = txtRemarks.Value;

            rfd.FilePath = hdnFileName.Value;
            //if (UploadFile.HasFile)
            //{
                 
            //    filename = Path.GetFileName(UploadFile.FileName);
            //    if (!File.Exists(HttpContext.Current.Server.MapPath("UploadedFiles/") + filename))
            //        UploadFile.SaveAs(HttpContext.Current.Server.MapPath("UploadedFiles/") + filename);
            //    rfd.FilePath = "UploadedFiles\\" + filename;
            //}
            //else
            //{
            //    rfd.FilePath = "";
            //}
            al.Add(rfd);

            ViewState["RFDetails"] = json.Serialize(al);
            txtCompressorSerialNumber.Text = "";
            txtCompressorModel.Text = "";
            txtCompressorReference.Text = "";

            int count = Int32.Parse(this.hdnRecordsCount.Value);
            count = count + 1;
            this.hdnRecordsCount.Value = count.ToString();
            updateLinkViewList();
        }
        catch (Exception ex)
        {
            showMessage(true, ex.Message);
        }
    }
    protected void btnAddNewClaim_Click(object sender, EventArgs e)
    {
        if (!isDateValid(formatDateForSQL(this.txtCompressorDateOfFailure.Text)))
        {
            showMessage(true, "Invalid date for Date of Failure");
            return;
        }

        String fromview = "";
        string filename = "";
        if (ViewState["RFDetails"] != null)
            fromview = ViewState["RFDetails"].ToString();
        JavaScriptSerializer json = new JavaScriptSerializer();
        ArrayList al = new ArrayList();
        if (String.IsNullOrEmpty(fromview) == false)
            al = json.Deserialize<ArrayList>(fromview);

        try
        {
            Model.ReturnFormDetail rfd = new Model.ReturnFormDetail();
            Model.ReturnFormHeader rfh = new Model.ReturnFormHeader();
            Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();

            rfh.ReferenceNumber = DateTime.Now.ToShortDateString().Substring(2, 2) + "-0000"; ;
            rfh.Description = "";
            rfh.ReturnDate = DateTime.Now.ToShortDateString();

            rfd.ProductType = optProductType.SelectedValue;

            rfd.Claim = ddCompressorClaimLists.SelectedValue;
            if (ddCompressorClaimLists.SelectedValue == "Cooling/Heating capacity too low" ||
                ddCompressorClaimLists.SelectedValue == "High Power Input" ||
                ddCompressorClaimLists.SelectedValue == "High Amps" ||
                ddCompressorClaimLists.SelectedValue == "Low COP/EER")
            {
                rfd.WorkingPointTo = txtCompressorWorkingPointTO.Text;
                rfd.WorkingPointTc = txtCompressorWorkingPointTC.Text;
                rfd.WorkingPointSuperHeat = txtCompressorWorkingPointSuperHeat.Text;
                rfd.WorkingPointSubcooling = txtCompressorWorkingPointSubCooling.Text;
            }
            else
            {
                rfd.WorkingPointTo = "";
                rfd.WorkingPointTc = "";
                rfd.WorkingPointSuperHeat = "";
                rfd.WorkingPointSubcooling = "";
            }
            if (ddCompressorClaimLists.SelectedValue == "Other")
            { rfd.ClaimDescription = txtCompressorClaimDescription.Text; }
            else
            { rfd.ClaimDescription = ""; }

            rfd.CustomerClaimReference = txtCustomerClaimReference.Text;
            rfd.SerialNumber = txtCompressorSerialNumber.Text;
            rfd.ModelProduct = txtCompressorModel.Text;
            rfd.DataCode = "";
            rfd.Warranty = "";
            rfd.Quantity = "";
            rfd.DateOfClaim = txtCompressorCreationDate.Text;
            rfd.CompressorComponentReference = txtCompressorReference.Text;
            rfd.TypeOfReturn = getReturnTypeValue(optCompressorTypeOfReturn.SelectedValue);
            rfd.Application = optCompressorApplication.SelectedValue;
            // ADD LOCATION ID
            rfd.LocationID = ddLocation.SelectedValue;

            rfd.Refrigerant = optCompressorRefrigerant.SelectedValue;
            if (optCompressorRefrigerant.SelectedValue == "Other")
            { rfd.OtherRefrigerant = txtCompressorRefrigerant.Text; }
            else
            { rfd.OtherRefrigerant = ""; }

            // [START] [jpbersonda] [5/25/2017] [changes in request radio button values]]
            rfd.Request = optCompressorRequest1.SelectedValue;
            if (optCompressorRequest1.SelectedValue == "Specific Request")
            { rfd.RequestSpecificTestRemarks = txtCompressorRequestSpecificTest.Text; }
            else
            { rfd.RequestSpecificTestRemarks = ""; }
            // [END] [jpbersonda] [5/25/2017] [changes in request radio button values]]

            rfd.CustomerNumber = ddCustomerList.SelectedValue;
            rfd.ItemNumber = hdnItemNumber.Value;
            rfd.Configuration = optCompressorConfiguration.SelectedValue;
            rfd.DateOfFailure = txtCompressorDateOfFailure.Text;
            rfd.RunningHours = (txtCompressorRunningHours.Text == "") ? "0" : txtCompressorRunningHours.Text;
            rfd.Remarks = txtRemarks.Value;

            rfd.FilePath = hdnFileName.Value;
         
            al.Add(rfd);

            ViewState["RFDetails"] = json.Serialize(al);
            refreshFields();

            int count = Int32.Parse(this.hdnRecordsCount.Value);
            count = count + 1;
            this.hdnRecordsCount.Value = count.ToString();
            updateLinkViewList();
        }
        catch (Exception ex)
        {
            showMessage(true, ex.Message);
        }
    }

    public string formatDateForSQL(string date)
    {
        return DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy");
    }

    protected void btnSaveAndFinalize_Click(object sender, EventArgs e)
    {
        try
        {
            if (!isDateValid(formatDateForSQL(this.txtCompressorDateOfFailure.Text)))
            {
                showMessage(true, "Invalid date for Date of Failure");
                return;
            }

            String fromview = "";
            string filename = "";
            if (ViewState["RFDetails"] != null)
                fromview = ViewState["RFDetails"].ToString();
            JavaScriptSerializer json = new JavaScriptSerializer();
            ArrayList al = new ArrayList();
            if (String.IsNullOrEmpty(fromview) == false)
                al = json.Deserialize<ArrayList>(fromview);

            Model.ReturnFormDetail rfd = new Model.ReturnFormDetail();
            Model.ReturnFormHeader rfh = new Model.ReturnFormHeader();
            Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();

            rfh.ReferenceNumber = DateTime.Now.ToShortDateString().Substring(2, 2) + "-0000"; ;
            rfh.Description = "";
            rfh.ReturnDate = DateTime.Now.ToShortDateString();

            rfd.ProductType = optProductType.SelectedValue;
            rfd.LocationID = ddLocation.SelectedValue;
            rfd.Claim = ddCompressorClaimLists.SelectedValue;
            rfd.CustomerClaimReference = txtCustomerClaimReference.Text;
            rfd.SerialNumber = txtCompressorSerialNumber.Text;
            rfd.ModelProduct = txtCompressorModel.Text;
            rfd.DataCode = "";
            rfd.Warranty = "";
            rfd.Quantity = "";
            rfd.DateOfClaim =  txtCompressorCreationDate.Text;
            rfd.CompressorComponentReference = txtCompressorReference.Text;
            rfd.TypeOfReturn = getReturnTypeValue(optCompressorTypeOfReturn.SelectedValue);
            rfd.Application = optCompressorApplication.SelectedValue;
            rfd.Refrigerant = optCompressorRefrigerant.SelectedValue;

            if (optCompressorRefrigerant.SelectedValue == "Other")
            { rfd.OtherRefrigerant = txtCompressorRefrigerant.Text; }
            else
            { rfd.OtherRefrigerant = ""; }

            // [START] [jpbersonda] [5/25/2017] [changes in request radio button values]]
            rfd.Request = optCompressorRequest1.SelectedValue;
            if (optCompressorRequest1.SelectedValue == "Specific Request")
            { rfd.RequestSpecificTestRemarks = txtCompressorRequestSpecificTest.Text; }
            else
            { rfd.RequestSpecificTestRemarks = ""; }
            // [END] [jpbersonda] [5/25/2017] [changes in request radio button values]]

            if (ddCompressorClaimLists.SelectedValue == "Cooling/Heating capacity too low" ||
                ddCompressorClaimLists.SelectedValue == "High Power Input" ||
                ddCompressorClaimLists.SelectedValue == "High Amps" ||
                ddCompressorClaimLists.SelectedValue == "Low COP/EER")
            {
                rfd.WorkingPointTo = txtCompressorWorkingPointTO.Text;
                rfd.WorkingPointTc = txtCompressorWorkingPointTC.Text;
                rfd.WorkingPointSuperHeat = txtCompressorWorkingPointSuperHeat.Text;
                rfd.WorkingPointSubcooling = txtCompressorWorkingPointSubCooling.Text;
            }
            else
            {
                rfd.WorkingPointTo = "";
                rfd.WorkingPointTc = "";
                rfd.WorkingPointSuperHeat = "";
                rfd.WorkingPointSubcooling = "";
            }

            if (ddCompressorClaimLists.SelectedValue == "Other")
                rfd.ClaimDescription = txtCompressorClaimDescription.Text;
            else
                rfd.ClaimDescription = "";

            rfd.CustomerNumber = ddCustomerList.SelectedValue;
            rfd.ItemNumber = hdnItemNumber.Value;

            rfd.Configuration = optCompressorConfiguration.SelectedValue;
            rfd.DateOfFailure = txtCompressorDateOfFailure.Text;
            rfd.RunningHours = (txtCompressorRunningHours.Text == "") ? "0" : txtCompressorRunningHours.Text;
            rfd.Remarks = txtRemarks.Value;

            rfd.FilePath = hdnFileName.Value;

            //if (UploadFile.HasFile)
            //{
            //    filename = Path.GetFileName(UploadFile.FileName);
            //    if (!File.Exists(HttpContext.Current.Server.MapPath("UploadedFiles/") + filename))
            //        UploadFile.SaveAs(HttpContext.Current.Server.MapPath("UploadedFiles/") + filename);
            //    rfd.FilePath = "UploadedFiles\\" + filename;
            //}
            //else
            //{
            //    rfd.FilePath = "";
            //}

            if (this.hdnOperation.Value == "add")
                al.Add(rfd);
            if (this.hdnOperation.Value == "edit")
                al[int.Parse(this.hdnIndexToEdit.Value)] = rfd;

            ViewState["RFDetails"] = json.Serialize(al);

            int count = Int32.Parse(this.hdnRecordsCount.Value);
            if (this.hdnOperation.Value == "add")
                count = count + 1;

            this.hdnRecordsCount.Value = count.ToString();
            updateLinkViewList();

            if (checkSerialNumber() == true)
            {
                saveAndFinalize();
            }

            

        }
        catch (Exception ex)
        {
            showMessage(true, ex.Message);
        }
    }
    protected void saveAndFinalize()
    {
        setBackupDetails();
        try
        {
            String fromview = "";
            if (ViewState["RFDetails"] != null)
                fromview = ViewState["RFDetails"].ToString();
            JavaScriptSerializer json = new JavaScriptSerializer();
            ArrayList al = new ArrayList();
            if (String.IsNullOrEmpty(fromview) == false)
                al = json.Deserialize<ArrayList>(fromview);

            if (ViewState["RFDetails"] == null)
            {
                showMessage(true, "No records to save.");
                refreshFields();
                return;
            }
            else
            {
                this.pnlReturnForm2.Visible = true;
                this.pnlReturnForm.Visible = false;
                displayItems();
                refreshFields();
            }
        }
        catch (Exception ex)
        {
            showMessage(true, ex.Message);
        }
    }
    // BACK UP THE DETAILS WHEN CLICK THE lnkViewList 
    public void setBackupDetails()
    {

        if (Session["RFCompBackupDetails"] != null) { return; }
        List<Model.ReturnFormDetail> al = new List<Model.ReturnFormDetail>();

        try
        {

            Model.ReturnFormDetail rfd = new Model.ReturnFormDetail();
          
            rfd.ProductType = optProductType.SelectedValue;

            rfd.Claim = ddCompressorClaimLists.SelectedValue;
            if (ddCompressorClaimLists.SelectedValue == "Cooling/Heating capacity too low" ||
                ddCompressorClaimLists.SelectedValue == "High Power Input" ||
                ddCompressorClaimLists.SelectedValue == "High Amps" ||
                ddCompressorClaimLists.SelectedValue == "Low COP/EER")
            {
                rfd.WorkingPointTo = txtCompressorWorkingPointTO.Text;
                rfd.WorkingPointTc = txtCompressorWorkingPointTC.Text;
                rfd.WorkingPointSuperHeat = txtCompressorWorkingPointSuperHeat.Text;
                rfd.WorkingPointSubcooling = txtCompressorWorkingPointSubCooling.Text;
            }
            else
            {
                rfd.WorkingPointTo = "";
                rfd.WorkingPointTc = "";
                rfd.WorkingPointSuperHeat = "";
                rfd.WorkingPointSubcooling = "";
            }
            if (ddCompressorClaimLists.SelectedValue == "Other")
            { rfd.ClaimDescription = txtCompressorClaimDescription.Text; }
            else
            { rfd.ClaimDescription = ""; }
            // ADD LOCATION ID
            rfd.LocationID = ddLocation.SelectedValue;
            rfd.CustomerClaimReference = txtCustomerClaimReference.Text;
            rfd.SerialNumber = txtCompressorSerialNumber.Text;
            rfd.ModelProduct = txtCompressorModel.Text;
            rfd.DataCode = "";
            rfd.Warranty = "";
            rfd.Quantity = "";
            rfd.DateOfClaim = txtCompressorCreationDate.Text;
            rfd.CompressorComponentReference = txtCompressorReference.Text;
            rfd.TypeOfReturn = (optCompressorTypeOfReturn.SelectedIndex == -1 ) ? "" : getReturnTypeValue(optCompressorTypeOfReturn.SelectedValue);
            rfd.Application = optCompressorApplication.SelectedValue;

            rfd.Refrigerant = optCompressorRefrigerant.SelectedValue;
            if (optCompressorRefrigerant.SelectedValue == "Other")
            { rfd.OtherRefrigerant = txtCompressorRefrigerant.Text; }
            else
            { rfd.OtherRefrigerant = ""; }

            // [START] [jpbersonda] [5/25/2017] [changes in request radio button values]]
            rfd.Request = optCompressorRequest1.SelectedValue;
            if (optCompressorRequest1.SelectedValue == "Specific Request")
            { rfd.RequestSpecificTestRemarks = txtCompressorRequestSpecificTest.Text; }
            else
            { rfd.RequestSpecificTestRemarks = ""; }
            // [END] [jpbersonda] [5/25/2017] [changes in request radio button values]]

            rfd.CustomerNumber = ddCustomerList.SelectedValue;
            rfd.ItemNumber = hdnItemNumber.Value;
            rfd.Configuration = optCompressorConfiguration.SelectedValue;
            rfd.DateOfFailure = txtCompressorDateOfFailure.Text;
            rfd.RunningHours = (txtCompressorRunningHours.Text == "") ? "0" : txtCompressorRunningHours.Text;
            rfd.Remarks = txtRemarks.Value;

            rfd.FilePath = hdnFileName.Value;
            

            al.Add(rfd);

            Session["RFCompBackupDetails"] = al;
        }
        catch (Exception e)
        {

        }
    }

    protected void displayItems()
    {
        DataTable dt = new DataTable();
        String fromview = ViewState["RFDetails"].ToString();
        JavaScriptSerializer json = new JavaScriptSerializer();
        ArrayList al = json.Deserialize<ArrayList>(fromview);

        int lineNumber = 0;
        dt.Columns.Add("Line");
        dt.Columns.Add("Serial Number");
        dt.Columns.Add("Model");
        dt.Columns.Add("Claim");

        for (int x = 0; x < al.Count; x++)
        {
            Model.ReturnFormDetail rd = new JavaScriptSerializer().ConvertToType<Model.ReturnFormDetail>(al[x]);
            DataRow dr = dt.NewRow();
            lineNumber = lineNumber + 1;
            dr["Line"] = lineNumber.ToString();
            dr["Serial Number"] = rd.SerialNumber;
            dr["Model"] = rd.ModelProduct;
            dr["Claim"] = rd.Claim;
            dt.Rows.Add(dr);
        }

        gridViewAccumulateData.DataSource = dt;
        gridViewAccumulateData.DataBind();
    }

    protected void cmdConfirm_Click(object sender, EventArgs e)
    {
        try
        {
            if (this.chkAllow.Checked)
            {
                bool proceed = true;
                if (String.IsNullOrEmpty(txtName.Text))
                {
                    this.rfName.Visible = true;
                    proceed = false;
                }
                else
                { this.rfName.Visible = false; }

                if (String.IsNullOrEmpty(txtEmail.Text))
                {
                    this.rfEmail.Visible = true;
                    if (proceed)
                        proceed = false;
                }
                else
                { this.rfEmail.Visible = false; }

                if (String.IsNullOrEmpty(txtPhone.Text))
                {
                    this.rfPhone.Visible = true;
                    if (proceed)
                        proceed = false;
                }
                else
                { this.rfPhone.Visible = false; }

                if (!proceed)
                { return; }
            }

            String fromview = "";
            if (ViewState["RFDetails"] != null)
                fromview = ViewState["RFDetails"].ToString();
            JavaScriptSerializer json = new JavaScriptSerializer();
            ArrayList al = new ArrayList();
            if (String.IsNullOrEmpty(fromview) == false)
                al = json.Deserialize<ArrayList>(fromview);

            //saves the records
            saveClaims(al);

            this.pnlReturnForm2.Visible = false;
            this.pnlReturnForm.Visible = true;
            Response.Redirect("SaveSuccessful.aspx?f=comp");
        }
        catch (Exception ex)
        {
            showMessage(true, ex.Message);
        }
    }
    protected void saveClaims(ArrayList al)
    {
        //int prevClaimID = 0;
        //int prevRFHeaderID = 0;
        int rfHeaderID = 0;
        int userID = int.Parse(sm.Decrypt(Session["UserID"].ToString()));

        for (int i = 0; i < al.Count; i++)
        {
            Model.ReturnFormDetail rd = new JavaScriptSerializer().ConvertToType<Model.ReturnFormDetail>(al[i]);
            
            // FORMAT MM/DD/YYYY FROM DD/MM/YYYY
            rd.DateOfClaim = formatDateForSQL(rd.DateOfClaim);
            rd.DateOfFailure = formatDateForSQL(rd.DateOfFailure);
            
            Maintenance.ReturnFormDetail rdMaint = new Maintenance.ReturnFormDetail();
            Model.ContactPerson cp = new Model.ContactPerson();

            //if (i == 0)
            //{
            //Comment to allow different portal reference
                rfHeaderID = rdMaint.addReturnFormHeader(rd, userID);
                //add person to notify
                cp.RFHeaderID = rd.RFHeaderID;
                if (chkAllow.Checked)
                {
                    cp.Name = this.txtName.Text;
                    cp.PhoneNumber = this.txtPhone.Text;
                    cp.EmailAddress = this.txtEmail.Text;
                }
                else
                {
                    cp.Name = sm.Decrypt(Session["UserFullName"].ToString());
                    cp.PhoneNumber = sm.Decrypt(Session["PhoneNumber"].ToString());
                    cp.EmailAddress = sm.Decrypt(Session["EmailAddress"].ToString());
                }
                rdMaint.addContactPerson(cp);
            //}
            //else
            //{
            //    if (prevClaimID != rd.ClaimID)
            //    {
            //        rd.RFHeaderID = rdMaint.addReturnFormHeader(rd, userID);//add person to notify
            //        cp.RFHeaderID = rd.RFHeaderID;
            //        if (chkAllow.Checked)
            //        {
            //            cp.Name = this.txtName.Text;
            //            cp.PhoneNumber = this.txtPhone.Text;
            //            cp.EmailAddress = this.txtEmail.Text;
            //        }
            //        else
            //        {
                //            cp.Name = sm.Decrypt(Session["UserFullName"].ToString());btnAddSameClaim
            //           cp.PhoneNumber = sm.Decrypt(Session["PhoneNumber"].ToString());
            //           cp.EmailAddress = sm.Decrypt(Session["EmailAddress"].ToString());
            //        }
            //        rdMaint.addContactPerson(cp);
            //    }
            //    else
            //        rd.RFHeaderID = prevRFHeaderID;
            //}

            //prevRFHeaderID = rd.RFHeaderID;
            rd.RFHeaderID = rfHeaderID;
            rdMaint.addReturnFormDetail(rd);
            //prevClaimID = rd.ClaimID;
            //Session.Add("CountClaim", prevClaimID + 1);
        }
        Session.Add("CountClaim", al.Count);
    }

    #region Utilities
    private void initialization()
    {
        showMessage(false, "");

        string tooltip = "Line Return : OEM only. Product non-conform at receipt, during assembly or final test \r\n"; //+ System.Environment.NewLine;
        tooltip += "Field Returns : Product failed at end customer after commissioning. \r\n ";
        tooltip += "Lab/sample Test : Product is returned after lab or sample test. ";

        img3.ToolTip = tooltip.Replace("\\n","\n");
        //img3.ToolTip = "Line Return : OEM only. Product non-conform at receipt, during assembly or final test " + Environment.NewLine
        //    + "Field Returns : Product failed at end customer after commissioning.";

        optCompressorRequest1.Items[1].Attributes.Add("onclick", "selectedRequestSpecificTest()");
        
    }
    private void updateLinkViewList()
    {
        this.lnkViewList.Visible = false;
        if (Int32.Parse(this.hdnRecordsCount.Value) > 0)
        {
            string recordCount;
            if (Int32.Parse(this.hdnRecordsCount.Value) > 1)
                recordCount = "You have recorded " + this.hdnRecordsCount.Value + " Compressor / Drives / Units.";
            else
                recordCount = "You have recorded " + this.hdnRecordsCount.Value + " Compressor / Drives / Units. ";
            recordCount += " Click this to view the list.";
            this.lnkViewList.Visible = true;
            this.lnkViewList.Text = recordCount;
        }
    }
    private void showMessage(bool showHide, string message)
    {
        LabelMessage.Text = message;
        LabelMessage.Visible = showHide;
        pnlLabelMessage.Visible = showHide;
    }
    
    private bool isDateValid(string dateString)
    {
        bool isValid = true;
        if (!String.IsNullOrEmpty(dateString))
        {
            DateTime date;
            System.Data.SqlTypes.SqlDateTime sqlDateTime;
            if (DateTime.TryParse(dateString, out date))
            {
                try { sqlDateTime = new System.Data.SqlTypes.SqlDateTime(date); isValid = true; }
                catch { isValid = false; }
            }
            else
                isValid = false;
        }
        return isValid;
    }
    protected string getReturnTypeValue(string _value)
    {
        string returnedValue;
        switch (_value)
        {
            case "Line Return":
                {
                    returnedValue = "LR";
                    break;
                }
            case "Field Return":
                {
                    returnedValue = "FR";
                    break;
                }
            case "Buy Back":
                {
                    returnedValue = "BB";
                    break;
                }
            case "Wrong Delivery":
                {
                    returnedValue = "WD";
                    break;
                }
            case "Transport Damage":
                {
                    returnedValue = "TD";
                    break;
                }
            default:
                {
                    returnedValue = "SA";
                    break;
                }
        }
        return returnedValue;
    }
    protected string getSelectedReturnTypeValue(string _value)
    {
        string returnedValue;
        switch (_value)
        {
            case "LR":
                {
                    returnedValue = "Line Return";
                    break;
                }
            case "FR":
                {
                    returnedValue = "Field Return";
                    break;
                }
            case "BB":
                {
                    returnedValue = "Buy Back";
                    break;
                }
            case "WD":
                {
                    returnedValue = "Wrong Delivery";
                    break;
                }
            case "TD":
                {
                    returnedValue = "Transport Damage";
                    break;
                }
            default:
                {
                    returnedValue = "Lab/Sample Test";
                    break;
                }
        }
        return returnedValue;
    }
    protected void refreshFields()
    {
        optProductType.SelectedIndex = 0;
        //ddCustomerList.SelectedIndex = 0;
        ddCompressorClaimLists.SelectedIndex = 0;
        lblCompressorClaimDescription.Visible = false;
        txtCompressorClaimDescription.Visible = false;
        txtCompressorClaimDescription.Text = "";
        txtCustomerClaimReference.Text = "";
        txtCompressorSerialNumber.Text = "";
        txtCompressorModel.Text = "";
        txtCompressorReference.Text = "";
        txtCompressorRunningHours.Text = "0";
        txtRemarks.Value = "";
        optCompressorTypeOfReturn.SelectedIndex = -1;
        optCompressorApplication.SelectedIndex = -1;
        optCompressorRefrigerant.SelectedIndex = -1;
        pnlCompressorRefrigerant.Visible = false;
        pnlCompressorWorkingPoint.Visible = false;
        optCompressorRequest1.SelectedIndex = -1;
        txtCompressorRequestSpecificTest.Text = "";
        //txtCompressorRequestSpecificTest.Visible = false;
        pnlCompressorRequestSpecificTest.Visible = false;
        optCompressorConfiguration.SelectedIndex = -1;
        txtCompressorCreationDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtCompressorDateOfFailure.Text = DateTime.Now.ToString("dd/MM/yyyy");
    }
    private void refreshLinkViewList()
    {
        this.hdnRecordsCount.Value = "0";
        this.lnkViewList.Visible = false;
    }
    #endregion
    protected void ddCompressorClaimLists_SelectedIndexChanged(object sender, EventArgs e)
    {
        //[START] [04/25/17] [jpbersonda]
        if (ddCompressorClaimLists.SelectedItem.Text.Equals("Buy back", StringComparison.CurrentCultureIgnoreCase))
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "", "alert('Buy back only authorized after written approval from Emerson Customer Service');", true);
     
        //[END] [04/25/17] [jpbersonda]

        if (ddCompressorClaimLists.SelectedItem.Text == "Other")
        {
            lblCompressorClaimDescription.Visible = true;
            txtCompressorClaimDescription.Visible = true;
        }
        else
        {
            lblCompressorClaimDescription.Visible = false;
            txtCompressorClaimDescription.Visible = false;
        }

        //if (ddCompressorClaimLists.SelectedIndex >= 5 && ddCompressorClaimLists.SelectedIndex <= 9)
        if (ddCompressorClaimLists.SelectedItem.Text == "High DLT" || 
            ddCompressorClaimLists.SelectedItem.Text == "Cooling/Heating capacity too low" ||
            ddCompressorClaimLists.SelectedItem.Text == "High Power Input" ||
            ddCompressorClaimLists.SelectedItem.Text == "High Amps" ||
            ddCompressorClaimLists.SelectedItem.Text == "Low COP/EER"){
                pnlCompressorWorkingPoint.Visible = true;

        }  
        else
            pnlCompressorWorkingPoint.Visible = false;
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            if (this.hdnOperation.Value == "edit")
            {
                this.pnlReturnForm.Visible = false;
                this.pnlReturnForm2.Visible = true;
            }
            if (this.hdnOperation.Value == "add")
            {
                this.pnlReturnForm.Visible = true;
                this.pnlReturnForm2.Visible = false;
                ViewState["RFDetails"] = null;
            }
            refreshFields();
            refreshLinkViewList();
            this.hdnOperation.Value = "add";
            showMessage(true, "Transaction was cancelled.");
        }
        catch (Exception ex)
        {
            showMessage(true, ex.Message);
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        showMessage(true, "Transaction was cancelled.");
        this.pnlReturnForm.Visible = true;
        this.pnlReturnForm2.Visible = false;
        this.hdnOperation.Value = "add";
        this.btnAddNewClaim.Visible = true;
        this.btnAddSameClaim.Visible = true;
        refreshFields();
        refreshLinkViewList();
        ViewState["RFDetails"] = null;
        //this.hdnLastClicked.Value = "Cancel";
    }

    protected void gridViewAccumulateData_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        Type type = sender.GetType();
        int index = Int32.Parse(e.CommandArgument.ToString());
        if (e.CommandName == "EditSelectedRow")
        {
            try
            {
                if (index == -1)
                {
                    showMessage(true, "No selected row to modify.");
                    return;
                }

                String fromview = ViewState["RFDetails"].ToString();
                JavaScriptSerializer json = new JavaScriptSerializer();
                ArrayList al = json.Deserialize<ArrayList>(fromview);

                int x = index;
                this.hdnIndexToEdit.Value = x.ToString();
                Model.ReturnFormDetail rfd = new JavaScriptSerializer().ConvertToType<Model.ReturnFormDetail>(al[x]);

                this.btnAddNewClaim.Visible = false;
                this.btnAddSameClaim.Visible = false;
                this.pnlReturnForm.Visible = true;
                this.pnlReturnForm2.Visible = false;
                this.hdnOperation.Value = "edit";
                //hdnClaimID.Value = rfd.ClaimID.ToString();

                optProductType.SelectedValue = rfd.ProductType;
                ddCompressorClaimLists.SelectedValue = rfd.Claim;
                txtCustomerClaimReference.Text = rfd.CustomerClaimReference;
                txtCompressorSerialNumber.Text = rfd.SerialNumber;
                txtCompressorModel.Text = rfd.ModelProduct;

                txtCompressorCreationDate.Text = rfd.DateOfClaim;
                txtCompressorReference.Text = rfd.CompressorComponentReference;
                optCompressorTypeOfReturn.SelectedValue = getSelectedReturnTypeValue(rfd.TypeOfReturn);
                optCompressorApplication.SelectedValue = rfd.Application;
                optCompressorRefrigerant.SelectedValue = rfd.Refrigerant;

                optCompressorRequest1.SelectedIndex = -1; //CLEAR SELECTED BEFORE SET THE NEW VALUE
                //Selected Value for Compressor Request
                for (int i = 0; i < this.optCompressorRequest1.Items.Count; i++)
                {
                    if (rfd.Request == this.optCompressorRequest1.Items[i].Value)
                    {
                        this.optCompressorRequest1.SelectedIndex = i;
                        break;
                    }
                }

                if (rfd.Claim == "Cooling/Heating capacity too low" ||
                    rfd.Claim == "High Power Input" ||
                    rfd.Claim == "High Amps" ||
                    rfd.Claim == "Low COP/EER")
                {
                    pnlCompressorWorkingPoint.Visible = true;
                    txtCompressorWorkingPointTO.Text = rfd.WorkingPointTo;
                    txtCompressorWorkingPointTC.Text = rfd.WorkingPointTc;
                    txtCompressorWorkingPointSuperHeat.Text = rfd.WorkingPointSuperHeat;
                    txtCompressorWorkingPointSubCooling.Text = rfd.WorkingPointSubcooling;
                }
                else
                {
                    pnlCompressorWorkingPoint.Visible = false;
                    txtCompressorWorkingPointTO.Text = "";
                    txtCompressorWorkingPointTC.Text = "";
                    txtCompressorWorkingPointSuperHeat.Text = "";
                    txtCompressorWorkingPointSubCooling.Text = "";
                }

                if (rfd.Claim == "Other")
                {
                    lblCompressorClaimDescription.Visible = true;
                    txtCompressorClaimDescription.Visible = true;
                    txtCompressorClaimDescription.Text = rfd.ClaimDescription;
                }
                else
                {
                    lblCompressorClaimDescription.Visible = false;
                    txtCompressorClaimDescription.Visible = false;
                    txtCompressorClaimDescription.Text = "";
                }

                if (rfd.Refrigerant == "Other")
                {
                    pnlCompressorRefrigerant.Visible = true;
                    txtCompressorRefrigerant.Text = rfd.OtherRefrigerant;
                }
                else
                {
                    pnlCompressorRefrigerant.Visible = false;
                    txtCompressorRefrigerant.Text = "";
                }

                if (rfd.Request == "Specific Test")
                {
                    //txtCompressorRequestSpecificTest.Visible = true;
                    pnlCompressorRequestSpecificTest.Visible = true;
                    txtCompressorRequestSpecificTest.Text = rfd.RequestSpecificTestRemarks;
                }
                else
                {
                    //txtCompressorRequestSpecificTest.Visible = false;
                    pnlCompressorRequestSpecificTest.Visible = false;
                    txtCompressorRequestSpecificTest.Text = "";
                }

                ddCustomerList.SelectedValue = rfd.CustomerNumber.ToString();
                hdnItemNumber.Value = rfd.ItemNumber;

                if (String.IsNullOrEmpty(rfd.Configuration))
                { optCompressorConfiguration.SelectedIndex = -1; }
                else
                { optCompressorConfiguration.SelectedValue = rfd.Configuration; }

                txtCompressorDateOfFailure.Text = rfd.DateOfFailure;
                txtCompressorRunningHours.Text = rfd.RunningHours;
                txtRemarks.Value = rfd.Remarks;

            }
            catch (Exception ex)
            {
                showMessage(true, ex.Message);
            }
        }
        if (e.CommandName == "RemoveSelectedRow")
        {
            String fromview = ViewState["RFDetails"].ToString();
            JavaScriptSerializer json = new JavaScriptSerializer();
            ArrayList al = json.Deserialize<ArrayList>(fromview);
            ArrayList al2 = new ArrayList();

            for (int i = 0; i < al.Count; i++)
            {
                if (i != index)
                {
                    al2.Add(al[i]);
                }
            }

            if (al2.Count == 0)
            {
                cmdCancel_Click(sender, e);
            }
            else
            {
                ViewState["RFDetails"] = json.Serialize(al2);
                saveAndFinalize();
            }
        }
    }

    protected void lnkViewList_Click(object sender, EventArgs e)
    {
        saveAndFinalize();
    }


    protected void chkAllow_CheckedChanged(object sender, EventArgs e)
    {
        if (this.chkAllow.Checked)
        {
            this.txtName.Enabled = true;
            this.txtEmail.Enabled = true;
            this.txtPhone.Enabled = true;
        }
        else
        {
            this.txtName.Enabled = false;
            this.txtEmail.Enabled = false;
            this.txtPhone.Enabled = false;
        }
    }
   
    [System.Web.Services.WebMethod()]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)] 
    public static ResponseObject GetSerialNumber(string term) {
        ResponseObject result = new ResponseObject(); 
        try
        {
            Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
            Model.ReturnFormDetail rfd = new Model.ReturnFormDetail();
            Utility u = new Utility();
            DataTable dt = mrfd.getModelBySerial(term);
            foreach (DataRow dr in dt.Rows)
            {
                rfd.ModelProduct = u.extractStringValue(dr["DESCP"], Utility.DataType.STRING);
                rfd.ItemNumber = u.extractStringValue(dr["ITNBR"], Utility.DataType.STRING); 
            }
            result.ResponseItem = rfd;
        }
        catch (Exception e) {
            result.ErrorMessage = e.Message;
        }
        return result;    
    }

    protected void cmdBackReturn_Click(object sender, EventArgs e)
    {
        if (Session["RFCompBackupDetails"] == null) { return; }
        List<Model.ReturnFormDetail> list = new List<Model.ReturnFormDetail>();
        list = (List<Model.ReturnFormDetail>)Session["RFCompBackupDetails"];
        refreshFields();
        foreach (Model.ReturnFormDetail rfd in list)
        {
            optProductType.SelectedValue = rfd.ProductType;
            ddCompressorClaimLists.SelectedValue = rfd.Claim;
            txtCustomerClaimReference.Text = rfd.CustomerClaimReference;
            txtCompressorSerialNumber.Text = rfd.SerialNumber;
            txtCompressorModel.Text = rfd.ModelProduct;

            txtCompressorCreationDate.Text = rfd.DateOfClaim;
            txtCompressorReference.Text = rfd.CompressorComponentReference;
            if (rfd.TypeOfReturn != "")
                optCompressorTypeOfReturn.SelectedValue = getSelectedReturnTypeValue(rfd.TypeOfReturn); 
            if (rfd.Application != "")
                optCompressorApplication.SelectedValue = rfd.Application;
            if (rfd.Refrigerant != "")
                optCompressorRefrigerant.SelectedValue = rfd.Refrigerant;

            optCompressorRequest1.SelectedIndex = -1; //CLEAR SELECTED BEFORE SET THE NEW VALUE
            //Selected Value for Compressor Request

            
            for (int i = 0; i < this.optCompressorRequest1.Items.Count; i++)
            {
                if (rfd.Request == this.optCompressorRequest1.Items[i].Value)
                {
                    this.optCompressorRequest1.SelectedIndex = i;
                    break;
                }
            }


            if (rfd.Claim == "Cooling/Heating capacity too low" ||
                rfd.Claim == "High Power Input" ||
                rfd.Claim == "High Amps" ||
                rfd.Claim == "Low COP/EER")
            {
                pnlCompressorWorkingPoint.Visible = true;
                txtCompressorWorkingPointTO.Text = rfd.WorkingPointTo;
                txtCompressorWorkingPointTC.Text = rfd.WorkingPointTc;
                txtCompressorWorkingPointSuperHeat.Text = rfd.WorkingPointSuperHeat;
                txtCompressorWorkingPointSubCooling.Text = rfd.WorkingPointSubcooling;
            }
            else
            {
                pnlCompressorWorkingPoint.Visible = false;
                txtCompressorWorkingPointTO.Text = "";
                txtCompressorWorkingPointTC.Text = "";
                txtCompressorWorkingPointSuperHeat.Text = "";
                txtCompressorWorkingPointSubCooling.Text = "";
            }

            if (rfd.Claim == "Other")
            {
                lblCompressorClaimDescription.Visible = true;
                txtCompressorClaimDescription.Visible = true;
                txtCompressorClaimDescription.Text = rfd.ClaimDescription;
            }
            else
            {
                lblCompressorClaimDescription.Visible = false;
                txtCompressorClaimDescription.Visible = false;
                txtCompressorClaimDescription.Text = "";
            }

            if (rfd.Refrigerant == "Other")
            {
                pnlCompressorRefrigerant.Visible = true;
                txtCompressorRefrigerant.Text = rfd.OtherRefrigerant;
            }
            else
            {
                pnlCompressorRefrigerant.Visible = false;
                txtCompressorRefrigerant.Text = "";
            }

            if (rfd.Request == "Specific Test")
            {
                //txtCompressorRequestSpecificTest.Visible = true;
                pnlCompressorRequestSpecificTest.Visible = true;
                txtCompressorRequestSpecificTest.Text = rfd.RequestSpecificTestRemarks;
            }
            else
            {
                //txtCompressorRequestSpecificTest.Visible = false;
                pnlCompressorRequestSpecificTest.Visible = false;
                txtCompressorRequestSpecificTest.Text = "";
            }

            ddCustomerList.SelectedValue = rfd.CustomerNumber.ToString();
            hdnItemNumber.Value = rfd.ItemNumber;

            if (String.IsNullOrEmpty(rfd.Configuration))
            { optCompressorConfiguration.SelectedIndex = -1; }
            else
            { optCompressorConfiguration.SelectedValue = rfd.Configuration; }

            txtCompressorDateOfFailure.Text = rfd.DateOfFailure;
            txtCompressorRunningHours.Text = rfd.RunningHours;
            txtRemarks.Value = rfd.Remarks;

        }


        Session["RFCompBackupDetails"] = null;
        this.hdnOperation.Value = "add";

        this.btnAddNewClaim.Visible = true;
        this.btnAddSameClaim.Visible = true;
        this.pnlReturnForm.Visible = true;
        this.pnlReturnForm2.Visible = false;

     

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Model.ReturnFormDetail> getSerialNumber(string serialNumber)
    {
        Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
        List<Model.ReturnFormDetail> list = new List<Model.ReturnFormDetail>();
        Utility u = new Utility();
        DataTable dt = mrfd.getModelBySerialSearch(serialNumber);
        foreach (DataRow dr in dt.Rows)
        {
            Model.ReturnFormDetail rfd = new Model.ReturnFormDetail(); 
            rfd.ModelProduct = u.extractStringValue(dr["DESCP"], Utility.DataType.STRING);
            rfd.SerialNumber = u.extractStringValue(dr["SERIAL"], Utility.DataType.STRING);
            rfd.ItemNumber = u.extractStringValue(dr["ITNBR"], Utility.DataType.STRING);
            list.Add(rfd);
        }

        return list; 
    }

    protected void ddCustomerList_SelectedIndexChanged(object sender, EventArgs e)
    {
        string customerno = ddCustomerList.SelectedValue;
        int userId = (Session["UserID"] == null) ? 0 : int.Parse(sm.Decrypt(Session["UserID"].ToString()));
        //bool isCustomer = (sm.Decrypt(Session["UserType"].ToString()) == "CUSTOMER") ? true : false;
        string usertype = sm.Decrypt(Session["UserType"].ToString());

        Maintenance.Customers c = new Maintenance.Customers();
        ddLocation.Items.Clear();
        ddLocation.DataSource = null;
        ddLocation.DataBind();

        ddLocation.DataSource = c.getLocationByCustomerNumberDT(customerno, userId, usertype);
        ddLocation.DataTextField = "LocationName";
        ddLocation.DataValueField = "LocationID";
        ddLocation.DataBind();
    }
}
