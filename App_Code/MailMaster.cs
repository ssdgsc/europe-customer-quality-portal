﻿using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Text;
using System.Net.Mail;
using System.Configuration;

public class MailMaster
{
    private MailAddress from = new MailAddress("EuropeCustomerQualityPortal@emerson.com", "Customer Quality Portal");
 
    private string _testEmailaddress = "ivo.kamenik@emerson.com";
    private string host = ConfigurationManager.AppSettings["SMTPServerIP"].ToString();
	public MailMaster(){}

    public void sendEmailforgotPassword(Model.UserInformation u) {
        General.AvianCrypto.SecurityMaster sm;
        string body = getMailTemplate("resetpassword");
        SmtpClient client = new SmtpClient();
        client.Host = host;
        client.Port = 25;
    
        body = body.Replace("#FULLNAME#", u.UserFirstName + " " + u.UserLastName);
        body = body.Replace("#PASSWORD#", u.UserPassword);
         
        MailMessage message = new MailMessage();   
        message.To.Add(new MailAddress(u.EmailAddress));

        message.From = from;
        message.Subject = "Reset your password - Emerson Customer Quality Portal";
        message.IsBodyHtml = true;
        message.Body = body;

        client.Send(message);
    }
    public string getMailTemplate(string html)
    {
        return File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("./") + "\\resources\\mail_template\\" + html + ".html");
    }

  
    public void sendMail(string _emailaddress, string _message, string _subject)
    {
        //string body = _message;
        //SmtpClient client = new SmtpClient();
        //client.Host = host;
        //client.Port = 25;
        //MailMessage message = new MailMessage();
        ////string[] values = _emailaddress.Split(',');
        ////for (int i = 0; i < values.Length; i++)
        ////{
        ////    message.To.Add(new MailAddress(values[i]));
        ////}.

        //message.To.Add(new MailAddress(_testEmailaddress));

        //message.From = from;
        //message.Subject = _subject;
        //message.IsBodyHtml = true;
        //message.Body = body;

        //client.Send(message);
    }

    public void sendMailWithAttachment(String to, String cc, String subject, String body, Attachment attachment)
    {
        //MailMessage email = new MailMessage();
        //email.IsBodyHtml = true;
        
        ////string[] values = to.Split(',');
        ////for (int i = 0; i<values.Length; i++) {
        ////    email.To.Add(new MailAddress(values[i]));  
        ////}

        //email.To.Add(new MailAddress(_testEmailaddress));

        //MailAddress bcc = new MailAddress("moraleskime@gmail.com");
        //email.Bcc.Add(bcc);
        //email.From = from;
        //email.Subject = subject;
        //email.Body = body;
        //email.Attachments.Add(attachment);
        //SmtpClient client = new SmtpClient();
        //client.Timeout = 2000000;
        //client.Host = host;
        //client.Port = 25;
        //client.Send(email);
    }
}
