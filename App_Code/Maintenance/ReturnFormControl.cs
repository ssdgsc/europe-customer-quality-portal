﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using General;
using System.Collections;
using System.Data;
using System.Text;
using Telerik.Reporting;
using System.IO;
using System.Configuration;
using System.Net;
using System.Xml;

 

/// <summary>
/// Summary description for ReturnFormControl
/// </summary>

namespace Maintenance
{
    public class ReturnFormControl
    {
        public ReturnFormControl()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public int getLastReferenceDigit(string currentYear, string currentMonth)
        {
            ConnectionMaster cm = new ConnectionMaster();
            int lastDigit = 0;
            string sqlStr = "";
            sqlStr += " SELECT TOP 1 LastRefDigit FROM ReturnFormHeader ";
            // comment for starting 0
            //sqlStr += " WHERE LastRefYear = " + currentYear;
            //sqlStr += " AND LastRefMonth = " + currentMonth;
            sqlStr += " ORDER BY RFHeaderID DESC";
            DataTable dt = cm.retrieveDataTable(sqlStr);
            if (dt.Rows.Count > 0)
            {
                lastDigit = int.Parse(dt.Rows[0]["LastRefDigit"].ToString());
            }
            return lastDigit;
        }

        public string getMonthCode(int currentMonth)
        {
            string monthCode = "";
            switch (currentMonth)
            {
                case 1: monthCode = "A";
                    break;
                case 2: monthCode = "B";
                    break;
                case 3: monthCode = "C";
                    break;
                case 4: monthCode = "D";
                    break;
                case 5: monthCode = "E";
                    break;
                case 6: monthCode = "F";
                    break;
                case 7: monthCode = "G";
                    break;
                case 8: monthCode = "H";
                    break;
                case 9: monthCode = "I";
                    break;
                case 10: monthCode = "J";
                    break;
                case 11: monthCode = "K";
                    break;
                case 12: monthCode = "L";
                    break;
            }
            return monthCode;
        }

        public int addReturnFormHeader(Model.ReturnFormControl rd, int userID)
        {
            ConnectionMaster cm = new ConnectionMaster();

            //get last reference number
            string currentYear = DateTime.Now.Year.ToString();
            int currentMonth = DateTime.Now.Month;
            int lastDigit = getLastReferenceDigit(currentYear, currentMonth.ToString());
            lastDigit = lastDigit + 1;

            //string referenceNumber = currentYear + getMonthCode(currentMonth) + lastDigit.ToString().PadLeft(5, '0');
            string referenceNumber = "WP81" + lastDigit.ToString().PadLeft(10, '0');
            string sql = "";
            sql += " INSERT INTO ReturnFormHeader (LastRefYear,LastRefMonth,LastRefDigit,ReferenceNumber,ReturnDate,CreatedBy,Created,Modified) ";
            sql += " VALUES ";
            sql += " ( ";
            sql += "    " + currentYear;
            sql += "    , " + currentMonth.ToString();
            sql += "    , " + lastDigit;
            sql += "    , '" + referenceNumber + "'";
            sql += "    , GETDATE() ";
            sql += "    , " + userID;
            sql += "    , GETDATE() ";
            sql += "    , GETDATE() ";
            sql += " ) ";

            rd.RFHeaderID = int.Parse(cm.executeCommand(sql, true).ToString());

            if (rd.RFHeaderID > 0)
                return rd.RFHeaderID;
            else
                return 0;
        }

        public Model.ReturnFormControl addReturnFormControl(Model.ReturnFormControl rd)
        {
            string sql = "";
            ConnectionMaster cm = new ConnectionMaster();
            Utility u = new Utility();

            //Variables for PortalClaimNumber
            string currentYear = DateTime.Now.Year.ToString();
            int currentMonth = DateTime.Now.Month;
            int lastDigit = getLastReferenceDigit(currentYear, currentMonth.ToString());

            //string portalClaimNumber = currentYear + getMonthCode(currentMonth) + lastDigit.ToString().PadLeft(5, '0');

            string portalClaimNumber = (rd.PortalReferenceHDR != null) ? rd.PortalReferenceHDR : "WP81" + lastDigit.ToString().PadLeft(10, '0'); //;

            portalClaimNumber = checkPortalRefExists(portalClaimNumber);

            sql += " INSERT INTO ReturnFormControl ";
            sql += " ( ";
            sql += " RFHeaderID, ";
            sql += " ProductType, ";
            sql += " Claim, ";
            sql += " ClaimDescription, ";
            sql += " CustomerClaimReference, ";
            sql += " PCNNumber, ";
            sql += " Model, ";
            sql += " DataCode, ";
            sql += " Warranty, ";
            sql += " Quantity, ";
            sql += " DateOfClaim, ";
            sql += " ControlComponentReference, ";
            sql += " TypeOfReturn, ";
            sql += " Application, ";
            sql += " Refrigerant, ";
            sql += " OtherRefrigerant, ";
            sql += " WorkingPointTO, ";
            sql += " WorkingPointTC, ";
            sql += " WorkingPointSuperHeat, ";
            sql += " WorkingPointSubCooling, ";
            sql += " Request, ";
            sql += " RequestWarranty, ";
            sql += " RequestAnalysis, ";
            sql += " RequestCreditOfBody, ";
            sql += " RequestOutOfWarranty, ";
            sql += " RequestSpecificTest, ";
            sql += " RequestSpecificTestRemarks, ";
            sql += " Configuration, ";
            sql += " DateOfFailure, ";
            sql += " RunningHours, ";
            sql += " Remarks, ";
            sql += " FilePath, ";
            sql += " CustomerNumber, ";
            //sql += " ItemNumber, ";
            sql += " PortalClaimNumber, ";
            sql += " RGANo, ";
            sql += " Customer, ";
            sql += " Created, ";
            sql += " Modified, ";
            sql += " PartIncomeDate, ";
            sql += " PartIncomeCW, ";
            sql += " MonthIncome, ";
            sql += " FYIncome, ";
            sql += " Line, ";
            sql += " ProductGroup, ";
            //sql += " OpenOrClosed, ";
            sql += " ReceptionDate, ";
            sql += " OracleNumber,";
            sql += " LocationID";
            sql += " ) ";
            sql += " VALUES ";
            sql += " ( ";
            sql += "    " + u.convertToSQLString(rd.RFHeaderID.ToString(), Utility.DataType.NUMBER) + " , ";
            sql += "    " + u.convertToSQLString(rd.ProductType, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.Claim, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.ClaimDescription, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.ControlCustomerClaimReference, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.PCNNumber, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.ModelProduct, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.DataCode, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.Warranty, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.Quantity, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.DateOfClaim, Utility.DataType.DATE) + " , ";
            sql += "    " + u.convertToSQLString(rd.ControlComponentReference, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.TypeOfReturn, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.Application, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.Refrigerant, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.OtherRefrigerant, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.WorkingPointTO, Utility.DataType.DECIMAL) + " , ";
            sql += "    " + u.convertToSQLString(rd.WorkingPointTC, Utility.DataType.DECIMAL) + " , ";
            sql += "    " + u.convertToSQLString(rd.WorkingPointSuperHeat, Utility.DataType.DECIMAL) + " , ";
            sql += "    " + u.convertToSQLString(rd.WorkingPointSubCooling, Utility.DataType.DECIMAL) + " , ";
            sql += "    " + u.convertToSQLString(rd.Request, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.RequestWarranty.ToString(), Utility.DataType.BOOLEAN) + " ,";
            sql += "    " + u.convertToSQLString(rd.RequestAnalysis.ToString(), Utility.DataType.BOOLEAN) + " ,";
            sql += "    " + u.convertToSQLString(rd.RequestCreditOfBody.ToString(), Utility.DataType.BOOLEAN) + " ,";
            sql += "    " + u.convertToSQLString(rd.RequestOutOfWarranty.ToString(), Utility.DataType.BOOLEAN) + " ,";
            sql += "    " + u.convertToSQLString(rd.RequestSpecificTest.ToString(), Utility.DataType.BOOLEAN) + " ,";
            sql += "    " + u.convertToSQLString(rd.RequestSpecificTestRemarks, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.Configuration, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.DateOfFailure, Utility.DataType.DATE) + " , ";
            sql += "    " + u.convertToSQLString(rd.RunningHours, Utility.DataType.NUMBER) + " ,";
            sql += "    " + u.convertToSQLString(rd.Remarks, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.FilePath, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.CustomerNumber, Utility.DataType.STRING) + " , ";
            //sql += "    " + u.convertToSQLString(rd.ItemNumber, Utility.DataType.STRING) + " , ";
            sql += "   '" + portalClaimNumber + "' ,";
            sql += "    " + u.convertToSQLString(rd.CustomerNumber, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.Customer, Utility.DataType.STRING) + " , ";
            sql += "    GETDATE() ,";
            sql += "    GETDATE() ,";
            sql += "    " + u.convertToSQLString(rd.DateOfPartIncome, Utility.DataType.DATE) + " , ";
            sql += "    " + u.convertToSQLString(rd.CurrentWeekOfPartIncome, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.MonthIncome, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.FiscalYearIncome, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.Line, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.ProductGroup, Utility.DataType.STRING) + " , ";
            //sql += "    " + u.convertToSQLString(rd.OpenOrClosed, Utility.DataType.STRING) + " , ";
            sql += "    GETDATE() ,";
            sql += "   '" + rd.OracleNumber + "',";
            sql += "   '" + rd.LocationID + "'";
            sql += " ) ";

            cm.executeCommand(sql, false);

            int insertedReturnFormControl = 0;
            string sqlStr = " SELECT MAX(RFDetailID) FROM ReturnFormControl ";
            insertedReturnFormControl = Int32.Parse(cm.executeCommand(sqlStr, true).ToString());
            rd.RFDetailID = insertedReturnFormControl;

            return rd;
        }
        // CHECK IF PORTAL CLAIM NUMBER HAS EXISTS THEN ADD PORTALNUMBER 2016-02-23
        public string checkPortalRefExists(string portalClaimNumber)
        {
            string newPortalClaimNumber = "";
            ConnectionMaster cn = new ConnectionMaster();
            string qry = "SELECT * FROM ReturnFormControl WHERE PortalClaimNumber LIKE '" + portalClaimNumber + "%'";
            DataTable dt = cn.retrieveDataTable(qry);
            int ctr = dt.Rows.Count;
            if (ctr > 0) 
                newPortalClaimNumber = portalClaimNumber + '-' + (ctr + 1); 
            else
                newPortalClaimNumber = portalClaimNumber;
            
           return newPortalClaimNumber;
        }

        public int getLastRFHeaderID()
        {
            int lastID = 0;
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            sqlStr += " SELECT MAX(RFHeaderID) FROM RetrunFormHeader ";
            lastID = Int32.Parse(cm.executeCommand(sqlStr, true).ToString());
            return lastID;
        }



        public Model.ReturnFormControl getReturnFormControl(string rfdID)
        {
            Model.ReturnFormControl rfd = new Model.ReturnFormControl();
            ConnectionMaster cm = new ConnectionMaster();
            Utility u = new Utility();
            string sqlStr = "";
            sqlStr += " SELECT rfc.RFHeaderID, ";
            sqlStr += " RFDetailID, ";
            sqlStr += " rfc.ProductType, ";
            //CUSTOMER LOCATION FIELDS
            sqlStr += " c.CustomerName, ";
            sqlStr += " rfc.LocationID, ";
            sqlStr += " loc.ZipCode, ";
            sqlStr += " loc.State, ";
            sqlStr += " loc.City, ";
            sqlStr += " loc.Country, "; 
            //
            sqlStr += " Claim, ";
            sqlStr += " ClaimDescription, ";
            sqlStr += " CustomerClaimReference, ";
            sqlStr += " rfc.PCNNumber, ";
            sqlStr += " Model, ";
            sqlStr += " DataCode, ";
            sqlStr += " CONVERT(VARCHAR,Warranty,103) AS 'Warranty', ";
            sqlStr += " Quantity, ";
            sqlStr += " CONVERT(VARCHAR,DateOfClaim,103) AS 'DateOfClaim', ";
            sqlStr += " ControlComponentReference, ";
            sqlStr += " TypeOfReturn, ";
            sqlStr += " Application, ";
            sqlStr += " Refrigerant, ";
            sqlStr += " OtherRefrigerant, ";
            sqlStr += " WorkingPointTO, ";
            sqlStr += " WorkingPointTC, ";
            sqlStr += " WorkingPointSuperHeat, ";
            sqlStr += " WorkingPointSubCooling, ";
            sqlStr += " Request, ";
            sqlStr += " RequestWarranty, ";
            sqlStr += " RequestAnalysis, ";
            sqlStr += " RequestCreditOfBody, ";
            sqlStr += " RequestOutOfWarranty, ";
            sqlStr += " RequestSpecificTest, ";
            sqlStr += " RequestSpecificTestRemarks, ";
            sqlStr += " Configuration, ";
            sqlStr += " CONVERT(VARCHAR,DateOfFailure,103) AS 'DateOfFailure', ";
            sqlStr += " RunningHours, ";
            sqlStr += " Remarks, ";
            sqlStr += " FilePath, ";
            sqlStr += " rfc.CustomerNumber, ";
            //sqlStr += "ItemNumber, ";
            sqlStr += " Customer, ";
            sqlStr += "  CONVERT(VARCHAR,PartIncomeDate,103) AS 'PartIncomeDate', ";
            sqlStr += " PartIncomeCW, ";
            sqlStr += " MonthIncome, ";
            sqlStr += " FYIncome, ";
            sqlStr += " cr.Line, ";
            sqlStr += " cr.ItemType ProductGroup, ";
            sqlStr += " FailureDescription, ";
            sqlStr += " Origin, ";
            sqlStr += " CauseOrGuilty, ";
            sqlStr += " CreditDecision, ";
            sqlStr += " ReportText, ";
            sqlStr += " SupplierAnalysis, ";
            sqlStr += " PartReturned, ";
            sqlStr += " Note, ";
            sqlStr += "  CONVERT(VARCHAR,ClosedInKolin,103) AS 'ClosedInKolin', ";
            sqlStr += " ClosedInCW, ";
            sqlStr += " ClosedInMonth, ";
            sqlStr += " FYClosed, ";
            sqlStr += " CONVERT(VARCHAR,ReceivedInKolin,103) AS 'ReceivedInKolin' , ";
            sqlStr += " ReceivedInCurrentWeek, ";
            sqlStr += " ReceivedInMonthIncome , ";
            sqlStr += " FiscalYearReceived , ";
            sqlStr += " OpenOrClosed ,";
            sqlStr += " PortalClaimNumber, ";
            // PORTAL REFERENCE NUMBER HEADER
            sqlStr += " rh.ReferenceNumber PortalClaimNumberHDR, "; 

            sqlStr += " OracleNumber "; // NEW
            sqlStr += " FROM ReturnFormControl rfc " ;

            sqlStr += " INNER JOIN CustomerReturns cr ";
            sqlStr += " ON rfc.PCNNumber = cr.PCNNumber ";
            sqlStr += " INNER JOIN CustomerLocation loc ";
            sqlStr += " ON rfc.LocationID = loc.LocationID  " ;
            sqlStr += " INNER JOIN (SELECT DISTINCT CustomerNumber,CustomerName,LocationId FROM customer) c ";
            sqlStr += " ON rfc.LocationId = c.LocationId ";

            // get the portal reference from header
            sqlStr += " INNER JOIN ReturnFormHeader rh ";
            sqlStr += " ON rfc.RFHeaderID = rh.RFHeaderID ";

            sqlStr += " WHERE rfc.RFDetailID IN (" + rfdID + ")";
            DataTable dt = cm.retrieveDataTable(sqlStr);
            foreach (DataRow dr in dt.Rows)
            {
                rfd.RFHeaderID = int.Parse(u.extractStringValue(dr["RFHeaderID"], Utility.DataType.NUMBER));
                rfd.RFDetailID = int.Parse(u.extractStringValue(dr["RFDetailID"], Utility.DataType.NUMBER));
                rfd.ProductType = u.extractStringValue(dr["ProductType"], Utility.DataType.STRING);
                //CUSTOMER LOCATION FIELD
                rfd.LocationID = u.extractStringValue(dr["LocationID"], Utility.DataType.STRING);
                rfd.CustomerName = u.extractStringValue(dr["CustomerName"], Utility.DataType.STRING);
                rfd.State = u.extractStringValue(dr["State"], Utility.DataType.STRING);
                rfd.City = u.extractStringValue(dr["City"], Utility.DataType.STRING);
                rfd.ZipCode = u.extractStringValue(dr["ZipCode"], Utility.DataType.STRING);
                rfd.Country = u.extractStringValue(dr["Country"], Utility.DataType.STRING);
                //
                rfd.Claim = u.extractStringValue(dr["Claim"], Utility.DataType.STRING);
                rfd.ClaimDescription = u.extractStringValue(dr["ClaimDescription"], Utility.DataType.STRING);
                rfd.ControlCustomerClaimReference = u.extractStringValue(dr["CustomerClaimReference"], Utility.DataType.STRING);
                rfd.PCNNumber = u.extractStringValue(dr["PCNNumber"], Utility.DataType.STRING);
                rfd.ModelProduct = u.extractStringValue(dr["Model"], Utility.DataType.STRING);
                rfd.DataCode = u.extractStringValue(dr["DataCode"], Utility.DataType.STRING);
                rfd.Warranty = u.extractStringValue(dr["Warranty"], Utility.DataType.STRING);
                rfd.Quantity = u.extractStringValue(dr["Quantity"], Utility.DataType.STRING);
                rfd.DateOfClaim = u.extractStringValue(dr["DateOfClaim"], Utility.DataType.DATE);
                rfd.ControlComponentReference = u.extractStringValue(dr["ControlComponentReference"], Utility.DataType.STRING);
                rfd.TypeOfReturn = u.extractStringValue(dr["TypeOfReturn"], Utility.DataType.STRING);
                rfd.Application = u.extractStringValue(dr["Application"], Utility.DataType.STRING);
                rfd.Refrigerant = u.extractStringValue(dr["Refrigerant"], Utility.DataType.STRING);
                rfd.OtherRefrigerant = u.extractStringValue(dr["OtherRefrigerant"], Utility.DataType.STRING);
                rfd.WorkingPointTO = u.extractStringValue(dr["WorkingPointTO"], Utility.DataType.DATE);
                rfd.WorkingPointTC = u.extractStringValue(dr["WorkingPointTC"], Utility.DataType.DATE);
                rfd.WorkingPointSuperHeat = u.extractStringValue(dr["WorkingPointSuperHeat"], Utility.DataType.DATE);
                rfd.WorkingPointSubCooling = u.extractStringValue(dr["WorkingPointSubCooling"], Utility.DataType.DATE);
                rfd.Request = u.extractStringValue(dr["Request"], Utility.DataType.STRING);
                rfd.RequestWarranty = Boolean.Parse(dr["RequestWarranty"].ToString());
                rfd.RequestAnalysis = Boolean.Parse(dr["RequestAnalysis"].ToString());
                rfd.RequestCreditOfBody = Boolean.Parse(dr["RequestCreditOfBody"].ToString());
                rfd.RequestOutOfWarranty = Boolean.Parse(dr["RequestOutOfWarranty"].ToString());
                rfd.RequestSpecificTest = Boolean.Parse(dr["RequestSpecificTest"].ToString());
                rfd.RequestSpecificTestRemarks = u.extractStringValue(dr["RequestSpecificTestRemarks"], Utility.DataType.STRING);
                rfd.Configuration = u.extractStringValue(dr["Configuration"], Utility.DataType.STRING);
                rfd.DateOfFailure = u.extractStringValue(dr["DateOfFailure"], Utility.DataType.DATE);
                rfd.RunningHours = u.extractStringValue(dr["RunningHours"], Utility.DataType.STRING);
                rfd.Remarks = u.extractStringValue(dr["Remarks"], Utility.DataType.STRING);
                rfd.FilePath = u.extractStringValue(dr["FilePath"], Utility.DataType.STRING);
                rfd.CustomerNumber = dr["CustomerNumber"].ToString(); 
                //rfd.ItemNumber = u.extractStringValue(dr["ItemNumber"], Utility.DataType.STRING);
                rfd.Customer = u.extractStringValue(dr["Customer"], Utility.DataType.STRING);
                rfd.DateOfPartIncome = u.extractStringValue(dr["PartIncomeDate"], Utility.DataType.STRING);
                rfd.CurrentWeekOfPartIncome = u.extractStringValue(dr["PartIncomeCW"], Utility.DataType.STRING);
                rfd.MonthIncome = u.extractStringValue(dr["MonthIncome"], Utility.DataType.STRING);
                rfd.FiscalYearIncome = u.extractStringValue(dr["FYIncome"], Utility.DataType.STRING);
                rfd.Line = u.extractStringValue(dr["Line"], Utility.DataType.STRING);
                rfd.ProductGroup = u.extractStringValue(dr["ProductGroup"], Utility.DataType.STRING);
                rfd.FailureDescription = u.extractStringValue(dr["FailureDescription"], Utility.DataType.STRING);
                rfd.Origin = u.extractStringValue(dr["Origin"], Utility.DataType.STRING);
                rfd.CauseOrGuilty = u.extractStringValue(dr["CauseOrGuilty"], Utility.DataType.STRING);
                rfd.CreditDecision = u.extractStringValue(dr["CreditDecision"], Utility.DataType.STRING);
                rfd.ReportText = u.extractStringValue(dr["ReportText"], Utility.DataType.STRING);
                rfd.SupplierAnalysis = u.extractStringValue(dr["SupplierAnalysis"], Utility.DataType.STRING);
                rfd.PartReturned = u.extractStringValue(dr["PartReturned"], Utility.DataType.STRING);
                rfd.Note = u.extractStringValue(dr["Note"], Utility.DataType.STRING);
                rfd.ClosedInKolin = u.extractStringValue(dr["ClosedInKolin"], Utility.DataType.DATE);
                rfd.ClosedInCurrentWeek = u.extractStringValue(dr["ClosedInCW"], Utility.DataType.STRING);
                rfd.ClosedInMonthIncome = u.extractStringValue(dr["ClosedInMonth"], Utility.DataType.STRING);
                rfd.FiscalYearClosed = u.extractStringValue(dr["FYClosed"], Utility.DataType.STRING);
                rfd.ReceivedInKolin = u.extractStringValue(dr["ReceivedInKolin"], Utility.DataType.DATE);
                rfd.ReceivedInCurrentWeek = u.extractStringValue(dr["ReceivedInCurrentWeek"], Utility.DataType.STRING);
                rfd.ReceivedInMonthIncome = u.extractStringValue(dr["ReceivedInMonthIncome"], Utility.DataType.STRING);
                rfd.FiscalYearReceived = u.extractStringValue(dr["FiscalYearReceived"], Utility.DataType.STRING);
                rfd.PortalReference = u.extractStringValue(dr["PortalClaimNumber"], Utility.DataType.STRING);
                rfd.PortalReferenceHDR = u.extractStringValue(dr["PortalClaimNumberHDR"], Utility.DataType.STRING);
                rfd.OracleNumber = u.extractStringValue(dr["OracleNumber"], Utility.DataType.STRING);
                rfd.OpenOrClosed = u.extractStringValue(dr["OpenOrClosed"], Utility.DataType.STRING);

              
            }

          

           
            return rfd;
        }



        public ArrayList getReturnFormControlArray(int headerID, int detailID = 0)
        { 
            ConnectionMaster cm = new ConnectionMaster();
            Utility u = new Utility();
            string sqlStr = "";
            sqlStr += " SELECT RFHeaderID, ";
            sqlStr += " RFDetailID, ";
            sqlStr += " ProductType, ";
            sqlStr += " Claim, "; 
            sqlStr += " rc.LocationID, "; 
            sqlStr += " ClaimDescription, ";
            sqlStr += " CustomerClaimReference, ";
            sqlStr += " PCNNumber, ";
            sqlStr += " Model, ";
            sqlStr += " DataCode, ";
            sqlStr += " CONVERT(VARCHAR,Warranty,103) AS 'Warranty', ";
            sqlStr += " Quantity, ";
            sqlStr += " CONVERT(VARCHAR,DateOfClaim,103) AS 'DateOfClaim', ";
            sqlStr += " ControlComponentReference, ";
            sqlStr += " TypeOfReturn, ";
            sqlStr += " Application, ";
            sqlStr += " Refrigerant, ";
            sqlStr += " OtherRefrigerant, ";
            sqlStr += " WorkingPointTO, ";
            sqlStr += " WorkingPointTC, ";
            sqlStr += " WorkingPointSuperHeat, ";
            sqlStr += " WorkingPointSubCooling, ";
            sqlStr += " Request, ";
            sqlStr += " RequestWarranty, ";
            sqlStr += " RequestAnalysis, ";
            sqlStr += " RequestCreditOfBody, ";
            sqlStr += " RequestOutOfWarranty, ";
            sqlStr += " RequestSpecificTest, ";
            sqlStr += " RequestSpecificTestRemarks, ";
            sqlStr += " Configuration, ";
            sqlStr += " CONVERT(VARCHAR,DateOfFailure,103) AS 'DateOfFailure', ";
            sqlStr += " RunningHours, ";
            sqlStr += " Remarks, ";
            sqlStr += " FilePath, ";
            sqlStr += " rc.CustomerNumber, ";
            //sqlStr += "ItemNumber, ";
            sqlStr += " Customer, ";
            sqlStr += "  CONVERT(VARCHAR,PartIncomeDate,103) AS 'PartIncomeDate', ";
            sqlStr += " PartIncomeCW, ";
            sqlStr += " MonthIncome, ";
            sqlStr += " FYIncome, ";
            sqlStr += " Line, ";
            sqlStr += " ProductGroup, ";
            sqlStr += " FailureDescription, ";
            sqlStr += " Origin, ";
            sqlStr += " CauseOrGuilty, ";
            sqlStr += " CreditDecision, ";
            sqlStr += " ReportText, ";
            sqlStr += " SupplierAnalysis, ";
            sqlStr += " PartReturned, ";
            sqlStr += " Note, ";
            sqlStr += "  CONVERT(VARCHAR,ClosedInKolin,103) AS 'ClosedInKolin', ";
            sqlStr += " ClosedInCW, ";
            sqlStr += " ClosedInMonth, ";
            sqlStr += " FYClosed, ";
            sqlStr += " CONVERT(VARCHAR,ReceivedInKolin,103) AS 'ReceivedInKolin' , ";
            sqlStr += " ReceivedInCurrentWeek, ";
            sqlStr += " ReceivedInMonthIncome , ";
            sqlStr += " FiscalYearReceived , ";
            sqlStr += " OpenOrClosed ,";
            sqlStr += " PortalClaimNumber, ";
            sqlStr += " OracleNumber, "; // NEW
            //CUSTOMER LOCATION FIELDS
            sqlStr += " (CAST(rc.LocationID as varchar) + ' - ' +  ISNULL(City,'') + ', ' + ISNULL(State,'') + ', ' + ISNULL(ZipCode,'')) LocationAddress, "; // Location City, State, ZipCode
            sqlStr += " cl.City, ";
            sqlStr += " cl.State, ";
            sqlStr += " cl.ZipCode, ";
            sqlStr += " cl.Country, ";
            sqlStr += " c.CustomerName ";
            //
            sqlStr += " FROM ReturnFormControl rc ";
            sqlStr += " INNER JOIN CustomerLocation cl ";
            sqlStr += " ON rc.LocationID = cl.LocationID ";
            sqlStr += " INNER JOIN (SELECT DISTINCT CustomerNumber,CustomerName,LocationId FROM customer) c ";
            sqlStr += " ON rc.LocationId = c.LocationId ";
            sqlStr += " WHERE RFHeaderID = " + headerID;
            if(detailID != 0)
                sqlStr += " AND RFDetailID = " + detailID;
 
            DataTable dt = cm.retrieveDataTable(sqlStr);
            ArrayList result = new ArrayList();
            foreach (DataRow dr in dt.Rows)
            {
                Model.ReturnFormControl rfd = new Model.ReturnFormControl();
                rfd.RFHeaderID = int.Parse(u.extractStringValue(dr["RFHeaderID"], Utility.DataType.NUMBER));
                rfd.RFDetailID = int.Parse(u.extractStringValue(dr["RFDetailID"], Utility.DataType.NUMBER));
                rfd.ProductType = u.extractStringValue(dr["ProductType"], Utility.DataType.STRING);
                rfd.Claim = u.extractStringValue(dr["Claim"], Utility.DataType.STRING);

                //CUSTOMER LOCATION FIELDS
                rfd.LocationAddress = u.extractStringValue(dr["LocationAddress"], Utility.DataType.STRING);
                rfd.LocationID = u.extractStringValue(dr["LocationID"], Utility.DataType.STRING);
                rfd.CustomerName = u.extractStringValue(dr["CustomerName"], Utility.DataType.STRING);
                rfd.City = u.extractStringValue(dr["City"], Utility.DataType.STRING);
                rfd.State = u.extractStringValue(dr["State"], Utility.DataType.STRING);
                rfd.ZipCode = u.extractStringValue(dr["ZipCode"], Utility.DataType.STRING);
                rfd.Country = u.extractStringValue(dr["Country"], Utility.DataType.STRING); 
                //

                rfd.ClaimDescription = u.extractStringValue(dr["ClaimDescription"], Utility.DataType.STRING);
                rfd.ControlCustomerClaimReference = u.extractStringValue(dr["CustomerClaimReference"], Utility.DataType.STRING);
                rfd.PCNNumber = u.extractStringValue(dr["PCNNumber"], Utility.DataType.STRING);
                rfd.ModelProduct = u.extractStringValue(dr["Model"], Utility.DataType.STRING);
                rfd.DataCode = u.extractStringValue(dr["DataCode"], Utility.DataType.STRING);
                rfd.Warranty = u.extractStringValue(dr["Warranty"], Utility.DataType.STRING);
                rfd.Quantity = u.extractStringValue(dr["Quantity"], Utility.DataType.STRING);
                rfd.DateOfClaim =  u.extractStringValue(dr["DateOfClaim"], Utility.DataType.DATE);
                rfd.ControlComponentReference = u.extractStringValue(dr["ControlComponentReference"], Utility.DataType.STRING);
                rfd.TypeOfReturn = u.extractStringValue(dr["TypeOfReturn"], Utility.DataType.STRING);
                rfd.Application = u.extractStringValue(dr["Application"], Utility.DataType.STRING);
                rfd.Refrigerant = u.extractStringValue(dr["Refrigerant"], Utility.DataType.STRING);
                rfd.OtherRefrigerant = u.extractStringValue(dr["OtherRefrigerant"], Utility.DataType.STRING);
                rfd.WorkingPointTO = u.extractStringValue(dr["WorkingPointTO"], Utility.DataType.DATE);
                rfd.WorkingPointTC = u.extractStringValue(dr["WorkingPointTC"], Utility.DataType.DATE);
                rfd.WorkingPointSuperHeat = u.extractStringValue(dr["WorkingPointSuperHeat"], Utility.DataType.DATE);
                rfd.WorkingPointSubCooling = u.extractStringValue(dr["WorkingPointSubCooling"], Utility.DataType.DATE);
                rfd.Request = u.extractStringValue(dr["Request"], Utility.DataType.STRING);
                rfd.RequestWarranty = Boolean.Parse(dr["RequestWarranty"].ToString());
                rfd.RequestAnalysis = Boolean.Parse(dr["RequestAnalysis"].ToString());
                rfd.RequestCreditOfBody = Boolean.Parse(dr["RequestCreditOfBody"].ToString());
                rfd.RequestOutOfWarranty = Boolean.Parse(dr["RequestOutOfWarranty"].ToString());
                rfd.RequestSpecificTest = Boolean.Parse(dr["RequestSpecificTest"].ToString());
                rfd.RequestSpecificTestRemarks = u.extractStringValue(dr["RequestSpecificTestRemarks"], Utility.DataType.STRING);
                rfd.Configuration = u.extractStringValue(dr["Configuration"], Utility.DataType.STRING);
                rfd.DateOfFailure = u.extractStringValue(dr["DateOfFailure"], Utility.DataType.DATE);
                rfd.RunningHours = u.extractStringValue(dr["RunningHours"], Utility.DataType.STRING);
                rfd.Remarks = u.extractStringValue(dr["Remarks"], Utility.DataType.STRING);
                rfd.FilePath = u.extractStringValue(dr["FilePath"], Utility.DataType.STRING);
                rfd.CustomerNumber =  dr["CustomerNumber"].ToString();
                //rfd.ItemNumber = u.extractStringValue(dr["ItemNumber"], Utility.DataType.STRING);
                rfd.Customer = u.extractStringValue(dr["Customer"], Utility.DataType.STRING);
                rfd.DateOfPartIncome = u.extractStringValue(dr["PartIncomeDate"], Utility.DataType.STRING);
                rfd.CurrentWeekOfPartIncome = u.extractStringValue(dr["PartIncomeCW"], Utility.DataType.STRING);
                rfd.MonthIncome = u.extractStringValue(dr["MonthIncome"], Utility.DataType.STRING);
                rfd.FiscalYearIncome = u.extractStringValue(dr["FYIncome"], Utility.DataType.STRING);
                rfd.Line = u.extractStringValue(dr["Line"], Utility.DataType.STRING);
                rfd.ProductGroup = u.extractStringValue(dr["ProductGroup"], Utility.DataType.STRING);
                rfd.FailureDescription = u.extractStringValue(dr["FailureDescription"], Utility.DataType.STRING);
                rfd.Origin = u.extractStringValue(dr["Origin"], Utility.DataType.STRING);
                rfd.CauseOrGuilty = u.extractStringValue(dr["CauseOrGuilty"], Utility.DataType.STRING);
                rfd.CreditDecision = u.extractStringValue(dr["CreditDecision"], Utility.DataType.STRING);
                rfd.ReportText = u.extractStringValue(dr["ReportText"], Utility.DataType.STRING);
                rfd.SupplierAnalysis = u.extractStringValue(dr["SupplierAnalysis"], Utility.DataType.STRING);
                rfd.PartReturned = u.extractStringValue(dr["PartReturned"], Utility.DataType.STRING);
                rfd.Note = u.extractStringValue(dr["Note"], Utility.DataType.STRING);
                rfd.ClosedInKolin = u.extractStringValue(dr["ClosedInKolin"], Utility.DataType.DATE);
                rfd.ClosedInCurrentWeek = u.extractStringValue(dr["ClosedInCW"], Utility.DataType.STRING);
                rfd.ClosedInMonthIncome = u.extractStringValue(dr["ClosedInMonth"], Utility.DataType.STRING);
                rfd.FiscalYearClosed = u.extractStringValue(dr["FYClosed"], Utility.DataType.STRING);
                rfd.ReceivedInKolin = u.extractStringValue(dr["ReceivedInKolin"], Utility.DataType.DATE);
                rfd.ReceivedInCurrentWeek  = u.extractStringValue(dr["ReceivedInCurrentWeek"], Utility.DataType.STRING);
                rfd.ReceivedInMonthIncome = u.extractStringValue(dr["ReceivedInMonthIncome"], Utility.DataType.STRING);
                rfd.FiscalYearReceived = u.extractStringValue(dr["FiscalYearReceived"], Utility.DataType.STRING);
                rfd.PortalReference = u.extractStringValue(dr["PortalClaimNumber"], Utility.DataType.STRING);
                rfd.OracleNumber = u.extractStringValue(dr["OracleNumber"], Utility.DataType.STRING);
                rfd.OpenOrClosed = u.extractStringValue(dr["OpenOrClosed"], Utility.DataType.STRING);

                // Please ship to:

                rfd.ProductLocation = Maintenance.ReturnFormDetail.GetProductLocation(2); //control
                rfd.ProductLocation = rfd.ProductLocation.Replace("\n","<br/>");

                // Add Contact Information for the Print
                string contactinfo = "";
                Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
                DataTable dtc = mrfd.getContactPerson();
                contactinfo += "If you have question or concerns about this returns, please contact " + dtc.Rows[0]["Name"];
                contactinfo += "<br/>Email Address: " + dtc.Rows[0]["EmailAddress"] + " Phone Number: " + dtc.Rows[0]["PhoneNumber"];
                rfd.ContactInformation = contactinfo;

                result.Add(rfd);
            }
            return result;
        }

        

        public Model.ReturnFormControl getReturnFormControlByHeaderID(string _headeID)
        {
            Model.ReturnFormControl rfd = new Model.ReturnFormControl();
            ConnectionMaster cm = new ConnectionMaster();
            Utility u = new Utility();
            string sqlStr = "";
            sqlStr += " SELECT RFDetailID, ";
            sqlStr += " ProductType, ";
            sqlStr += " Claim, ";
            sqlStr += " ClaimDescription, ";
            sqlStr += " CustomerClaimReference, ";
            sqlStr += " PCNNumber, ";
            sqlStr += " Model, ";
            sqlStr += " DataCode, ";
            sqlStr += " Warranty, ";
            sqlStr += " Quantity, ";
            sqlStr += " CONVERT(VARCHAR,DateOfClaim,101) AS 'DateOfClaim', ";
            sqlStr += " ControlComponentReference, ";
            sqlStr += " TypeOfReturn, ";
            sqlStr += " Application, ";
            sqlStr += " Refrigerant, ";
            sqlStr += " OtherRefrigerant, ";
            sqlStr += " WorkingPointTO, ";
            sqlStr += " WorkingPointTC, ";
            sqlStr += " WorkingPointSuperHeat, ";
            sqlStr += " WorkingPointSubCooling, ";
            sqlStr += " Request, ";
            sqlStr += " RequestWarranty, ";
            sqlStr += " RequestAnalysis, ";
            sqlStr += " RequestCreditOfBody, ";
            sqlStr += " RequestOutOfWarranty, ";
            sqlStr += " RequestSpecificTest, ";
            sqlStr += " RequestSpecificTestRemarks, ";
            sqlStr += " Configuration, ";
            sqlStr += " CONVERT(VARCHAR,DateOfFailure,101) AS 'DateOfFailure', ";
            sqlStr += " RunningHours, ";
            sqlStr += " Remarks, ";
            sqlStr += " FilePath, ";
            sqlStr += " CustomerNumber, ";
            //sqlStr += "ItemNumber, ";
            sqlStr += " Customer, ";
            sqlStr += " PartIncomeDate, ";
            sqlStr += " PartIncomeCW, ";
            sqlStr += " MonthIncome, ";
            sqlStr += " FYIncome, ";
            sqlStr += " Line, ";
            sqlStr += " ProductGroup, ";
            sqlStr += " FailureDescription, ";
            sqlStr += " Origin, ";
            sqlStr += " CauseOrGuilty, ";
            sqlStr += " CreditDecision, ";
            sqlStr += " ReportText, ";
            sqlStr += " SupplierAnalysis, ";
            sqlStr += " PartReturned, ";
            sqlStr += " Note, ";
            sqlStr += " ClosedInKolin, ";
            sqlStr += " ClosedInCW, ";
            sqlStr += " ClosedInMonth, ";
            sqlStr += " FYClosed, ";
            sqlStr += " ReceivedInKolin, ";
            sqlStr += " ReceivedInCurrentWeek, ";
            sqlStr += " ReceivedInMonthIncome, ";
            sqlStr += " FiscalYearReceived, ";
            sqlStr += " OpenOrClosed ";
            sqlStr += " FROM ReturnFormControl WHERE RFHeaderID IN (" + _headeID + ")";
            DataTable dt = cm.retrieveDataTable(sqlStr);
            foreach (DataRow dr in dt.Rows)
            {
                rfd.RFDetailID = int.Parse(u.extractStringValue(dr["RFDetailID"], Utility.DataType.STRING));
                rfd.ProductType = u.extractStringValue(dr["ProductType"], Utility.DataType.STRING);
                rfd.Claim = u.extractStringValue(dr["Claim"], Utility.DataType.STRING);
                rfd.ClaimDescription = u.extractStringValue(dr["ClaimDescription"], Utility.DataType.STRING);
                rfd.ControlCustomerClaimReference = u.extractStringValue(dr["CustomerClaimReference"], Utility.DataType.STRING);
                rfd.PCNNumber = u.extractStringValue(dr["PCNNumber"], Utility.DataType.STRING);
                rfd.ModelProduct = u.extractStringValue(dr["Model"], Utility.DataType.STRING);
                rfd.DataCode = u.extractStringValue(dr["DataCode"], Utility.DataType.STRING);
                rfd.Warranty = u.extractStringValue(dr["Warranty"], Utility.DataType.STRING);
                rfd.Quantity = u.extractStringValue(dr["Quantity"], Utility.DataType.STRING);
                rfd.DateOfClaim = u.extractStringValue(dr["DateOfClaim"], Utility.DataType.DATE);
                rfd.ControlComponentReference = u.extractStringValue(dr["ControlComponentReference"], Utility.DataType.STRING);
                rfd.TypeOfReturn = u.extractStringValue(dr["TypeOfReturn"], Utility.DataType.STRING);
                rfd.Application = u.extractStringValue(dr["Application"], Utility.DataType.STRING);
                rfd.Refrigerant = u.extractStringValue(dr["Refrigerant"], Utility.DataType.STRING);
                rfd.OtherRefrigerant = u.extractStringValue(dr["OtherRefrigerant"], Utility.DataType.STRING);
                rfd.WorkingPointTO = u.extractStringValue(dr["WorkingPointTO"], Utility.DataType.DATE);
                rfd.WorkingPointTC = u.extractStringValue(dr["WorkingPointTC"], Utility.DataType.DATE);
                rfd.WorkingPointSuperHeat = u.extractStringValue(dr["WorkingPointSuperHeat"], Utility.DataType.DATE);
                rfd.WorkingPointSubCooling = u.extractStringValue(dr["WorkingPointSubCooling"], Utility.DataType.DATE);
                rfd.Request = u.extractStringValue(dr["Request"], Utility.DataType.STRING);
                rfd.RequestWarranty = Boolean.Parse(dr["RequestWarranty"].ToString());
                rfd.RequestAnalysis = Boolean.Parse(dr["RequestAnalysis"].ToString());
                rfd.RequestCreditOfBody = Boolean.Parse(dr["RequestCreditOfBody"].ToString());
                rfd.RequestOutOfWarranty = Boolean.Parse(dr["RequestOutOfWarranty"].ToString());
                rfd.RequestSpecificTest = Boolean.Parse(dr["RequestSpecificTest"].ToString());
                rfd.RequestSpecificTestRemarks = u.extractStringValue(dr["RequestSpecificTestRemarks"], Utility.DataType.STRING);
                rfd.Configuration = u.extractStringValue(dr["Configuration"], Utility.DataType.STRING);
                rfd.DateOfFailure = u.extractStringValue(dr["DateOfFailure"], Utility.DataType.DATE);
                rfd.RunningHours = u.extractStringValue(dr["RunningHours"], Utility.DataType.STRING);
                rfd.Remarks = u.extractStringValue(dr["Remarks"], Utility.DataType.STRING);
                rfd.FilePath = u.extractStringValue(dr["FilePath"], Utility.DataType.STRING);
                rfd.CustomerNumber = dr["CustomerNumber"].ToString();
                //rfd.ItemNumber = u.extractStringValue(dr["ItemNumber"], Utility.DataType.STRING);
                rfd.Customer = u.extractStringValue(dr["Customer"], Utility.DataType.STRING);
                rfd.DateOfPartIncome = u.extractStringValue(dr["PartIncomeDate"], Utility.DataType.STRING);
                rfd.CurrentWeekOfPartIncome = u.extractStringValue(dr["PartIncomeCW"], Utility.DataType.STRING);
                rfd.MonthIncome = u.extractStringValue(dr["MonthIncome"], Utility.DataType.STRING);
                rfd.FiscalYearIncome = u.extractStringValue(dr["FYIncome"], Utility.DataType.STRING);
                rfd.Line = u.extractStringValue(dr["Line"], Utility.DataType.STRING);
                rfd.ProductGroup = u.extractStringValue(dr["ProductGroup"], Utility.DataType.STRING);
                rfd.FailureDescription = u.extractStringValue(dr["FailureDescription"], Utility.DataType.STRING);
                rfd.Origin = u.extractStringValue(dr["Origin"], Utility.DataType.STRING);
                rfd.CauseOrGuilty = u.extractStringValue(dr["CauseOrGuilty"], Utility.DataType.STRING);
                rfd.CreditDecision = u.extractStringValue(dr["CreditDecision"], Utility.DataType.STRING);
                rfd.ReportText = u.extractStringValue(dr["ReportText"], Utility.DataType.STRING);
                rfd.SupplierAnalysis = u.extractStringValue(dr["SupplierAnalysis"], Utility.DataType.STRING);
                rfd.PartReturned = u.extractStringValue(dr["PartReturned"], Utility.DataType.STRING);
                rfd.Note = u.extractStringValue(dr["Note"], Utility.DataType.STRING);
                rfd.ClosedInKolin = u.extractStringValue(dr["ClosedInKolin"], Utility.DataType.DATE);
                rfd.ClosedInCurrentWeek = u.extractStringValue(dr["ClosedInCW"], Utility.DataType.STRING);
                rfd.ClosedInMonthIncome = u.extractStringValue(dr["ClosedInMonth"], Utility.DataType.STRING);
                rfd.FiscalYearClosed = u.extractStringValue(dr["FYClosed"], Utility.DataType.STRING);
                rfd.ReceivedInKolin = u.extractStringValue(dr["ReceivedInKolin"], Utility.DataType.DATE);
                rfd.ReceivedInCurrentWeek = u.extractStringValue(dr["ReceivedInCurrentWeek"], Utility.DataType.STRING);
                rfd.ReceivedInMonthIncome = u.extractStringValue(dr["ReceivedInMonthIncome"], Utility.DataType.STRING);
                rfd.FiscalYearReceived = u.extractStringValue(dr["FiscalYearReceived"], Utility.DataType.STRING);

                rfd.OpenOrClosed = u.extractStringValue(dr["OpenOrClosed"], Utility.DataType.STRING);
            }
            return rfd;
        }

        public static void deleteReturnFormControl(int rfdetailid) {
            ConnectionMaster cm = new ConnectionMaster();
            cm.executeCommand("DELETE FROM ReturnFormControl WHERE RFDetailID = " + rfdetailid, false); 
        }
 
        public Model.ReturnFormControl updateReturnFormControl(Model.ReturnFormControl rc)
        {
            Model.ReturnFormControl rfc = new Model.ReturnFormControl();
            ConnectionMaster cm = new ConnectionMaster();
            Utility u = new Utility();
            string sqlStr = "";
            sqlStr += " UPDATE ReturnFormControl SET ";
            sqlStr += " LocationID = '" + u.convertToSQLString(rc.LocationID.ToString(), Utility.DataType.NUMBER) + "', ";
            sqlStr += " Claim = " + u.convertToSQLString(rc.Claim, Utility.DataType.STRING) + ", ";
            sqlStr += " ClaimDescription = " + u.convertToSQLString(rc.ClaimDescription, Utility.DataType.STRING) + ", ";
            sqlStr += " CustomerClaimReference = " + u.convertToSQLString(rc.ControlCustomerClaimReference, Utility.DataType.STRING) + ", ";
            sqlStr += " PCNNumber = " + u.convertToSQLString(rc.PCNNumber, Utility.DataType.STRING) + ", ";
            sqlStr += " Model = " + u.convertToSQLString(rc.ModelProduct, Utility.DataType.STRING) + ", ";
            sqlStr += " ProductGroup = " + u.convertToSQLString(rc.ProductGroup, Utility.DataType.STRING) + ", ";
            sqlStr += " Line = " + u.convertToSQLString(rc.Line, Utility.DataType.STRING) + ", ";
            sqlStr += " DataCode = " + u.convertToSQLString(rc.DataCode, Utility.DataType.STRING) + ", ";
            sqlStr += " Warranty = " + u.convertToSQLString(rc.Warranty, Utility.DataType.STRING) + ", ";
            sqlStr += " Quantity = " + u.convertToSQLString(rc.Quantity, Utility.DataType.STRING) + ", ";
            sqlStr += " DateOfClaim = " + u.convertToSQLString(rc.DateOfClaim, Utility.DataType.DATE) + ", ";
            sqlStr += " ControlComponentReference = " + u.convertToSQLString(rc.ControlComponentReference, Utility.DataType.STRING) + ", ";
            sqlStr += " TypeOfReturn = " + u.convertToSQLString(rc.TypeOfReturn, Utility.DataType.STRING) + ", ";
            sqlStr += " Application = " + u.convertToSQLString(rc.Application, Utility.DataType.STRING) + ", ";
            sqlStr += " Refrigerant = " + u.convertToSQLString(rc.Refrigerant, Utility.DataType.STRING) + ", ";
            sqlStr += " OtherRefrigerant = " + u.convertToSQLString(rc.OtherRefrigerant, Utility.DataType.STRING) + ", ";
            sqlStr += " WorkingPointTo = " + u.convertToSQLString(rc.WorkingPointTO, Utility.DataType.DECIMAL) + ", ";
            sqlStr += " WorkingPointTc = " + u.convertToSQLString(rc.WorkingPointTC, Utility.DataType.DECIMAL) + ", ";
            sqlStr += " WorkingPointSuperHeat = " + u.convertToSQLString(rc.WorkingPointSuperHeat, Utility.DataType.DECIMAL) + ", ";
            sqlStr += " WorkingPointSubCooling = " + u.convertToSQLString(rc.WorkingPointSubCooling, Utility.DataType.DECIMAL) + ", ";
            sqlStr += " Request = " + u.convertToSQLString(rc.Request, Utility.DataType.STRING) + ", ";
            sqlStr += " RequestSpecificTestRemarks = " + u.convertToSQLString(rc.RequestSpecificTestRemarks, Utility.DataType.STRING) + ", ";
            sqlStr += " Configuration = " + u.convertToSQLString(rc.Configuration, Utility.DataType.STRING) + ", ";
            sqlStr += " DateOfFailure = " + u.convertToSQLString(rc.DateOfFailure, Utility.DataType.DATE) + ", ";
            sqlStr += " RunningHours = " + u.convertToSQLString(rc.RunningHours, Utility.DataType.NUMBER) + ", ";
            sqlStr += " Remarks = " + u.convertToSQLString(rc.Remarks, Utility.DataType.STRING) + ", ";
            sqlStr += " FailureDescription = " + u.convertToSQLString(rc.FailureDescription, Utility.DataType.STRING) + ", ";
            sqlStr += " Origin = " + u.convertToSQLString(rc.Origin, Utility.DataType.STRING) + ", ";
            sqlStr += " CauseOrGuilty = " + u.convertToSQLString(rc.CauseOrGuilty, Utility.DataType.STRING) + ", ";
            sqlStr += " CreditDecision = " + u.convertToSQLString(rc.CreditDecision, Utility.DataType.STRING) + ", ";
            sqlStr += " ReportText = " + u.convertToSQLString(rc.ReportText, Utility.DataType.STRING) + ", ";
            sqlStr += " SupplierAnalysis = " + u.convertToSQLString(rc.SupplierAnalysis, Utility.DataType.STRING) + ", ";
            sqlStr += " PartReturned = " + u.convertToSQLString(rc.PartReturned, Utility.DataType.STRING) + ", ";
            sqlStr += " Note = " + u.convertToSQLString(rc.Note, Utility.DataType.STRING) + ", ";
            sqlStr += " ClosedInKolin = " + u.convertToSQLString(rc.ClosedInKolin, Utility.DataType.DATE) + ", ";
            sqlStr += " ClosedInCW = " + u.convertToSQLString(rc.ClosedInCurrentWeek, Utility.DataType.STRING) + ", ";
            sqlStr += " ClosedInMonth = " + u.convertToSQLString(rc.ClosedInMonthIncome, Utility.DataType.STRING) + ", ";
            sqlStr += " FYClosed = " + u.convertToSQLString(rc.FiscalYearClosed, Utility.DataType.STRING) + ", ";

            sqlStr += " ReceivedInKolin = " + u.convertToSQLString(rc.ReceivedInKolin, Utility.DataType.DATE) + ", ";
            sqlStr += " ReceivedInCurrentWeek = " + u.convertToSQLString(rc.ReceivedInCurrentWeek, Utility.DataType.STRING) + ", ";
            sqlStr += " ReceivedInMonthIncome = " + u.convertToSQLString(rc.ReceivedInMonthIncome, Utility.DataType.STRING) + ", ";
            sqlStr += " FiscalYearReceived = " + u.convertToSQLString(rc.FiscalYearReceived, Utility.DataType.STRING) + ", ";
            sqlStr += " OracleNumber = " + u.convertToSQLString(rc.OracleNumber, Utility.DataType.STRING) + ", ";

            sqlStr += " OpenOrClosed = " + u.convertToSQLString(rc.OpenOrClosed, Utility.DataType.STRING) + ", ";
            sqlStr += " Analyzeddate = GetDate() ";

            //if (rc.FailureDescription != "")
            //{
                sqlStr += "  WHERE RFDetailID = '" + rc.RFDetailID + "'";
            //}

            cm.executeCommand(sqlStr, false);
            return rc;
        }

        public String getCustomerNumberByDetailID(int id) {
            ConnectionMaster cm = new ConnectionMaster();
            string _sqlStr = "";
           
            _sqlStr = "SELECT CustomerNumber FROM ReturnFormControl WHERE RFDetailID = " + id;
            DataTable dt = cm.retrieveDataTable(_sqlStr);
            return dt.Rows[0]["CustomerNumber"].ToString();
        }

        public DataTable getMyReturnControl(int userid, int searchBy, string searchText, string selectedCustNum)
        {
            /*
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            sqlStr += " SELECT a.RFDetailID, a.Model,a.PCNNumber, ";
            sqlStr += " a.ControlComponentReference AS 'CustRef',a.TypeOfReturn AS 'Reject',  ";
            sqlStr += " a.ReceptionDate AS 'ReceptionDate',a.AnalyzedDate AS 'AnalyzeDate'  ";
            sqlStr += " FROM ReturnFormControl a ORDER BY RFDetailID DESC ";
           
            DataTable dt = cm.retrieveDataTable(sqlStr);
            return dt;
            */
            //
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            sqlStr += " SELECT a.Customer, a.RFDetailID, a.Model, a.PCNNumber, a.PortalClaimNumber AS 'ReferenceNumber',  a.OracleNumber,";
            sqlStr += " a.CustomerClaimReference 'CustRef',a.TypeOfReturn AS 'Reject',CONVERT(varchar(11),b.ReturnDate,103) AS 'SubmissionDate',  ";
            sqlStr += "CONVERT(varchar(11),a.ReceivedInKolin,103) AS 'ReceptionDate',CONVERT(varchar(11),a.ClosedInKolin,103) AS 'AnalyzeDate',  ";
            sqlStr += "d.CreditDecision AS 'CreditNote',  ";
            sqlStr += " CONCAT('If you have question or concerns about this returns, please contact ', cp.NAME, '<br/>Email Address: ', cp.EmailAddress, ' Phone number: ', cp.PhoneNumber) ContactInfo ";
            sqlStr += "FROM ReturnFormControl a  ";
            sqlStr += "INNER JOIN ReturnFormHeader b ON (a.rfheaderid = b.rfheaderid)  ";
            sqlStr += "INNER JOIN ContactPerson cp ON (a.rfheaderid = cp.rfheaderid)  ";
            sqlStr += "LEFT JOIN ReturnFormControl d ON (a.rfdetailid = d.rfdetailid)  ";
            sqlStr += "INNER JOIN Customer cs ON (a.CustomerNumber = cs.CustomerNumber AND a.LocationID = cs.LocationID and Control = 1) ";
            sqlStr += "LEFT JOIN TEST_CUSTQCLMD1 c ON a.PCNNumber = c.CPSERN AND b.ReferenceNumber = c.PortalReference ";
            sqlStr += "WHERE (1=1) ";

            // list all assigned customers to user
            sqlStr += " AND a.CustomerNUmber IN (SELECT cus.CustomerNumber FROM UserMapping  um "; 
            sqlStr += " INNER JOIN Customer cus on um.CustomerID = cus.CustomerID ";
            sqlStr += " WHERE um.UserId = " + userid + " )";

            if (selectedCustNum != "-- ALL --")
                sqlStr += " AND a.CustomerNumber IN (" + selectedCustNum + ") ";
            else
                sqlStr += " AND a.CustomerNumber IN (SELECT DISTINCT CUSTOMERNUMBER FROM CUSTOMER WHERE CUSTOMERID IN (SELECT CUSTOMERID FROM USERMAPPING WHERE USERID = " + userid + ")) ";
            if (searchText != "")
            {
                if (searchBy == 1)
                    sqlStr += " AND LTRIM(RTRIM(a.PCNNumber)) like '" + searchText.Replace("*", "%") + "'";
                if (searchBy == 2)
                    sqlStr += " AND LTRIM(RTRIM(a.Model)) like '" + searchText.Replace("*", "%") + "'";
            }
           
           

            sqlStr += " ORDER BY a.Created DESC, 'Model'";
            DataTable dtc = cm.retrieveDataTable(sqlStr);
            return dtc;
        }

        public DataTable getMyReturnControlAll( int searchBy, string searchText, string selectedCustNum, string userType, int userId)
        {

            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            sqlStr += " SELECT a.Customer,a.RFDetailID, a.Model, a.PCNNumber, a.PortalClaimNumber AS 'ReferenceNumber', a.OracleNumber, ";
           
            sqlStr += " a.CustomerClaimReference 'CustRef',a.TypeOfReturn AS 'Reject',CONVERT(varchar(11),b.ReturnDate,103) AS 'SubmissionDate',  ";
          
            sqlStr += "CONVERT(varchar(11),a.ReceivedInKolin,103) AS 'ReceptionDate',CONVERT(varchar(11),a.ClosedInKolin,103) AS 'AnalyzeDate',  ";
           
            sqlStr += "d.CreditDecision AS 'CreditNote',  ";
            sqlStr += " CONCAT('If you have question or concerns about this returns, please contact ', cp.NAME, '<br/>Email Address: ', cp.EmailAddress, ' Phone number: ', cp.PhoneNumber) ContactInfo ";
 
            sqlStr += "FROM ReturnFormControl a  ";
            sqlStr += "INNER JOIN ReturnFormHeader b ON (a.rfheaderid = b.rfheaderid)  ";
            sqlStr += "INNER JOIN ContactPerson cp ON (a.rfheaderid = cp.rfheaderid)  ";
            sqlStr += "LEFT JOIN ReturnFormControl d ON (a.rfdetailid = d.rfdetailid)  ";
            sqlStr += "INNER JOIN Customer cs ON (a.CustomerNumber = cs.CustomerNumber AND a.LocationID = cs.LocationID and Control = 1)"; 
        
            sqlStr += "LEFT JOIN TEST_CUSTQCLMD1 c ON a.PCNNumber = c.CPSERN AND b.ReferenceNumber = c.PortalReference ";
    
            //[START] [jpbersonda] [5/10/17] [For CSO user type]
            if (userType == "CSO")
            {
                sqlStr += "INNER JOIN SalesOfficeCode  soc ON CONVERT(varchar,soc.SalesPersonNumber) = cs.SalesOfficeCOde  ";
                sqlStr += "INNER JOIN UserInformations ui  ON ui.SOCID = soc.SOCID ";
            }
            //[END] [jpbersonda] [5/10/17] [For CSO user type]

            sqlStr += "WHERE (1=1) ";

            if (userType == "CSO")
                sqlStr += "AND ui.UserId = " + userId;

            if (selectedCustNum != "-- ALL --")
                sqlStr += "AND a.CustomerNumber IN (" + selectedCustNum + ") ";
            
            if (searchText != "") {
                if (searchBy == 1)
                    sqlStr += "AND LTRIM(RTRIM(a.PCNNumber)) like '" + searchText.Replace("*", "%") + "'";
                if (searchBy == 2)
                    sqlStr += "AND LTRIM(RTRIM(a.Model)) like '" + searchText.Replace("*", "%") + "'";
            }
            

            sqlStr += "ORDER BY a.Created DESC, 'Model'";
            DataTable dtc = cm.retrieveDataTable(sqlStr);

           
            return dtc;
        }

        public DataTable getStatisticsByYOM(int userid, string selectedCustNum)
        {
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            sqlStr += "SELECT SUM(CAST(TBL2.[RETURN] AS FLOAT)) AS 'PPM',TBL2.YOM AS 'YEAR' FROM ";
            sqlStr += "(SELECT COUNT(TBL1.CPNSER) AS 'RETURN',TBL1.YOM,TBL1.CPIDEN FROM ";
            sqlStr += "(SELECT CPNSER,YOM,CPIDEN FROM CUSTQCLMD ";
            sqlStr += "WHERE (1=1) ";

            if (selectedCustNum != "-- ALL --")
                sqlStr += "AND CPNCLI = " + selectedCustNum + " ";
            else
                sqlStr += "AND CPNCLI IN (SELECT DISTINCT CUSTOMERNUMBER FROM CUSTOMER WHERE CUSTOMERID IN (SELECT CUSTOMERID FROM USERMAPPING WHERE USERID = " + userid + ")) ";

            sqlStr += "AND CPRJCT IN ('LR','FR') ";
            sqlStr += "AND CPWARR IN (1,2,5,8)) AS TBL1 ";
            sqlStr += "GROUP BY TBL1.YOM,TBL1.CPIDEN) AS TBL2 ";
            sqlStr += "WHERE TBL2.YOM IN (SELECT DISTINCT YOM FROM CUSTQCLMD WHERE YOM BETWEEN ";
            sqlStr += "(CONVERT(INT,YEAR(GETDATE())) - 5) AND YEAR(GETDATE())) GROUP BY TBL2.YOM";
            DataTable dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        public DataTable getStatisticsByYOR(int userid, string selectedCustNum)
        {
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            sqlStr += "SELECT SUM(CAST(TBL2.[RETURN] AS FLOAT)) AS 'PPM',TBL2.YOR AS 'YEAR' FROM ";
            sqlStr += "(SELECT COUNT(TBL1.CPNSER) AS 'RETURN',TBL1.YOR,TBL1.CPIDEN FROM ";
            sqlStr += "(SELECT CPNSER,YOR,CPIDEN FROM CUSTQCLMD ";
            sqlStr += "WHERE (1=1) ";

            if (selectedCustNum != "-- ALL --")
                sqlStr += "AND CPNCLI = " + selectedCustNum + " ";
            else
                sqlStr += "AND CPNCLI IN (SELECT DISTINCT CUSTOMERNUMBER FROM CUSTOMER WHERE CUSTOMERID IN (SELECT CUSTOMERID FROM USERMAPPING WHERE USERID = " + userid + ")) ";

            sqlStr += "AND CPRJCT IN ('LR','FR') ";
            sqlStr += "AND CPWARR IN (1,2,5,8)) AS TBL1 ";
            sqlStr += "GROUP BY TBL1.YOR,TBL1.CPIDEN) AS TBL2 ";
            sqlStr += "WHERE TBL2.YOR IN (SELECT DISTINCT YOR FROM CUSTQCLMD WHERE YOR BETWEEN ";
            sqlStr += "(CONVERT(INT,YEAR(GETDATE())) - 5) AND YEAR(GETDATE())) GROUP BY TBL2.YOR";
            DataTable dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        public DataTable getStatisticsByModel(int userid, string selectedCustNum)
        {
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            sqlStr += "SELECT COUNT(TBL1.CPIDEN) AS 'PPM',TBL1.YOM AS 'YEAR' FROM ";
            sqlStr += "(SELECT YOM,CPIDEN FROM CUSTQCLMD ";
            sqlStr += "WHERE (1=1) ";

            if (selectedCustNum != "-- ALL --")
                sqlStr += "AND CPNCLI = " + selectedCustNum + " ";
            else
                sqlStr += "AND CPNCLI IN (SELECT DISTINCT CUSTOMERNUMBER FROM CUSTOMER WHERE CUSTOMERID IN (SELECT CUSTOMERID FROM USERMAPPING WHERE USERID = " + userid + ")) ";

            sqlStr += "AND CPRJCT IN ('LR','FR')  ";
            sqlStr += "AND CPWARR IN (1,2,5,8)) AS TBL1 ";
            sqlStr += "WHERE TBL1.YOM IN (SELECT DISTINCT YOM FROM CUSTQCLMD WHERE YOM BETWEEN  ";
            sqlStr += "(CONVERT(INT,YEAR(GETDATE())) - 5) AND YEAR(GETDATE())) ";
            sqlStr += "GROUP BY TBL1.YOM";
            DataTable dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        public DataTable getModelByPCNNumber(string _pcnNumber)
        {
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            sqlStr += "SELECT DISTINCT PCNNumber, CustomerReturnsID, Description1, Description2, ProductType, ItemType, Line FROM CustomerReturns WHERE PCNNumber = '" + _pcnNumber + "' ORDER BY PCNNumber";
            DataTable dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        public DataTable getModelByPCNNumberSearch(string _pcnNumber)
        {
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            _pcnNumber = (_pcnNumber != "") ? " WHERE PCNNumber LIKE '" + _pcnNumber + "%' " : "";
            sqlStr += "SELECT DISTINCT TOP 10 PCNNumber, CustomerReturnsID, PCNNumber + ' - ' + Description1 Description1, Description2, ProductType, ItemType, Line FROM CustomerReturns " + _pcnNumber + " ORDER BY PCNNumber";
            DataTable dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        public DataTable getModelByPCNNumber()
        {
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            sqlStr += "SELECT DISTINCT PCNNumber,  Description1 FROM CustomerReturns ORDER BY Description1";
            DataTable dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        public DataTable getCustomerList(int userid)
        {
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            sqlStr += "SELECT DISTINCT CUSTOMERNUMBER,CUSTOMERNAME,CONVERT(VARCHAR,CUSTOMERNUMBER) + ' - ' + CUSTOMERNAME AS 'CUSTOMERNAMENUMBER' FROM CUSTOMER ";
            sqlStr += "WHERE CUSTOMERID IN (SELECT CUSTOMERID FROM USERMAPPING WHERE USERID = " + userid + " )  ";
            sqlStr += "and Control=1 ORDER BY CUSTOMERNAME ";
            DataTable dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        public DataTable getAllCustomerListControl(string userType, int userId)
        {
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";

            if (userType == "CSO")
            {
                sqlStr += "SELECT DISTINCT cus.CUSTOMERNUMBER,cus.CUSTOMERNAME,CONVERT(VARCHAR,cus.CUSTOMERNUMBER) + ' - ' + cus.CUSTOMERNAME AS 'CUSTOMERNAMENUMBER' FROM CUSTOMER cus ";
                sqlStr += "INNER JOIN SalesOfficeCode  soc ON CONVERT(varchar,soc.SalesPersonNumber) = cus.SalesOfficeCOde  ";
                sqlStr += "INNER JOIN UserInformations ui  ON ui.SOCID = soc.SOCID ";
                sqlStr += "WHERE ui.UserId =" + userId;
                sqlStr += "  AND Control=1 ORDER BY cus.CUSTOMERNAME";
            }
            
            else 
            {
                sqlStr += "SELECT DISTINCT CUSTOMERNUMBER,CUSTOMERNAME,CONVERT(VARCHAR,CUSTOMERNUMBER) + ' - ' + CUSTOMERNAME AS 'CUSTOMERNAMENUMBER' FROM CUSTOMER ";
                sqlStr += "WHERE ";
                sqlStr += " Control=1 ORDER BY CUSTOMERNAME";
            }
            
            DataTable dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }


        public DataTable getCustomerList()
        {
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            sqlStr += "SELECT DISTINCT CUSTOMERID,CUSTOMERNAME, CONVERT(VARCHAR,CUSTOMERNUMBER) + ' - ' + CUSTOMERNAME AS 'CUSTOMERNAMENUMBER' FROM CUSTOMER ORDER BY CUSTOMERNAME";
            DataTable dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        public DataTable getHeaderByHeaderID(int _id)
        {
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            sqlStr += "SELECT * from [ReturnFormHeader] where [RFHeaderID]=" + _id;
            DataTable dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        public string getSpecificCustomerName(string customerNumber)
        {
            Utility u = new Utility();
            ConnectionMaster cm = new ConnectionMaster();
            string customerName = "";
            string sqlStr = "";
            sqlStr += "SELECT CUSTOMERNAME FROM CUSTOMER WHERE CUSTOMERNUMBER = " + u.convertToSQLString(customerNumber, Utility.DataType.STRING);
            DataTable dt = cm.retrieveDataTable(sqlStr);
            foreach (DataRow dr in dt.Rows)
            {
                customerName = u.extractStringValue(dr["CUSTOMERNAME"], Utility.DataType.STRING);
            }
            return customerName;
        }

        //public DataTable getSavedReturnFormHeader(int count)
        public DataTable getSavedReturnFormHeader()
        {
            ConnectionMaster cm = new ConnectionMaster();
            DataTable dt = new DataTable();
            string sqlStr = "";
            //sqlStr += "SELECT RFHeaderID,ReferenceNumber FROM (SELECT TOP " + count + " RFHeaderID,ReferenceNumber FROM ReturnFormHeader ORDER BY RFHeaderID DESC) a ORDER BY a.RFHeaderID";
            sqlStr += "SELECT TOP 1 RFHeaderID,ReferenceNumber FROM ReturnFormHeader ORDER BY RFHeaderID DESC ";
            dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        //public DataTable getSavedReturnFormHeader(int count)
        public DataTable getSavedReturnFormHeaderByID(int _id)
        {
            ConnectionMaster cm = new ConnectionMaster();
            DataTable dt = new DataTable();
            string sqlStr = "";
            //sqlStr += "SELECT RFHeaderID,ReferenceNumber FROM (SELECT TOP " + count + " RFHeaderID,ReferenceNumber FROM ReturnFormHeader ORDER BY RFHeaderID DESC) a ORDER BY a.RFHeaderID";
            sqlStr += "SELECT TOP 1 RFHeaderID,ReferenceNumber FROM ReturnFormHeader where RFHeaderID=" + _id  + " ORDER BY RFHeaderID DESC ";
            dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        //public DataTable getContactPerson(int count)
        public DataTable getContactPerson()
        {
            ConnectionMaster cm = new ConnectionMaster();
            DataTable dt = new DataTable();
            string sqlStr = "";
            //sqlStr += "SELECT DISTINCT Name,EmailAddress,PhoneNumber FROM ContactPerson WHERE RFHeaderID IN (SELECT TOP " + count + " RFHeaderID FROM ReturnFormHeader ORDER BY RFHeaderID DESC)";
            sqlStr += "SELECT DISTINCT Name,EmailAddress,PhoneNumber FROM ContactPerson WHERE RFHeaderID IN (SELECT TOP 1 RFHeaderID FROM ReturnFormHeader ORDER BY RFHeaderID DESC)";
            dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        public Model.ContactPerson addContactPerson(Model.ContactPerson cp)
        {
            ConnectionMaster cm = new ConnectionMaster();
            Utility u = new Utility();
            string sqlStr = "";

            sqlStr += " INSERT INTO ContactPerson (RFHeaderID, Name, EmailAddress, PhoneNumber) VALUES ( ";
            sqlStr += " " + u.convertToSQLString(cp.RFHeaderID.ToString(), Utility.DataType.PRIMARYKEY) + ", ";
            sqlStr += " " + u.convertToSQLString(cp.Name, Utility.DataType.STRING) + " , ";
            sqlStr += " " + u.convertToSQLString(cp.EmailAddress, Utility.DataType.STRING) + " , ";
            sqlStr += " " + u.convertToSQLString(cp.PhoneNumber, Utility.DataType.STRING) + " ) ";

            cm.executeCommand(sqlStr, false);
            return cp;
        }

        public string getContactPersonsEmailByHeaderID(int _id)
        {
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            sqlStr += "SELECT * from ContactPerson where RFHeaderID = " + _id;

            string result = "";
            DataTable dt = cm.retrieveDataTable(sqlStr);
            foreach (DataRow dr in dt.Rows)
            {
                Model.ContactPerson contactPerson = new Model.ContactPerson();
                Utility u = new Utility();
                contactPerson.Name = u.extractStringValue(dr["Name"], Utility.DataType.STRING);
                contactPerson.EmailAddress = u.extractStringValue(dr["EmailAddress"], Utility.DataType.STRING);
                contactPerson.PhoneNumber = u.extractStringValue(dr["PhoneNumber"], Utility.DataType.STRING);

                result += contactPerson.EmailAddress + ",";
            }
            char[] charsToTrim = { ',', '.', ' ' };
            return result.TrimEnd(charsToTrim).ToString();
        }

        public DataTable getSavedReturnFormControl(int rfheaderid)
        {
            ConnectionMaster cm = new ConnectionMaster();
            DataTable dt = new DataTable();
            string sqlStr = "";
            sqlStr += "SELECT h.ReferenceNumber,d.PCNNumber,d.Model,d.Claim FROM ReturnFormControl d ";
            sqlStr += "INNER JOIN ReturnFormHeader h ON (d.RFHeaderID = h.RFHeaderID) ";
            sqlStr += "WHERE d.RFHeaderID = " + rfheaderid;
            dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        public void sendEmailToUser(string userID, int type, string portalReference)
        {
            MailMaster mailer = new MailMaster();
            UserInformation userdao = new UserInformation();
            Model.UserInformation userinfo = userdao.GetUserInfo(userID);

            string msg = string.Empty;
            msg += " Dear {0},";
            msg += " \nYour compressor(s) registered under portal reference {1} has (have)";
            switch (type)
            {
                case 1:
                    msg += " been received.";
                    break;
                case 2:
                    msg += " been analyzed.";
                    break;
                case 3:
                    msg += " received a credit decision.";
                    break;
            }

            msg += "\nThis is an automated email. Please do not respond.";
            msg = string.Format(msg, userinfo.UserFullName, portalReference);
            mailer.sendMail(userinfo.EmailAddress, msg, "Customer Quality Portal");
        }

     


        public string getPortalReference(int _customerNumber)
        {
            ConnectionMaster cm = new ConnectionMaster();
            DataTable dt = new DataTable();
            string sqlStr = " ";
            sqlStr += "SELECT ";
            sqlStr += "CONVERT(NVARCHAR(50),YEAR(getdate()),101) ";
            sqlStr += "+ SUBSTRING(CONVERT(NVARCHAR(50),CustomerName),1,1) ";
            sqlStr += "+ ";
            sqlStr += "CASE  (SELECT LEN(CONVERT(NVARCHAR(50),MAX(RFDetailID))) FROM ReturnFormControl) ";
            sqlStr += "WHEN (1) ";
            sqlStr += "THEN  (SELECT '0000'+CONVERT(NVARCHAR(50),MAX(RFDetailID)) FROM ReturnFormControl) ";
            sqlStr += "WHEN (2) ";
            sqlStr += "THEN  (SELECT '000'+CONVERT(NVARCHAR(50),MAX(RFDetailID)) FROM ReturnFormControl) ";
            sqlStr += "WHEN (3) ";
            sqlStr += "THEN  (SELECT '00'+CONVERT(NVARCHAR(50),MAX(RFDetailID)) FROM ReturnFormControl) ";
            sqlStr += "WHEN (4) ";
            sqlStr += "THEN  (SELECT '0'+CONVERT(NVARCHAR(50),MAX(RFDetailID)) FROM ReturnFormControl) ";
            sqlStr += "WHEN (5) ";
            sqlStr += "THEN  (SELECT CONVERT(NVARCHAR(50),MAX(RFDetailID)) FROM ReturnFormControl) ";
            sqlStr += "END ";
            sqlStr += "FROM customer ";
            sqlStr += "WHERE CustomerNumber = " + _customerNumber;

            dt = cm.retrieveDataTable(sqlStr);
            DataRow dr = dt.Rows[0];
            return dr[0].ToString();
        }

        public ReportBook printFormControl(string _controlID, Boolean isNew = false)
        {
            var xmlSerializer = new Telerik.Reporting.XmlSerialization.ReportXmlSerializer();
            var server = HttpContext.Current.Server;
            var reportDocument = (Telerik.Reporting.Report)xmlSerializer.Deserialize(server.MapPath("~/resources/reports/EuropeCustomerQualityPortal.trdx"));
            
            ArrayList partsReferencesCollection = new ArrayList();

            // Logic Ends here : datasource
            Model.ReturnFormControl returnFormDetailControl = new Model.ReturnFormControl();
            ReturnFormControl returnFormControl = new ReturnFormControl();
            returnFormDetailControl = returnFormControl.getReturnFormControl(_controlID);
            Model.ReturnFormHeader rfh = new Model.ReturnFormHeader();

            // COMMENT TO SELECTED SPECIFIC PORTAL REFERNCE NUMBER
            //returnFormDetailControl.PortalReference = returnFormControl.getSavedReturnFormHeaderByID(returnFormDetailControl.RFHeaderID).Rows[0]["ReferenceNumber"].ToString();

            reportDocument.DataSource = returnFormDetailControl;
            ReportBook reportBook = new ReportBook();
            // On New control created, not add this report. 
            if(!isNew)
                reportBook.Reports.Add(reportDocument);
            //foreach (Model.ReturnFormControl returnFormDetailControlItem in returnFormControl.getReturnFormControlArray(returnFormDetailControl.RFHeaderID))

            var reportDocument2 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(server.MapPath("~/resources/reports/printControl" + ((isNew) ? " - on new" : "") + ".trdx"));
            reportDocument2.DataSource = returnFormControl.getReturnFormControlArray(returnFormDetailControl.RFHeaderID, ((isNew) ? 0 : returnFormDetailControl.RFDetailID));
            reportBook.Reports.Add(reportDocument2);

            //foreach (Model.ReturnFormControl returnFormDetailControlItem in returnFormControl.getReturnFormControlArray(returnFormDetailControl.RFHeaderID, ((isNew) ? 0 : returnFormDetailControl.RFDetailID)))
            //{
            //    var reportDocument2 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(server.MapPath("~/resources/reports/printControl" + ((isNew) ? " - on new" : "") + ".trdx"));
            //    reportDocument2.DataSource = returnFormDetailControlItem;
            //    reportBook.Reports.Add(reportDocument2);
            //}
            return reportBook;
        }

        public bool ftpTransfer(MemoryStream ms, string _filename)
        {
            try
            {
                string sysmode = ConfigurationManager.AppSettings["SystemMode"].ToString();
                string strmode = (sysmode == "TEST") ? "_TEST" : "";
                string login = ConfigurationManager.AppSettings["ftpUserIP" + strmode].ToString();
                string pass = ConfigurationManager.AppSettings["ftpPassIP" + strmode].ToString();
                string address = ConfigurationManager.AppSettings["ftpIP" + strmode].ToString();

                string ftpPath = "ftp:" + address + _filename;
                ftpPath = ftpPath.Replace("\\", "/");

                System.Net.WebRequest request = (System.Net.WebRequest)WebRequest.Create(ftpPath);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(login, pass);

                //request.UseBinary = true;
                byte[] buffer = new byte[ms.Length];
                ms.Read(buffer, 0, buffer.Length);
                ms.Close();

                Stream requestStream = request.GetRequestStream();
                requestStream.Write(buffer, 0, buffer.Length);
                requestStream.Close();
                
                return true;
            }
            catch (Exception )
            {
                throw;

            }
            
        }

        public bool ftpTransferPDF(MemoryStream ms, string _filename)
        {
            try
            {
                string sysmode = ConfigurationManager.AppSettings["SystemMode"].ToString();
                string strmode = (sysmode == "TEST") ? "_TEST" : "";
                string login = ConfigurationManager.AppSettings["ftpUserPDF" + strmode].ToString();
                string pass = ConfigurationManager.AppSettings["ftpPassPDF" + strmode].ToString();
                string address = ConfigurationManager.AppSettings["ftpPDF" + strmode].ToString();
                string ftpPath = "ftp:" + address + _filename;
                ftpPath = ftpPath.Replace("\\", "/");
                System.Net.WebRequest request = (System.Net.WebRequest)WebRequest.Create(ftpPath);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(login, pass);

                //request.UseBinary = true;
                byte[] buffer = new byte[ms.Length];
                ms.Read(buffer, 0, buffer.Length);
                ms.Close();

                Stream requestStream = request.GetRequestStream();
                requestStream.Write(buffer, 0, buffer.Length);
                requestStream.Close();

                return true;
            }
            catch (Exception)
            {
                throw;

            }
            //return true;
        }

        public void generateXml(string _xml, string _fileName)
        {
            XmlDocument xm = new XmlDocument();
            MemoryStream xmlStream = new MemoryStream();
            xm.LoadXml(_xml);
            xm.Save(xmlStream);
            xmlStream.Flush();
            xmlStream.Position = 0;
             try
             {
                string sysmode = ConfigurationManager.AppSettings["SystemMode"].ToString();
                string strmode = (sysmode == "TEST") ? "_TEST" : "";
                string login = ConfigurationManager.AppSettings["ftpUserXML"+ strmode].ToString();
                string pass = ConfigurationManager.AppSettings["ftpPassXML" + strmode].ToString();
                string address = ConfigurationManager.AppSettings["ftpXML" + strmode].ToString();
                string ftpPath = "ftp:" + address + _fileName;
                ftpPath = ftpPath.Replace("\\", "/");
                System.Net.WebRequest request = (System.Net.WebRequest)WebRequest.Create(ftpPath);
              
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(login, pass);

                //request.UseBinary = true;
                byte[] buffer = new byte[xmlStream.Length];
                xmlStream.Read(buffer, 0, buffer.Length);
               

                Stream requestStream = request.GetRequestStream();

                requestStream.Write(buffer, 0, buffer.Length);
                requestStream.Close();
                xmlStream.Close();
            }
            catch (Exception)
            {
                throw;

            }
        }

        public void generateXmlOnbase(string _xml, string _fileName)
        {
            XmlDocument xm = new XmlDocument();
            MemoryStream xmlStream = new MemoryStream();
            xm.LoadXml(_xml);
            xm.Save(xmlStream);
            xmlStream.Flush();
            xmlStream.Position = 0;
            try
            {
                string sysmode = ConfigurationManager.AppSettings["SystemMode"].ToString();
                string strmode = (sysmode == "TEST") ? "_TEST" : "";
                string login = ConfigurationManager.AppSettings["ftpUserXMLOnBase" + strmode].ToString();
                string pass = ConfigurationManager.AppSettings["ftpPassXMLOnBase" + strmode].ToString();
                string address = ConfigurationManager.AppSettings["ftpXMLOnBase" + strmode].ToString();
                string ftpPath = "ftp:" + address + _fileName;
                ftpPath = ftpPath.Replace("\\", "/");
                System.Net.WebRequest request = (System.Net.WebRequest)WebRequest.Create(ftpPath);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(login, pass);

                //request.UseBinary = true;
                byte[] buffer = new byte[xmlStream.Length];
                xmlStream.Read(buffer, 0, buffer.Length);
               
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(buffer, 0, buffer.Length);
                requestStream.Close();
                xmlStream.Close();

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
    }
}