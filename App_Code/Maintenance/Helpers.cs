﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Maintenance;
using General;

/// <summary>
/// Summary description for Helpers
/// Authory by: Mark Friaz
/// 
/// </summary>
public class Helpers
{
	public Helpers() { }
    // Menu navaigation with trigger what the user access depends on the user type
    public static string menuNavi(int sel = -1){
        string menu = "";
        General.AvianCrypto.SecurityMaster sm = new General.AvianCrypto.SecurityMaster();

        // -- get from login.aspx this session
        string usertype = sm.Decrypt(HttpContext.Current.Session["UserType"].ToString());  
        DataTable dt = (DataTable)HttpContext.Current.Session["UserAccess"]; 
        DataRow row = dt.Rows[0];

        menu +="<div id=\"holderLeftNav\">";
        menu +="    <table>";   
        menu +="        <tr>";
        menu += "            <td><a href=\"home.aspx\" " + selected(sel, 1) + ">Portal Home</td>";
        menu +="        </tr>";
       
        if ((Boolean)row["AutorizationForm"]) {
            menu += "        <tr>";
            menu += "            <td><a href=\"ReturnFormCompressor.aspx\" " + selected(sel, 2) + ">Return Authorization Form</a></td>";
            menu += "        </tr>";

        }

        if ((Boolean)row["MyReturnsMaintain"] || (Boolean)row["MyReturnsView"])
        {
            menu += "        <tr>";
            menu += "            <td><a href=\"myreturn.aspx\" " + selected(sel, 3) + ">My Returns</a></td>";
            menu += "        </tr>";
        }


        menu +="        <tr>";
        menu += "            <td><a href=\"changepassword.aspx\" " + selected(sel, 4) + ">Change Password</a></td>";
        menu +="        </tr>";
        
        menu +="        <tr>";
        menu +="            <td><a href=\"logout.aspx\">Logout</a></td>";
        menu +="        </tr>";
        menu +="    </table>";
        menu +="    <br />";


        if ((Boolean)row["Customer"] || (Boolean)row["UserMaintain"])
        {
            menu += "    <asp:Panel ID=\"pnlMaintenance\" runat=\"server\">";
            menu += "        <table>";
            menu += "            <tr>";
            menu += "                <th><a style=\"color:Blue\">Maintenance</a><br /></th>";
            menu += "            </tr>";

            if ((Boolean)row["Customer"])
            {
                menu += "            <tr>";
                menu += "                <td><a href=\"customers.aspx\" " + selected(sel, 5) + ">Customers</a></td>";
                menu += "            </tr>";
            }
           
            if ((Boolean)row["UserMaintain"])
            {
                menu += "            <tr>";
                menu += "                <td><a href=\"usermanagement.aspx\" " + selected(sel, 6) + ">User Management</a></td>";
                menu += "            </tr>";
            }
            
            if (usertype == "CUSTOMER QUALITY" || usertype == "IT ADMIN") 
            {
                menu += "            <tr>";
                menu += "                <td><a href=\"productlocation.aspx\" " + selected(sel, 7) + ">Product Location</a></td>";
                menu += "            </tr>";
            }
            


            menu += "        </table>";
            menu += "    </asp:Panel>";
        }
        menu +="</div>";
        return menu;
    }

   

    public static string selected(int sel, int cur){
        return (sel == cur) ? "style=\"color:#f7941d;\"" : "";
    }

    public static string getProductLocation(int i) {
        ConnectionMaster cm = new ConnectionMaster();
        DataRow row = cm.retrieveDataTable("SELECT * FROM ProductType WHERE ID = " + i.ToString()).Rows[0];
        string location = row["Location"].ToString();    
        location = location.Replace("\n","<br/>");
        return location;
    }

}