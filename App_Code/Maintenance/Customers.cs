﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using General;
using System.Data;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for Customers
/// </summary>
/// 
namespace Maintenance
{
    public class Customers
    {
        public ArrayList getCustomers(string userType, int userId) // Added parameters for CSO users 5/8/2017
        {
            ArrayList customersList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            String sql = "";
            Model.Customers ui = new Model.Customers();

            sql += " SELECT c.CustomerID,";
		    sql += " c.CustomerNumber,";
		    sql += " c.CustomerName,";
            sql += " c.SalesOfficeCode, ";
            sql += " loc.LocationID, ";
            sql += " (City + ', ' + State + ', ' + ZipCode + ', ' + Country) Address,";
	 
            sql += " c.Created , ";
            sql += " c.Modified , ";
            sql += " Control , ";
            sql += " Compressor,  ";
            sql += " Accessories  ";

            sql += " FROM";
            sql += " [dbo].[customer] c";
            sql += " INNER JOIN CustomerLocation loc";
            sql += " ON c.CustomerNumber = loc.CustomerNumber and c.LocationID = loc.LocationID";

            //[START] [jpbersonda] [5/8/17]
            if (userType == "CSO")
            { 
                sql += " INNER JOIN SalesOfficeCode  soc ON CONVERT(varchar,soc.SalesPersonNumber) = c.SalesOfficeCOde ";
                sql += " INNER JOIN UserInformations ui  ON ui.SOCID = soc.SOCID WHERE ui.UserID = ";
                sql += userId + " ; ";
            }
            //[END] [jpbersonda] [5/8/17]

            DataTable dt = cm.retrieveDataTable(sql);


            foreach (DataRow dr in dt.Rows)
            {
                Model.Customers customers = new Model.Customers();
                customers.CustomerID = int.Parse(dtu.extractStringValue(dr["CustomerID"], DataTransformerUtility.DataType.PRIMARYKEY));
                customers.CustomerNumber = dtu.extractStringValue(dr["CustomerNumber"], DataTransformerUtility.DataType.STRING);
                customers.LocationID = dtu.extractStringValue(dr["LocationID"], DataTransformerUtility.DataType.STRING);
                customers.CustomerName = dtu.extractStringValue(dr["CustomerName"], DataTransformerUtility.DataType.STRING);
                customers.SalesOfficeCode = dtu.extractStringValue(dr["SalesOfficeCode"], DataTransformerUtility.DataType.STRING);
                customers.Created = dtu.extractStringValue(dr["Created"], DataTransformerUtility.DataType.STRING);
                customers.Modified = dtu.extractStringValue(dr["Modified"], DataTransformerUtility.DataType.STRING);
                customers.Address = dtu.extractStringValue(dr["Address"], DataTransformerUtility.DataType.STRING);
                customers.Control = bool.Parse(dtu.extractStringValue(dr["Control"], DataTransformerUtility.DataType.BOOLEAN));
                customers.Compressor = bool.Parse(dtu.extractStringValue(dr["Compressor"], DataTransformerUtility.DataType.BOOLEAN));
                customers.Accessories = bool.Parse(dtu.extractStringValue(dr["Accessories"], DataTransformerUtility.DataType.BOOLEAN));
                customersList.Add(customers);
            }

            return customersList;
        }

        public ArrayList getCustomers(int _customerID, string locationID = "0")
        {
            ArrayList customersList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            String sql = "";
            Model.Customers ui = new Model.Customers();

            //sql += " SELECT ";
            //sql += "    CustomerID , ";
            //sql += " CustomerNumber, ";
            //sql += " CustomerName, ";
            //sql += "    SalesOfficeCode , ";
            //sql += "    Created , ";
            //sql += "    Modified , ";
            //sql += "    Control , ";
            //sql += "    Compressor,  ";
            //sql += "    CustomerEmail  ";
            //sql += " FROM ";
            //sql += "    Customer ";
            sql += " SELECT c.CustomerID,";
            sql += " c.CustomerNumber,";
            sql += " c.CustomerName,";
            sql += " c.SalesOfficeCode, ";
            sql += " loc.LocationID, ";
            sql += " City , "; 
            sql += " State , ";
            sql += " ZipCode , ";
            sql += " Country , ";
            sql += " Created , ";
            sql += " Modified , ";
            sql += " Control , ";
            sql += " Accessories , ";
            sql += " Compressor, CustomerEmail  ";

            sql += " FROM";
            sql += " [dbo].[customer] c";
            sql += " INNER JOIN CustomerLocation loc";
            sql += " ON c.CustomerNumber = loc.CustomerNumber";
            sql += " WHERE ";
            sql += " c.CustomerID=" + _customerID;
            if(locationID != "0") 
                sql += " AND loc.LocationID='" + locationID + "'";

            DataTable dt = cm.retrieveDataTable(sql);


            foreach (DataRow dr in dt.Rows)
            {
                Model.Customers customers = new Model.Customers();
                customers.CustomerID = int.Parse(dtu.extractStringValue(dr["CustomerID"], DataTransformerUtility.DataType.PRIMARYKEY));
                customers.CustomerNumber = dtu.extractStringValue(dr["CustomerNumber"], DataTransformerUtility.DataType.STRING);
                customers.LocationID = dtu.extractStringValue(dr["LocationID"], DataTransformerUtility.DataType.STRING);
                customers.CustomerName = dtu.extractStringValue(dr["CustomerName"], DataTransformerUtility.DataType.STRING);
                customers.SalesOfficeCode = dtu.extractStringValue(dr["SalesOfficeCode"], DataTransformerUtility.DataType.STRING);
                customers.Created = dtu.extractStringValue(dr["Created"], DataTransformerUtility.DataType.STRING);
                customers.City = dtu.extractStringValue(dr["City"], DataTransformerUtility.DataType.STRING);
                customers.State = dtu.extractStringValue(dr["State"], DataTransformerUtility.DataType.STRING);
                customers.ZipCode = dtu.extractStringValue(dr["ZipCode"], DataTransformerUtility.DataType.STRING);
                customers.Country = dtu.extractStringValue(dr["Country"], DataTransformerUtility.DataType.STRING);
                customers.CustomerEmail = dtu.extractStringValue(dr["CustomerEmail"], DataTransformerUtility.DataType.STRING);
                customers.Modified = dtu.extractStringValue(dr["Modified"], DataTransformerUtility.DataType.STRING);
                customers.Control = bool.Parse(dtu.extractStringValue(dr["Control"], DataTransformerUtility.DataType.BOOLEAN));
                customers.Compressor = bool.Parse(dtu.extractStringValue(dr["Compressor"], DataTransformerUtility.DataType.BOOLEAN));
                customers.Accessories = bool.Parse(dtu.extractStringValue(dr["Accessories"], DataTransformerUtility.DataType.BOOLEAN));

                customersList.Add(customers);
            }

            return customersList;
        }



        public ArrayList getCustomersByCustomerNumber(string _custNumber)
        {
            ArrayList customersList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            String sql = "";
            Model.Customers ui = new Model.Customers();

            sql += " SELECT ";
            sql += "    CustomerID , ";
            sql += " CustomerNumber, ";
            sql += " CustomerName, ";
            sql += "    SalesOfficeCode , ";
            sql += "    Created , ";
            sql += "    Modified , ";
            sql += "    Control , ";
            sql += "    Compressor,  ";
            sql += "    CustomerEmail  ";
            sql += " FROM ";
            sql += "    Customer ";
            sql += " WHERE ";
            sql += " CUSTOMERNUMBER ='" + _custNumber + "'";
         

            DataTable dt = cm.retrieveDataTable(sql);


            foreach (DataRow dr in dt.Rows)
            {
                Model.Customers customers = new Model.Customers();
                customers.CustomerID = int.Parse(dtu.extractStringValue(dr["CustomerID"], DataTransformerUtility.DataType.PRIMARYKEY));
                customers.CustomerNumber = dtu.extractStringValue(dr["CustomerNumber"], DataTransformerUtility.DataType.STRING);
                customers.CustomerName = dtu.extractStringValue(dr["CustomerName"], DataTransformerUtility.DataType.STRING);
                customers.SalesOfficeCode = dtu.extractStringValue(dr["SalesOfficeCode"], DataTransformerUtility.DataType.STRING);
                customers.Created = dtu.extractStringValue(dr["Created"], DataTransformerUtility.DataType.STRING);
                customers.CustomerEmail = dtu.extractStringValue(dr["CustomerEmail"], DataTransformerUtility.DataType.STRING);
                customers.Modified = dtu.extractStringValue(dr["Modified"], DataTransformerUtility.DataType.STRING);
                customers.Control = bool.Parse(dtu.extractStringValue(dr["Control"], DataTransformerUtility.DataType.BOOLEAN));
                customers.Compressor = bool.Parse(dtu.extractStringValue(dr["Compressor"], DataTransformerUtility.DataType.BOOLEAN));

                customersList.Add(customers);
            }

            return customersList;
        }

        public ArrayList getCustomersByCustomerNumberLocation(int _custNumber, string _locationId)
        {
            ArrayList customersList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            String sql = "";
            Model.Customers ui = new Model.Customers();

            sql += " SELECT ";
            sql += "    CustomerID , ";
            sql += " l.CustomerNumber, ";
            sql += " CustomerName, ";
            sql += "    SalesOfficeCode , ";
            sql += "    Created , ";
            sql += "    Modified , ";
            sql += "    Control , ";
            sql += "    Compressor,  ";
            sql += "    CustomerEmail  ";
            sql += " FROM ";
            sql += "    Customer c ";
            sql += " INNER JOIN CustomerLocation l ";
            //sql += "    ON c.CustomerNumber = l.CustomerNumber ";
            sql += "    ON c.LocationID = l.LocationID ";
            sql += " WHERE ";
            sql += " l.CUSTOMERNUMBER =" + _custNumber;
            sql += " AND l.LocationID ='" + _locationId + "'";
           

            DataTable dt = cm.retrieveDataTable(sql);


            foreach (DataRow dr in dt.Rows)
            {
                Model.Customers customers = new Model.Customers();
                customers.CustomerID = int.Parse(dtu.extractStringValue(dr["CustomerID"], DataTransformerUtility.DataType.PRIMARYKEY));
                customers.CustomerNumber = dtu.extractStringValue(dr["CustomerNumber"], DataTransformerUtility.DataType.STRING);
                customers.CustomerName = dtu.extractStringValue(dr["CustomerName"], DataTransformerUtility.DataType.STRING);
                customers.SalesOfficeCode = dtu.extractStringValue(dr["SalesOfficeCode"], DataTransformerUtility.DataType.STRING);
                customers.Created = dtu.extractStringValue(dr["Created"], DataTransformerUtility.DataType.STRING);
                customers.CustomerEmail = dtu.extractStringValue(dr["CustomerEmail"], DataTransformerUtility.DataType.STRING);
                customers.Modified = dtu.extractStringValue(dr["Modified"], DataTransformerUtility.DataType.STRING);
                customers.Control = bool.Parse(dtu.extractStringValue(dr["Control"], DataTransformerUtility.DataType.BOOLEAN));
                customers.Compressor = bool.Parse(dtu.extractStringValue(dr["Compressor"], DataTransformerUtility.DataType.BOOLEAN));

                customersList.Add(customers);
            }

            return customersList;
        }

        public DataTable getCustomersByCustomerNumberDT(int _custNumber)
        {
            ArrayList customersList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            String sql = "";
            Model.Customers ui = new Model.Customers();

            sql += " SELECT ";
            sql += "    CustomerID , ";
            sql += " CustomerNumber, ";
            sql += " CustomerName, ";
            sql += "    SalesOfficeCode , ";
            sql += "    Created , ";
            sql += "    Modified , ";
            sql += "    Control , ";
            sql += "    Compressor,  ";
            sql += "    CustomerEmail  ";
            sql += " FROM ";
            sql += "    Customer ";
            sql += " WHERE ";
            sql += " CUSTOMERNUMBER =" + _custNumber;

            DataTable dt = cm.retrieveDataTable(sql);
            return dt;
             
        }


        public ArrayList getAllCustomerArray(int customerNumber) {
            ArrayList customersList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
        
            String sql = "";
            sql += " SELECT Top 10 CustomerNumber FROM customer WHERE CustomerNumber like '%" + customerNumber + "%'"; 
            
            DataTable dt = cm.retrieveDataTable(sql);

            foreach (DataRow dataRow in dt.Rows)
                customersList.Add(dataRow["CustomerNumber"].ToString());

            return customersList;
        }

        public string getAllCustomerNameByCustomerNumber(string customerNo)
        {
            ArrayList customersList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();

            String sql = "";
            sql += " SELECT CustomerName FROM customer WHERE CustomerNumber = '" + customerNo + "'";

            DataTable dt = cm.retrieveDataTable(sql);
            return dt.Rows[0]["CustomerName"].ToString(); 
             
        }


        public int deleteCustomerByID(int _customerID)
        {
            ArrayList customersList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            String sql = "";
            Model.Customers ui = new Model.Customers();

            sql += " DELETE FROM CUSTOMER ";
            sql += " WHERE ";
            sql += " CustomerID='" + _customerID + "'"; 

            cm.executeCommand(sql, false);

            return 1;
        }

        public DataTable getCSOList() {
            // [START] [EDIT] [jpbersonda] [5/8/2017]
            string sql = "SELECT SOCID CSOID, CSOShortCode + ' - ' + CodeDescription DESCRIPTION FROM SalesOfficeCode WHERE CSOShortCode IS NOT NULL AND CodeDescription IS NOT NULL";
            // [END] [EDIT] [jpbersonda] [5/8/2017]
            ConnectionMaster cm = new ConnectionMaster();
            return cm.retrieveDataTable(sql);
        }

        public DataTable getSalesOffices()
        {
            // [START] [EDIT] [jpbersonda] [5/8/2017]
            //string sql = "SELECT SALESPERSON_NO ID, NAME + ' - ' + CAST(SALESPERSON_NO AS VARCHAR) DESCRIPTION FROM SalesOfficeCode";
            string sql = "SELECT SalesPersonNumber ID, CSOShortCode + ' - ' + CAST(CodeDescription AS VARCHAR) DESCRIPTION FROM SalesOfficeCode WHERE SalesPersonNumber IS NOT NULL AND CSOShortCode IS NOT NULL";
            // [END] [EDIT] [jpbersonda] [5/8/2017]
            ConnectionMaster cm = new ConnectionMaster();
            return cm.retrieveDataTable(sql);
        }


        public ArrayList getCustomersContainsCustomerName(string _customerCode,string _customerName, string _userType, int _userId)
        {
            ArrayList customersList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            String sql = "";
            Model.Customers ui = new Model.Customers();

            sql += " SELECT DISTINCT c.CustomerID,";
            sql += " c.CustomerNumber,";
            sql += " c.CustomerName,";
            sql += " c.SalesOfficeCode, ";
            sql += " loc.LocationID, ";
            sql += " City , ";
            sql += " (City + ', ' + State + ', ' + ZipCode + ', ' + Country) Address,";
            sql += " State , ";
            sql += " ZipCode , ";
            sql += " Country , ";
            sql += " c.Created , ";
            sql += " c.Modified , ";
            sql += " Control , ";
            sql += " Accessories , ";
            sql += " Compressor, CustomerEmail  ";

            sql += " FROM";
            sql += " [dbo].[customer] c";
            sql += " INNER JOIN CustomerLocation loc";
            sql += " ON c.CustomerNumber = loc.CustomerNumber AND c.LocationID = loc.LocationID";

            //[START] [jpbersonda] [5/8/17]
            if (_userType == "CSO")
            {
                sql += " INNER JOIN SalesOfficeCode  soc ON CONVERT(varchar,soc.SalesPersonNumber) = c.SalesOfficeCOde ";
                sql += " INNER JOIN UserInformations ui  ON ui.SOCID = soc.SOCID";
            }
            //[END] [jpbersonda] [5/8/17]


            sql += " WHERE (1=1)";

            if (_userType == "CSO")
                sql += " AND  ui.UserID = " + _userId;

            if (_customerName != "")
                sql += " AND c.CustomerName LIKE '%" + _customerName + "%'";
            if (_customerCode != "")
                sql += " AND c.CustomerNumber LIKE '%" + _customerCode + "%'";
            DataTable dt = cm.retrieveDataTable(sql);


            foreach (DataRow dr in dt.Rows)
            {
                Model.Customers customers = new Model.Customers();
              
                customers.CustomerID = int.Parse(dtu.extractStringValue(dr["CustomerID"], DataTransformerUtility.DataType.PRIMARYKEY));
                customers.CustomerNumber = dtu.extractStringValue(dr["CustomerNumber"], DataTransformerUtility.DataType.STRING);
                customers.LocationID = dtu.extractStringValue(dr["LocationID"], DataTransformerUtility.DataType.STRING);
                customers.CustomerName = dtu.extractStringValue(dr["CustomerName"], DataTransformerUtility.DataType.STRING);
                customers.SalesOfficeCode = dtu.extractStringValue(dr["SalesOfficeCode"], DataTransformerUtility.DataType.STRING);
                customers.Created = dtu.extractStringValue(dr["Created"], DataTransformerUtility.DataType.STRING);
                customers.Address = dtu.extractStringValue(dr["Address"], DataTransformerUtility.DataType.STRING);
                customers.City = dtu.extractStringValue(dr["City"], DataTransformerUtility.DataType.STRING);
                customers.State = dtu.extractStringValue(dr["State"], DataTransformerUtility.DataType.STRING);
                customers.ZipCode = dtu.extractStringValue(dr["ZipCode"], DataTransformerUtility.DataType.STRING);
                customers.Country = dtu.extractStringValue(dr["Country"], DataTransformerUtility.DataType.STRING);
                customers.CustomerEmail = dtu.extractStringValue(dr["CustomerEmail"], DataTransformerUtility.DataType.STRING);
                customers.Modified = dtu.extractStringValue(dr["Modified"], DataTransformerUtility.DataType.STRING);
                customers.Control = bool.Parse(dtu.extractStringValue(dr["Control"], DataTransformerUtility.DataType.BOOLEAN));
                customers.Compressor = bool.Parse(dtu.extractStringValue(dr["Compressor"], DataTransformerUtility.DataType.BOOLEAN));
                customers.Accessories = bool.Parse(dtu.extractStringValue(dr["Accessories"], DataTransformerUtility.DataType.BOOLEAN));

                customersList.Add(customers);
            }

            return customersList;
        }

        public int addCustomers(Model.Customers customers)
        {
            ConnectionMaster cm = new ConnectionMaster();
            String sql = "";
            Model.Customers ui = new Model.Customers();

            sql += " INSERT INTO Customer values('";
            sql +=  customers.CustomerNumber + "', ";
            sql += "'" + customers.CustomerName + "', ";
            sql += "'" + customers.CustomerEmail + "', ";
            sql += "'" + customers.SalesOfficeCode + "' , ";
            sql += "'" + customers.Created + "' , ";
            sql += "'" + DateTime.Now + "' , ";
            sql += convertBoolToInt(customers.Control) + " , ";
            sql += convertBoolToInt(customers.Compressor) + ",  ";
            sql += convertBoolToInt(customers.Accessories) + ",  ";
            sql += " '', ";
            sql += " '"+ customers.LocationID +"')";

            return int.Parse(cm.executeCommand(sql, true).ToString()); ;
        }

        public int updateCustomers(Model.Customers customers)
        {
            ConnectionMaster cm = new ConnectionMaster();
            String sql = "";
            Model.Customers ui = new Model.Customers();

            sql += " UPDATE Customer SET ";
            sql += " CustomerNumber='" + customers.CustomerNumber+ "', ";
            sql += " CustomerName='" + customers.CustomerName + "', ";
            sql += "    SalesOfficeCode='" + customers.SalesOfficeCode + "' , ";
            sql += "    Created='" + customers.Created + "' , ";
            sql += "    Modified='" + DateTime.Now + "' , ";
            sql += "    Control=" + convertBoolToInt(customers.Control) + " , ";
            sql += "    Compressor=" + convertBoolToInt(customers.Compressor) + ",  ";
            sql += "    Accessories=" + convertBoolToInt(customers.Accessories) + ",  ";
            sql += "    CustomerEmail='" + customers.CustomerEmail + "',  ";
            sql += "    LocationID='" + customers.LocationID + "' ";
            sql += " WHERE ";
            sql += " CustomerID=" + customers.CustomerID;

             cm.retrieveDataTable(sql);

            return customers.CustomerID;
        }

        public int convertBoolToInt(bool _value)
        {
            if (_value == true)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public bool isCustomerLocationExists(string customer, string location) {
            ConnectionMaster cm = new ConnectionMaster();
            string sql = "SELECT * FROM CustomerLocation WHERE CustomerNumber = '" + customer + "' and LocationID='" + location + "'";

            return (cm.retrieveDataTable(sql).Rows.Count > 0) ? true : false;

        }
        

        public int countRecordWithCondition(string _table, string _field, string _value,int _currentID, string _fieldNameID)
        {
            ConnectionMaster cm = new ConnectionMaster();
            string query = "";
            query += " SELECT COUNT(*) FROM " + _table;
            query += " WHERE " + _field;
            query += " = '" + _value + "' ";
            if (_currentID != 0) {
                query +=" and "+ _fieldNameID + " <> " + _currentID + "";    
            }

            DataTable dt = cm.retrieveDataTable(query);
            DataRow dr = dt.Rows[0];
            int result = int.Parse(dr[0].ToString());
            return result;
        }

        //Customer Location
        public CustomerLocation getLocationByLocationID(long locationID)
        {
            ArrayList list = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            string _sql = "";
            _sql += "SELECT * FROM CustomerLocation WHERE LocationID = '@locationID'";

            _sql = _sql.Replace("@locationID", locationID.ToString());

            DataTable dt = cm.retrieveDataTable(_sql);
            CustomerLocation cust = new CustomerLocation();
            DataRow row = dt.Rows[0];

            cust.LocationID = dtu.extractStringValue(row["LocationID"], DataTransformerUtility.DataType.STRING);
            cust.CustomerNumber = dtu.extractStringValue(row["CustomerNumber"], DataTransformerUtility.DataType.STRING);
            cust.City = dtu.extractStringValue(row["City"], DataTransformerUtility.DataType.STRING);
            cust.State = dtu.extractStringValue(row["State"], DataTransformerUtility.DataType.STRING);
            cust.ZipCode = dtu.extractStringValue(row["ZipCode"], DataTransformerUtility.DataType.STRING);
            cust.Country = dtu.extractStringValue(row["Country"], DataTransformerUtility.DataType.STRING);
            
             
            return cust;
        }
        public ArrayList getLocationByCustomerNumber(long customerno)
        {
            ArrayList list = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            string _sql = "";
            _sql += "SELECT * FROM CustomerLocation WHERE CustomerNumber = '@customerno'";

            _sql = _sql.Replace("@customerno", customerno.ToString());

            DataTable dt = cm.retrieveDataTable(_sql);

            foreach (DataRow row in dt.Rows)
            {
                CustomerLocation cust = new CustomerLocation();
                cust.LocationID = dtu.extractStringValue(row["LocationID"], DataTransformerUtility.DataType.STRING);
                cust.CustomerNumber = dtu.extractStringValue(row["CustomerNumber"], DataTransformerUtility.DataType.STRING);
                cust.City = dtu.extractStringValue(row["City"], DataTransformerUtility.DataType.STRING);
                cust.State = dtu.extractStringValue(row["State"], DataTransformerUtility.DataType.STRING);
                cust.ZipCode = dtu.extractStringValue(row["ZipCode"], DataTransformerUtility.DataType.STRING);
                cust.Country = dtu.extractStringValue(row["Country"], DataTransformerUtility.DataType.STRING);
                list.Add(cust);
            }


            return list;
        }


        public DataTable getLocationByCustomerNumberDT(string customerno, int userId , string userType)
        {
            
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            //string _sql = "";
            //[START] [jpbersonda] [5/23/2017] [EDIT]
            //_sql += "SELECT LocationID, (CAST(LocationID AS VARCHAR) + ' - ' + ISNULL(City,'') + ', ' + ISNULL(State,'') + ', ' + ISNULL(ZipCode,'')) LocationName FROM CustomerLocation WHERE CustomerNumber = '@customerno'";

            //_sql += "SELECT CL.LocationID, (CAST(CL.LocationID AS VARCHAR) + ' - ' + ISNULL(City,'') + ', ' + ISNULL(State,'') + ', ' + ";
            //_sql += " ISNULL(ZipCode,'')) LocationName FROM CustomerLocation CL ";
            //_sql += " INNER JOIN Customer CUS ON CUS.LocationID = CL.LocationID ";
            //_sql += " INNER JOIN UserMapping UM ON UM.CustomerID = CUS.CustomerID ";
            //_sql += " WHERE CL.CustomerNumber = @customerno AND UM.UserId = @userId ";
            
            //_sql = _sql.Replace("@customerno", customerno.ToString());
            //_sql = _sql.Replace("@userId", userId.ToString());

            StringBuilder query = new StringBuilder();
            query.Append("SELECT CL.LocationID, (CAST(CL.LocationID AS VARCHAR) + ' - ' + ISNULL(City,'') + ', ' + ISNULL(State,'') + ', ' + ");
            query.Append(" ISNULL(ZipCode,'')) LocationName FROM CustomerLocation CL ");
            query.Append(" INNER JOIN Customer CUS ON CUS.LocationID = CL.LocationID ");

                        if (userType == "CUSTOMER")
                            query.Append(" INNER JOIN UserMapping UM ON UM.CustomerID = CUS.CustomerID ");

                        if (userType == "CSO")
                        {
                            query.Append(" INNER JOIN SalesOfficeCode  soc ON CONVERT(varchar,soc.SalesPersonNumber) = CUS.SalesOfficeCOde ");
                            query.Append(" INNER JOIN UserInformations ui  ON ui.SOCID = soc.SOCID ");
                        }

            query.Append(" WHERE CL.CustomerNumber =  '" + customerno + "' ");

                        if (userType == "CUSTOMER")
                            query.Append(" AND UM.UserId =   " + userId);

                        if(userType == "CSO")
                            query.Append(" AND ui.UserID =  " + userId);

            return cm.retrieveDataTable(query.ToString()); ;

            //[END] [jpbersonda] [5/23/2017] [EDIT]
        }

        public ArrayList getLocationByCustomerNumberList(long customerno)
        {

            ArrayList list = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            string _sql = "";
            _sql += "SELECT LocationID, (CAST(LocationID AS VARCHAR) + ' - ' + ISNULL(City,'') + ', ' + ISNULL(State,'') + ', ' + ISNULL(ZipCode,'')) LocationName FROM CustomerLocation WHERE CustomerNumber = '@customerno'";

            _sql = _sql.Replace("@customerno", customerno.ToString());

            DataTable dt = cm.retrieveDataTable(_sql);
            foreach (DataRow row in dt.Rows)
            {
                CustomerLocation loc = new CustomerLocation();
                loc.LocationID = row["LocationID"].ToString();
                loc.LocationName = row["LocationName"].ToString();
                list.Add(loc);
            }
            return list;
        }


        public Boolean addLocation(CustomerLocation custLoc)
        {
            ConnectionMaster cm = new ConnectionMaster();
            string _sqlString = "INSERT INTO CustomerLocation(LocationID,CustomerNumber,Address1,City,State,ZipCode,Country) VALUES(";

            _sqlString += " '@LocationID'";
            _sqlString += ",'@CustomerNumber'";
            _sqlString += ",'@Address1'";
            _sqlString += ",'@City'";
            _sqlString += ",'@State'";
            _sqlString += ",'@ZipCode'";
            _sqlString += ",'@Country')";


            _sqlString = _sqlString.Replace("@LocationID", custLoc.LocationID.ToString());
            _sqlString = _sqlString.Replace("@CustomerNumber", custLoc.CustomerNumber.ToString());
            _sqlString = _sqlString.Replace("@Address1", custLoc.Address.ToString());
            _sqlString = _sqlString.Replace("@City", custLoc.City.ToString());
            _sqlString = _sqlString.Replace("@State", custLoc.State.ToString());
            _sqlString = _sqlString.Replace("@ZipCode", custLoc.ZipCode.ToString());
            _sqlString = _sqlString.Replace("@Country", custLoc.Country.ToString());


            return (cm.executeCommand(_sqlString, false) > 0) ? true : false;  
 
        }

        public Boolean UpdateLocation(CustomerLocation custLoc)
        {
            ConnectionMaster cm = new ConnectionMaster();
            string _sqlString = "UPDATE CustomerLocation SET "; 

            _sqlString += " Address1 = '@Address1'";
            _sqlString += ",City = '@City'";
            _sqlString += ",State = '@State'";
            _sqlString += ",ZipCode = '@ZipCode'";
            _sqlString += ",LocationID = '@LocationID'";
            _sqlString += ",Country = '@Country' WHERE LocationID = '@LocationID'";


            _sqlString = _sqlString.Replace("@LocationID", custLoc.LocationID.ToString());
            _sqlString = _sqlString.Replace("@Address1", custLoc.Address.ToString());
            _sqlString = _sqlString.Replace("@City", custLoc.City.ToString());
            _sqlString = _sqlString.Replace("@State", custLoc.State.ToString());
            _sqlString = _sqlString.Replace("@ZipCode", custLoc.ZipCode.ToString());
            _sqlString = _sqlString.Replace("@Country", custLoc.Country.ToString());


            return (cm.executeCommand(_sqlString, false) > 0) ? true : false;

        }

        public Boolean DeleteLocation(string locationID, string CustomerNo)
        {
            ConnectionMaster cm = new ConnectionMaster();
            string _sqlString = "DELETE FROM CustomerLocation WHERE  CustomerNumber = '@CustomerNo' AND LocationID = '@LocationID'";
            _sqlString = _sqlString.Replace("@LocationID", locationID.ToString());
            _sqlString = _sqlString.Replace("@CustomerNo", CustomerNo.ToString()); 
            return (cm.executeCommand(_sqlString, false) > 0) ? true : false;

        }

        public Boolean checkCustomerExistsLocation(string CustomerNo, string locationid = "") 
        {
            ConnectionMaster cm = new ConnectionMaster();
            string _sqlString = "SELECT * FROM CustomerLocation WHERE  CustomerNumber = '@CustomerNo'";
        
            _sqlString = _sqlString.Replace("@CustomerNo", CustomerNo.ToString());
            return (cm.retrieveDataTable(_sqlString).Rows.Count > 0) ? true : false;
        }

        
        
    }

}