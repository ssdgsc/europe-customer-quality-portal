﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Collections;
using General.AvianCrypto;
/// <summary>
/// Summary description for UserInformation
/// </summary>
/// 
namespace Maintenance
{
    public class UserInformation
    {
        General.ConnectionMaster cm;
        General.AvianCrypto.SecurityMaster sm;
        General.Utility u;

        public UserInformation()
        {
            cm = new General.ConnectionMaster();
            sm = new General.AvianCrypto.SecurityMaster();
            u = new General.Utility();
        }


        public Model.UserInformation ChangePassword(Model.UserInformation data)
        {
            String sql = "";

                sql += " UPDATE UserInformations ";
                sql += " SET ";
                sql += "    UserPassword=" + u.convertToSQLString(sm.Encrypt(data.UserPassword), General.Utility.DataType.PASSWORD) + ",";
                sql += "    Modified=GETDATE() ";
                sql += " WHERE ";
                sql += "    UserID=" + u.convertToSQLString(data.UserID.ToString(), General.Utility.DataType.PRIMARYKEY);

                data.UserID = int.Parse(cm.executeCommand(sql, false).ToString());

            if (data.UserID > 0)
                return data;
            else
                throw new Exception("Cannot Process the request");
        }

        public Model.UserInformation CreateUpdateUserInformation(Model.UserInformation data)
        {
            String sql = "";

            if (data.UserID < 1)
            {
                sql += " INSERT INTO ";
                sql += " UserInformations ";
                sql += " ( ";
                //sql += " CustomerName, ";
                sql += " UserType, ";
                sql += " CompanyName, ";
                sql += " Username, ";
                sql += " UserFirstName, ";
                sql += " UserLastName, ";
                sql += " Department, ";
                sql += " [Function], ";
                sql += " PhoneNumber, ";
                sql += " EmailAddress, ";
                sql += " UserTitle, ";
                sql += " Login, ";
                sql += " UserPassword, ";
                sql += " SOCID, ";
                //sql += " BackupFirstName, ";
                //sql += " BackupEmailAddress, ";
                sql += " Created, ";
                sql += " Modified, ";
                sql += " IsActive,";
                //[START] [5/5/17] [jpbersonda] [ADD]
                sql += " ReceptionActivated, ";
                sql += " AnalysisActivated, ";
                sql += " CreditDecisionActivated ";   

                //[END] [5/5/17] [jpbersonda] [ADD]
                sql += " ) ";
                sql += " VALUES ";
                sql += " ( ";
               // sql += " " + u.convertToSQLString(data.CustomerName, General.Utility.DataType.STRING) + ", ";
                sql += " " + u.convertToSQLString(data.UserType, General.Utility.DataType.STRING) + ", ";
                sql += " " + u.convertToSQLString(data.CompanyName, General.Utility.DataType.STRING) + ", ";
                sql += " " + u.convertToSQLString(data.Username, General.Utility.DataType.STRING) + ", ";
                sql += " " + u.convertToSQLString(data.UserFirstName, General.Utility.DataType.STRING) + ", ";
                sql += " " + u.convertToSQLString(data.UserLastName, General.Utility.DataType.STRING) + ", ";
                sql += " " + u.convertToSQLString(data.Department, General.Utility.DataType.STRING) + ", ";
                sql += " " + u.convertToSQLString(data.Function, General.Utility.DataType.STRING) + ", ";
                sql += " " + u.convertToSQLString(data.PhoneNumber, General.Utility.DataType.STRING) + ", ";
                sql += " " + u.convertToSQLString(data.EmailAddress, General.Utility.DataType.STRING) + ", ";
                sql += " " + u.convertToSQLString(data.UserTitle, General.Utility.DataType.STRING) + ", ";
                sql += " " + u.convertToSQLString(data.Login, General.Utility.DataType.STRING) + ", ";
                sql += " " + u.convertToSQLString(sm.Encrypt(data.UserPassword), General.Utility.DataType.PASSWORD) + ",";
                sql += " " + u.convertToSQLString(data.CSO, General.Utility.DataType.STRING) + ", ";
                //sql += " " + u.convertToSQLString(data.BackupFirstName, General.Utility.DataType.STRING) + ", ";
                //sql += " " + u.convertToSQLString(data.BackupEmailAddress, General.Utility.DataType.STRING) + ", ";
                sql += " GETDATE() , ";
                sql += " GETDATE() , ";
                sql += " 1 ,";
                //[START] [5/5/17] [jpbersonda] [ADD]
                sql += " " + convertBoolToBit(data.ReceptionActivated) + ", ";
                sql += " " + convertBoolToBit(data.AnalysisActivated) + ", ";
                sql += " " + convertBoolToBit(data.CDActivated) + " ";
                sql += " ) ";
                //[END] [5/5/17] [jpbersonda] [ADD]

                data.UserID = int.Parse(cm.executeCommand(sql, true).ToString());
            }
            else
            {
                sql += " UPDATE UserInformations ";
                sql += " SET ";
                sql += " CustomerName=" + u.convertToSQLString(data.CustomerName, General.Utility.DataType.STRING) + ",";
                sql += " UserType=" + u.convertToSQLString(data.UserType, General.Utility.DataType.STRING) + ",";
                sql += " CompanyName=" + u.convertToSQLString(data.CompanyName, General.Utility.DataType.STRING) + ",";
                sql += " Username=" + u.convertToSQLString(data.Username, General.Utility.DataType.STRING) + ",";
                sql += " UserFirstName=" + u.convertToSQLString(data.UserFirstName, General.Utility.DataType.STRING) + ",";
                sql += " UserLastName=" + u.convertToSQLString(data.UserLastName, General.Utility.DataType.STRING) + ",";
                sql += " [Function] =" + u.convertToSQLString(data.Function, General.Utility.DataType.STRING) + ",";
                sql += " Department=" + u.convertToSQLString(data.Department, General.Utility.DataType.STRING) + ",";
                sql += " PhoneNumber=" + u.convertToSQLString(data.PhoneNumber, General.Utility.DataType.STRING) + ",";
                sql += " EmailAddress=" + u.convertToSQLString(data.EmailAddress, General.Utility.DataType.STRING) + ",";
                sql += " Login=" + u.convertToSQLString(data.Login, General.Utility.DataType.STRING) + ",";
                sql += " UserPassword=" + u.convertToSQLString(sm.Encrypt(data.UserPassword), General.Utility.DataType.PASSWORD) + ",";
                sql += " BackupName=" + u.convertToSQLString(data.BackupName, General.Utility.DataType.STRING) + ",";
                sql += " BackupFirstName=" + u.convertToSQLString(data.BackupFirstName, General.Utility.DataType.STRING) + ",";
                sql += " BackupEmailAddress=" + u.convertToSQLString(data.BackupEmailAddress, General.Utility.DataType.STRING) + ",";
                sql += " Modified=GETDATE() ";
                sql += " WHERE ";
                sql += " UserID=" + u.convertToSQLString(data.UserID.ToString(), General.Utility.DataType.PRIMARYKEY);

                data.UserID = int.Parse(cm.executeCommand(sql, false).ToString());
            }

            if (data.UserID > 0)
                return data;
            else
                throw new Exception("Cannot Process the request");
        }

        public void SaveUserCustomerMapping(int userid, int custid)
        {
            string sqlStr = "";
            sqlStr += "INSERT INTO USERMAPPING (USERID, CUSTOMERID, CREATED, MODIFIED) VALUES ( ";
            sqlStr += " " + userid + "," + custid + ",GETDATE(),GETDATE() )";
            cm.executeCommand(sqlStr, false);
        }

        public void clearUserCustomerMappingByUserID(int userid)
        {
            string sqlStr = "";
            sqlStr += "DELETE FROM USERMAPPING WHERE UserID = " + userid;
            cm.executeCommand(sqlStr, false);
            
        } 

        public  DataTable getUserCustomerMappingByUserID(int userid){
            string sqlStr = "";
            sqlStr += "SELECT CustomerID FROM USERMAPPING WHERE UserID = " + userid;
            DataTable dt =  cm.retrieveDataTable(sqlStr);
            return dt;  
        } 

        public Model.UserInformation ValidateLoginCredentials(string _username)
        {
            String sql = "";
            Model.UserInformation ui = new Model.UserInformation();

            sql += " SELECT ";
            sql += "    UserID , ";
            sql += " CustomerName, ";
            sql += " UserType, ";
            sql += "    CompanyName , ";
            sql += "    UserType , ";
            sql += "    Username , ";
            sql += "    UserPassword , ";
            sql += "    UserFirstName + ' ' + UserLastName 'UserFullName' , ";
            sql += "    PhoneNumber , ";
            sql += "    EmailAddress ";
            sql += "   ,ISNULL(IsFirstLogin, 'TRUE') IsFirstLogin";  
            sql += " FROM ";
            sql += "    UserInformations ";
            sql += " WHERE ";
            sql += "    Username=" + u.convertToSQLString(_username, General.Utility.DataType.STRING);
            sql += " and IsActive=1 ";
            DataTable dt = cm.retrieveDataTable(sql);

            if (dt.Rows.Count > 0)
            {
                ui.UserID = int.Parse(dt.Rows[0]["UserID"].ToString());
                ui.CustomerName = dt.Rows[0]["CustomerName"].ToString();
                ui.UserType = dt.Rows[0]["UserType"].ToString();
                ui.CompanyName = dt.Rows[0]["CompanyName"].ToString();  
                ui.Username = dt.Rows[0]["Username"].ToString();
                ui.UserPassword = dt.Rows[0]["UserPassword"].ToString();
                ui.UserFullName = dt.Rows[0]["UserFullName"].ToString();
                ui.PhoneNumber = dt.Rows[0]["PhoneNumber"].ToString();
                ui.EmailAddress = dt.Rows[0]["EmailAddress"].ToString();
                ui.IsFirstLogin = bool.Parse(dt.Rows[0]["IsFirstLogin"].ToString());

                ui.UserPassword = sm.Decrypt(ui.UserPassword);
            }

            return ui;
        }

        public DataTable getUserType(string usertype = "") {
            usertype = (usertype != "") ? " WHERE UPPER(UserType) = '" + usertype + "'" : "";
            return cm.retrieveDataTable("SELECT * FROM UserType " + usertype + " ORDER BY ID");
        }




        public Model.UserInformation GetUserInfo(string userID)
        {
            String sql = "";
            Model.UserInformation ui = new Model.UserInformation();

            sql += " SELECT ";
            sql += " UserID, ";
            sql += " CustomerName, ";
            sql += " UserType, ";
            sql += " SOCID, ";
            sql += " CompanyName, ";
            sql += " UserType, ";
            sql += " Username, ";
            sql += " UserFirstName, ";
            sql += " UserLastName, ";
            sql += " PhoneNumber, ";
            sql += " UserTitle, ";
            sql += " Department, ";
            sql += " [Function], ";
            sql += " EmailAddress, ";
            sql += " ISNULL(ReceptionActivated, 'false') ReceptionActivated, ";
            sql += " ISNULL(AnalysisActivated, 'false') AnalysisActivated, ";
            sql += " ISNULL(CreditDecisionActivated, 'false') CreditDecisionActivated ";
            sql += " FROM ";
            sql += "    UserInformations ";
            sql += " WHERE ";
            sql += "    UserID = " + u.convertToSQLString(userID, General.Utility.DataType.STRING);

            DataTable dt = cm.retrieveDataTable(sql);

            if (dt.Rows.Count > 0)
            {
                ui.UserID = int.Parse(dt.Rows[0]["UserID"].ToString());
                ui.CustomerName = dt.Rows[0]["CustomerName"].ToString();
                ui.UserType = dt.Rows[0]["UserType"].ToString();
                ui.CSO = dt.Rows[0]["SOCID"].ToString();
                ui.CompanyName = dt.Rows[0]["CompanyName"].ToString();
                ui.Username = dt.Rows[0]["Username"].ToString();
                ui.UserFirstName = dt.Rows[0]["UserFirstName"].ToString();
                ui.UserLastName = dt.Rows[0]["UserLastName"].ToString();
                ui.PhoneNumber = dt.Rows[0]["PhoneNumber"].ToString();
                ui.EmailAddress = dt.Rows[0]["EmailAddress"].ToString();
                ui.UserTitle = dt.Rows[0]["UserTitle"].ToString();
                ui.Department = dt.Rows[0]["Department"].ToString();
                ui.Function = dt.Rows[0]["Function"].ToString();
                ui.ReceptionActivated = bool.Parse(dt.Rows[0]["ReceptionActivated"].ToString());
                ui.AnalysisActivated = bool.Parse(dt.Rows[0]["AnalysisActivated"].ToString());
                ui.CDActivated = bool.Parse(dt.Rows[0]["CreditDecisionActivated"].ToString());
            }



            return ui;
        }

        public ArrayList getUsers(string usertype = "")
        {
            String sql = "";
            sql += " SELECT ";
            sql += " UserID, ";
            sql += " CustomerName, ";
            sql += " UserType, ";
            sql += " CompanyName, ";
            sql += " Username, ";
            sql += " UserFirstName, ";
            sql += " UserLastName, ";
            sql += " PhoneNumber, ";
            sql += " UserTitle, ";
            sql += " [Function], ";
            sql += " EmailAddress, ";
            sql += " ISNULL(ReceptionActivated, 'false') ReceptionActivated, ";
            sql += " ISNULL(AnalysisActivated, 'false') AnalysisActivated, ";
            sql += " ISNULL(CreditDecisionActivated, 'false') CreditDecisionActivated ";
            sql += " FROM ";
            sql += "    UserInformations Where IsActive=1";
            if (usertype != "")
                sql += " AND UserType = '" + usertype + "'";
            ArrayList result = new ArrayList();
            DataTable dt = cm.retrieveDataTable(sql);

            DataTransformerUtility dtu = new DataTransformerUtility();
            ArrayList arr = new ArrayList();

          	foreach (DataRow dr in dt.Rows) 
	        {
                Model.UserInformation userInformation = new Model.UserInformation();
		        userInformation.UserID = int.Parse(dtu.extractStringValue(dr["UserID"], DataTransformerUtility.DataType.PRIMARYKEY));
		        userInformation.CustomerName = dtu.extractStringValue(dr["CustomerName"], DataTransformerUtility.DataType.STRING);
		        userInformation.UserType = dtu.extractStringValue(dr["UserType"], DataTransformerUtility.DataType.STRING);
		        userInformation.CompanyName = dtu.extractStringValue(dr["CompanyName"], DataTransformerUtility.DataType.STRING);
		        userInformation.Username = dtu.extractStringValue(dr["Username"], DataTransformerUtility.DataType.STRING);
		        userInformation.UserFirstName = dtu.extractStringValue(dr["UserFirstName"], DataTransformerUtility.DataType.STRING);
		        userInformation.UserLastName = dtu.extractStringValue(dr["UserLastName"], DataTransformerUtility.DataType.STRING);
		        userInformation.PhoneNumber = dtu.extractStringValue(dr["PhoneNumber"], DataTransformerUtility.DataType.STRING);
		        userInformation.EmailAddress = dtu.extractStringValue(dr["EmailAddress"], DataTransformerUtility.DataType.STRING);
		        userInformation.UserTitle = dtu.extractStringValue(dr["UserTitle"], DataTransformerUtility.DataType.STRING);
		        userInformation.Function = dtu.extractStringValue(dr["Function"], DataTransformerUtility.DataType.STRING);
		        userInformation.ReceptionActivated = bool.Parse(dtu.extractStringValue(dr["ReceptionActivated"], DataTransformerUtility.DataType.BOOLEAN));
		        userInformation.AnalysisActivated = bool.Parse(dtu.extractStringValue(dr["AnalysisActivated"], DataTransformerUtility.DataType.BOOLEAN));
                userInformation.CDActivated = bool.Parse(dtu.extractStringValue(dr["CreditDecisionActivated"], DataTransformerUtility.DataType.BOOLEAN));
                arr.Add(userInformation);
	        }

            return arr;
        }


        public ArrayList getUsersByUserName(string _userName,string _userFirstName, string _userLastName)
        {
            String sql = "";
            sql += " SELECT ";
            sql += " UserID, ";
            sql += " CustomerName, ";
            sql += " UserType, ";
            sql += " CompanyName, ";
            sql += " Username, ";
            sql += " UserFirstName, ";
            sql += " UserLastName, ";
            sql += " PhoneNumber, ";
            sql += " UserTitle, ";
            sql += " [Function], ";
            sql += " EmailAddress, ";
            sql += " ISNULL(ReceptionActivated, 'false') ReceptionActivated, ";
            sql += " ISNULL(AnalysisActivated, 'false') AnalysisActivated, ";
            sql += " ISNULL(CreditDecisionActivated, 'false') CreditDecisionActivated ";
            sql += " FROM ";
            sql += "    UserInformations ";
            sql += " Where IsActive=1 and Username like '%" + _userName + "%'";
            sql += " and UserFirstName like '%" + _userFirstName + "%'";
            sql += " and UserLastName like '%" + _userLastName + "%'";
            ArrayList result = new ArrayList();
            DataTable dt = cm.retrieveDataTable(sql);

            DataTransformerUtility dtu = new DataTransformerUtility();
            ArrayList arr = new ArrayList();

            foreach (DataRow dr in dt.Rows)
            {
                Model.UserInformation userInformation = new Model.UserInformation();
                userInformation.UserID = int.Parse(dtu.extractStringValue(dr["UserID"], DataTransformerUtility.DataType.PRIMARYKEY));
                userInformation.CustomerName = dtu.extractStringValue(dr["CustomerName"], DataTransformerUtility.DataType.STRING);
                userInformation.UserType = dtu.extractStringValue(dr["UserType"], DataTransformerUtility.DataType.STRING);
                userInformation.CompanyName = dtu.extractStringValue(dr["CompanyName"], DataTransformerUtility.DataType.STRING);
                userInformation.Username = dtu.extractStringValue(dr["Username"], DataTransformerUtility.DataType.STRING);
                userInformation.UserFirstName = dtu.extractStringValue(dr["UserFirstName"], DataTransformerUtility.DataType.STRING);
                userInformation.UserLastName = dtu.extractStringValue(dr["UserLastName"], DataTransformerUtility.DataType.STRING);
                userInformation.PhoneNumber = dtu.extractStringValue(dr["PhoneNumber"], DataTransformerUtility.DataType.STRING);
                userInformation.EmailAddress = dtu.extractStringValue(dr["EmailAddress"], DataTransformerUtility.DataType.STRING);
                userInformation.UserTitle = dtu.extractStringValue(dr["UserTitle"], DataTransformerUtility.DataType.STRING);
                userInformation.Function = dtu.extractStringValue(dr["Function"], DataTransformerUtility.DataType.STRING);
                userInformation.ReceptionActivated = bool.Parse(dtu.extractStringValue(dr["ReceptionActivated"], DataTransformerUtility.DataType.BOOLEAN));
                userInformation.AnalysisActivated = bool.Parse(dtu.extractStringValue(dr["AnalysisActivated"], DataTransformerUtility.DataType.BOOLEAN));
                userInformation.CDActivated = bool.Parse(dtu.extractStringValue(dr["CreditDecisionActivated"], DataTransformerUtility.DataType.BOOLEAN));
                arr.Add(userInformation);
            }

            return arr;
        }

        public int updateUsers(Model.UserInformation userInformation)
        {
            SecurityMaster sm=new SecurityMaster();

            String sql = "";
            sql += " Update UserInformations ";
            sql += " set CustomerName = '" + userInformation.CustomerName+"'";
            sql += " ,UserType= '" + userInformation.UserType + "'";
            sql += " ,SOCID= '" + userInformation.CSO + "'";
            sql += " ,CompanyName= '" + userInformation.CompanyName + "'";
            sql += " ,Username= '" + userInformation.Username + "'";
            sql += " ,UserFirstName= '" + userInformation.UserFirstName + "'";
            sql += " ,UserLastName= '" + userInformation.UserLastName + "'";
            sql += " ,PhoneNumber= '" + userInformation.PhoneNumber + "'";
            sql += " ,UserTitle= '" + userInformation.UserTitle + "'";
            sql += " ,[Function]= '" + userInformation.Function + "'";
            sql += " ,EmailAddress= '" + userInformation.EmailAddress + "'";
            sql += " , ReceptionActivated= " + convertBoolToInt(userInformation.ReceptionActivated);
            sql += " , AnalysisActivated= " + convertBoolToInt(userInformation.AnalysisActivated);
            sql += " , CreditDecisionActivated = " + convertBoolToInt(userInformation.CDActivated);

            if (userInformation.UserPassword != "") {
                sql += " , UserPassword = '" + sm.Encrypt(userInformation.UserPassword)+"'";
            }

            sql += " WHERE ";
            sql += "    UserID= " + userInformation.UserID;

            ArrayList result = new ArrayList();
            int id = int.Parse(cm.executeCommand(sql,false).ToString());

            return id;
        }

        public bool isUserNameExist(string _username)
        {
            String sql = "";
            Model.UserInformation ui = new Model.UserInformation();

            sql += " SELECT ";
            sql += " UserID , ";
            sql += " Username , ";
            sql += " UserPassword , ";
            sql += " UserFirstName , ";
            sql += " PhoneNumber , ";
            sql += " EmailAddress ";
            sql += " FROM ";
            sql += " UserInformations ";
            sql += " WHERE ";
            sql += " Username=" + u.convertToSQLString(_username, General.Utility.DataType.STRING);

            DataTable dt = cm.retrieveDataTable(sql);

            if (dt.Rows.Count > 0)
            {
                return true;
            }
            else
                return false;
        }

        public int changeUserStatus(string userid)
        {
            string sqlString = string.Empty;
            sqlString += " UPDATE UserInformations";
            sqlString += " SET IsFirstLogin = 0 ";
            sqlString += " WHERE UserID = " + u.convertToSQLString(userid, General.Utility.DataType.STRING); ;

            int result = (int)cm.executeCommand(sqlString, false);
            return result;
        }

        public int updateUserInfo(Model.UserInformation ui)
        {
            string sqlString = string.Empty;
            
            sqlString += " UPDATE UserInformations";
            sqlString += " SET ";
            sqlString += "    UserFirstName =" + u.convertToSQLString(ui.UserFirstName, General.Utility.DataType.STRING);
            sqlString += "  , UserLastName =" + u.convertToSQLString(ui.UserLastName, General.Utility.DataType.STRING);
            sqlString += "  , PhoneNumber =" + u.convertToSQLString(ui.PhoneNumber, General.Utility.DataType.STRING);
            sqlString += "  , EmailAddress =" + u.convertToSQLString(ui.EmailAddress, General.Utility.DataType.STRING);
            sqlString += "  , UserTitle =" + u.convertToSQLString(ui.Title, General.Utility.DataType.STRING);
            sqlString += "  , [Function] =" + u.convertToSQLString(ui.Function, General.Utility.DataType.STRING);
            sqlString += "  , ReceptionActivated =" + u.convertToSQLString(convertBoolToBit(ui.ReceptionActivated), General.Utility.DataType.STRING);
            sqlString += "  , AnalysisActivated =" + u.convertToSQLString(convertBoolToBit(ui.AnalysisActivated), General.Utility.DataType.STRING);
            sqlString += "  , CreditDecisionActivated =" + u.convertToSQLString(convertBoolToBit(ui.CDActivated), General.Utility.DataType.STRING);
            sqlString += "  , Modified = GETDATE()";
            sqlString += " WHERE UserID = " + u.convertToSQLString(ui.UserID.ToString(), General.Utility.DataType.STRING);

            int result = (int)cm.executeCommand(sqlString, false);
            return result;
        }

        public void deactivateUserByUserId(int _userid)
        {
            string sqlStr = "";
            sqlStr += " Update UserInformations set IsActive=0 where userid=" + _userid;
            //sqlStr += " Delete from USERMAPPING where userid=" + _userid;
            cm.executeCommand(sqlStr, false);
        }


        private string convertBoolToBit(bool val)
        {
            string result = val ? "1" : "0";
            return result;
        }

        public int convertBoolToInt(bool _value)
        {
            if (_value == true)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        // [START] [jpbersonda] [5/12/2017] Change Email to Username
        public bool checkUsername(string userName) {
            string sqlString = "SELECT Username FROM UserInformations WHERE Username = '" + userName + "'";
            return (cm.retrieveDataTable(sqlString).Rows.Count > 0) ? true : false;
        }


        public Model.UserInformation updatePasswordOfUsername(string userName)
        {
            Model.UserInformation u = new Model.UserInformation();
            string sqlString = "SELECT UserFirstName,UserLastName,EmailAddress FROM UserInformations WHERE Username = '" + userName + "'";
            DataTable dt = cm.retrieveDataTable(sqlString);
            DataRow row = dt.Rows[0];

            u.UserFirstName = row["UserFirstName"].ToString();
            u.UserLastName = row["UserLastName"].ToString();
            u.EmailAddress = row["EmailAddress"].ToString();
            u.UserPassword = CreatePassword(6);

            cm = new General.ConnectionMaster();
            sqlString = "UPDATE UserInformations SET UserPassword = '" + sm.Encrypt(u.UserPassword) + "' WHERE Username = '" + userName + "'";
            cm.executeCommand(sqlString,false);

            return u;
        }
        // [END] [jpbersonda] [5/12/2017] Change Email to Username
        public string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }

        //[START] [jpbersonda] [5/12/2017] New | Check Username if already exist
        public bool CheckUsernameIfExist(string userName, int userId)
        {
            string sqlStr = "SELECT UserID FROM UserInformations WHERE Username = '" + userName + "'" + " AND UserID != " + userId;

            DataTable dt = cm.retrieveDataTable(sqlStr);

            if (dt.Rows.Count > 0)
                return true;
            else 
                return false;
        }
        //[END] [jpbersonda] [5/12/2017] New | Check Username if already exist
    }
}