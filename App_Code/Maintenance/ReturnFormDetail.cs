﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using General;
using System.Collections;
using System.Data; 
using System.Text;
using General.AvianCrypto;
using Telerik.Reporting;
/// <summary>
/// Summary description for ReturnFormDetail
/// </summary>
/// 
namespace Maintenance
{

    public class ReturnFormDetail
    {
        public ReturnFormDetail()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        
        public int getLastReferenceDigit(string currentYear, string currentMonth)
        {
            ConnectionMaster cm = new ConnectionMaster();
            int lastDigit = 0;
            string sqlStr = "";
            sqlStr += " SELECT TOP 1 LastRefDigit FROM ReturnFormHeader ";
            // [START] [jpbersonda] [4/11/17] [to allign w/ Control & Accessories]
            
            //sqlStr += " WHERE LastRefYear = " + currentYear;
            //sqlStr += " AND LastRefMonth = " + currentMonth;
            
            // [END] [jpbersonda] [4/11/17]
            sqlStr += " ORDER BY RFHeaderID DESC";
            DataTable dt = cm.retrieveDataTable(sqlStr);
            if (dt.Rows.Count > 0)
            {
                lastDigit = int.Parse(dt.Rows[0]["LastRefDigit"].ToString());
            }
            return lastDigit;
        }

        public string getMonthCode(int currentMonth)
        {
            string monthCode="";
            switch (currentMonth)
            {
                case 1: monthCode = "A";
                    break;
                case 2: monthCode = "B";
                    break;
                case 3: monthCode = "C";
                    break;
                case 4: monthCode = "D";
                    break;
                case 5: monthCode = "E";
                    break;
                case 6: monthCode = "F";
                    break;
                case 7: monthCode = "G";
                    break;
                case 8: monthCode = "H";
                    break;
                case 9: monthCode = "I";
                    break;
                case 10: monthCode = "J";
                    break;
                case 11: monthCode = "K";
                    break;
                case 12: monthCode = "L";
                    break;
            }
            return monthCode;
        }

        public int addReturnFormHeader(Model.ReturnFormDetail rd, int userID)
        {
            ConnectionMaster cm = new ConnectionMaster();

            //get last reference number
            string currentYear = DateTime.Now.Year.ToString();
            int currentMonth = DateTime.Now.Month;
            int lastDigit = getLastReferenceDigit(currentYear, currentMonth.ToString());
            lastDigit = lastDigit + 1;

            //string referenceNumber = currentYear + getMonthCode(currentMonth) + lastDigit.ToString().PadLeft(5, '0');
            string referenceNumber = "WP81" + lastDigit.ToString().PadLeft(10, '0');

            string sql = "";
            sql += " INSERT INTO ReturnFormHeader (LastRefYear,LastRefMonth,LastRefDigit,ReferenceNumber,ReturnDate,CreatedBy,Created,Modified) ";
            sql += " VALUES ";
            sql += " ( ";
            sql += "    " + currentYear;
            sql += "    , " + currentMonth.ToString();
            sql += "    , " + lastDigit;
            sql += "    , '" + referenceNumber + "'";
            sql += "    , GETDATE() ";
            sql += "    , " + userID;
            sql += "    , GETDATE() ";
            sql += "    , GETDATE() ";
            sql += " ) ";

            rd.RFHeaderID = int.Parse(cm.executeCommand(sql, true).ToString());

            if (rd.RFHeaderID > 0)
                return rd.RFHeaderID;
            else
                return 0;
        }

        public Model.ReturnFormDetail addReturnFormDetail(Model.ReturnFormDetail rd)
        {
            string sql = "";
            ConnectionMaster cm = new ConnectionMaster();
            Utility u = new Utility();

            sql += " INSERT INTO ReturnFormDetail ";
            sql += " ( ";
            sql += " RFHeaderID, ";
            sql += " ProductType, ";
            sql += " Claim, ";
            sql += " ClaimDescription, ";
            sql += " CustomerClaimReference, ";
            sql += " SerialNumber, ";
            sql += " Model, ";
            sql += " DataCode, ";
            sql += " Warranty, ";
            sql += " Quantity, ";
            sql += " DateOfClaim, ";
            sql += " CompressorComponentReference, ";
            sql += " TypeOfReturn, ";
            sql += " Application, ";
            sql += " Refrigerant, ";
            sql += " OtherRefrigerant, ";
            sql += " Request, ";
            sql += " RequestWarranty, ";
            sql += " Analysis, ";
            sql += " CreditOfBody, ";
            sql += " OutOfWarranty, ";
            sql += " SpecificTest, ";
            sql += " RequestSpecificTestRemarks, ";
            sql += " Configuration, ";
            sql += " DateOfFailure, ";
            sql += " RunningHours, ";
            sql += " Remarks, ";
            sql += " FilePath, ";
            sql += " CustomerNumber, ";
            sql += " ItemNumber, ";
            sql += " WorkingPointTo, ";
            sql += " WorkingPointTc, ";
            sql += " SuperHeat, ";
            sql += " SubCooling, ";
            sql += " Created, ";
            sql += " Modified, ";
            sql += " LocationID ";
            sql += " ) ";
            sql += " VALUES ";
            sql += " ( ";
            sql += "    " + u.convertToSQLString(rd.RFHeaderID.ToString(), Utility.DataType.NUMBER) + " , ";
            sql += "    " + u.convertToSQLString(rd.ProductType, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.Claim, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.ClaimDescription, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.CustomerClaimReference, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.SerialNumber, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.ModelProduct, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.DataCode, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.Warranty, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.Quantity, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.DateOfClaim, Utility.DataType.DATE) + " , ";
            sql += "    " + u.convertToSQLString(rd.CompressorComponentReference, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.TypeOfReturn, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.Application, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.Refrigerant, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.OtherRefrigerant, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.Request, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.RequestWarranty.ToString(), Utility.DataType.BOOLEAN) + " ,";
            sql += "    " + u.convertToSQLString(rd.Analysis.ToString(), Utility.DataType.BOOLEAN) + " ,";
            sql += "    " + u.convertToSQLString(rd.CreditOfBody.ToString(), Utility.DataType.BOOLEAN) + " ,";
            sql += "    " + u.convertToSQLString(rd.OutOfWarranty.ToString(), Utility.DataType.BOOLEAN) + " ,";
            sql += "    " + u.convertToSQLString(rd.SpecificTest.ToString(), Utility.DataType.BOOLEAN) + " ,";
            sql += "    " + u.convertToSQLString(rd.RequestSpecificTestRemarks, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.Configuration, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.DateOfFailure, Utility.DataType.DATE) + " , ";
            sql += "    " + u.convertToSQLString(rd.RunningHours, Utility.DataType.NUMBER) + " ,";
            sql += "    " + u.convertToSQLString(rd.Remarks, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.FilePath, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.CustomerNumber.ToString(), Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.ItemNumber, Utility.DataType.STRING) + " , ";
            sql += "    " + u.convertToSQLString(rd.WorkingPointTo, Utility.DataType.DECIMAL) + " , ";
            sql += "    " + u.convertToSQLString(rd.WorkingPointTc, Utility.DataType.DECIMAL) + " , ";
            sql += "    " + u.convertToSQLString(rd.WorkingPointSuperHeat, Utility.DataType.DECIMAL) + " , ";
            sql += "    " + u.convertToSQLString(rd.WorkingPointSubcooling, Utility.DataType.DECIMAL) + " , ";
            sql += "    GETDATE() ,";
            sql += "    GETDATE(), ";
            sql += "    " + u.convertToSQLString(rd.LocationID.ToString(), Utility.DataType.STRING) + "  ";
            sql += " ) ";

            cm.executeCommand(sql, false);
            return rd;
        }
         
        public int getLastRFHeaderID()
        {
            int lastID = 0;
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            sqlStr += " SELECT MAX(RFHeaderID) FROM RetrunFormHeader ";
            lastID = Int32.Parse(cm.executeCommand(sqlStr, true).ToString());
            return lastID;
        }

        public Model.ReturnFormDetail getReturnFormDetail(int rfdID)
        {
            Model.ReturnFormDetail rfd = new Model.ReturnFormDetail();
            ConnectionMaster cm = new ConnectionMaster();
            Utility u = new Utility();
            string sqlStr = "";
            sqlStr += " SELECT d.RFDetailID, d.RFHeaderID,";
            sqlStr += " ProductType, ";
            sqlStr += " LocationID, ";
            sqlStr += " Claim, ";
            sqlStr += " ClaimDescription, ";
            sqlStr += " CustomerClaimReference, ";
            sqlStr += " SerialNumber, ";
            sqlStr += " Model, ";
            sqlStr += " DataCode, ";
            sqlStr += " Warranty, ";
            sqlStr += " Quantity, ";
            sqlStr += " CONVERT(VARCHAR,DateOfClaim,101) AS 'DateOfClaim', ";
            sqlStr += " CompressorComponentReference, ";
            sqlStr += " TypeOfReturn, ";
            sqlStr += " Application, ";
            sqlStr += " Refrigerant, ";
            sqlStr += " OtherRefrigerant, ";
            sqlStr += " Request, ";
            sqlStr += " RequestWarranty, ";
            sqlStr += " Analysis, ";
            sqlStr += " CreditOfBody, ";
            sqlStr += " OutOfWarranty, ";
            sqlStr += " SpecificTest, ";
            sqlStr += " RequestSpecificTestRemarks, ";
            sqlStr += " Configuration, ";
            sqlStr += " CONVERT(VARCHAR,DateOfFailure,101) AS 'DateOfFailure', ";
            sqlStr += " RunningHours, ";
            sqlStr += " Remarks, ";
            sqlStr += " FilePath, ";
            sqlStr += " CustomerNumber, ";
            sqlStr += " ItemNumber, ";
            sqlStr += " WorkingPointTo, ";
            sqlStr += " WorkingPointTc, ";
            sqlStr += " SuperHeat, ";
            sqlStr += " SubCooling, ";
            sqlStr += " OracleNumber, ";
            sqlStr += " h.ReferenceNumber ";
            sqlStr += " FROM ReturnFormDetail d";
            sqlStr += " INNER JOIN ReturnFormHeader h ON d.RFHeaderID = h.RFHeaderID";
            sqlStr += " WHERE d.RFDetailID = " + rfdID;
            DataTable dt = cm.retrieveDataTable(sqlStr);
            foreach (DataRow dr in dt.Rows)
            {
                rfd.RFHeaderID = int.Parse(u.extractStringValue(dr["RFHeaderID"], Utility.DataType.NUMBER));
                rfd.RFDetailID = int.Parse(u.extractStringValue(dr["RFDetailID"], Utility.DataType.NUMBER));

                rfd.ProductType = u.extractStringValue(dr["ProductType"], Utility.DataType.STRING);
                string locationID = dr["LocationID"].ToString();
                rfd.LocationID = (locationID != "") ? locationID : "0";
                rfd.Claim = u.extractStringValue(dr["Claim"], Utility.DataType.STRING);
                rfd.ClaimDescription = u.extractStringValue(dr["ClaimDescription"], Utility.DataType.STRING);
                rfd.CustomerClaimReference = u.extractStringValue(dr["CustomerClaimReference"], Utility.DataType.STRING);
                rfd.SerialNumber = u.extractStringValue(dr["SerialNumber"], Utility.DataType.STRING);
                rfd.ModelProduct = u.extractStringValue(dr["Model"], Utility.DataType.STRING);
                rfd.DataCode = u.extractStringValue(dr["DataCode"], Utility.DataType.STRING);
                rfd.Warranty = u.extractStringValue(dr["Warranty"], Utility.DataType.STRING);
                rfd.Quantity = u.extractStringValue(dr["Quantity"], Utility.DataType.STRING);
                rfd.DateOfClaim = u.extractStringValue(dr["DateOfClaim"], Utility.DataType.DATE);
                rfd.CompressorComponentReference = u.extractStringValue(dr["CompressorComponentReference"], Utility.DataType.STRING);
                rfd.TypeOfReturn = u.extractStringValue(dr["TypeOfReturn"], Utility.DataType.STRING);
                rfd.Application = u.extractStringValue(dr["Application"], Utility.DataType.STRING);
                rfd.Refrigerant = u.extractStringValue(dr["Refrigerant"], Utility.DataType.STRING);
                rfd.OtherRefrigerant = u.extractStringValue(dr["OtherRefrigerant"], Utility.DataType.STRING);
                rfd.Request = u.extractStringValue(dr["Request"], Utility.DataType.STRING);
                rfd.RequestWarranty = Boolean.Parse(dr["RequestWarranty"].ToString());
                rfd.Analysis = Boolean.Parse(dr["Analysis"].ToString());
                rfd.CreditOfBody = Boolean.Parse(dr["CreditOfBody"].ToString());
                rfd.OutOfWarranty = Boolean.Parse(dr["OutOfWarranty"].ToString());
                rfd.SpecificTest = Boolean.Parse(dr["SpecificTest"].ToString());
                rfd.RequestSpecificTestRemarks = u.extractStringValue(dr["RequestSpecificTestRemarks"], Utility.DataType.STRING);
                rfd.Configuration = u.extractStringValue(dr["Configuration"], Utility.DataType.STRING);
                rfd.DateOfFailure = u.extractStringValue(dr["DateOfFailure"], Utility.DataType.DATE);
                rfd.RunningHours = u.extractStringValue(dr["RunningHours"], Utility.DataType.STRING);
                rfd.Remarks = u.extractStringValue(dr["Remarks"], Utility.DataType.STRING);
                rfd.FilePath = u.extractStringValue(dr["FilePath"], Utility.DataType.STRING);
                rfd.CustomerNumber = dr["CustomerNumber"].ToString();
                rfd.ItemNumber = u.extractStringValue(dr["ItemNumber"], Utility.DataType.STRING);
                rfd.WorkingPointTo = u.extractStringValue(dr["WorkingPointTo"], Utility.DataType.DATE);
                rfd.WorkingPointTc = u.extractStringValue(dr["WorkingPointTc"], Utility.DataType.DATE);
                rfd.WorkingPointSuperHeat = u.extractStringValue(dr["SuperHeat"], Utility.DataType.DATE);
                rfd.WorkingPointSubcooling = u.extractStringValue(dr["SubCooling"], Utility.DataType.DATE);
                rfd.OracleNumber = u.extractStringValue(dr["OracleNumber"], Utility.DataType.STRING);
                rfd.ReferenceNo = u.extractStringValue(dr["ReferenceNumber"], Utility.DataType.STRING);

            }
            return rfd;
        }

        public Model.ReturnFormDetail updateReturnFormDetail(Model.ReturnFormDetail rd)
        {
            Model.ReturnFormDetail rfd = new Model.ReturnFormDetail();
            ConnectionMaster cm = new ConnectionMaster();
            Utility u = new Utility();
            string sqlStr = "";
            sqlStr += " UPDATE ReturnFormDetail SET ";
            sqlStr += " LocationID = " + u.convertToSQLString(rd.LocationID.ToString(), Utility.DataType.STRING) + ", ";
            sqlStr += " Claim = " + u.convertToSQLString(rd.Claim, Utility.DataType.STRING) + ", ";
            sqlStr += " ClaimDescription = " + u.convertToSQLString(rd.ClaimDescription, Utility.DataType.STRING) + ", ";
            sqlStr += " CustomerClaimReference = " + u.convertToSQLString(rd.CustomerClaimReference, Utility.DataType.STRING) + ", ";
            sqlStr += " SerialNumber = " + u.convertToSQLString(rd.SerialNumber, Utility.DataType.STRING) + ", ";
            sqlStr += " Model = " + u.convertToSQLString(rd.ModelProduct, Utility.DataType.STRING) + ", ";
            sqlStr += " DateOfClaim = " + u.convertToSQLString(rd.DateOfClaim, Utility.DataType.DATE) + ", ";
            sqlStr += " CompressorComponentReference = " + u.convertToSQLString(rd.CompressorComponentReference, Utility.DataType.STRING) + ", ";
            sqlStr += " TypeOfReturn = " + u.convertToSQLString(rd.TypeOfReturn, Utility.DataType.STRING) + ", ";
            sqlStr += " Application = " + u.convertToSQLString(rd.Application, Utility.DataType.STRING) + ", ";
            sqlStr += " Refrigerant = " + u.convertToSQLString(rd.Refrigerant, Utility.DataType.STRING) + ", ";
            sqlStr += " OtherRefrigerant = " + u.convertToSQLString(rd.OtherRefrigerant, Utility.DataType.STRING) + ", ";
            sqlStr += " Request = " + u.convertToSQLString(rd.Request, Utility.DataType.STRING) + ", ";
            sqlStr += " RequestWarranty = " + u.convertToSQLString(rd.RequestWarranty.ToString(), Utility.DataType.BOOLEAN) + ", ";
            sqlStr += " Analysis = " + u.convertToSQLString(rd.Analysis.ToString(), Utility.DataType.BOOLEAN) + ", ";
            sqlStr += " CreditOfBody = " + u.convertToSQLString(rd.CreditOfBody.ToString(), Utility.DataType.BOOLEAN) + ", ";
            sqlStr += " OutOfWarranty = " + u.convertToSQLString(rd.OutOfWarranty.ToString(), Utility.DataType.BOOLEAN) + ", ";
            sqlStr += " SpecificTest = " + u.convertToSQLString(rd.SpecificTest.ToString(), Utility.DataType.BOOLEAN) + ", ";
            sqlStr += " RequestSpecificTestRemarks = " + u.convertToSQLString(rd.RequestSpecificTestRemarks, Utility.DataType.STRING) + ", ";
            sqlStr += " Configuration = " + u.convertToSQLString(rd.Configuration, Utility.DataType.STRING) + ", ";
            sqlStr += " DateOfFailure = " + u.convertToSQLString(rd.DateOfFailure, Utility.DataType.DATE) + ", ";
            sqlStr += " RunningHours = " + u.convertToSQLString(rd.RunningHours, Utility.DataType.NUMBER) + ", ";
            sqlStr += " Remarks = " + u.convertToSQLString(rd.Remarks, Utility.DataType.STRING) + ", ";
            //sqlStr += " FilePath = " + u.convertToSQLString(rd.FilePath, Utility.DataType.STRING) + ", ";
            //sqlStr += " ItemNumber = " + u.convertToSQLString(rd.ItemNumber, Utility.DataType.STRING) + ", ";
            sqlStr += " WorkingPointTo = " + u.convertToSQLString(rd.WorkingPointTo, Utility.DataType.DECIMAL) + ", ";
            sqlStr += " WorkingPointTc = " + u.convertToSQLString(rd.WorkingPointTc, Utility.DataType.DECIMAL) + ", ";
            sqlStr += " SuperHeat = " + u.convertToSQLString(rd.WorkingPointSuperHeat, Utility.DataType.DECIMAL) + ", ";
            sqlStr += " SubCooling = " + u.convertToSQLString(rd.WorkingPointSubcooling, Utility.DataType.DECIMAL) + ", ";
            sqlStr += " OracleNumber = " + u.convertToSQLString(rd.OracleNumber, Utility.DataType.STRING) + " "; 
            sqlStr += " WHERE RFDetailID = '" + rd.RFDetailID + "' ";
            cm.executeCommand(sqlStr, false);
            return rd;
        }
        
        public DataTable getMyReturn(int userid, int searchBy, string searchText, string selectedCustNum)
        {
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            sqlStr += "SELECT (CAST(a.CustomerNumber AS VARCHAR) + ' - '  + cus.CustomerName) CustomerName, a.RFDetailID, a.Model,a.SerialNumber,b.ReferenceNumber AS 'ReferenceNumber', ";
            sqlStr += "a.CustomerClaimReference AS 'CustRef',a.TypeOfReturn AS 'Reject',CONVERT(varchar(11),b.ReturnDate,103) AS 'SubmissionDate',  ";
            sqlStr += "CONVERT(varchar(11),a.ReceptionDate,103) AS 'ReceptionDate',CONVERT(varchar(11),a.AnalyzedDate,103) AS 'AnalyzeDate',  ";
            sqlStr += "d.CreditDecision AS 'CreditNote', a.OracleNumber,  ";
            sqlStr += " CONCAT('If you have question or concerns about this returns, please contact ', cp.NAME, '<br/>Email Address: ', cp.EmailAddress, ' Phone number: ', cp.PhoneNumber) ContactInfo ";

            sqlStr += "FROM ReturnFormDetail a  ";
            sqlStr += "INNER JOIN ReturnFormHeader b ON (a.rfheaderid = b.rfheaderid)  ";
            sqlStr += "INNER JOIN ContactPerson cp ON  a.rfheaderid = cp.RFHeaderID ";
            sqlStr += "LEFT JOIN ReturnFormControl d ON (a.rfdetailid = d.rfdetailid)  ";
            sqlStr += "INNER JOIN (SELECT DISTINCT CustomerNumber,CustomerName,LocationID  FROM customer WHERE Compressor = 1) cus ON (a.CustomerNumber = cus.CustomerNumber  and a.LocationID = cus.LocationID)   "; 
            sqlStr += "LEFT JOIN TEST_CUSTQCLMD1 c ON a.SerialNumber = c.CPSERN AND b.ReferenceNumber = c.PortalReference ";
            sqlStr += "WHERE (1=1) ";

            // list all assigned customers to user
            sqlStr += " AND a.CustomerNUmber IN (SELECT cus.CustomerNumber FROM UserMapping  um ";
            sqlStr += " INNER JOIN Customer cus on um.CustomerID = cus.CustomerID ";
            sqlStr += " WHERE um.UserId = " + userid + " )";

            if (selectedCustNum != "-- ALL --")
                sqlStr += "AND a.CustomerNumber in (" + selectedCustNum + ") ";
            else
                sqlStr += "AND a.CustomerNumber IN (SELECT DISTINCT CUSTOMERNUMBER FROM CUSTOMER WHERE CUSTOMERID IN (SELECT CUSTOMERID FROM USERMAPPING WHERE USERID = " + userid + ")) ";
            if (searchBy == 1)
                sqlStr += "AND LTRIM(RTRIM(a.SerialNumber)) like '" + searchText.Replace("*", "%") + "'" ;
            if (searchBy == 2)
                sqlStr += "AND LTRIM(RTRIM(a.Model)) like '" + searchText.Replace("*", "%") + "'" ;
          
            sqlStr += "ORDER BY a.Created DESC, 'Model'";
            DataTable dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        public DataTable getMyReturnAll(int searchBy, string searchText, string selectedCustNum, string userType, int userId)
        {
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            sqlStr += "SELECT (CAST(a.CustomerNumber AS VARCHAR) + ' - '  + cus.CustomerName) CustomerName, a.RFDetailID, a.Model,a.SerialNumber,b.ReferenceNumber AS 'ReferenceNumber', ";
            sqlStr += "a.CustomerClaimReference AS 'CustRef',a.TypeOfReturn AS 'Reject',CONVERT(varchar(11),b.ReturnDate,103) AS 'SubmissionDate',  ";
            sqlStr += "CONVERT(varchar(11),a.ReceptionDate,103) AS 'ReceptionDate',CONVERT(varchar(11),a.AnalyzedDate,103) AS 'AnalyzeDate',  ";
            sqlStr += "d.CreditDecision AS 'CreditNote', a.OracleNumber,  ";
            sqlStr += " CONCAT('If you have question or concerns about this returns, please contact ', cp.NAME, '<br/>Email Address: ', cp.EmailAddress, ' Phone number: ', cp.PhoneNumber) ContactInfo ";
            sqlStr += "FROM ReturnFormDetail a  ";
            sqlStr += "INNER JOIN ReturnFormHeader b ON (a.rfheaderid = b.rfheaderid)  ";
            sqlStr += "INNER JOIN (SELECT DISTINCT CustomerNumber,CustomerName,LocationID,SalesOfficeCode  FROM customer WHERE Compressor = 1) cus ON (a.CustomerNumber = cus.CustomerNumber  and a.LocationID = cus.LocationID)  ";
            sqlStr += "INNER JOIN ContactPerson cp ON  a.rfheaderid = cp.RFHeaderID ";
            sqlStr += "LEFT JOIN ReturnFormControl d ON (a.rfdetailid = d.rfdetailid)  "; 
            sqlStr += "LEFT JOIN TEST_CUSTQCLMD1 c ON a.SerialNumber = c.CPSERN AND b.ReferenceNumber = c.PortalReference ";
            //[START] [jpbersonda] [5/10/17] [For CSO user type]
            if (userType == "CSO")
            {
                sqlStr += "INNER JOIN SalesOfficeCode  soc ON CONVERT(varchar,soc.SalesPersonNumber) = cus.SalesOfficeCOde  ";
                sqlStr += "INNER JOIN UserInformations ui  ON ui.SOCID = soc.SOCID ";
            }
            //[END] [jpbersonda] [5/10/17] [For CSO user type]
            
            sqlStr += "WHERE (1=1) ";

            if (userType == "CSO")
                sqlStr += "AND ui.UserId = " + userId;

            if (selectedCustNum != "-- ALL --")
                sqlStr += "AND a.CustomerNumber in (" + selectedCustNum + ") ";
          
            if (searchBy == 1)
                sqlStr += "AND LTRIM(RTRIM(a.SerialNumber)) like '" + searchText.Replace("*", "%") + "'";
            if (searchBy == 2)
                sqlStr += "AND LTRIM(RTRIM(a.Model)) like '" + searchText.Replace("*", "%") + "'";

            sqlStr += "ORDER BY a.Created DESC, 'Model'";
            DataTable dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        public DataTable getStatisticsByYOM(int userid, string selectedCustNum)
        {
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            sqlStr += "SELECT SUM(CAST(TBL2.[RETURN] AS FLOAT)) AS 'PPM',TBL2.YOM AS 'YEAR' FROM ";
            sqlStr += "(SELECT COUNT(TBL1.CPNSER) AS 'RETURN',TBL1.YOM,TBL1.CPIDEN FROM ";
            sqlStr += "(SELECT CPNSER,YOM,CPIDEN FROM CUSTQCLMD ";
            sqlStr += "WHERE (1=1) ";

            if (selectedCustNum != "-- ALL --")
                sqlStr += "AND CPNCLI = " + selectedCustNum + " ";
            else
                sqlStr += "AND CPNCLI IN (SELECT DISTINCT CUSTOMERNUMBER FROM CUSTOMER WHERE CUSTOMERID IN (SELECT CUSTOMERID FROM USERMAPPING WHERE USERID = " + userid + ")) ";
            
            sqlStr += "AND CPRJCT IN ('LR','FR') ";
            sqlStr += "AND CPWARR IN (1,2,5,8)) AS TBL1 ";
            sqlStr += "GROUP BY TBL1.YOM,TBL1.CPIDEN) AS TBL2 ";
            sqlStr += "WHERE TBL2.YOM IN (SELECT DISTINCT YOM FROM CUSTQCLMD WHERE YOM BETWEEN ";
            sqlStr += "(CONVERT(INT,YEAR(GETDATE())) - 5) AND YEAR(GETDATE())) GROUP BY TBL2.YOM"; 
            DataTable dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        public DataTable getStatisticsByYOR(int userid, string selectedCustNum)
        {
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            sqlStr += "SELECT SUM(CAST(TBL2.[RETURN] AS FLOAT)) AS 'PPM',TBL2.YOR AS 'YEAR' FROM ";
            sqlStr += "(SELECT COUNT(TBL1.CPNSER) AS 'RETURN',TBL1.YOR,TBL1.CPIDEN FROM ";
            sqlStr += "(SELECT CPNSER,YOR,CPIDEN FROM CUSTQCLMD ";
            sqlStr += "WHERE (1=1) ";

            if (selectedCustNum != "-- ALL --")
                sqlStr += "AND CPNCLI = " + selectedCustNum + " ";
            else
                sqlStr += "AND CPNCLI IN (SELECT DISTINCT CUSTOMERNUMBER FROM CUSTOMER WHERE CUSTOMERID IN (SELECT CUSTOMERID FROM USERMAPPING WHERE USERID = " + userid + ")) ";

            sqlStr += "AND CPRJCT IN ('LR','FR') ";
            sqlStr += "AND CPWARR IN (1,2,5,8)) AS TBL1 ";
            sqlStr += "GROUP BY TBL1.YOR,TBL1.CPIDEN) AS TBL2 ";
            sqlStr += "WHERE TBL2.YOR IN (SELECT DISTINCT YOR FROM CUSTQCLMD WHERE YOR BETWEEN ";
            sqlStr += "(CONVERT(INT,YEAR(GETDATE())) - 5) AND YEAR(GETDATE())) GROUP BY TBL2.YOR"; 
            DataTable dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        public DataTable getStatisticsByModel(int userid, string selectedCustNum)
        {
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            sqlStr += "SELECT COUNT(TBL1.CPIDEN) AS 'PPM',TBL1.YOM AS 'YEAR' FROM ";
            sqlStr += "(SELECT YOM,CPIDEN FROM CUSTQCLMD ";
            sqlStr += "WHERE (1=1) ";

            if (selectedCustNum != "-- ALL --")
                sqlStr += "AND CPNCLI = " + selectedCustNum + " ";
            else
                sqlStr += "AND CPNCLI IN (SELECT DISTINCT CUSTOMERNUMBER FROM CUSTOMER WHERE CUSTOMERID IN (SELECT CUSTOMERID FROM USERMAPPING WHERE USERID = " + userid + ")) ";

            sqlStr += "AND CPRJCT IN ('LR','FR')  ";
            sqlStr += "AND CPWARR IN (1,2,5,8)) AS TBL1 ";
            sqlStr += "WHERE TBL1.YOM IN (SELECT DISTINCT YOM FROM CUSTQCLMD WHERE YOM BETWEEN  ";
            sqlStr += "(CONVERT(INT,YEAR(GETDATE())) - 5) AND YEAR(GETDATE())) ";
            sqlStr += "GROUP BY TBL1.YOM";
            DataTable dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        public DataTable getModelBySerial(string _serial = "")
        {
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            sqlStr += "SELECT DISTINCT SERIAL, DESCP, ITNBR FROM CUSTQPORTA" + ((_serial != "") ? " WHERE SERIAL = '" + _serial + "'" : "") + " ORDER BY SERIAL";
            DataTable dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        public DataTable getCustomerList(int userid, string usertype = "")
        {
          
            ConnectionMaster cm = new ConnectionMaster();
            
            string sqlStr = "";
            sqlStr += "SELECT DISTINCT CUSTOMERNUMBER,CUSTOMERNAME,CONVERT(VARCHAR,CUSTOMERNUMBER) + ' - ' + CUSTOMERNAME AS 'CUSTOMERNAMENUMBER' FROM CUSTOMER ";
            sqlStr += "WHERE (1=1) ";
            if (usertype != "CUSTOMER QUALITY") {
                sqlStr += "  AND CUSTOMERID IN (SELECT CUSTOMERID FROM USERMAPPING WHERE USERID = " + userid + ") ";
            }
            sqlStr += " and Compressor=1 ORDER BY CUSTOMERNAME";
            DataTable dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        public ArrayList getCustomerListArray(int userid, string usertype = "")
        { 
            ArrayList list = new ArrayList();
            DataTable dt = getCustomerList(userid, usertype);
            foreach (DataRow row in dt.Rows) {
                Model.Customers c = new Model.Customers();
                c.CustomerNumber = row["CUSTOMERNUMBER"].ToString();
                c.CustomerName = row["CUSTOMERNAMENUMBER"].ToString();
                list.Add(c);
            }
            return list; 
        }


        public DataTable getAllCustomerListCompressor(string userType, int userId)
        {
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            
            if (userType == "CSO")
            {
                sqlStr += "SELECT DISTINCT cus.CUSTOMERNUMBER,cus.CUSTOMERNAME,CONVERT(VARCHAR,cus.CUSTOMERNUMBER) + ' - ' + cus.CUSTOMERNAME AS 'CUSTOMERNAMENUMBER' FROM CUSTOMER cus ";
                sqlStr += "INNER JOIN SalesOfficeCode  soc ON CONVERT(varchar,soc.SalesPersonNumber) = cus.SalesOfficeCOde  ";
                sqlStr += "INNER JOIN UserInformations ui  ON ui.SOCID = soc.SOCID ";
                sqlStr += "WHERE ui.UserId =" + userId;
                sqlStr += "  AND Compressor=1 ORDER BY cus.CUSTOMERNAME";
            }

            else
            {
                sqlStr += "SELECT DISTINCT CUSTOMERNUMBER,CUSTOMERNAME,CONVERT(VARCHAR,CUSTOMERNUMBER) + ' - ' + CUSTOMERNAME AS 'CUSTOMERNAMENUMBER' FROM CUSTOMER ";
                sqlStr += "WHERE ";
                sqlStr += " Compressor=1 ORDER BY CUSTOMERNAME";
            }
            
            
            DataTable dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        public ArrayList getAllCustomerListCompressorArray()
        {
            ArrayList list = new ArrayList();
            DataTable dt = getAllCustomerListCompressor("",0);
            foreach (DataRow row in dt.Rows)
            {
                Model.Customers c = new Model.Customers();
                c.CustomerNumber = row["CUSTOMERNUMBER"].ToString();
                c.CustomerName = row["CUSTOMERNAMENUMBER"].ToString();
                list.Add(c);
            }
            return list;
        }

        public DataTable getCustomerList()
        {
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            sqlStr += "getCustomersList";
            DataTable dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        public string getSpecificCustomerName(string customerNumber)
        {
            Utility u = new Utility();
            ConnectionMaster cm = new ConnectionMaster();
            string customerName = "";
            string sqlStr = "";
            sqlStr += "SELECT CUSTOMERNAME FROM CUSTOMER WHERE CUSTOMERNUMBER = " + u.convertToSQLString(customerNumber,Utility.DataType.STRING);
            DataTable dt = cm.retrieveDataTable(sqlStr);
            foreach (DataRow dr in dt.Rows){
                customerName = u.extractStringValue(dr["CUSTOMERNAME"], Utility.DataType.STRING);
            }
            return customerName;
        }

        public DataTable getSavedReturnFormHeader(int count = 1)
        //public DataTable getSavedReturnFormHeader()
        {
            ConnectionMaster cm = new ConnectionMaster();
            DataTable dt = new DataTable();
            string sqlStr = "";
            //sqlStr += "SELECT RFHeaderID,ReferenceNumber FROM (SELECT TOP " + count + " RFHeaderID,ReferenceNumber FROM ReturnFormHeader ORDER BY RFHeaderID DESC) a ORDER BY a.RFHeaderID";
            sqlStr += "SELECT TOP "+ count +" RFHeaderID,ReferenceNumber FROM ReturnFormHeader ORDER BY RFHeaderID DESC ";
            dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        //public DataTable getContactPerson(int count)
        public DataTable getContactPerson()
        {
            ConnectionMaster cm = new ConnectionMaster();
            DataTable dt = new DataTable();
            string sqlStr = "";
            //sqlStr += "SELECT DISTINCT Name,EmailAddress,PhoneNumber FROM ContactPerson WHERE RFHeaderID IN (SELECT TOP " + count + " RFHeaderID FROM ReturnFormHeader ORDER BY RFHeaderID DESC)";
            sqlStr += "SELECT DISTINCT Name,EmailAddress,PhoneNumber FROM ContactPerson WHERE RFHeaderID IN (SELECT TOP 1 RFHeaderID FROM ReturnFormHeader ORDER BY RFHeaderID DESC)";
            dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        public Model.ContactPerson addContactPerson(Model.ContactPerson cp)
        {
            ConnectionMaster cm = new ConnectionMaster();
            Utility u = new Utility();
            string sqlStr = "";

            sqlStr += " INSERT INTO ContactPerson (RFHeaderID, Name, EmailAddress, PhoneNumber) VALUES ( ";
            sqlStr += " " + u.convertToSQLString(cp.RFHeaderID.ToString(), Utility.DataType.PRIMARYKEY) + ", "; 
            sqlStr += " " + u.convertToSQLString(cp.Name, Utility.DataType.STRING) + " , "; 
            sqlStr += " " + u.convertToSQLString(cp.EmailAddress, Utility.DataType.STRING) + " , "; 
            sqlStr += " " + u.convertToSQLString(cp.PhoneNumber, Utility.DataType.STRING) + " ) ";

            cm.executeCommand(sqlStr, false);
            return cp;
        }

        public DataTable getSavedReturnFormDetail(string rfheaderid)
        {
            ConnectionMaster cm = new ConnectionMaster();
            DataTable dt = new DataTable();
            string sqlStr = "";
            sqlStr += "SELECT h.ReferenceNumber,d.SerialNumber,d.Model,d.Claim, d.RFDetailID FROM ReturnFormDetail d ";
            sqlStr += "INNER JOIN ReturnFormHeader h ON (d.RFHeaderID = h.RFHeaderID) ";
            sqlStr += "WHERE d.RFHeaderID in(" + rfheaderid + ")";
            dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }
        public DataTable getSavedReturnFormControl(int rfheaderid)
        {
            ConnectionMaster cm = new ConnectionMaster();
            DataTable dt = new DataTable();
            string sqlStr = "";
            sqlStr += "SELECT h.ReferenceNumber,d.PCNNumber [SerialNumber],d.Model,d.Claim FROM ReturnFormControl d ";
            sqlStr += "INNER JOIN ReturnFormHeader h ON (d.RFHeaderID = h.RFHeaderID) ";
            sqlStr += "WHERE d.RFHeaderID LIKE '" + rfheaderid + "%'";
            dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }
        public DataTable getSavedReturnFormAccessories(int rfheaderid)
        {
            ConnectionMaster cm = new ConnectionMaster();
            DataTable dt = new DataTable();
            string sqlStr = "";
            sqlStr += "SELECT h.ReferenceNumber,d.PCNNumber [SerialNumber],d.Model,d.Claim FROM ReturnFormAccessories d ";
            sqlStr += "INNER JOIN ReturnFormHeader h ON (d.RFHeaderID = h.RFHeaderID) ";
            sqlStr += "WHERE d.RFHeaderID LIKE '" + rfheaderid + "%'";
            dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        public void sendEmailToUser(string userID, int type, string portalReference)
        {
            MailMaster mailer = new MailMaster();
            UserInformation userdao = new UserInformation();
            Model.UserInformation userinfo = userdao.GetUserInfo(userID);

            string msg = string.Empty;
            msg += " Dear {0},";
            msg += " \nYour compressor(s) registered under portal reference {1} has (have)";
            switch(type){
                case 1:
                    msg += " been received.";
                    break;
                case 2:
                    msg += " been analyzed.";
                    break;
                case 3:
                    msg +=" received a credit decision.";
                    break;
            }

            msg += "\nThis is an automated email. Please do not respond.";
            msg = string.Format(msg, userinfo.UserFullName, portalReference);
            mailer.sendMail(userinfo.EmailAddress, msg, "Customer Quality Portal");
        }

        public DataTable getModelBySerialSearch(string _serial = "")
        {
            ConnectionMaster cm = new ConnectionMaster();
            string sqlStr = "";
            sqlStr += "SELECT DISTINCT TOP 10 SERIAL, DESCP, ITNBR FROM CUSTQPORTA" + ((_serial != "") ? " WHERE SERIAL LIKE '" + _serial + "%'" : "") + " ORDER BY SERIAL";
            DataTable dt = cm.retrieveDataTable(sqlStr);
            return dt;
        }

        //[START] [jpbersonda] [5/18/2017] [NEW - For print function - FormDetail = FormCompressor]
        public Model.ReturnFormDetail getReturnFormDetailByHeaderId(int _headerId)
        {
            Model.ReturnFormDetail rfd = new Model.ReturnFormDetail();
            ConnectionMaster cm = new ConnectionMaster();
            Utility u = new Utility();
            StringBuilder queryBuilder = new StringBuilder();

            queryBuilder.Append(" SELECT RFDetailId, ProductType, LocationId, Claim, ClaimDescription, ");
            queryBuilder.Append(" CustomerClaimReference, SerialNumber, Model, DataCode, ");
            queryBuilder.Append(" Warranty, Quantity, CONVERT(VARCHAR,DateOfClaim,101) AS 'DateOfClaim', ");
            queryBuilder.Append(" CompressorComponentReference, TypeOfReturn, Application, Refrigerant, ");
            queryBuilder.Append(" OtherRefrigerant, Request, RequestWarranty, Analysis, CreditOfBody, ");
            queryBuilder.Append(" OutOfWarranty, SpecificTest, RequestSpecificTestRemarks, Configuration, ");
            queryBuilder.Append(" CONVERT(VARCHAR,DateOfFailure,101) AS 'DateOfFailure', RunningHours,");
            queryBuilder.Append(" Remarks, FilePath, CustomerNumber, ItemNumber, WorkingPointTo,WorkingPointTc,");
            queryBuilder.Append(" SuperHeat, SubCooling, OracleNumber FROM ReturnFormDetail ");
            queryBuilder.Append(" WHERE RFHeaderID = " + _headerId);


            DataTable dt = cm.retrieveDataTable(queryBuilder.ToString());

            queryBuilder = null;

            foreach (DataRow dr in dt.Rows)
            {
                rfd.ProductType = u.extractStringValue(dr["ProductType"], Utility.DataType.STRING);
                string locationID = dr["LocationID"].ToString();
                rfd.LocationID = (locationID != "") ? locationID : "0";
                rfd.Claim = u.extractStringValue(dr["Claim"], Utility.DataType.STRING);
                rfd.ClaimDescription = u.extractStringValue(dr["ClaimDescription"], Utility.DataType.STRING);
                rfd.CustomerClaimReference = u.extractStringValue(dr["CustomerClaimReference"], Utility.DataType.STRING);
                rfd.SerialNumber = u.extractStringValue(dr["SerialNumber"], Utility.DataType.STRING);
                rfd.ModelProduct = u.extractStringValue(dr["Model"], Utility.DataType.STRING);
                rfd.DataCode = u.extractStringValue(dr["DataCode"], Utility.DataType.STRING);
                rfd.Warranty = u.extractStringValue(dr["Warranty"], Utility.DataType.STRING);
                rfd.Quantity = u.extractStringValue(dr["Quantity"], Utility.DataType.STRING);
                rfd.DateOfClaim = u.extractStringValue(dr["DateOfClaim"], Utility.DataType.DATE);
                rfd.CompressorComponentReference = u.extractStringValue(dr["CompressorComponentReference"], Utility.DataType.STRING);
                rfd.TypeOfReturn = u.extractStringValue(dr["TypeOfReturn"], Utility.DataType.STRING);
                rfd.Application = u.extractStringValue(dr["Application"], Utility.DataType.STRING);
                rfd.Refrigerant = u.extractStringValue(dr["Refrigerant"], Utility.DataType.STRING);
                rfd.OtherRefrigerant = u.extractStringValue(dr["OtherRefrigerant"], Utility.DataType.STRING);
                rfd.Request = u.extractStringValue(dr["Request"], Utility.DataType.STRING);
                rfd.RequestWarranty = Boolean.Parse(dr["RequestWarranty"].ToString());
                rfd.Analysis = Boolean.Parse(dr["Analysis"].ToString());
                rfd.CreditOfBody = Boolean.Parse(dr["CreditOfBody"].ToString());
                rfd.OutOfWarranty = Boolean.Parse(dr["OutOfWarranty"].ToString());
                rfd.SpecificTest = Boolean.Parse(dr["SpecificTest"].ToString());
                rfd.RequestSpecificTestRemarks = u.extractStringValue(dr["RequestSpecificTestRemarks"], Utility.DataType.STRING);
                rfd.Configuration = u.extractStringValue(dr["Configuration"], Utility.DataType.STRING);
                rfd.DateOfFailure = u.extractStringValue(dr["DateOfFailure"], Utility.DataType.DATE);
                rfd.RunningHours = u.extractStringValue(dr["RunningHours"], Utility.DataType.STRING);
                rfd.Remarks = u.extractStringValue(dr["Remarks"], Utility.DataType.STRING);
                rfd.FilePath = u.extractStringValue(dr["FilePath"], Utility.DataType.STRING);
                rfd.CustomerNumber = dr["CustomerNumber"].ToString();
                rfd.ItemNumber = u.extractStringValue(dr["ItemNumber"], Utility.DataType.STRING);
                rfd.WorkingPointTo = u.extractStringValue(dr["WorkingPointTo"], Utility.DataType.DATE);
                rfd.WorkingPointTc = u.extractStringValue(dr["WorkingPointTc"], Utility.DataType.DATE);
                rfd.WorkingPointSuperHeat = u.extractStringValue(dr["SuperHeat"], Utility.DataType.DATE);
                rfd.WorkingPointSubcooling = u.extractStringValue(dr["SubCooling"], Utility.DataType.DATE);
                rfd.OracleNumber = u.extractStringValue(dr["OracleNumber"], Utility.DataType.STRING);
                rfd.RFDetailID = int.Parse(u.extractStringValue(dr["RFDetailID"], Utility.DataType.STRING));

            }
            return rfd;
        }

        // Added compReportControlID for compressor with multiple controlID
        public ReportBook printFormDetail(string _controlID, string compReportControlID,Boolean isNew = false)
        {
            ReportBook reportBook = new ReportBook();

            try
            {

                //For mulitple controlID
                int[] controlIDs = compReportControlID.Split(',').Select(int.Parse).ToArray();

                for (int x = 0; x < controlIDs.Count(); x++)
                {
                    var xmlSerializer = new Telerik.Reporting.XmlSerialization.ReportXmlSerializer();
                    var server = HttpContext.Current.Server;
                    var reportDocument = (Telerik.Reporting.Report)xmlSerializer.Deserialize(server.MapPath("~/resources/reports/EuropeCustomerQualityPortal.trdx"));

                    ArrayList partsReferencesCollection = new ArrayList();

                    // Logic Ends here : datasource
                    Model.ReturnFormDetail objReturnFormDetail = new Model.ReturnFormDetail();
                    ReturnFormDetail returnFormDetail = new ReturnFormDetail();
                    objReturnFormDetail = returnFormDetail.getReturnFormDetail(int.Parse(controlIDs[x].ToString())); //_controlID
                    Model.ReturnFormHeader rfh = new Model.ReturnFormHeader();

                    reportDocument.DataSource = objReturnFormDetail;

                    // On New control created, not add this report. 
                    if (!isNew)
                        reportBook.Reports.Add(reportDocument);

                    var reportDocument2 = (Telerik.Reporting.Report)xmlSerializer.Deserialize(server.MapPath("~/resources/reports/printCompressor" + ((isNew) ? " - on new" : "") + ".trdx"));
                    reportDocument2.DataSource = returnFormDetail.getReturnFormDetailArray(objReturnFormDetail.RFHeaderID, ((isNew) ? 0 : objReturnFormDetail.RFDetailID));
                    reportBook.Reports.Add(reportDocument2);
                }

                
            }
            catch (Exception ex)
            { }
           

        
            return reportBook;
        }

        public ArrayList getReturnFormDetailArray(int headerID, int detailID = 0)
        {
            ConnectionMaster cm = new ConnectionMaster();
            Utility u = new Utility();
            StringBuilder query = new StringBuilder();
            query.Append(" SELECT rc.RFHeaderID,RFDetailID, ProductType,Claim,rc.LocationID,ClaimDescription,CustomerClaimReference,");
            query.Append(" SerialNumber, Model, DataCode, CONVERT(VARCHAR,Warranty,103) AS 'Warranty', Quantity,");
            query.Append(" CONVERT(VARCHAR,DateOfClaim,103) AS 'DateOfClaim',  TypeOfReturn,");
            query.Append(" Application, Refrigerant, OtherRefrigerant, WorkingPointTO,WorkingPointTC, ");
            query.Append(" Request, RequestWarranty, RequestSpecificTestRemarks, Configuration,");
            query.Append(" CONVERT(VARCHAR,DateOfFailure,103) AS 'DateOfFailure', RunningHours,");
            query.Append(" Remarks,FilePath,rc.CustomerNumber + ' - ' + c.CustomerName [Customer], rc.CustomerNumber,");
            query.Append(" OracleNumber, cl.City,cl.State, cl.ZipCode,cl.Country,c.CustomerName,RFH.ReferenceNumber,");
            query.Append(" (CAST(rc.LocationID as varchar) + ' - ' +  ISNULL(City,'') + ', ' + ISNULL(State,'') + ', ' + ISNULL(ZipCode,'')) LocationAddress");
            query.Append(" FROM ReturnFormDetail rc  INNER JOIN CustomerLocation cl");
            query.Append(" ON rc.LocationID = cl.LocationID ");
            query.Append(" INNER JOIN (SELECT DISTINCT CustomerNumber,CustomerName,LocationId FROM customer) c ");
            query.Append(" ON rc.LocationId = c.LocationId");
            query.Append(" INNER JOIN ReturnFormHeader RFH ON RFH.RFHeaderID = rc.RFHeaderId");
            query.Append(" WHERE rc.RFHeaderID = " + headerID);

            if (detailID != 0)
                query.Append(" AND RFDetailID = " + detailID);

            DataTable dt = cm.retrieveDataTable(query.ToString());
            ArrayList result = new ArrayList();
            //int count = dt.Rows.Count;
            foreach (DataRow dr in dt.Rows)
            {
                Model.ReturnFormDetail rfd = new Model.ReturnFormDetail();
                rfd.RFHeaderID = int.Parse(u.extractStringValue(dr["RFHeaderID"], Utility.DataType.NUMBER));
                rfd.RFDetailID = int.Parse(u.extractStringValue(dr["RFDetailID"], Utility.DataType.NUMBER));
                rfd.ProductType = u.extractStringValue(dr["ProductType"], Utility.DataType.STRING);
                rfd.Claim = u.extractStringValue(dr["Claim"], Utility.DataType.STRING);

                //CUSTOMER LOCATION FIELDS
                rfd.LocationAddress = u.extractStringValue(dr["LocationAddress"], Utility.DataType.STRING);
                rfd.LocationID = u.extractStringValue(dr["LocationID"], Utility.DataType.STRING);
                //rfd.CustomerName = u.extractStringValue(dr["CustomerName"], Utility.DataType.STRING);
                rfd.City = u.extractStringValue(dr["City"], Utility.DataType.STRING);
                rfd.State = u.extractStringValue(dr["State"], Utility.DataType.STRING);
                rfd.ZipCode = u.extractStringValue(dr["ZipCode"], Utility.DataType.STRING);
                rfd.Country = u.extractStringValue(dr["Country"], Utility.DataType.STRING);
                //
               
                rfd.ClaimDescription = u.extractStringValue(dr["ClaimDescription"], Utility.DataType.STRING);
                rfd.CustomerClaimReference = u.extractStringValue(dr["CustomerClaimReference"], Utility.DataType.STRING);
               
                rfd.SerialNumber = u.extractStringValue(dr["SerialNumber"],Utility.DataType.STRING);
                rfd.ModelProduct = u.extractStringValue(dr["Model"], Utility.DataType.STRING);
                rfd.DataCode = u.extractStringValue(dr["DataCode"], Utility.DataType.STRING);
                rfd.Warranty = u.extractStringValue(dr["Warranty"], Utility.DataType.STRING);
                rfd.Quantity = u.extractStringValue(dr["Quantity"], Utility.DataType.STRING);
                rfd.DateOfClaim = u.extractStringValue(dr["DateOfClaim"], Utility.DataType.DATE);
              
                rfd.TypeOfReturn = u.extractStringValue(dr["TypeOfReturn"], Utility.DataType.STRING);
                rfd.Application = u.extractStringValue(dr["Application"], Utility.DataType.STRING);
                rfd.Refrigerant = u.extractStringValue(dr["Refrigerant"], Utility.DataType.STRING);
                rfd.OtherRefrigerant = u.extractStringValue(dr["OtherRefrigerant"], Utility.DataType.STRING);
                rfd.WorkingPointTo = u.extractStringValue(dr["WorkingPointTO"], Utility.DataType.DATE);
                rfd.WorkingPointTc = u.extractStringValue(dr["WorkingPointTC"], Utility.DataType.DATE);
               
               
                rfd.Request = u.extractStringValue(dr["Request"], Utility.DataType.STRING);
                rfd.RequestWarranty = Boolean.Parse(dr["RequestWarranty"].ToString());
                rfd.RequestSpecificTestRemarks = u.extractStringValue(dr["RequestSpecificTestRemarks"], Utility.DataType.STRING);
                rfd.Configuration = u.extractStringValue(dr["Configuration"], Utility.DataType.STRING);
                rfd.DateOfFailure = u.extractStringValue(dr["DateOfFailure"], Utility.DataType.DATE);
                rfd.RunningHours = u.extractStringValue(dr["RunningHours"], Utility.DataType.STRING);
                rfd.Remarks = u.extractStringValue(dr["Remarks"], Utility.DataType.STRING);
                rfd.FilePath = u.extractStringValue(dr["FilePath"], Utility.DataType.STRING);
                rfd.CustomerNumber = dr["CustomerNumber"].ToString();
                //rfd.ItemNumber = u.extractStringValue(dr["ItemNumber"], Utility.DataType.STRING);
                rfd.CustomerName = u.extractStringValue(dr["Customer"], Utility.DataType.STRING);
                rfd.OracleNumber = u.extractStringValue(dr["OracleNumber"], Utility.DataType.STRING);
                rfd.PortalReference = u.extractStringValue(dr["ReferenceNumber"], Utility.DataType.STRING);

                // Please ship to:
                rfd.ProductLocation = GetProductLocation(1); //compressor
                rfd.ProductLocation = rfd.ProductLocation.Replace("\n", "<br/>");

                // Add Contact Information for the Print
                string contactinfo = "";
                Maintenance.ReturnFormDetail mrfd = new Maintenance.ReturnFormDetail();
                DataTable dtc = mrfd.getContactPerson();
                contactinfo += "If you have question or concerns about this return, please contact " + dtc.Rows[0]["Name"];
                contactinfo += "<br/>Email Address: " + dtc.Rows[0]["EmailAddress"] + " Phone Number: " + dtc.Rows[0]["PhoneNumber"];
                rfd.ContactInformation = contactinfo;

                result.Add(rfd);
            }
            return result;
        }
        //[END] [jpbersonda] [5/18/2017] [NEW - For print function]

        public static string GetProductLocation(int productTypeId)
        {
            string productLocation = string.Empty;
            ConnectionMaster cm = new ConnectionMaster();
            StringBuilder queryBuilder = new StringBuilder();
            try
            {
                queryBuilder.Append("SELECT Location FROM ProductType WHERE ID = " + productTypeId);

                DataTable dt = cm.retrieveDataTable(queryBuilder.ToString());

                productLocation = dt.Rows[0]["Location"].ToString();
                dt = null;
            }
            catch (Exception ex) { }

            queryBuilder = null;
            cm = null;
           

            return productLocation;
        }

    }
}