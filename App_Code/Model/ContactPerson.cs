﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ContactPerson
/// </summary>
/// 
namespace Model
{
    public class ContactPerson
    {
        private int iD;
        private int rfHeaderID;
        private string name;
        private string emailAddress;
        private string phoneNumber;

        public int ID { get { return this.iD; } set { this.iD = value; } }
        public int RFHeaderID { get { return this.rfHeaderID; } set { this.rfHeaderID = value; } }
        public string Name { get { return this.name; } set { this.name = value; } }
        public string EmailAddress { get { return this.emailAddress; } set { this.emailAddress = value; } }
        public string PhoneNumber { get { return this.phoneNumber; } set { this.phoneNumber = value; } }

        public ContactPerson() { }
        public ContactPerson(
            int _iD
            , int _rfHeaderID
            , string _name
            , string _emailAddress
            , string _phoneNumber
            )
        {

            this.iD = _iD;
            this.rfHeaderID = _rfHeaderID;
            this.name = _name;
            this.emailAddress = _emailAddress;
            this.phoneNumber = _phoneNumber;
        }
    }
}
