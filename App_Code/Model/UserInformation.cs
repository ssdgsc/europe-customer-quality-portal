﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Sql;
using System.Data.SqlClient;

/// <summary>
/// Summary description for UserInformation
/// </summary>
/// 
namespace Model
{
    public class UserInformation
    {
        private int userID;    
        private string customerName;
        private int userTypeID;
        private string userType;
        private string cso;
        private string companyName;
        private string username;
        private string userFirstName;
        private string userLastName;
        private string userFullName;
        private string department;
        private string phoneNumber;
        private string emailAddress;
        private string login;
        private string userPassword;
        private string backupName;
        private string backupFirstName;
        private string backupEmailAddress;
        
        private bool isfirstlogin;
        private string function;
        private string title;
        private bool receptionactivated;
        private bool analysisactivated;
        private bool cdactivated;
        private string userTitle;

        public int UserID { get { return this.userID; } set { this.userID = value; } }
        public string CustomerName { get { return this.customerName; } set { this.customerName = value; } }
        public int UserTypeID { get { return this.userTypeID; } set { this.userTypeID = value; } }
        public string UserType { get { return this.userType.ToUpper(); ; } set { this.userType = value; } }
        public string CSO { get { return this.cso; } set { this.cso = value; } }
        public string CompanyName { get { return this.companyName; } set { this.companyName = value; } }
        public string Username { get { return this.username; } set { this.username = value; } }
        public string UserFirstName { get { return this.userFirstName; } set { this.userFirstName = value; } }
        public string UserLastName { get { return this.userLastName; } set { this.userLastName = value; } }
        public string UserFullName { get { return this.userFullName; } set { this.userFullName = value; } }
        public string Department { get { return department; } set { department = value; } }
        public string PhoneNumber { get { return phoneNumber; } set { phoneNumber = value; } }
        public string EmailAddress { get { return this.emailAddress; } set { this.emailAddress = value; } }
        public string Login { get { return login; } set { login = value; } }
        public string UserPassword { get { return this.userPassword; } set { this.userPassword = value; } }
        public string BackupName { get { return this.backupName; } set { this.backupName = value; } }
        public string BackupFirstName { get { return this.backupFirstName; } set { this.backupFirstName = value; } }
        public string BackupEmailAddress { get { return this.backupEmailAddress; } set { this.backupEmailAddress = value; } }
        public string UserTitle { get { return this.userTitle; } set { this.userTitle = value; } }

        public bool IsFirstLogin { get { return this.isfirstlogin; } set { this.isfirstlogin = value; } }
        public string Function { get { return this.function; } set { this.function = value; } }
        public string Title { get { return this.title; } set { this.title = value; } }
        public bool ReceptionActivated { get { return this.receptionactivated; } set { this.receptionactivated = value; } }
        public bool AnalysisActivated { get { return this.analysisactivated; } set { this.analysisactivated = value; } }
        public bool CDActivated { get { return this.cdactivated; } set { this.cdactivated = value; } }

        public SqlParameter userIDParam = new SqlParameter();
        public SqlParameter customerNameParam = new SqlParameter();
        public SqlParameter userTypeParam = new SqlParameter();
        public SqlParameter companyNameParam = new SqlParameter();
        public SqlParameter usernameParam = new SqlParameter();
        public SqlParameter userFirstNameParam = new SqlParameter();
        public SqlParameter userLastNameParam = new SqlParameter();
        public SqlParameter userFullNameParam = new SqlParameter();
        public SqlParameter departmentParam = new SqlParameter();
        public SqlParameter phoneNumberParam = new SqlParameter();
        public SqlParameter emailAddressParam = new SqlParameter();
        public SqlParameter loginParam = new SqlParameter();
        public SqlParameter userPasswordParam = new SqlParameter();
        public SqlParameter backupNameParam = new SqlParameter();
        public SqlParameter backupFirstNameParam = new SqlParameter();
        public SqlParameter backupEmailAddressParam = new SqlParameter();
        public SqlParameter isFirstLoginParam = new SqlParameter();

        public UserInformation()
        {
            userIDParam.ParameterName = "@userID";
            customerNameParam.ParameterName = "@customerName";
            userTypeParam.ParameterName = "@userType";
            companyNameParam.ParameterName = "@companyName";
            usernameParam.ParameterName = "@username";
            userFirstNameParam.ParameterName = "@userFirstName";
            userLastNameParam.ParameterName = "@userLastName";
            userFullNameParam.ParameterName = "@userFullName";
            departmentParam.ParameterName = "@department";
            phoneNumberParam.ParameterName = "@phoneNumber";
            emailAddressParam.ParameterName = "@emailAddress";
            loginParam.ParameterName = "@login";
            userPasswordParam.ParameterName = "@userPassword";
            backupNameParam.ParameterName = "@backupName";
            backupFirstNameParam.ParameterName = "@backupFirstName";
            backupEmailAddressParam.ParameterName = "@backupEmailAddress";
            isFirstLoginParam.ParameterName = "@isFirstLoginParam";
        }

        public UserInformation
        (
            int _userID,
            string _customerName,
            string _userType,
            string _companyName,
            string _username,
            string _userFirstName,
            string _userLastName,
            string _userFullName,
            string _department,
            string _phoneNumber,
            string _emailAddress,
            string _login,
            string _userPassword,
            string _backupName,
            string _backupFirstName,
            string _backupEmailAddress,
            bool _isFirstLogin
        ) 
        {
            this.UserID = _userID;
            this.CustomerName = _customerName;
            this.UserType = _userType;
            this.CompanyName = _companyName;
            this.Username = _username;
            this.UserFirstName = _userFirstName;
            this.UserLastName = _userLastName;
            this.UserFullName = _userFullName;
            this.Department = _department;
            this.PhoneNumber = _phoneNumber;
            this.EmailAddress = _emailAddress;
            this.Login = _login;
            this.UserPassword = _userPassword;
            this.BackupName = _backupName;
            this.BackupFirstName = _backupFirstName;
            this.BackupEmailAddress = _backupEmailAddress;
            this.isfirstlogin = _isFirstLogin;
        }

    }
}