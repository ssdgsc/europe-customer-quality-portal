﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Customers
/// </summary>
/// 
namespace Model
{
    public class Customers
    {

        private int customerID;
        private string customerNumber;

        private string locationID;

        private string customerName;
        private string salesOfficeCode;
        private string created;
        private string modified;

        private string address;
        private string city;
        private string state;
        private string zipCode;
        private string country;

        private bool control;
        private bool compressor;
        private bool accessories;
        private string customerEmail;

        public int CustomerID { get { return this.customerID; } set { this.customerID = value; } }
        public string CustomerNumber { get { return this.customerNumber; } set { this.customerNumber = value; } }
        public string LocationID { get { return this.locationID; } set { this.locationID = value; } }
        public string CustomerName { get { return this.customerName; } set { this.customerName = value; } }
        public string SalesOfficeCode { get { return this.salesOfficeCode; } set { this.salesOfficeCode = value; } }
        public string Created { get { return this.created; } set { this.created = value; } }
        public string Modified { get { return this.modified; } set { this.modified = value; } }
        public string Address { get { return this.address; } set { this.address = value; } }
        public string City { get { return this.city; } set { this.city = value; } }
        public string State { get { return this.state; } set { this.state = value; } }
        public string ZipCode { get { return this.zipCode; } set { this.zipCode = value; } }
        public string Country { get { return this.country; } set { this.country = value; } }
        public bool Control { get { return this.control; } set { this.control = value; } }
        public bool Compressor { get { return this.compressor; } set { this.compressor = value; } }
        public bool Accessories { get { return this.accessories; } set { this.accessories = value; } }
        public string CustomerEmail { get { return this.customerEmail; } set { this.customerEmail = value; } }

        public Customers() { }
        public Customers(
            int _customerID
            , string _customerNumber
            , string _customerName
            , string _salesOfficeCode
            , string _created
            , string _modified
            , bool _control
            , bool _compressor)
        {
            this.customerID = _customerID;
            this.customerNumber = _customerNumber;
            this.customerName = _customerName;
            this.salesOfficeCode = _salesOfficeCode;
            this.created = _created;
            this.modified = _modified;
            this.control = _control;
            this.compressor = _compressor;
        }
    }
}