﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ReturnFormControl
/// </summary>

namespace Model
{
    public class ReturnFormControl
    {
	    private int rfDetailID;
        //CUSTOMER LOCATION FIELDS
        private string customerName;
        private string locationID;
        private string street;
        private string state;
        private string zipCode;
        private string city;
        private string country;
        //
        private int claimID;
        private int rfHeaderID;
        private string productType;
        private string claim;
        private string locationAddress;
        private string claimDescription;
        private string controlCustomerClaimReference;
        private string pcnNumber;
        private string modelProduct;
        private string productGroup;
        private string line;
        private string dataCode;
        private string warranty;
        private string quantity;
        private string dateOfClaim;
        private string typeOfReturn;
        private string application;
        private string refrigerant;
        private string otherRefrigerant;
        private string workingPointTO;
        private string workingPointTC;
        private string workingPointSuperHeat;
        private string workingPointSubCooling;
        private string request;
        private bool requestWarranty;
        private bool requestAnalysis;
        private bool requestCreditOfBody;
        private bool requestOutOfWarranty;
        private bool requestSpecificTest;
        private string requestSpecificTestRemarks;
        private string configuration;
        private string dateOfFailure;
        private string runningHours;
        private string dateOfPartIncome;
        private string currentWeekOfPartIncome;
        private string monthIncome;
        private string fiscalYearIncome;
        private string remarks;
        private string filePath;
        private string customerNumber;
        private string itemNumber;
        private string customer;
        private string controlComponentReference;
        private string referenceNo;
        private string returnDate;
        private string failureDescription;
        private string origin;
        private string causeOrGuilty;
        private string creditDecision;
        private string reportText;
        private string supplierAnalysis;
        private string partReturned;
        private string note;
        private string closedInKolin;
        private string closedInCurrentWeek;
        private string closedInMonthIncome;
        private string fiscalYearClosed;

        private string receivedInKolin;
        private string receivedInCurrentWeek;
        private string receivedInMonthIncome;
        private string fiscalYearReceived;

        private string openOrClosed;
        private string portalReference;
        private string portalReferenceHDR;
        private string oracleNumber; //NEW 
        private string contactInformation; //NEW 
        private string productLocation;

        public int RFDetailID { get { return this.rfDetailID; } set { this.rfDetailID = value; } }

        // CUSTOMER LOCATION FIELDS
        public string LocationID { get { return this.locationID; } set { this.locationID = value; } }
        public string CustomerName { get { return this.customerName; } set { this.customerName = value; } }
        public string State { get { return this.state; } set { this.state = value; } }
        public string Street { get { return this.street; } set { this.street = value; } }
        public string City { get { return this.city; } set { this.city = value; } }
        public string ZipCode { get { return this.zipCode; } set { this.zipCode = value; } }
        public string Country { get { return this.country; } set { this.country = value; } }
        //

        public int ClaimID { get { return this.claimID; } set { this.claimID = value; } }
        public int RFHeaderID { get { return this.rfHeaderID; } set { this.rfHeaderID = value; } }
        public string ProductType { get { return this.productType; } set { this.productType = value; } }
        public string Claim { get { return this.claim; } set { this.claim = value; } }
        public string LocationAddress { get { return this.locationAddress; } set { this.locationAddress = value; } }
        public string ClaimDescription { get { return this.claimDescription; } set { this.claimDescription = value; } }
        public string ControlCustomerClaimReference { get { return this.controlCustomerClaimReference; } set { this.controlCustomerClaimReference = value; } }
        public string PCNNumber { get { return this.pcnNumber; } set { this.pcnNumber = value; } }
        public string ModelProduct { get { return this.modelProduct; } set { this.modelProduct = value; } }
        public string ProductGroup { get { return this.productGroup; } set { this.productGroup = value; } }
        public string Line { get { return this.line; } set { this.line = value; } }
        public string DataCode { get { return this.dataCode; } set { this.dataCode = value; } }
        public string Warranty { get { return this.warranty; } set { this.warranty = value; } }
        public string Quantity { get { return this.quantity; } set { this.quantity = value; } }
        public string DateOfClaim { get { return this.dateOfClaim; } set { this.dateOfClaim = value; } }
        public string TypeOfReturn { get { return this.typeOfReturn; } set { this.typeOfReturn = value; } }
        public string Application { get { return this.application; } set { this.application = value; } }
        public string Refrigerant { get { return this.refrigerant; } set { this.refrigerant = value; } }
        public string OtherRefrigerant { get { return this.otherRefrigerant; } set { this.otherRefrigerant = value; } }
        public string WorkingPointTO { get { return workingPointTO; } set { workingPointTO = value; } }
        public string WorkingPointTC { get { return workingPointTC; } set { workingPointTC = value; } }
        public string WorkingPointSuperHeat { get { return workingPointSuperHeat; } set { workingPointSuperHeat = value; } }
        public string WorkingPointSubCooling { get { return workingPointSubCooling; } set { workingPointSubCooling = value; } }
        public string Request { get { return this.request; } set { this.request = value; } }
        public bool RequestWarranty { get { return this.requestWarranty; } set { this.requestWarranty = value; } }
        public bool RequestAnalysis { get { return this.requestAnalysis; } set { this.requestAnalysis = value; } }
        public bool RequestCreditOfBody { get { return this.requestCreditOfBody; } set { this.requestCreditOfBody = value; } }
        public bool RequestOutOfWarranty { get { return this.requestOutOfWarranty; } set { this.requestOutOfWarranty = value; } }
        public bool RequestSpecificTest { get { return this.requestSpecificTest; } set { this.requestSpecificTest = value; } }
        public string RequestSpecificTestRemarks { get { return this.requestSpecificTestRemarks; } set { this.requestSpecificTestRemarks = value; } }
        public string Configuration { get { return this.configuration; } set { this.configuration = value; } }
        public string DateOfFailure { get { return this.dateOfFailure; } set { this.dateOfFailure = value; } }
        public string RunningHours { get { return this.runningHours; } set { this.runningHours = value; } }
        public string DateOfPartIncome { get { return RemoveTimeOnDate(this.dateOfPartIncome); } set { this.dateOfPartIncome = value; } }
        public string CurrentWeekOfPartIncome { get { return this.currentWeekOfPartIncome; } set { this.currentWeekOfPartIncome = value; } }
        public string MonthIncome { get { return this.monthIncome; } set { this.monthIncome = value; } }
        public string FiscalYearIncome { get { return this.fiscalYearIncome; } set { this.fiscalYearIncome = value; } }
        public string Remarks { get { return this.remarks; } set { this.remarks = value; } }
        public string FilePath { get { return this.filePath; } set { this.filePath = value; } }
        public string CustomerNumber { get { return this.customerNumber; } set { this.customerNumber = value; } }
        public string ItemNumber { get { return this.itemNumber; } set { this.itemNumber = value; } }
        public string Customer { get { return this.customer; } set { this.customer = value; } }
        public string ControlComponentReference { get { return controlComponentReference; } set { controlComponentReference = value; } }

        

        public string ReferenceNo { get { return this.referenceNo; } set { this.referenceNo= value; } }
        public string ReturnDate { get { return this.returnDate; } set { this.returnDate = value; } }
        public string FailureDescription { get { return this.failureDescription; } set { this.failureDescription = value; } }
        public string Origin { get { return this.origin; } set { this.origin = value; } }
        public string CauseOrGuilty { get { return this.causeOrGuilty; } set { this.causeOrGuilty = value; } }
        public string CreditDecision { get { return this.creditDecision; } set { this.creditDecision = value; } }
        public string ReportText { get { return this.reportText; } set { this.reportText = value; } }
        public string SupplierAnalysis { get { return this.supplierAnalysis; } set { this.supplierAnalysis = value; } }
        public string PartReturned { get { return this.partReturned; } set { this.partReturned = value; } }
        public string Note { get { return this.note; } set { this.note = value; } }
        public string ClosedInKolin { get { return RemoveTimeOnDate(this.closedInKolin); } set { this.closedInKolin = value; } }
        public string ClosedInCurrentWeek { get { return this.closedInCurrentWeek; } set { this.closedInCurrentWeek = value; } }
        public string ClosedInMonthIncome { get { return this.closedInMonthIncome; } set { this.closedInMonthIncome = value; } }
        public string FiscalYearClosed { get { return this.fiscalYearClosed; } set { this.fiscalYearClosed = value; } }
        public string ReceivedInKolin { get { return RemoveTimeOnDate(this.receivedInKolin); } set { this.receivedInKolin = value; } }
        public string ReceivedInCurrentWeek { get { return this.receivedInCurrentWeek; } set { this.receivedInCurrentWeek = value; } }
        public string ReceivedInMonthIncome { get { return this.receivedInMonthIncome; } set { this.receivedInMonthIncome = value; } }
        public string FiscalYearReceived { get { return this.fiscalYearReceived; } set { this.fiscalYearReceived = value; } }
        public string OpenOrClosed { get { return this.openOrClosed; } set { this.openOrClosed = value; } }
        public string PortalReference { get { return this.portalReference; } set { this.portalReference = value; } }
        public string PortalReferenceHDR { get { return this.portalReferenceHDR; } set { this.portalReferenceHDR = value; } }
        public string OracleNumber { get { return this.oracleNumber; } set { this.oracleNumber = value; } }
        public string ContactInformation { get { return this.contactInformation; } set { this.contactInformation = value; } }
        public string ProductLocation { get { return this.productLocation; } set { this.productLocation = value; } }
        
        public ReturnFormControl() { }
        public ReturnFormControl(
            int _rfDetailID
            , int _claimID
            , int _rfHeaderID
            , string _productType
            , string _claim
            , string _claimDescription
            , string _controlCustomerClaimReference
            , string _pcnNumber
            , string _modelProduct
            , string _productGroup
            , string _line
            , string _dataCode
            , string _warranty
            , string _quantity
            , string _dateOfClaim
            , string _typeOfReturn
            , string _application
            , string _refrigerant
            , string _otherRefrigerant
            , string _workingPointTO
            , string _workingPointTC
            , string _workingPointSuperHeat
            , string _workingPointSubCooling
            , string _request
            , bool _requestWarranty
            , bool _requestAnalysis
            , bool _requestCreditOfBody
            , bool _requestOutOfWarranty
            , bool _requestSpecificTest
            , string _requestSpecificTestRemarks
            , string _configuration
            , string _dateOfFailure
            , string _runningHours
            , string _dateOfPartIncome
            , string _remarks
            , string _filePath
            , string _customerNumber
            , string _itemNumber
            , string _customer
            , string _controlComponentReference
            , string _referenceNo 
            , string _returnDate
            , string _failureDescription
            , string _origin
            , string _causeOrGuilty
            , string _creditDecision
            , string _reportText
            , string _supplierAnalysis
            , string _partReturned
            , string _note
            , string _closedInKolin
            , string _closedInCurrentWeek
            , string _closedInMonthIncome
            , string _fiscalYearClosed
            , string _openOrClosed
            )
        {
            this.rfDetailID = _rfDetailID;
            this.claimID = _claimID;
            this.rfHeaderID = _rfHeaderID;
            this.productType = _productType;
            this.claim = _claim;
            this.claimDescription = _claimDescription;
            this.controlCustomerClaimReference = _controlCustomerClaimReference;
            this.pcnNumber = _pcnNumber;
            this.modelProduct = _modelProduct;
            this.productGroup = _productGroup;
            this.line = _line;
            this.dataCode = _dataCode;
            this.warranty = _warranty;
            this.quantity = _quantity;
            this.dateOfClaim = _dateOfClaim;
            this.typeOfReturn = _typeOfReturn;
            this.application = _application;
            this.refrigerant = _refrigerant;
            this.otherRefrigerant = _otherRefrigerant;
            this.workingPointTO = _workingPointTO;
            this.workingPointTC = _workingPointTC;
            this.workingPointSuperHeat = _workingPointSuperHeat;
            this.workingPointSubCooling = _workingPointSubCooling;
            this.request = _request;
            this.requestWarranty = _requestWarranty; ;
            this.requestAnalysis = _requestAnalysis;
            this.requestCreditOfBody = _requestCreditOfBody;
            this.requestOutOfWarranty = _requestOutOfWarranty;
            this.requestSpecificTest = _requestSpecificTest;
            this.requestSpecificTestRemarks = _requestSpecificTestRemarks;
            this.configuration = _configuration;
            this.dateOfFailure = _dateOfFailure;
            this.runningHours = _runningHours;
            this.dateOfPartIncome = _dateOfPartIncome;
            this.remarks = _remarks;
            this.filePath = _filePath;
            this.customerNumber = _customerNumber;
            this.itemNumber = _itemNumber;
            this.customer = _customer;
            this.controlComponentReference = _controlComponentReference;
            this.referenceNo = _referenceNo;
            this.returnDate = _returnDate;
            this.failureDescription = _failureDescription;
            this.origin = _origin;
            this.causeOrGuilty = _causeOrGuilty;
            this.creditDecision = _creditDecision;
            this.reportText = _reportText;
            this.supplierAnalysis = _supplierAnalysis;
            this.partReturned = _partReturned;
            this.note = _note;
            this.closedInKolin = _closedInKolin;
            this.closedInCurrentWeek = _closedInCurrentWeek;
            this.closedInMonthIncome = _closedInMonthIncome;
            this.fiscalYearClosed = _fiscalYearClosed;
            this.openOrClosed = _openOrClosed;
        }

        public string RemoveTimeOnDate(string _value){
            if (_value==null)
            {
                return null;
            }else{
                return _value.Replace("12:00:00 AM", "");
            }
        }
    }

}