﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ReturnFormHeader
/// </summary>
/// 
namespace Model
{
    public class ReturnFormHeader
    {
        private int rfHeaderID;
        private string referenceNumber;
        private string description;
        private string returnDate;

        public int RFHeaderID { get { return this.rfHeaderID; } set { this.rfHeaderID = value; } }
        public string ReferenceNumber { get { return this.referenceNumber; } set { this.referenceNumber = value; } }
        public string Description { get { return this.description; } set { this.description = value; } }
        public string ReturnDate { get { return this.returnDate; } set { this.returnDate = value; } }

        public ReturnFormHeader() { }
        public ReturnFormHeader(
            int _rfHeaderID
            , string _referenceNumber
            , string _description
            , string _returnDate
            )
        {

            this.rfHeaderID = _rfHeaderID;
            this.referenceNumber = _referenceNumber;
            this.description = _description;
            this.returnDate = _returnDate;
        }
    }
}
