﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using General;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for Param
/// </summary>
public class Param
{
	public Param() { }

    public void getParams(DropDownList dd,string paramgroup)
    {
        ConnectionMaster cm = new ConnectionMaster();
        string sql = ""; 
        sql = "SELECT PARAMVALUE,PARAMDESC FROM tblParams WHERE PARAMGROUP = '" + paramgroup + "' ORDER BY PARAMDESC";
        DataTable dt = new DataTable(); 
        dd.DataSource = cm.retrieveDataTable(sql);
        dd.DataValueField = "PARAMVALUE";
        dd.DataTextField = "PARAMDESC";
        dd.DataBind();


    }

}