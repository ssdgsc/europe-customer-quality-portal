﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ReturnFormDetail
/// </summary>
/// 
namespace Model
{
    public class ReturnFormDetail
    {
        //CUSTOMER LOCATION FIELDS
        private string customerName;
        private string street;
        private string state;
        private string zipCode;
        private string city;
        private string country;
        private string locationAddress;


        private int rfDetailID;
        private string locationID;
        private int claimID;
        private int rfHeaderID;
        private string productType;
        private string claim;
        private string claimDescription;
        private string customerClaimReference;
        private string serialNumber;
        private string modelProduct;
        private string dataCode;
        private string warranty;
        private string quantity;
        private string dateOfClaim;
        private string typeOfReturn;
        private string application;
        private string refrigerant;
        private string otherRefrigerant;
        private string request;
        private bool requestWarranty;
        private bool analysis;
        private bool creditOfBody;
        private bool outOfWarranty;
        private bool specificTest;
        private string specificTestRemarks;
        private string requestSpecificTestRemarks;
        private string configuration;
        private string dateOfFailure;
        private string runningHours;
        private string remarks;
        private string filePath;
        private string customerNumber;
        private string itemNumber;
        private string compressorComponentReference;

        private string workingPointTo;
        private string workingPointTc;
        private string workingPointSuperHeat;
        private string workingPointSubcooling;

        private string referenceNo;
        private string returnDate;
        private string oracleNumber;
        private string contactInformation; //NEW 
        private string portalReference;
        private string productLocation;

        // CUSTOMER LOCATION FIELDS
      
        public string CustomerName { get { return this.customerName; } set { this.customerName = value; } }
        public string State { get { return this.state; } set { this.state = value; } }
        public string Street { get { return this.street; } set { this.street = value; } }
        public string City { get { return this.city; } set { this.city = value; } }
        public string ZipCode { get { return this.zipCode; } set { this.zipCode = value; } }
        public string Country { get { return this.country; } set { this.country = value; } }
        public string LocationAddress { get { return this.locationAddress; } set { this.locationAddress = value; } }

        public string PortalReference { get { return this.portalReference; } set { this.portalReference = value; } }
        public int RFDetailID { get { return this.rfDetailID; } set { this.rfDetailID = value; } }
        public string LocationID { get { return this.locationID; } set { this.locationID = value; } }
        public int ClaimID { get { return this.claimID; } set { this.claimID = value; } }
        public int RFHeaderID { get { return this.rfHeaderID; } set { this.rfHeaderID = value; } }
        public string ProductType { get { return this.productType; } set { this.productType = value; } }
        public string Claim { get { return this.claim; } set { this.claim = value; } }
        public string ClaimDescription { get { return this.claimDescription; } set { this.claimDescription = value; } }
        public string CustomerClaimReference { get { return this.customerClaimReference; } set { this.customerClaimReference = value; } }
        public string SerialNumber { get { return this.serialNumber; } set { this.serialNumber = value; } }
        public string ModelProduct { get { return this.modelProduct; } set { this.modelProduct = value; } }
        public string DataCode { get { return this.dataCode; } set { this.dataCode = value; } }
        public string Warranty { get { return this.warranty; } set { this.warranty = value; } }
        public string Quantity { get { return this.quantity; } set { this.quantity = value; } }
        public string DateOfClaim { get { return this.dateOfClaim; } set { this.dateOfClaim = value; } }
        public string TypeOfReturn { get { return this.typeOfReturn; } set { this.typeOfReturn = value; } }
        public string Application { get { return this.application; } set { this.application = value; } }
        public string Refrigerant { get { return this.refrigerant; } set { this.refrigerant = value; } }
        public string Request { get { return this.request; } set { this.request = value; } }
        public string OtherRefrigerant { get { return this.otherRefrigerant; } set { this.otherRefrigerant = value; } }
        public bool RequestWarranty { get { return this.requestWarranty; } set { this.requestWarranty = value; } }
        public bool Analysis { get { return this.analysis; } set { this.analysis = value; } }
        public bool CreditOfBody { get { return this.creditOfBody; } set { this.creditOfBody = value; } }
        public bool OutOfWarranty { get { return this.outOfWarranty; } set { this.outOfWarranty = value; } }
        public bool SpecificTest { get { return this.specificTest; } set { this.specificTest = value; } }
        public string SpecificTestRemarks { get { return this.specificTestRemarks; } set { this.specificTestRemarks = value; } }
        public string RequestSpecificTestRemarks { get { return this.requestSpecificTestRemarks; } set { this.requestSpecificTestRemarks = value; } }
        public string Configuration { get { return this.configuration; } set { this.configuration = value; } }
        public string DateOfFailure { get { return this.dateOfFailure; } set { this.dateOfFailure = value; } }
        public string RunningHours { get { return this.runningHours; } set { this.runningHours = value; } }
        public string Remarks { get { return this.remarks; } set { this.remarks = value; } }
        public string FilePath { get { return this.filePath; } set { this.filePath = value; } }
        public string CustomerNumber { get { return this.customerNumber; } set { this.customerNumber = value; } }
        public string ItemNumber { get { return this.itemNumber; } set { this.itemNumber = value; } }
        public string CompressorComponentReference { get { return compressorComponentReference; } set { compressorComponentReference = value; } }

        public string WorkingPointTo { get { return workingPointTo; } set { workingPointTo = value; } }
        public string WorkingPointTc { get { return workingPointTc; } set { workingPointTc = value; } }
        public string WorkingPointSuperHeat { get { return workingPointSuperHeat; } set { workingPointSuperHeat = value; } }
        public string WorkingPointSubcooling { get { return workingPointSubcooling; } set { workingPointSubcooling = value; } }

        public string ReferenceNo { get { return this.referenceNo; } set { this.referenceNo= value; } }
        public string ReturnDate { get { return this.returnDate; } set { this.returnDate = value; } }
        public string OracleNumber { get { return this.oracleNumber; } set { this.oracleNumber = value; } }

        public string ContactInformation { get { return this.contactInformation; } set { this.contactInformation = value; } }
        public string ProductLocation { get { return this.productLocation; } set { this.productLocation = value; } }

        public ReturnFormDetail() { }
        public ReturnFormDetail(
            int _rfDetailID
            , int _claimID
            , int _rfHeaderID
            , string _productType
            , string _claim
            , string _claimDescription
            , string _customerClaimReference
            , string _serialNumber
            , string _modelProduct
            , string _dataCode
            , string _warranty
            , string _quantity
            , string _dateOfClaim
            , string _typeOfReturn
            , string _application
            , string _refrigerant
            , string _otherRefrigerant
            , string _request
            , bool _requestWarranty
            , bool _analysis
            , bool _creditOfBody
            , bool _outOfWarranty
            , bool _specificTest
            , string _specificTestRemarks
            , string _requestSpecificTestRemarks
            , string _configuration
            , string _dateOfFailure
            , string _runningHours
            , string _remarks
            , string _filePath
            , string _customerNumber
            , string _itemNumber
            , string _compressorComponentReference
            , string _referenceNo 
            , string _returnDate

            , string _workingPointTo
            , string _workingPointTc
            , string _workingPointSuperHeat
            , string _workingPointSubcooling
            )
        {

            this.rfDetailID = _rfDetailID;
            this.claimID = _claimID;
            this.rfHeaderID = _rfHeaderID;
            this.productType = _productType;
            this.claim = _claim;
            this.claimDescription = _claimDescription;
            this.customerClaimReference = _customerClaimReference;
            this.serialNumber = _serialNumber;
            this.modelProduct = _modelProduct;
            this.dataCode = _dataCode;
            this.warranty = _warranty;
            this.quantity = _quantity;
            this.dateOfClaim = _dateOfClaim;
            this.typeOfReturn = _typeOfReturn;
            this.application = _application;
            this.refrigerant = _refrigerant;
            this.otherRefrigerant = _otherRefrigerant;
            this.request = _request;
            this.requestWarranty = _requestWarranty; ;
            this.analysis = _analysis;
            this.creditOfBody = _creditOfBody;
            this.outOfWarranty = _outOfWarranty;
            this.specificTest = _specificTest;
            this.specificTestRemarks = _specificTestRemarks;
            this.requestSpecificTestRemarks = _requestSpecificTestRemarks;
            this.configuration = _configuration;
            this.dateOfFailure = _dateOfFailure;
            this.runningHours = _runningHours;
            this.remarks = _remarks;
            this.filePath = _filePath;
            this.customerNumber = _customerNumber;
            this.itemNumber = _itemNumber;
            this.compressorComponentReference = _compressorComponentReference;
            this.referenceNo = _referenceNo;
            this.returnDate = _returnDate;
            this.workingPointTo = _workingPointTo;
            this.workingPointTc = _workingPointTc;
            this.workingPointSuperHeat = _workingPointSuperHeat;
            this.workingPointSubcooling = _workingPointSubcooling;
        }
    }
}
