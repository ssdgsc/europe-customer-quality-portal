﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CustomerLocation
/// </summary>
public class CustomerLocation
{
    private string locationID;
    private string locationName;
    private string customerNumber;
    private string address;
    private string city;
    private string state;
    private string zipCode;
    private string country;
    
	public CustomerLocation()
	{ }

    public string LocationID { get { return this.locationID; } set { this.locationID = value; } }
    public string LocationName { get { return this.locationName; } set { this.locationName = value; } }
    public string CustomerNumber { get { return this.customerNumber; } set { this.customerNumber = value; } }
    public string Address { get { return this.address; } set { this.address = value; } }
    public string City { get { return this.city; } set { this.city = value; } }
    public string State { get { return this.state; } set { this.state = value; } }
    public string ZipCode { get { return this.zipCode; } set { this.zipCode = value; } }
    public string Country { get { return this.country; } set { this.country = value; } }
}