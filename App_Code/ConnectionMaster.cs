﻿using System;
using System.Data;
using System.Web;
using System.Data.SqlClient;

namespace General
{
    public class ConnectionMaster
    {
        private string connectionString = "";
        public ConnectionMaster()
        {
            this.connectionString = System.Configuration.ConfigurationSettings.AppSettings["DBConnectionString"];
        }
        /// <returns>DataTable</returns>
        public DataTable retrieveDataTable(string _sqlString)
        {

            SqlConnection sqlCon = new SqlConnection(this.connectionString);
            sqlCon.Open();

            SqlCommand sqlCom = new SqlCommand(_sqlString, sqlCon);
            SqlDataReader reader = sqlCom.ExecuteReader();

            DataTable ret = new DataTable();
            ret.Load(reader);

            sqlCom.Dispose();
            reader.Close();
            reader.Dispose();
            sqlCon.Close();
            sqlCon.Dispose();
            return ret;
        }
        public long executeCommand(string _sqlString, bool retrieveID)
        {
            SqlConnection sqlCon = new SqlConnection(this.connectionString);
            sqlCon.Open();
            long ret = 0;

            if (retrieveID == true)
            {
                _sqlString += "; SELECT SCOPE_IDENTITY();";
                SqlCommand sqlCom = new SqlCommand(_sqlString, sqlCon);
                ret = Convert.ToInt64(sqlCom.ExecuteScalar());
                sqlCom.Dispose();
                sqlCon.Close();
                sqlCon.Dispose();
            }
            else
            {
                SqlCommand sqlCom = new SqlCommand(_sqlString, sqlCon);
                ret = sqlCom.ExecuteNonQuery();
                sqlCom.Dispose();
                sqlCon.Close();
                sqlCon.Dispose();
            }
            return ret;
        }

        public DataTable limitDataTable(DataTable _refTable, int _limit)
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < _refTable.Columns.Count; i++)
                dt.Columns.Add(_refTable.Columns[i].ColumnName, _refTable.Columns[i].DataType);

            if (_refTable.Rows.Count < _limit)
                _limit = _refTable.Rows.Count;
            for (int i = 0; i < _limit; i++)
                dt.Rows.Add(_refTable.Rows[i].ItemArray);

            return dt;
        }
    }
}
