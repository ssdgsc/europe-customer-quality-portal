﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Standard item to be returned to the client upon calling JSON Script Services
/// </summary>
public class ResponseObject
{
    private object responseItem;
    private string errorMessage;

    public object ResponseItem { get { return this.responseItem; } set { this.responseItem = value; } }
    public string ErrorMessage { get { return this.errorMessage; } set { this.errorMessage = value; } }
    
    public ResponseObject(){}
    public ResponseObject(object _responseItem, string _errorMessage) 
    {
        this.responseItem = _responseItem;
        this.errorMessage = _errorMessage;
    }
}
