﻿var listCustomer = new Array();
var timer;
jQuery(function (e) {
    jQuery("#lbxCustomer").hide();

    jQuery(window).load(function () {
        // NEW selector
        jQuery.expr[':'].Contains = function (a, i, m) {
            return jQuery(a).text().toUpperCase()
                .indexOf(m[3].toUpperCase()) >= 0;
        };



        jQuery("#lbxCustomer input[type=checkbox]").change(function (e) {
            var elem = jQuery(this)

            if (elem.prop("checked")) {
                elem.closest("td").addClass("selected");
            }
            else {
                elem.closest("td").removeClass("selected");

            } 
            var totalSelected = jQuery("#lbxCustomer tr input[type=checkbox]:not([disabled=disabled]):checked").length
            jQuery("#totalSelected").text((totalSelected == 0) ? "No customer selected" : totalSelected + " customer(s) selected");
        })

        //function insertCustomer(customer) {

        //    if (listCustomer.indexOf(customer) == -1)
        //        listCustomer.push(customer) 

        //}
        //function deleteCustomer(customer) {

        //    if (listCustomer.indexOf(customer) > -1)
        //        delete listCustomer[listCustomer.indexOf(customer)]; 

        //}

        jQuery("#lbxCustomer input[disabled=disabled]").closest("tr").hide();
        setTimeout(function (e) {
            jQuery("#lbxCustomer").fadeIn();
            jQuery("#loading").hide();
            jQuery("#lbxCustomer p.empty-row").remove();
            if (jQuery("#lbxCustomer tr[style='display: none;']").length == 0)
                jQuery("#lbxCustomer tbody").prepend("<p class='empty-row' align='center'>No result(s) founds.</p>")

        }, 500)

        jQuery("#lbxCustomer input[type=checkbox]:checked").change();
        var totalSelected = jQuery("#lbxCustomer tr input[type=checkbox]:not([disabled=disabled]):checked").length
        jQuery("#totalSelected").text((totalSelected == 0) ? "No customer selected" : totalSelected + " customer(s) selected");

        jQuery("#customerSearch")
	.keyup(function (e) {

	    if (jQuery("#loading:hidden").length) {
	        jQuery("#lbxCustomer tr").hide();
	        jQuery("#loading").show();
	        jQuery("#lbxCustomer p.empty-row").remove();
	    }


	    clearTimeout(timer);
	    var search = jQuery(this)
	    timer = setTimeout(function (e) {

	        var filteredRow = jQuery("#lbxCustomer input:not([disabled=disabled])").next("label:Contains('" + search.val() + "')").closest("tr")
	        jQuery("#lbxCustomer tr").hide();
	        jQuery("#loading").hide();

	        if (search.val() == "")
	        {
	            jQuery("#lbxCustomer input:not([disabled=disabled])").closest("tr").show();

	        }

	        if (!filteredRow.length)
	            jQuery("#lbxCustomer tbody").prepend("<p class='empty-row' align='center'>No result(s) founds.</p>")
	        else
	            filteredRow.show();

	    }, 700)
	}) 



    })

})