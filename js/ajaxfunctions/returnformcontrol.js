﻿
jQuery.noConflict();
(function ($) {

    //$("#frmMain").on("submit", function () {

    //    alert("Submit");
    //});

    var IsControl = jQuery("#optProductType_1").is(":checked") ? true : false;

    if (!IsControl) {
        jQuery("#txtControlPCNNumber").val(jQuery("#hdnItemNumber").val());
        GetItemDescriptionDropdown(jQuery("#txtControlPCNNumber").val());
    }
   


    jQuery.widget("custom.combobox", {
        _create: function () {
            this.wrapper = $("<span>")
              .addClass("custom-combobox")
              .insertAfter(this.element);

            this.element.hide();
            this._createAutocomplete();
            this._createShowAllButton();
        },

        _createAutocomplete: function () {
            var selected = this.element.children(":selected"),
              value = selected.val() ? selected.text() : "";

            this.input = $("<input>")
              .appendTo(this.wrapper)
              .val(value)
              .attr("placeholder", "Type your search here...")
              .attr("title", "")
              .attr("style", "width:295px;")
              .addClass("custom-combobox-input")
              .autocomplete({
                  delay: 1000,
                  minLength: 0,
                  source: $.proxy(this, "_source")
              })
              .tooltip({
                  tooltipClass: "ui-state-highlight"
              });

            this._on(this.input, {
                autocompleteselect: function (event, ui) {
                    ui.item.option.selected = true;
                    this._trigger("select", event, {
                        item: ui.item.option
                    });
                },

                autocompletechange: "_removeIfInvalid"
            });
        },

        _createShowAllButton: function () {
            var input = this.input,
              wasOpen = false;

            $("<a>")
              .attr("tabIndex", -1)
              //.attr("title", "Show All Items")
              //.tooltip()
              .appendTo(this.wrapper)
              .button({
                  icons: {
                      primary: "ui-icon-triangle-1-s"
                  },
                  text: false
              })
              .removeClass("ui-corner-all")
              .addClass("custom-combobox-toggle ui-corner-right")
              .mousedown(function () {
                  wasOpen = input.autocomplete("widget").is(":visible");
              })
              .click(function () {
                  input.focus();

                  // Close if already visible
                  if (wasOpen) {
                      return;
                  }

                  // Pass empty string as value to search for, displaying all results
                  input.autocomplete("search", "");
              });
        },

        _source: function (request, response) {
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            response(this.element.children("option").map(function () {
                var text = $(this).text();
                if (this.value && (!request.term || matcher.test(text)))
                    return {
                        label: text,
                        value: text,
                        option: this
                    };
            }));
        },

        _removeIfInvalid: function (event, ui) {

            // Selected an item, nothing to do
            if (ui.item) {
                return;
            }

            // Search for a match (case-insensitive)
            var value = this.input.val(),
              valueLowerCase = value.toLowerCase(),
              valid = false;
            this.element.children("option").each(function () {
                if ($(this).text().toLowerCase() === valueLowerCase) {
                    this.selected = valid = true;
                    return false;
                }
            });

            // Found a match, nothing to do
            if (valid) {
                return;
            }

            // Remove invalid value
            this.input
              .val("")
              .attr("title", value + " didn't match any item")
              .tooltip("open");
            this.element.val("");
            this._delay(function () {
                this.input.tooltip("close").attr("title", "");
            }, 2500);
            this.input.autocomplete("instance").term = "";
        },

        _destroy: function () {
            this.wrapper.remove();
            this.element.show();
        }
    });

            //var ua = window.navigator.userAgent;
			 
			// //Trident == IE 
			// var indexTrident = ua.indexOf("Trident"); 
	
			//if(indexTrident != -1)
			//{
			    

			//    $("#txtControlDataCode").focus(function () {
				
			//		var input = $(this);
					
			//		if(input.val() == input.attr('placeholder'))
			//		{
			//			input.val('');test
			//			input.removeClass('placeholder');
			//		}
				
			//	}).blur(function() {
					
			//		var input = $(this);
					
			//		if (input.val().trim() == '' || input.val() == input.attr('placeholder')) {
			//			input.addClass('placeholder');
			//			input.val(input.attr('placeholder'));
			//		}

			//		if (input.val() == input.attr('placeholder'))
			//		{
					   
			//		    var validatorObject = document.getElementById('req11');
			//		    validatorObject.enabled = false;
			//		    //validatorObject.isvalid = true;
			//		    ValidatorEnable(validatorObject, false);
			//		    ValidatorUpdateDisplay(validatorObject);

			//		    console.log(validatorObject)
			//		}
				
			//	}).blur();
			//}

			

})(jQuery);

function disableButton(toggle) {
    jQuery("#btnAddSameClaim").prop("disabled", toggle)
    jQuery("#btnAddNewClaim").prop("disabled", toggle)
    jQuery("#btnSaveAndFinalize").prop("disabled", toggle)
    jQuery("#btnCancel").prop("disabled", toggle)

}

jQuery(function () {
    //var listPCN = [];
     
    //jQuery("#txtControlModel").children().each(function (e) {
    //    var elem = jQuery(this)
    //    var pcn = new Object();
    //    pcn.value = elem.val()
    //    pcn.label = elem.text()
    //    listPCN.push(pcn)
    //})
   
    //jQuery("#txtControlPCNNumber")
    //.autocomplete({
    //    minLength: 0,
    //    delay:500,
    //    source: listPCN, 
    //    select: function (event, ui) {
    //        jQuery("#txtControlPCNNumber").val(ui.item.value); 
    //        jQuery("#txtControlModel").val("")
    //        return false;
    //    },
    //    focus: function(event, ui){ 
    //        jQuery("#txtControlPCNNumber").val(ui.item.value); 
    //        return false; 
    //    },
    //    search: function (event, ui) {
    //        jQuery('span.working').show();
    //    },
    //    open: function (event, ui) {
    //        jQuery('span.working').hide(); 
    //           //jQuery(".ui-autocomplete li.ui-menu-item") 
    //           // .find('a')
    //           // .each(function () {
    //           //     var me = jQuery(this);
    //           //     var keywords = me.text().split(' ').join('|');
    //           //     me.html(me.text().replace(new RegExp("(" + keywords + ")", "gi"), '<b>$1</b>'));
    //           // });
    //    },
    //    response: function (event, ui) { 
    //        if (ui.content.length === 0) {
    //            jQuery('span.working').hide();
    //            jQuery("#empty-message").text("No results found");
    //        } else {
    //            jQuery("#empty-message").empty();
    //        }
    //    }



    //})
    //.autocomplete("instance")._renderItem = function (ul, item) {
    //    return jQuery("<li>")
    //      .append("<a class=\"pcn-item\" data-value=\""+ item.value +"\">" + item.label + "</a>")
    //      .appendTo(ul);
    //};
     


    jQuery(window).load(function () {


        //jQuery("#ddCustomerList").combobox();

       
        SearchPCNText();


        //jQuery("#txtControlModel").combobox();
        //jQuery("#txtCompressorDateOfFailure").attr("placeholder", "MM/DD/YYYY");
        //jQuery("#txtControlRunningHours").keyup(function () {
        //    if (this.value == "")
        //        this.value = 0;
        //    else
        //        this.value = this.value.replace(/[^0-9]/g, '');
        //});

        jQuery("input[type=radio][name=optControlTypeOfReturn]").change(function (e) {
            var selected = jQuery("input[type=radio][name=optControlTypeOfReturn]:checked").val()
            if (selected == "Commercial Return") {
                alert('This might be chosen only if already agreed with customer service or sales representative. If not approved, return will not be accepted');
            }
        })
        var oldCustomer;
        jQuery("#ddCustomerList").next(".custom-combobox").find(".custom-combobox-input").focus(function (e) {
            oldCustomer = jQuery(this).val();
        })
        jQuery("#ddCustomerList").next(".custom-combobox").find(".custom-combobox-input").on("blur", function (e) {
            if (jQuery("#ddCustomerList option:selected").text().trim() == "") { return; }
            if (oldCustomer == jQuery("#ddCustomerList option:selected").text()) { return; }
            jQuery("#ddCustomerList").change();
        })

        var oldProduct;
        jQuery("#txtControlModel").next(".custom-combobox").find(".custom-combobox-input").focus(function (e) {
            oldProduct = jQuery(this).val();
        })
        jQuery("#txtControlModel").next(".custom-combobox").find(".custom-combobox-input").on("blur", function (e) {
            if (jQuery("#txtControlModel option:selected").text().trim() == "") { return; }
            if (oldProduct == jQuery("#txtControlModel option:selected").text()) { return; }
            jQuery("#txtControlModel").change();
        })

        jQuery("#cmdBackReturn").click(function (e) {
            
        })

        //jQuery.widget("ui.tooltip", jQuery.ui.tooltip, {
        //    options: {
        //        content: function () {
        //            return jQuery(this).prop('title');
        //        }
        //    }
        //});
        //jQuery(".help-icon").tooltip({
        //    position: { my: "center top+10", at: "left bottom" }
        //});

        jQuery("#UploadFile").change(function (e) {
            
            jQuery("#imgLoading").show()
            disableButton(true)
            elem = jQuery(this)
            formData = new FormData();
            formData.append('file', elem[0].files[0]);
            jQuery.ajax({
                url: "uploadfiles.ashx",
                type: 'POST',
                data: formData,
                cache: false,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    disableButton(false)
                    jQuery("#imgLoading").hide()
                    if (data.Result == "OK") {
                        jQuery("#imgSuccess").show()
                        jQuery("#hdnFileName").val(data.Filename + "|" + data.OrigFilename);
                        setTimeout(function (e) {
                            jQuery("#imgSuccess").fadeOut("slow")
                        }, 1500)
                    }
                    else {
                        jQuery(".file-errormessage").show()
                        jQuery(".file-errormessage").text(data.Message)
                        setTimeout(function (e) {
                            jQuery(".file-errormessage").fadeOut("slow")
                            jQuery("#UploadFile").val("")
                        }, 2500)
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    disableButton(false)
                    jQuery(".file-errormessage").show()
                    jQuery("#imgLoading").hide()
                    jQuery(".file-errormessage").text("Uploading failed. Kindly contact the administrator.")
                    setTimeout(function (e) {
                        jQuery(".file-errormessage").fadeOut("slow")
                        jQuery("#UploadFile").val("")
                    }, 2500)
                }
            })
        })
    });
})


function SearchPCNText() {


    var IsControl = jQuery("#optProductType_1").is(":checked") ? true : false;
   

    jQuery("#txtControlPCNNumber").autocomplete({
        delay: 1000,
        source: function (request, response) {
            jQuery("#pPCNSrc").hide();
            jQuery("#imgPCNLoading").show()
            jQuery("#btnCheckControlPCNNumber").hide()
            jQuery.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "returnformcontrol.aspx/CheckControlPCNumber",
                data: "{'pcnNumber':'" + document.getElementById('txtControlPCNNumber').value + "','isControl':'"+ IsControl + "'}",
                dataType: "json",
                success: function (data) {
                    var data = data.d;
                    if (data.length > 0) {
                        response(jQuery.map(data, function (item) {
                            return {
                                label: item.ModelProduct,
                                value: item.PCNNumber
                            }
                        }));
                    }
                    else {
                        jQuery("#pPCNSrc").show();
                    }

                    jQuery("#imgPCNLoading").fadeOut()
                    jQuery("#btnCheckControlPCNNumber").show()

                },
                error: function (result) {
                    jQuery("#pPCNSrc").hide();
                    jQuery("#imgPCNLoading").fadeOut()
                    jQuery("#btnCheckControlPCNNumber").show()
                }
            });

        },

        select: function (e, ui) {
            //jQuery("#txtControlModel").val("");

            if (IsControl)
                jQuery("#txtControlModel").val(ui.item.value)

            else {
                jQuery("#hdnItemNumber").val(ui.item.value);
                GetItemDescriptionDropdown(ui.item.value);
            }
           

         
            //jQuery("#txtControlModel").change();
        }
    })

}


function GetItemDescriptionDropdown(itemValue) {

    param = new Object();
    param._itemNumber = itemValue;

    $.ajax({
        url: "ReturnFormAccessories.aspx/GetItemDescriptionDropdown",
        type: "POST",
        data: JSON.stringify(param),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (response) {

            var result = response;
            var data = result["d"];
            if (data == null)
                data = result;

            var obj = data.ResponseItem;

         

            $("#txtControlModel").empty();


            for (var i = 0; i < data.ResponseItem.length; i++) {
                $("#txtControlModel").append($('<option/>', {
                    value: data.ResponseItem[i]["PCNNumber"],
                    text: data.ResponseItem[i]["ModelProduct"],
                    name: i
                }));
            }

            jQuery("#txtControlModel").val(itemValue)
            jQuery("#hdnItemDescription").val(jQuery("#txtControlModel").text());
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("", $(jqXHR.responseText).filter('title').text());
        },
    });

}