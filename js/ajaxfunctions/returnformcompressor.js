﻿jQuery.noConflict();
(function ($) {
    jQuery.widget("custom.combobox", {
        _create: function () {
            this.wrapper = $("<span>")
              .addClass("custom-combobox")
              .insertAfter(this.element);

            this.element.hide();
            this._createAutocomplete();
            this._createShowAllButton();
        },

        _createAutocomplete: function () {
            var selected = this.element.children(":selected"),
              value = selected.val() ? selected.text() : "";

            this.input = $("<input>")
              .appendTo(this.wrapper)
              .val(value)
              .attr("title", "")
              .attr("placeholder", "Type your search here...")
                .attr("style", "width:295px;")
              .addClass("custom-combobox-input")
              .autocomplete({
                  delay: 1000,
                  minLength: 0,
                  source: $.proxy(this, "_source")
              })
              .tooltip({
                  tooltipClass: "ui-state-highlight"
              });

            this._on(this.input, {
                autocompleteselect: function (event, ui) {
                    ui.item.option.selected = true;
                    this._trigger("select", event, {
                        item: ui.item.option
                    });
                },

                autocompletechange: "_removeIfInvalid"
            });
        },

        _createShowAllButton: function () {
            var input = this.input,
              wasOpen = false;

            $("<a>")
              .attr("tabIndex", -1)
              //.attr("title", "Show All Items")
              //.tooltip()
              .appendTo(this.wrapper)
              .button({
                  icons: {
                      primary: "ui-icon-triangle-1-s"
                  },
                  text: false
              })
              .removeClass("ui-corner-all")
              .addClass("custom-combobox-toggle ui-corner-right")
              .mousedown(function () {
                  wasOpen = input.autocomplete("widget").is(":visible");
              })
              .click(function () {
                  input.focus();

                  // Close if already visible
                  if (wasOpen) {
                      return;
                  }

                  // Pass empty string as value to search for, displaying all results
                  input.autocomplete("search", "");
              });
        },

        _source: function (request, response) {
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            response(this.element.children("option").map(function () {
                var text = $(this).text();
                if (this.value && (!request.term || matcher.test(text)))
                    return {
                        label: text,
                        value: text,
                        option: this
                    };
            }));
        },

        _removeIfInvalid: function (event, ui) {

            // Selected an item, nothing to do
            if (ui.item) {
                return;
            }

            // Search for a match (case-insensitive)
            var value = this.input.val(),
              valueLowerCase = value.toLowerCase(),
              valid = false;
            this.element.children("option").each(function () {
                if ($(this).text().toLowerCase() === valueLowerCase) {
                    this.selected = valid = true;
                    return false;
                }
            });

            // Found a match, nothing to do
            if (valid) {
                return;
            }

            // Remove invalid value
            this.input
              .val("")
              .attr("title", value + " didn't match any item")
              .tooltip("open");
            this.element.val("");
            this._delay(function () {
                this.input.tooltip("close").attr("title", "");
            }, 2500);
            this.input.autocomplete("instance").term = "";
        },

        _destroy: function () {
            this.wrapper.remove();
            this.element.show();
        }
    });

    

})(jQuery);

//jQuery("#ddCustomerList").combobox();
 

jQuery(window).load(function () {
    
 

    //jQuery("#txtCompressorDateOfFailure").attr("placeholder", "MM/DD/YYYY");
    //jQuery("#txtCompressorRunningHours").keyup(function () {
    //    if (this.value == "")
    //        this.value = 0;
    //    else
    //        this.value = this.value.replace(/[^0-9]/g, '');
    //});
    var oldCustomer;
    jQuery("#ddCustomerList").next(".custom-combobox").find(".custom-combobox-input").focus(function (e) {
        oldCustomer = jQuery(this).val();
    })
    jQuery("#ddCustomerList").next(".custom-combobox").find(".custom-combobox-input").on("blur", function (e) {
        if (jQuery("#ddCustomerList option:selected").text().trim() == "") { return; }
        if (oldCustomer == jQuery("#ddCustomerList option:selected").text()) { return; }
        jQuery("#ddCustomerList").change();
    })
    

    //jQuery.widget("ui.tooltip", jQuery.ui.tooltip, {
    //    options: {
    //        content: function () {
    //            return jQuery(this).prop('title');
    //        }
    //    }
    //});
    //jQuery(".help-icon").tooltip({
    //    position: { my: "center top+10", at: "left bottom" }
    //});

    function disableButton(toggle) {
        jQuery("#btnAddSameClaim").prop("disabled",toggle)
        jQuery("#btnAddNewClaim").prop("disabled", toggle)
        jQuery("#btnSaveAndFinalize").prop("disabled", toggle)
        jQuery("#btnCancel").prop("disabled", toggle)
            
    }

    jQuery("#UploadFile").change(function (e) {
        
        jQuery("#imgLoading").show()
        disableButton(true)
        elem = jQuery(this)
        formData = new FormData();
        formData.append('file', elem[0].files[0]);
        jQuery.ajax({
            url: "uploadfiles.ashx",
            type: 'POST',
            data: formData,
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (data) {
                disableButton(false)
                jQuery("#imgLoading").hide()
                if (data.Result == "OK") { 
                    jQuery("#imgSuccess").show()
                    jQuery("#hdnFileName").val(data.Filename + "|" + data.OrigFilename);
                    setTimeout(function (e) {
                        jQuery("#imgSuccess").fadeOut("slow")
                    }, 1500) 
                }
                else {
                    jQuery(".file-errormessage").show()
                    jQuery(".file-errormessage").text(data.Message)
                    setTimeout(function (e) {
                        jQuery(".file-errormessage").fadeOut("slow")
                        jQuery("#UploadFile").val("")
                    }, 2500)
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                disableButton(false)
                jQuery(".file-errormessage").show()
                jQuery("#imgLoading").hide()
                jQuery(".file-errormessage").text("Uploading failed. Kindly contact the administrator.")
                setTimeout(function (e) {
                    jQuery(".file-errormessage").fadeOut("slow")
                    jQuery("#UploadFile").val("")
                }, 2500)
            }
        })
    })

    jQuery("#ddCompressorClaimLists").change(function (e) {
        var selected = jQuery(this).val();
        if (selected == "Other") {
            jQuery("#lblCompressorClaimDescription").show()
            jQuery("#txtCompressorClaimDescription").show()
        }
        else {
            jQuery("#lblCompressorClaimDescription").hide()
            jQuery("#txtCompressorClaimDescription").hide()
        }
        
        if (jQuery(this).find("option:selected").index() >= 5 && jQuery(this).find("option:selected").index() <= 9)
            jQuery("#pnlCompressorWorkingPoint").show()
        else 
            jQuery("#pnlCompressorWorkingPoint").hide()

    })

    jQuery("input[name='optCompressorRefrigerant']").change(function (e) {
        var selected = jQuery(this).val()
        if (selected == "Other") {
            jQuery("#pnlCompressorRefrigerant").show();
            jQuery("#txtCompressorRefrigerant").val("")
        }
        else
            jQuery("#pnlCompressorRefrigerant").hide();

    })

    jQuery("input[name='optCompressorRequest1']").change(function (e) {
        var selected = jQuery(this).val()
        if (selected == "Specific Test")
            jQuery("#pnlCompressorRequestSpecificTest").show();
        else {
            jQuery("#pnlCompressorRequestSpecificTest").hide();
            jQuery("#txtCompressorRequestSpecificTest").val("");
        } 
    })

    jQuery("input.save").click(function (e) {
        jQuery(".required").hide();
        var isOkay = true;
        if (jQuery("input[name='optCompressorRefrigerant']:checked").val() == "Other") {
            if (jQuery("#txtCompressorRefrigerant-required").val() == "") {
                jQuery("#txtCompressorRefrigerant-required").show();
                isOkay = false
            }
           
        }
        if (jQuery("input[name='optCompressorRequest1']:checked").val() == "Specific Test") {
            if (jQuery("#txtCompressorRequestSpecificTest-required").val() == "") {
                jQuery("#txtCompressorRequestSpecificTest-required").show();
                isOkay = false
            }
           
        }

        if(!isOkay)
            e.preventDefault();

    })

    //SearchSerialText();

});
 
//function SearchSerialText() {

    jQuery("#txtCompressorSerialNumber").autocomplete({
        delay:1000,
        source: function (request, response) {
                jQuery("#pSerialSrc").hide();
                jQuery("#imgSerialLoading").show()
                jQuery("#btnCheckCompressorSerialNumber").hide()
                jQuery.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "returnformcompressor.aspx/getSerialNumber",
                    data: "{'serialNumber':'" + document.getElementById('txtCompressorSerialNumber').value + "'}",
                    dataType: "json",
                    success: function (data) {
                        var data = data.d;
                        if (data.length > 0) {
                            response(jQuery.map(data, function (item) {
                                return {
                                    label: item.ModelProduct,
                                    value: item.SerialNumber,
                                    itemNumber: item.ItemNumber
                                }
                            }));
                        }
                        else {
                            jQuery("#pSerialSrc").show();
                        }
                      
                        jQuery("#imgSerialLoading").fadeOut()
                        jQuery("#btnCheckCompressorSerialNumber").show()

                    },
                    error: function (result) {
                        jQuery("#pSerialSrc").hide();
                        jQuery("#imgSerialLoading").fadeOut()
                        jQuery("#btnCheckCompressorSerialNumber").show()
                    }
                });
         
        },

        select: function (e, ui) {
            jQuery("#txtCompressorModel").val(ui.item.label);
            jQuery("#hdnItemNumber").val(ui.item.itemNumber);

        }
    })
    
//}