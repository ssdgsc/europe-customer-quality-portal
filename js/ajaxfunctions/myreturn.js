﻿//var listPCN = [];

//jQuery("#txtControlProduct").children().each(function (e) {
//        var elem = jQuery(this)
//        var pcn = new Object();
//        pcn.value = elem.val()
//        pcn.label = elem.text()
//        listPCN.push(pcn)
//    })
   
//jQuery("#txtControlPCNNumber")
//    .autocomplete({
//        minLength: 0,
//        delay: 500,
//        source: listPCN, 
//        select: function (event, ui) {
//            jQuery("#txtControlPCNNumber").val(ui.item.value);
//            jQuery("#txtControlProduct").val("")
//            jQuery("#txtControlProductGroup").val("")
//            return false;
//        },
//        focus: function(event, ui){ 
//            jQuery("#txtControlPCNNumber").val(ui.item.value);
//            return false; 
//        },
//        search: function (event, ui) {
//            jQuery('span.working').show();
//        },
//        open: function (event, ui) {
//            jQuery('span.working').hide(); 
//               //jQuery(".ui-autocomplete li.ui-menu-item") 
//               // .find('a')
//               // .each(function () {
//               //     var me = jQuery(this);
//               //     var keywords = me.text().split(' ').join('|');
//               //     me.html(me.text().replace(new RegExp("(" + keywords + ")", "gi"), '<b>$1</b>'));
//               // });
//        },
//        response: function (event, ui) { 
//            if (ui.content.length === 0) {
//                jQuery('span.working').hide();
//                jQuery("#empty-message").text("No results found");
//            } else {
//                jQuery("#empty-message").empty();
//            }
//        }



//    })
//    .autocomplete("instance")._renderItem = function (ul, item) {
//        return jQuery("<li>")
//          .append("<a class=\"pcn-item\" data-value=\""+ item.value +"\">" + item.label + "</a>")
//          .appendTo(ul);
//    };
     


jQuery(window).load(function () {
     
    //[START] [7/17-2017]
    var productType = jQuery("#ddProductType").val();

    if (productType == "Accessories") {
        jQuery('label[for=radioControlPCN]').html('Search by Item Number');
        jQuery('label[for=radioControlProduct]').html('Search by Item Description');

        var headerProduct = jQuery('a:contains("Product")');
        var headerPCN = jQuery('a:contains("PCN Number")');

        jQuery(headerProduct).html("Item Description");
        jQuery(headerPCN).html("Item Number");

        //jQuery("#lblPCNNumber").html("Item Number:");
        //jQuery("#lblProduct").html("Item Description:");
    }

    //[END] [7/17-2017]
    jQuery('#ddCustomerList,#ddControlCustomerList').multipleSelect({
        filter: true,
        placeholder: "SEARCH CUSTOMER HERE"
    });

    
  
    jQuery("#pnlCompressorCustomerList").show();
    jQuery("#pnlControlCustomerList").show();
    jQuery(".reset-list")
        .on("click", function (e) {
            var list = jQuery(this).parent().find("select")
            if(list.find("option:selected").length){
                list.multipleSelect("uncheckAll"); 
                jQuery(this).parent().find("input[type=submit]").click();
            }

            ClearSearchFields();
            
            
        })
        .show();

    jQuery(".clear-search")
        .on("click", function (e) {
           
            ClearSearchFields();

        })
        .show();

    
    function ClearSearchFields()
    {
        //Compressor
        jQuery("#txtSearch").val("");
        jQuery("#radioSerial").prop("checked", true);
        jQuery("#btnSearch").click();


        //Control and Accessories
        jQuery("#txtControlSearch").val("");
        jQuery("#radioControlPCN").prop("checked", true);
        jQuery("#btnControlSearch").click();

    }
        


    jQuery.widget("ui.tooltip", jQuery.ui.tooltip, {
        options: {
            content: function () {
                return jQuery(this).prop('title');
            }
        }
    });
    jQuery(".tooltip-contact").tooltip({
        position: { my: "center top+10", at: "left bottom" }
    }); 
    
    jQuery(".help-icon").tooltip({
        position: { my: "center top+10", at: "left bottom" }
    });

    jQuery("#aFileAttachmentComp,aFileAttachment").tooltip({
        position: { my: "center top+10", at: "cemter bottom" }
    });

    

    //jQuery("#txtCompressorDateOfFailure").attr("placeholder", "MM/DD/YYYY");
    //jQuery("#txtCompressorRunningHours").keyup(function () {
    //    if (this.value == "")
    //        this.value = 0;
    //    else
    //        this.value = this.value.replace(/[^0-9]/g, '');
    //});
    jQuery("#txtControlRunningHours").keyup(function () {
        if (this.value == "")
            this.value = 0;
        else
            this.value = this.value.replace(/[^0-9]/g, '');
    });
    SearchSerialText();
    SearchPCNText();
});
 
function SearchSerialText() {

    jQuery("#txtCompressorSerialNumber").autocomplete({
        delay: 1000,
        source: function (request, response) {
            
            jQuery("#pSerialSrc").hide()
            jQuery("#imgSerialLoading").show()
                jQuery("#btnCheckCompressorSerialNumber").hide()
                jQuery.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "returnformcompressor.aspx/getSerialNumber",
                    data: "{'serialNumber':'" + document.getElementById('txtCompressorSerialNumber').value + "'}",
                    dataType: "json",
                    success: function (data) {
                        var data = data.d
                        if (data.length > 0) {
                            response(jQuery.map(data, function (item) {
                                return {
                                    label: item.ModelProduct,
                                    value: item.SerialNumber
                                }
                            }));
                        }
                        else {
                            jQuery("#pSerialSrc").show()

                        }
                       
                        jQuery("#imgSerialLoading").fadeOut()
                        jQuery("#btnCheckCompressorSerialNumber").show()

                    },
                    error: function (result) {
                        jQuery("#pSerialSrc").hide() 
                        jQuery("#imgSerialLoading").fadeOut()
                        jQuery("#btnCheckCompressorSerialNumber").show()
                    }
                }); 
        },
        select: function (e, ui) {
            jQuery("#txtCompressorModel").val("")
            jQuery("#hdnItemNumber").val("")

        }
    })

}
function SearchPCNText() {

    jQuery("#txtControlPCNNumber").autocomplete({
        delay: 1000,
        source: function (request, response) {
            jQuery("#pPCNSrc").hide();
            jQuery("#imgPCNLoading").show()
            jQuery("#btnCheckControlPCNNumber").hide()
            jQuery.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "returnformcontrol.aspx/CheckControlPCNumber",
                data: "{'pcnNumber':'" + document.getElementById('txtControlPCNNumber').value + "'}",
                dataType: "json",
                success: function (data) {
                    var data = data.d;
                    if (data.length > 0) {
                        response(jQuery.map(data, function (item) {
                            return {
                                label: item.ModelProduct,
                                value: item.PCNNumber
                            }
                        }));
                    }
                    else {
                        jQuery("#pPCNSrc").show();
                    }

                    jQuery("#imgPCNLoading").fadeOut()
                    jQuery("#btnCheckControlPCNNumber").show()

                },
                error: function (result) {
                    jQuery("#pPCNSrc").hide();
                    jQuery("#imgPCNLoading").fadeOut()
                    jQuery("#btnCheckControlPCNNumber").show()
                }
            });

        },

        select: function (e, ui) {
            jQuery("#txtControlProduct").val("")
            jQuery("#txtControlProductGroup").val("")

        }
    })

}