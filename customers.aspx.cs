﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Services;
using System.Web.UI.WebControls;
using General.AvianCrypto;
using System.Data;

using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

public partial class customers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       

        if (Session["UserID"] == null)
        {
            Response.Redirect("login.aspx");
            return;
        }

        if (!Page.IsPostBack)
        {
           
            SecurityMaster sm = new SecurityMaster();
            string user = sm.Decrypt(Session["UserType"].ToString());
            //if (user != "CUSTOMER QUALITY")
            //{
            //    PanelUnAuthorizedAccess.Visible = true;
            //    pnlGridView.Visible = false;
            //    CustomerPanel.Visible = false;
            //    return;
            //}
            bindData();
        }
    }

    protected void bindData()
    {
        SecurityMaster sm = new SecurityMaster();
        //[START] [jpbersonda] [5/8/2017] -- Added for CSO usertype
        string userType = sm.Decrypt(Session["UserType"].ToString());
        int userId = Convert.ToInt32(sm.Decrypt(Session["UserID"].ToString()));

        Maintenance.Customers customers = new Maintenance.Customers();
        DataTable dt = new DataTable();
        this.CustomersGrid.DataSource = customers.getCustomers(userType, userId);
        this.CustomersGrid.DataBind();

        if (txtSalesOfficeCode.Items.Count == 1)
        {
            txtSalesOfficeCode.DataSource = customers.getSalesOffices();
            txtSalesOfficeCode.DataValueField = "ID";
            txtSalesOfficeCode.DataTextField = "DESCRIPTION";
            txtSalesOfficeCode.DataBind();
        }
    

       

    }
    protected void CustomersGrid_SelectedIndexChanged(object sender, EventArgs e)
    {
        //lblError.Text = "";
        Label lblCustomerID = (Label)CustomersGrid.SelectedRow.Cells[1].FindControl("lblCustomerID");
        string lblLocationID =  CustomersGrid.SelectedRow.Cells[2].Text;

        hdCustomerID.Value = lblCustomerID.Text;
        Maintenance.Customers customers = new Maintenance.Customers();
        Model.Customers customersModel = (Model.Customers)customers.getCustomers(int.Parse(lblCustomerID.Text), lblLocationID)[0];

        this.txtCreated.Text = customersModel.Created;
        this.txtCustomerName.Text = customersModel.CustomerName;
        this.txtCustomerNumber.Text = customersModel.CustomerNumber.ToString();
        this.txtLocation.Text = customersModel.LocationID.ToString();
        this.txtCity.Text = customersModel.City;
        this.txtState.Text = customersModel.State;
        this.txtZipCode.Text = customersModel.ZipCode;
        this.txtCountry.Text = customersModel.Country;

        this.txtModified.Text = customersModel.Modified;
        //this.txtSalesOfficeCode.Text = customersModel.SalesOfficeCode;
        if (customersModel.SalesOfficeCode != ""){
              if (txtSalesOfficeCode.Items.FindByValue(customersModel.SalesOfficeCode) != null)
                    this.txtSalesOfficeCode.SelectedValue = customersModel.SalesOfficeCode;
              else
                    txtSalesOfficeCode.SelectedIndex = -1;
        } 
        else
            txtSalesOfficeCode.SelectedIndex = -1;

        this.chkCompressor.Checked = customersModel.Compressor;
        this.chkControl.Checked = customersModel.Control;
        //this.chkAccessories.Checked = customersModel.Accessories;
        this.lete.Visible = true;


        pnlFilterCustomer.Visible = false;
        btnAddNewCustomer.Visible = false;

        string temp_email = string.Empty;
        temp_email = customersModel.CustomerEmail.ToString().Replace(",", "\n").Trim();
        temp_email = temp_email.Replace(" ", "\n").Trim();
        temp_email = temp_email.Replace(";", "\n").Trim();

        this.txtCustomerEmail.Text = temp_email;

        pnlGridView.Visible = false;
        CustomerPanel.Visible = true;
    }

    protected void gvCustomersGrid_Sorting(object sender, EventArgs e) {
        string x = "Test";
        //this.CustomersGrid.DataSource = sortGridView(dtc, false);
        //this.CustomersGrid.DataBind();
        //this.CustomersGrid.PageIndex = curPageIndex;
    }

    protected void CustomersGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        bindData();
        CustomersGrid.PageIndex = e.NewPageIndex;

        CustomersGrid.DataBind();
        //Your code
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Maintenance.Customers customers = new Maintenance.Customers();

        //[START] [jpbersonda] [5/17/2017] [EDIT]

        string tempEmail = txtCustomerEmail.Text.Trim();

        tempEmail = tempEmail.Replace(";", ",");
        tempEmail = tempEmail.Replace(" ", ",");
        tempEmail = tempEmail.Replace("\r", "");
        tempEmail = tempEmail.Replace("\n", ",");
        tempEmail = tempEmail.Replace("\\r", "");
        tempEmail = tempEmail.Replace("\\n", ",");

        String[] tempS = tempEmail.Split(',');
        List<String> s = new List<String>();

        //Remove empty or space values then add to list of string
        for (int x = 0; x < tempS.Length; x++)
        {
            if (!String.IsNullOrEmpty(tempS[x].ToString()) && !String.IsNullOrWhiteSpace(tempS[x].ToString()))
            {
                s.Add(tempS[x].ToString());
            }

            else 
            {
                // ----- skip | 

                //Page.ClientScript.RegisterStartupScript(this.GetType(), "alert" + UniqueID,
                // "alert('Email address is invalid:\\n" + tempEmail + "');", true);
                //return;
            }
        }

            tempEmail = string.Empty;

        //[END] [jpbersonda] [5/17/2017] [EDIT]


        string invalidEmail=string.Empty;
        string validEmail = string.Empty;

		for (int i = 0; i < s.Count; i++)
		{
            string email = s[i];
            //Regex regex = new Regex(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
            Regex regex = new Regex(@"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?");
            Match match = regex.Match(email);

            if (match.Success) 
            {
                if (i == s.Count() - 1)
                    validEmail += s[i].ToString();
                else
                    validEmail += s[i].ToString()  + ",";
                
            }
            // Continue
            else
            {
                invalidEmail += s[i]+"\\n";
            }
		}

        if (invalidEmail != string.Empty)
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert" + UniqueID,
            "alert('Email address is invalid:\\n" + invalidEmail + "');", true);
        }
        else
        {
            
            Model.Customers customersModel = new Model.Customers();

            customersModel.CustomerID = int.Parse(this.hdCustomerID.Value);
            customersModel.Created = this.txtCreated.Text;
            customersModel.CustomerName = this.txtCustomerName.Text;
            customersModel.CustomerNumber = this.txtCustomerNumber.Text;
            customersModel.Modified = this.txtModified.Text;
            customersModel.SalesOfficeCode = this.txtSalesOfficeCode.SelectedValue;
            // SAME COMPRESSOR AND ACCESSORIES 
            customersModel.Compressor = this.chkCompressor.Checked;
            customersModel.Accessories = this.chkCompressor.Checked;
            //
            customersModel.Control = this.chkControl.Checked;
            customersModel.CustomerEmail = validEmail; //  txtCustomerEmail.Text;
            customersModel.LocationID = txtLocation.Text;

            CustomerLocation custLoc = new CustomerLocation();
            custLoc.CustomerNumber = txtCustomerNumber.Text; 
            custLoc.LocationID = txtLocation.Text;
            custLoc.City = txtCity.Text;
            custLoc.State = txtState.Text;
            custLoc.ZipCode = txtZipCode.Text;
            custLoc.Country = txtCountry.Text;
            custLoc.Address = "";

            Maintenance.Customers c = new Maintenance.Customers();

            if (customersModel.CustomerID == 0)
            {
                customersModel.Created = DateTime.Now.ToShortDateString();
                customersModel.Modified = DateTime.Now.ToShortDateString();

                if (customers.isCustomerLocationExists(customersModel.CustomerNumber.ToString(), customersModel.LocationID)) {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "alert" + UniqueID,
                            "alert('Location Site ID provided already exists.');", true);
                    return;
                }
                //if (customers.countRecordWithCondition("Customer", "CustomerNumber", txtCustomerNumber.Text.Trim(), int.Parse(hdCustomerID.Value), "CustomerID") <= 0) 
                customers.addCustomers(customersModel);
                customers.addLocation(custLoc);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert" + UniqueID, 
                 "alert('1 record successfully added.');", true);
                 
            }
            else
            {
                customers.updateCustomers(customersModel);
                customers.UpdateLocation(custLoc);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert" + UniqueID, 
                "alert('1 record successfully modified.');", true); 
            }
            bindData();
            btnSearch_Click(this, EventArgs.Empty);
            PanelUnAuthorizedAccess.Visible = false;
            pnlGridView.Visible = true;
            CustomerPanel.Visible = false;
            pnlFilterCustomer.Visible = true;
            btnAddNewCustomer.Visible = true;
        
        }
       
       
    }

    protected void BtnCancel_Click(object sender, EventArgs e)
    {
        pnlFilterCustomer.Visible = true;
        btnAddNewCustomer.Visible = true;
        PanelUnAuthorizedAccess.Visible = false;
        pnlGridView.Visible = true;
        CustomerPanel.Visible = false;
    }
    protected void btnAddNewCustomer_Click(object sender, EventArgs e)
    {
        pnlFilterCustomer.Visible = false;
        btnAddNewCustomer.Visible = false;
        PanelUnAuthorizedAccess.Visible = false;
        pnlGridView.Visible = true;
        CustomerPanel.Visible = false;

        this.lete.Visible = false;
        this.hdCustomerID.Value ="0";
        this.txtCreated.Text = "";
        this.txtLocation.Text = "";
        this.txtCity.Text = "";
        this.txtState.Text = "";
        this.txtZipCode.Text = "";
        this.txtCountry.Text = "";
        this.txtCustomerName.Text = "";
        this.txtCustomerNumber.Text = "";
        this.txtModified.Text = "";
        this.txtSalesOfficeCode.SelectedIndex = -1;
        this.txtCustomerEmail.Text = "";
        this.chkCompressor.Checked = false;
        this.chkControl.Checked = false;

        PanelUnAuthorizedAccess.Visible = false;
        pnlGridView.Visible = false;
        CustomerPanel.Visible = true;

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //[START] [jpbersonda] [5/8/2017] -- Added for CSO usertype
        SecurityMaster sm = new SecurityMaster();
        string userType = sm.Decrypt(Session["UserType"].ToString());
        int userId = Convert.ToInt32(sm.Decrypt(Session["UserID"].ToString()));

        Maintenance.Customers customers = new Maintenance.Customers();
        DataTable dt = new DataTable();
        this.CustomersGrid.DataSource = customers.getCustomersContainsCustomerName(txtCustomerCodeSearch.Text, txtCustomerNameSearch.Text, userType,userId);
        this.CustomersGrid.DataBind();
    }
    protected void lete_Click(object sender, EventArgs e)
    {
        Maintenance.Customers customers = new Maintenance.Customers();
        customers.DeleteLocation(txtLocation.Text, txtCustomerNumber.Text); 
        customers.deleteCustomerByID(int.Parse(hdCustomerID.Value));

        Page.ClientScript.RegisterStartupScript(this.GetType(), "alert" + UniqueID,
        "alert('1 record successfully deleted.');", true);
        pnlFilterCustomer.Visible = true;
        btnAddNewCustomer.Visible = true;
        PanelUnAuthorizedAccess.Visible = false;
        pnlGridView.Visible = true;
        CustomerPanel.Visible = false;
        bindData();
    }




    [WebMethod]
    public static List<string> getAllCustomerArray(int customerNumber)
    { 
        Maintenance.Customers c = new Maintenance.Customers();
        ArrayList arr = new ArrayList();
        arr = c.getAllCustomerArray(customerNumber);
        List<string> list = new List<string>(arr.Count);
        foreach (string item in arr) list.Add(item.ToString());
        return list;
    }


    [WebMethod]
    public static List<string> getAllCustomerNameByCustomerNumber(string customerNumber)
    {
        Maintenance.Customers c = new Maintenance.Customers();
        ArrayList arr = new ArrayList();
        List<string> list = new List<string>(1);
        list.Add(c.getAllCustomerNameByCustomerNumber(customerNumber));
        return list;
    }  


    // CUSTOMERS LOCATION
    //protected void btnAddLocations_Click(object sender, EventArgs e)
    //{
    //    lblcustomernoLoc.Text = txtCustomerNumber.Text;
    //    lblcustomernameLoc.Text = txtCustomerName.Text;
    //    CustomerPanel.Visible = false;
    //    panelLocations.Visible = true;
    //    Maintenance.Customers c = new Maintenance.Customers();

    //    gridLocations.DataSource = c.getLocationByCustomerNumber(long.Parse(txtCustomerNumber.Text));
    //    gridLocations.DataBind();
    //    txtCity.Focus();
    //}
    //protected void btnCancelLoc_Click(object sender, EventArgs e)
    //{
    //    panelLocations.Visible = false;
    //    CustomerPanel.Visible = true;
       
    //}

    //private void populateLocation() 
    //{
    //    Maintenance.Customers c = new Maintenance.Customers();
    //    gridLocations.DataSource = c.getLocationByCustomerNumber(long.Parse(txtCustomerNumber.Text));
    //    gridLocations.DataBind();
    //}

    //private void clearLocations()
    //{
    //    txtAddress.Text = "";
    //    txtCity.Text = "";
    //    txtState.Text = "";
    //    txtZipCode.Text = "";
    //    txtCountry.Text = "";
    //    btnSaveLoc.Text = "Add";
    //    btnDeleteLoc.Visible = false;
    //}


    //protected void btnSaveLoc_Click(object sender, EventArgs e)
    //{

    //    CustomerLocation custLoc = new CustomerLocation();
    //    custLoc.CustomerNumber = long.Parse(lblcustomernoLoc.Text);
    //    custLoc.Address = txtAddress.Text;
    //    custLoc.City = txtCity.Text;
    //    custLoc.State = txtState.Text;
    //    custLoc.ZipCode = txtZipCode.Text;
    //    custLoc.Country = txtCountry.Text;
    //    Maintenance.Customers c = new Maintenance.Customers();
    //    if (btnSaveLoc.Text == "Add")
    //    {
    //        if (c.addLocation(custLoc))
    //            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert",
    //                            "alert(\"Successfully added.\");", true);

    //    }
    //    else if (btnSaveLoc.Text == "Update")
    //    {
    //        Label lblLocationID = (Label)gridLocations.SelectedRow.Cells[1].FindControl("lblLocationID");
    //        custLoc.LocationID = long.Parse(lblLocationID.Text);
    //        if (c.UpdateLocation(custLoc))
    //            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert",
    //                            "alert(\"Successfully udpated.\");", true);


    //    }
    //    clearLocations();
    //    populateLocation();

   
    //}
    //protected void gridLocations_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    Label lblLocationID = (Label)gridLocations.SelectedRow.Cells[1].FindControl("lblLocationID");
    //    CustomerLocation cust = new CustomerLocation();
    //    Maintenance.Customers c = new Maintenance.Customers();

    //    cust = c.getLocationByLocationID(long.Parse(lblLocationID.Text));
        
    //    txtAddress.Text = cust.Address;
    //    txtCity.Text = cust.City;
    //    txtState.Text = cust.State;
    //    txtZipCode.Text = cust.ZipCode;
    //    txtCountry.Text = cust.Country;

    //    btnSaveLoc.Text = "Update";
    //    btnDeleteLoc.Visible = true;
    //}
    //protected void btnDeleteLoc_Click(object sender, EventArgs e)
    //{
    //    Label lblLocationID = (Label)gridLocations.SelectedRow.Cells[1].FindControl("lblLocationID");
    //    Maintenance.Customers c = new Maintenance.Customers();
    //    if (c.DeleteLocation(long.Parse(lblLocationID.Text))) {
    //        Page.ClientScript.RegisterStartupScript(this.GetType(), "alert",
    //                            "alert(\"Successfully deleted.\");", true);
    //        clearLocations();
    //        populateLocation();

    //    }
    //}
 
    //protected void gridLocations_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    populateLocation();
    //    gridLocations.PageIndex = e.NewPageIndex;

    //    gridLocations.DataBind();
    //}
}