﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Deliveries.aspx.cs" Inherits="Deliveries" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Europe Customer Quality Portal</title>
        <link type="text/css" rel="Stylesheet" href="css/default.css" />
        <link type="text/css" rel="Stylesheet" href="css/style.css" />
</head>
<body>
<form id="frmDeliveries" runat="server">
    <div>
        <div id="holder_main" align="center">
            <div id="holderBody" style="width: 980px;">
                    <div class="MainPanel2">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr valign="top">
                            <td valign="top" height="100%" class="LeftNavigation">
                                <div id="holderLeftNav">
                                    <table>
                                        <tr>
                                            <td><a href="home.aspx">Portal Home</a></td>
                                        </tr>
                                        <tr>
                                            <td><a href="ReturnFormCompressor.aspx">Return Authorization Form</a></td>
                                        </tr>
                                        <tr>
                                            <td><a href="myreturn.aspx">My Returns</a></td>
                                        </tr>
                                        <%--<tr>
                                            <td><a href="statistics.aspx">Statistics</a></td>
                                        </tr>
                                        <tr>
                                            <td><a style="color:#f7941d;">Deliveries</a></td>
                                        </tr>--%>
                                        <tr>
                                            <td><a href="changepassword.aspx">Change Password</a></td>
                                        </tr>
                                        <tr>
                                            <td><a href="logout.aspx">Logout</a></td>
                                        </tr>
                                    </table>
                                    <br />
                                    <asp:Panel ID="pnlMaintenance" runat="server">
                                        <table>
                                            <tr>
                                                <th><a style="color:Blue">Maintenance</a><br /></th>
                                            </tr>
                                            <tr>
                                                <td><a href="customers.aspx">Customers</a></td>
                                            </tr>
                                            <tr>
                                                <td><a href="usermanagement.aspx">User Management</a></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                            </td>
                            <td valign="top" height="100%" class="DottedBorderLeft" style="width:770px">
                                <div id="holderMaincontent">
                                        <div><asp:Label ID="LabelMessage" runat="server" Font-Bold="True" ForeColor="Red" Text="" Visible="False"></asp:Label></div>
                                        <br /> 
                                        <h1>Waterfall</h1>
                                        <hr /><br />
                                        <div><img src="images/Waterfall.png" alt="Waterfall" /></div>                                        
                                        <br /> 
                                        <h1>RPR</h1>
                                        <hr /><br />
                                        <div><img src="images/RPR.png" alt="Waterfall" /></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                 </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
