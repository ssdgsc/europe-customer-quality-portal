﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="productlocation.aspx.cs" Inherits="productlocation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link type="text/css" rel="Stylesheet" href="css/default.css" />
    <link type="text/css" rel="Stylesheet" href="css/style.css" />

    <style type="text/css">
        .style2
        {
            width: 124px;
        }
        .style3
        {
            width: 106px;
        }
        .style4
        {
            width: 144px;
        }
        .style5
        {
            width: 201px;
        }
        .style6
        {
            color: #FF3300;
        }
        .style7
        {
            width: 2px;
        }
    </style>
    
</head>
<body>
    <form id="frmMain" runat="server">
    <asp:HiddenField ID="hdcuscomp" runat="server" />
    <asp:HiddenField ID="hdcusctrl" runat="server" />
    <asp:HiddenField ID="hdcusaccs" runat="server" />
    <asp:HiddenField ID="hdUserID" runat="server" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <div>
        <div id="holder_main" align="center">
            <div id="holderBody" class="container">
                <div class="MainPanel2">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr valign="top">
                            <td valign="top" height="100%" class="LeftNavigation">
                                  <% Response.Write(Helpers.menuNavi(7)); %>
                            </td>
                            <td valign="top" height="100%" class="DottedBorderLeft" style="width:770px">
                                <div id="holderMaincontent">
                                    <div style="position: relative; top:  0px; padding-right: 5px; padding-bottom: 5px; padding-top: 0px; display: table-cell; text-align: left;">
                                        <div>
                                            <% string sysmode = System.Configuration.ConfigurationManager.AppSettings["SystemMode"]; %>
                                            <h3>Product Location Maintenance <% Response.Write((sysmode == "TEST") ? "(TEST SITE)" : "(PRODUCTION)"); %></h3>
                                        </div>
                                         <asp:Panel ID="pnlGridView" runat="server" Width="745px" ScrollBars="Auto">
                                             
                                         
                                            <br />

                                            <asp:GridView ID="ProductLocationGrid" runat="server" AllowPaging="True"  
                                                AllowSorting="True" CellPadding="4" CssClass="ui_grid" 
                                                EmptyDataText="No Record Found" ForeColor="#333333" PageSize="20" 
                                                Width="740px" AutoGenerateColumns="False" OnSelectedIndexChanged="ProductLocationGrid_SelectedIndexChanged">
                                                <AlternatingRowStyle BackColor="White" />
                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="True" />
                                                    <asp:TemplateField HeaderText="ID" Visible="False">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ID") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblProductLocationID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle Width="0px" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="ProductType" HeaderText="Product Type" />
                                                    <asp:BoundField DataField="Location" HeaderText="Location" />
                                                     
                                                </Columns>
                                                <EditRowStyle BackColor="#2461BF" />
                                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Left" />
                                                <RowStyle BackColor="#EFF3FB" />
                                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                            </asp:GridView>
                                        </asp:Panel>
                                        <asp:HiddenField ID="hdnmode" runat="server"/>
                                        <%--<asp:Button ID="btnAddUser" Text="Add User" runat="server" CssClass="buttons" OnClick="btnAddUser_Click"/>--%>

                                        <asp:Panel ID="ProductLocationPanel" runat="server" Visible="False">
                                            <table style="width:100%;"> 
                                                <tr>
                                                    <td class="style3"> Product Type: <b><span runat="server" id="spanProductType"></span></b> </td>
                                                </tr>
                                                <tr>
                                                    <td class="style3"> Location: </td>
                                                </tr>
                                                 <tr>
                                                    <td class="style3"> 
                                                        <textarea runat="server" id="txtlocation" style="width: 400px; height: 200px;">

                                                        </textarea>
                                                    </td>
                                                </tr>
                                                   

                                                <tr> 
                                                    <td class="style2">
                                                        <asp:Button ID="btnSave" class="buttons" runat="server" Text="Update" OnClick="btnSave_Click" />
                                                        &nbsp;<asp:Button class="buttons" ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false" OnClick="btnCancel_Click" 
                                                           />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style3">
                                                        &nbsp;</td>
                                                    <td class="style7">
                                                        &nbsp;</td>
                                                    <td class="style2">
                                                        <asp:Label ID="lblError" runat="server" style="color: #FF0000"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                           
                                        </asp:Panel>

                                        <asp:Panel ID="PanelUnAuthorizedAccess" runat="server" Visible="False">
                                            <b>Authorized users can access this page.</b>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
 <script type="text/javascript" src="js/jquery.1.10.2.js"></script> 
<script type="text/javascript" src="js/ajaxfunctions/usermanagement.js"></script>

</html>
