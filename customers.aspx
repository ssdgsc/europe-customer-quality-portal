﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="customers.aspx.cs" Inherits="customers" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link type="text/css" rel="Stylesheet" href="css/default.css" />
    <link type="text/css" rel="Stylesheet" href="css/style.css" />
    <link type="text/css" rel="Stylesheet" href="css/jquery-ui.css" />
  
    <style type="text/css">
        .style1
        {
            width: 196px;
        }
        .style2
        {
            width: 132px;
        }
    </style>
    </head>
<body>
    <form id="frmMain" runat="server">
    <asp:HiddenField ID="hdCustomerID" runat="server" />
    <div>
        <div id="holder_main" align="center">
            <div id="holderBody" class="container">
                <div class="MainPanel2">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr valign="top">
                            <td valign="top" height="100%" class="LeftNavigation">
                                 <% Response.Write(Helpers.menuNavi(5)); %>
                            </td>
                            <td valign="top" height="100%" class="DottedBorderLeft" style="width:770px">
                                <div id="holderMaincontent">
                                    <div style="position: relative; top:  0px; padding-right: 5px; padding-bottom: 5px; padding-top: 0px; display: table-cell; text-align: left;">
                                        <div>
                                            <% string sysmode = System.Configuration.ConfigurationManager.AppSettings["SystemMode"]; %>
                                            <h3>Customer Maintenance <% Response.Write((sysmode == "TEST") ? "(TEST SITE)" : "(PRODUCTION)"); %></h3>
                                        </div>
                                       
                                            <asp:Panel ID="pnlFilterCustomer" runat="server" Width="745px" >
                                            <table style="width: 100%;">
                                                 <tr>
                                                     <td class="style2">
                                                         Customer Number
                                                     </td>
                                                     <td class="style1">
                                                         <asp:TextBox ID="txtCustomerCodeSearch" runat="server" Width="190px" MaxLength="10"></asp:TextBox>
                                                     </td>
                                                     <td></td>
                                                 </tr>
                                                 <tr>
                                                     <td class="style2">
                                                         Customer Name
                                                     </td>
                                                     <td class="style1">
                                                         <asp:TextBox ID="txtCustomerNameSearch" runat="server" Width="190px"></asp:TextBox>
                                                     </td>
                                                     <td>
                                                         <asp:Button ID="btnSearch" runat="server" Text="Search" 
                                                             onclick="btnSearch_Click" class="buttons" />
                                                     </td>
                                                 </tr>
                                             </table>
                                            </asp:Panel>
                                            <br />
                                          <asp:Panel ID="pnlGridView" runat="server" Width="745px" ScrollBars="Auto">
                                             <asp:GridView ID="CustomersGrid" runat="server" AllowPaging="True" EnableNextPrevNumericPager="true"
                                                 OnPageIndexChanging="CustomersGrid_PageIndexChanging"
                                                 AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" onselectedindexchanged="CustomersGrid_SelectedIndexChanged" 
                                                  OnSorting="gvCustomersGrid_Sorting" PageSize="20" AllowSorting="True" CssClass="ui_grid" ShowHeaderWhenEmpty="True" EmptyDataText="No records found.">
                                                  
                                                 <PagerStyle CssClass="paginationGridView"/>

                                                 <AlternatingRowStyle BackColor="White" />
                                                 <Columns>
                                                     <asp:CommandField ShowSelectButton="True" />
                                                     <asp:TemplateField HeaderText="CustomerID " Visible="False">
                                                         <EditItemTemplate>
                                                             <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("CustomerID") %>'></asp:TextBox>
                                                         </EditItemTemplate>
                                                         <ItemTemplate>
                                                             <asp:Label ID="lblCustomerID" runat="server" Text='<%# Bind("CustomerID") %>'></asp:Label>
                                                         </ItemTemplate> 
                                                     </asp:TemplateField>
                                                     
                                                     <asp:BoundField DataField="LocationID" HeaderText="Location" /> 
                                                     <asp:BoundField DataField="CustomerName" HeaderText="Customer Name" />
                                                     <asp:BoundField DataField="CustomerNumber" HeaderText="Customer Number" /> 
                                                     <asp:BoundField DataField="Address" HeaderText="City, State, Zip Code, Country" /> 
                                                    <%-- <asp:BoundField DataField="SalesOfficeCode" HeaderText="Sales Office Code" />
                                                     <asp:BoundField DataField="Created" HeaderText="Created" Visible="False" />
                                                     <asp:BoundField DataField="Modified" HeaderText="Modified" Visible="False" />--%>
                                                     <asp:TemplateField HeaderText="Controls and Electronics">
                                                         <EditItemTemplate>
                                                             <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Control") %>'></asp:TextBox>
                                                         </EditItemTemplate>
                                                         <ItemTemplate>
                                                             <asp:CheckBox ID="chkControl" runat="server" Checked='<%# Bind("Control") %>' 
                                                                 Enabled="False" />
                                                         </ItemTemplate>
                                                     </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Compressors, Accessories, Drives, Units">
                                                         <EditItemTemplate>
                                                             <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Compressor") %>'></asp:TextBox>
                                                         </EditItemTemplate>
                                                         <ItemTemplate>
                                                             <asp:CheckBox ID="chkCompressor" runat="server" 
                                                                 Checked='<%# Bind("Compressor") %>' Enabled="False" />
                                                         </ItemTemplate>
                                                     </asp:TemplateField>
                                                     <%--<asp:TemplateField HeaderText="Accessories">
                                                         <EditItemTemplate>
                                                             <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Accessories") %>'></asp:TextBox>
                                                         </EditItemTemplate>
                                                         <ItemTemplate>
                                                             <asp:CheckBox ID="chkAccessories" runat="server" 
                                                                 Checked='<%# Bind("Accessories") %>' Enabled="False" />
                                                         </ItemTemplate>
                                                     </asp:TemplateField>--%>
                                                 </Columns>
                                                 <EditRowStyle BackColor="#2461BF" />
                                                 <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                 <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                 <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Left" />
                                                 <RowStyle BackColor="#EFF3FB" />
                                                 <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                 <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                 <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                 <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                 <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                             </asp:GridView>
                                             <br />         
                                        </asp:Panel>    
                                                                       
                                             <asp:Button ID="btnAddNewCustomer" runat="server" Text="Add Customer" 
                                                 onclick="btnAddNewCustomer_Click" class="buttons"/>
                                       

                                        <asp:Panel ID="CustomerPanel" runat="server" Visible="False">
                                            <table>
                                                <tr>
                                                    <td>Customer Number</td>
                                                    <td> 
                                                       
                                                        <span class="style6"><strong>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                                            ControlToValidate="txtCustomerNumber" ErrorMessage="Please Input Number Only" 
                                                            ValidationExpression="^\d+$" ForeColor="red"></asp:RegularExpressionValidator>
                                                        </strong></span></td>
                                                    <td><asp:TextBox ID="txtCustomerNumber" runat="server" Width="200px" MaxLength="10"></asp:TextBox>
                                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                                            ControlToValidate="txtCustomerNumber" ErrorMessage="*" ForeColor="red"></asp:RequiredFieldValidator>
                                                        <asp:Button
                                                         onclientclick="return confirm(&quot;Are you sure to delete this record?&quot;)"
                                                            ID="lete"  class="buttons" runat="server" Text="Delete" 
                                                            onclick="lete_Click" 
                                                           
                                                             /></td>
                                                </tr>
                                                <tr>
                                                    <td>Customer Name</td>
                                                    <td> </td>
                                                    <td><asp:TextBox ID="txtCustomerName" runat="server" Width="200px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                            ControlToValidate="txtCustomerName" ErrorMessage="*" ForeColor="red"></asp:RequiredFieldValidator></td>
                                                </tr>

                                                 

                                                <tr>
                                                    <td>Sales Office Code </td>
                                                    <td> </td>
                                                    <td>
                                                        <asp:DropDownList ID="txtSalesOfficeCode" Width="214px" runat="server" AppendDataBoundItems="True">
                                                            <asp:ListItem></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <%--<asp:TextBox ID="txtSalesOfficeCode" runat="server" Width="200px"></asp:TextBox>--%>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                            ControlToValidate="txtSalesOfficeCode" ErrorMessage="*" ForeColor="red"></asp:RequiredFieldValidator>
                                                    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Email Address</td>
                                                    <td> </td>
                                                    <td><asp:TextBox ID="txtCustomerEmail" runat="server" Width="200px" Height="196px" style="resize:none;"
                                                            TextMode="MultiLine"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                                            ControlToValidate="txtCustomerEmail" ErrorMessage="*" ForeColor="red" ></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>

                                                  <tr style="visible:false">
                                                    <td>Location Site ID</td>
                                                    <td></td>
                                                 
                                                    <td><asp:TextBox ID="txtLocation" runat="server" Width="200px" Enabled="true"></asp:TextBox>
                                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" 
                                                            ControlToValidate="txtLocation" ErrorMessage="*" ForeColor="red"></asp:RequiredFieldValidator>
                                                    </td>
                                                  
                                                </tr>

                                                <tr style="visible:false">
                                                    <td>City</td>
                                                    <td>&nbsp;</td>
                                                    <td><asp:TextBox ID="txtCity" runat="server" Width="200px" Enabled="true"></asp:TextBox>
                                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" 
                                                            ControlToValidate="txtCity" ErrorMessage="*" ForeColor="red"></asp:RequiredFieldValidator>
                                                    </td>
                                                  
                                                </tr>
                                                <tr style="visible:false">
                                                    <td>State</td>
                                                    <td>&nbsp;</td>
                                                    <td><asp:TextBox ID="txtState" runat="server" Width="200px" Enabled="true"></asp:TextBox>
                                                         
                                                    </td>
                                                </tr>
                                                <tr style="visible:false">
                                                    <td>Zip Code</td>
                                                    <td>&nbsp;</td>
                                                    <td><asp:TextBox ID="txtZipCode" runat="server" Width="200px" Enabled="true"></asp:TextBox>
                                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" 
                                                            ControlToValidate="txtZipCode" ErrorMessage="*" ForeColor="red"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                  <tr style="visible:false">
                                                    <td>Country</td>
                                                    <td>&nbsp;</td>
                                                    <td><asp:TextBox ID="txtCountry" runat="server" Width="200px" Enabled="true"></asp:TextBox>
                                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" 
                                                            ControlToValidate="txtCountry" ErrorMessage="*" ForeColor="red"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>

                                                <tr style="visible:false">
                                                    <td>Created  </td>
                                                    <td>&nbsp;</td>
                                                    <td><asp:TextBox ID="txtCreated" runat="server" Width="200px" Enabled="False"></asp:TextBox></td>
                                                </tr>
                                                <tr style="visible:false">
                                                    <td>Modified  </td>
                                                    <td>&nbsp;</td>
                                                    <td><asp:TextBox ID="txtModified" runat="server" Width="200px" Enabled="False"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td>Controls and electronics</td>
                                                    <td>&nbsp;</td>
                                                    <td><asp:CheckBox ID="chkControl" runat="server" /></td>
                                                </tr>
                                                <tr>
                                                    <td>Compressors, Accessories,<br /> Drives, Units</td>
                                                    <td>&nbsp;</td>
                                                    <td><asp:CheckBox ID="chkCompressor" runat="server" /></td>
                                                </tr>
                                               <%-- <tr>
                                                    <td>Accessories   </td>
                                                    <td>&nbsp;</td>
                                                    <td><asp:CheckBox ID="chkAccessories" runat="server" /></td>
                                                </tr>--%>


                                                

                                                <tr>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td>
                                                        <asp:Button ID="btnSave" class="buttons" runat="server" Text="Save" onclick="btnSave_Click" />
                                                        &nbsp;<asp:Button ID="btnCancel" class="buttons" runat="server" Text="Cancel" 
                                                            onclick="BtnCancel_Click" CausesValidation="False" />
                                                      <%--  <asp:Button ID="btnAddLocations" class="buttons" runat="server" Text="Add Location" OnClick="btnAddLocations_Click" />--%>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td >
                                                        &nbsp;</td>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td>
                                                        <asp:Label ID="lblError" runat="server" style="color: #FF0000"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                            
                                            
                                        </asp:Panel>

                                        <asp:Panel ID="PanelUnAuthorizedAccess" runat="server" Visible="False">
                                            <b>Authorized users can access this page.</b>
                                        </asp:Panel>


                                       <%-- <asp:Panel ID="panelLocations" runat="server" Visible="false">
                                            <h4>Customer Location</h4>
                                             <table>
                                                 <tr >
                                                    <td>Customer Number</td>
                                                    <td>&nbsp;</td>
                                                    <td><asp:Label ID="lblcustomernoLoc" runat="server" Text="-"></asp:Label></td>
                                                </tr>
                                                 <tr >
                                                    <td>Customer Name</td>
                                                    <td>&nbsp;</td>
                                                    <td><asp:Label ID="lblcustomernameLoc" runat="server" Text="-"></asp:Label></td>
                                                </tr>

                                            

                                                <tr style="display: none;">
                                                    <td>Address</td>
                                                    <td>&nbsp;</td>
                                                    <td><asp:TextBox ID="txtAddress" runat="server" Width="200px" Enabled="true" Visible="False"></asp:TextBox></td>
                                                </tr>

                                                <tr style="visible:false">
                                                    <td>City</td>
                                                    <td>&nbsp;</td>
                                                    <td><asp:TextBox ID="txtCity" runat="server" Width="200px" Enabled="true"></asp:TextBox>
                                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                                                            ControlToValidate="txtCity" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </td>
                                                  
                                                </tr>
                                                <tr style="visible:false">
                                                    <td>State</td>
                                                    <td>&nbsp;</td>
                                                    <td><asp:TextBox ID="txtState" runat="server" Width="200px" Enabled="true"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                                                            ControlToValidate="txtState" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr style="visible:false">
                                                    <td>Zip Code</td>
                                                    <td>&nbsp;</td>
                                                    <td><asp:TextBox ID="txtZipCode" runat="server" Width="200px" Enabled="true"></asp:TextBox>
                                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                                                            ControlToValidate="txtZipCode" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                  <tr style="visible:false">
                                                    <td>Country</td>
                                                    <td>&nbsp;</td>
                                                    <td><asp:TextBox ID="txtCountry" runat="server" Width="200px" Enabled="true"></asp:TextBox>
                                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                                                            ControlToValidate="txtCountry" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                 <tr>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td>
                                                        <asp:Button ID="btnSaveLoc" class="buttons" runat="server" Text="Add" OnClick="btnSaveLoc_Click" />
                                                        <asp:Button ID="btnDeleteLoc" class="buttons" runat="server" Text="Delete" OnClick="btnDeleteLoc_Click" onclientclick="return confirm(&quot;Are you sure to delete selected location?&quot;)" Visible="False" />
                                                        &nbsp;<asp:Button ID="btnCancelLoc" class="buttons" runat="server" Text="Cancel" 
                                                            CausesValidation="False" OnClick="btnCancelLoc_Click" />

                                                    </td>
                                                </tr>

                                            </table>
                                            <asp:Panel ID="panelLocGrid" runat="server">
                                                <asp:GridView ID="gridLocations" runat="server" AllowPaging="True"  
                                                 AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" PageSize="5" AllowSorting="True" CssClass="ui_grid" OnSelectedIndexChanged="gridLocations_SelectedIndexChanged" OnPageIndexChanging="gridLocations_PageIndexChanging">
                                                 <AlternatingRowStyle BackColor="White" />
                                                     <Columns>
                                                     <asp:CommandField ShowSelectButton="True" />
                                                     <asp:TemplateField HeaderText="LocationID " Visible="False">
                                                         <EditItemTemplate>
                                                             <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("LocationID") %>'></asp:TextBox>
                                                         </EditItemTemplate>
                                                         <ItemTemplate>
                                                             <asp:Label ID="lblLocationID" runat="server" Text='<%# Bind("LocationID") %>'></asp:Label>
                                                         </ItemTemplate>
                                                     </asp:TemplateField>
                                                     <asp:BoundField DataField="LocationID" HeaderText="Location" />
                                                     <asp:BoundField DataField="CustomerNumber" HeaderText="Customer Number" />
                                                     <%--<asp:BoundField DataField="Address" HeaderText="Address" /> 
                                                     <asp:BoundField DataField="City" HeaderText="City" />
                                                     <asp:BoundField DataField="State" HeaderText="State" />
                                                     <asp:BoundField DataField="ZipCode" HeaderText="Zip Code"   />
                                                     <asp:BoundField DataField="Country" HeaderText="Country"   />
                                                    

                                                 </Columns>
                                                 <EditRowStyle BackColor="#2461BF" />
                                                 <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                 <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                 <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Left" />
                                                 <RowStyle BackColor="#EFF3FB" />
                                                 <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                 <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                 <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                 <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                 <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                </asp:GridView>
                                            </asp:Panel>
                                            
                                        </asp:Panel>--%>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    </form>
</body>

    <script src="js/jquery.1.10.2.js" type="text/javascript"></script>
    <script src="js/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
         
            SearchText();
            if ($(".paginationGridView > td > table > tbody > tr > td:last-child > a").text() == "...")
                $(".paginationGridView > td > table > tbody > tr > td:last-child > a").text("Next >");

            if ($(".paginationGridView > td > table > tbody > tr > td:first-child > a").text() == "...")
                $(".paginationGridView > td > table > tbody > tr > td:first-child > a").text("< Prev");
        });
        function SearchText() {
            jQuery("#txtCustomerNumber").autocomplete({
                source: function (request, response) {
                    jQuery.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "customers.aspx/getAllCustomerArray",
                        data: "{'customerNumber':" + document.getElementById('txtCustomerNumber').value + "}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);

                            jQuery(".ui-menu-item").click(function (e) {
                                var li = jQuery(this)
                                jQuery.ajax({
                                    contentType: "application/json; charset=utf-8",
                                    type: "POST", 
                                    url: "customers.aspx/getAllCustomerNameByCustomerNumber",
                                    data: "{'customerNumber':" + li.text() + "}",
                                    dataType: "json",
                                    success: function (data) {
                                        jQuery("#txtCustomerName").val(data.d)
                                    }
                                });
                            })
                        },
                        error: function (result) {
                            
                        }
                    });
                }
            });

            
        }
    </script>
</html>



